﻿using System.ComponentModel.Composition;

using Ensage.SDK.Service;
using Ensage.SDK.Service.Metadata;

using Ensage;

namespace MeepoExtraPaidFeature
{

    [ExportPlugin(
        mode: StartupMode.Auto,
        name: "MeepoExtraPaidFeature",
        units: new[] {  HeroId.npc_dota_hero_meepo })]
    public sealed class MeepoExtraPaidFeature : Plugin
    {
        [ImportingConstructor]
        public MeepoExtraPaidFeature([Import] IServiceContext context)
        {
            Context = context;
        }

        public IServiceContext Context { get; }
    }

    public static class Test
    {
        public static int ValidTest()
        {
            return 322;
        }
    }
}
