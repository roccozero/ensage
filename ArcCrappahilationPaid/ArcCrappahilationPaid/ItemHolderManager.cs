﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ensage;
using Ensage.Common.Objects.UtilityObjects;
using Ensage.SDK.Handlers;
using Ensage.SDK.Helpers;
using Ensage.SDK.Renderer;

namespace ArcCrappahilationPaid
{
    public class ItemHolderManager
    {
        private static ITextureManager _textureManager;

        public class ItemHolder
        {
            public Item Item;
            public AbilityId Id;
            //public IUpdateHandler Updater { get; }
            public float Cooldown;
            private readonly Sleeper _sleeper;
            public bool IsValid => Item != null && Item.IsValid;
            public static List<ItemHolder> FirstTime = new List<ItemHolder>();
            public ItemHolder(Item item)
            {
                Item = item;
                Id = item.Id;
                Cooldown = item.Cooldown;
                _sleeper = new Sleeper();
                //Updater = UpdateManager.Subscribe(CooldownUpdater, 1000);
                if (FirstTime.Any(x=>x.Id==Id))
                    return;
                FirstTime.Add(this);
                _textureManager.LoadAbilityFromDota(Id);
            }

            public ItemHolder Activate()
            {
                //Updater.IsEnabled = true;
                return this;
            }

            public ItemHolder Deactivate()
            {
                //Updater.IsEnabled = false;
                return this;
            }

            private void CooldownUpdater()
            {
                if (/*Cooldown-- < 0 || */!IsValid)
                {
                    Deactivate();
                }
                else
                {
                    try
                    {
                        if (Item.Owner.IsAlive)
                            Cooldown = Item.Cooldown;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
            }

            public void UpdateCooldown()
            {
                if (_sleeper.Sleeping)
                    return;
                _sleeper.Sleep(250);
                if (!IsValid)
                    return;
                /*if (!Item.Owner.IsAlive)
                    return;*/
                Cooldown = Item.Cooldown;
                if (Cooldown <= 0/* || Updater.IsEnabled*/)
                    return;
                Activate();
            }
        }

        public ItemHolderManager(ITextureManager textureManager)
        {
            ItemHolderManager._textureManager = textureManager;
            List = new List<ItemHolder>();
        }

        public ItemHolder GetOrCreate(Item item)
        {
            if (item != null && item.IsValid)
            {
                var first = List.FirstOrDefault(x => x.Id == item.Id);
                if (first != null)
                {
                    return first;
                }
                first = new ItemHolder(item);
                //ArcRektolation.Log.Debug($"New ItemHolder: {item.Id}");
                List.Add(first);
                return first;
            }
            ArcCrappahilation.Log.Debug($"ItemHolder.GetOrCreate -> item = null");
            return null;
        }

        public ItemHolder GetById(AbilityId id)
        {
            var first = List.FirstOrDefault(x => x.Id == id);
            if (first != null)
            {
                return first;
            }
            ArcCrappahilation.Log.Debug($"ItemHolder.GetById -> cant find id [{id}]");
            return null;
        }

        public void RemoveById(AbilityId id)
        {
            var any = List.Any(x => x.Id == id);
            if (any)
            {
                List.RemoveAll(x=>x.Id == id);
                //ArcRektolation.Log.Debug($"ItemHolder.RemoveById -> [{id}]");
            }
        }
        public List<ItemHolder> List;
        
    }
}