﻿using System.Linq;
using ArcCrappahilationPaid.Units;
using Ensage;
using Ensage.Common.Enums;
using Ensage.Common.Extensions;
using Ensage.Common.Objects.UtilityObjects;
using Ensage.SDK.Helpers;

namespace ArcCrappahilationPaid
{
    public class AutoMidas
    {
        private readonly ArcCrappahilation _main;

        public AutoMidas(ArcCrappahilation main)
        {
            _main = main;
            _sleeper = new MultiSleeper();
        }

        private readonly MultiSleeper _sleeper;
        public void MidasChecker()
        {
            foreach (var unit in _main.Updater.Units.Where(x=>x.IsAlive && (x is MainHeroUnit || x is TempestHeroUnit)))
            {
                /*var midas = unit.MovementManager.Context.Inventory.Items.FirstOrDefault(x =>
                    x.Id == AbilityId.item_hand_of_midas && x.Item.CanBeCasted());*/
                if (unit.Owner.IsInvisible())
                    continue;
                var midas = unit.Owner.GetItemById(ItemId.item_hand_of_midas);
                if (midas != null && midas.CanBeCasted())
                {

                    var creep =
                        EntityManager<Unit>.Entities.Where(
                                x =>
                                    x.IsValid && x.IsAlive && !(x is Hero) && !(x is Building) &&
                                    x.Team != unit.Owner.Team && midas.CanHit(x) && !x.IsAncient &&
                                    !x.IsMagicImmune() && (x.IsSpawned || x.Name == "npc_dota_invoker_forged_spirit"))
                            .OrderByDescending(x => x.Health).FirstOrDefault();
                    if (creep != null)
                    {
                        if (_main.Menu.AutoMidasConfig.IsDelay)
                        {
                            var s = "First";
                            if (unit is MainHeroUnit)
                            {
                                s += "Main";
                            }
                            else
                            {
                                s += "Tempest";
                            }
                            if (_sleeper.Sleeping(s))
                            {
                                    continue;
                            }
                            if (!_sleeper.Sleeping(s + "InSys"))
                            {
                                _sleeper.Sleep(1500, s);
                                _sleeper.Sleep(5000, s + "InSys");
                                continue;
                            }
                        }
                        midas.UseAbility(creep);
                    }
                }
            }
        }
    }
}