﻿using System;
using ArcCrappahilationPaid.Orders;
using ArcCrappahilationPaid.Units;
using Ensage;

namespace ArcCrappahilationPaid
{
    public class OrderManager
    {
        public ControllableUnit ControllableUnit { get; }
        public Unit Owner => ControllableUnit.Owner;

        [Flags]
        public enum Orders
        {
            Idle,
            Combo,
            Pushing,
            Tempest
        }

        public Orders CurrentOrderId => CurrentOrder.Id;
        public BaseOrder CurrentOrder;
        public OrderManager(ControllableUnit controllableUnit)
        {
            ControllableUnit = controllableUnit;
            CurrentOrder = new IdleOrder(controllableUnit);
        }

        public void SetOrder(BaseOrder order)
        {
            if (CurrentOrder.Id == order.Id)
                return;
            CurrentOrder = order;
        }

        public void FlushOrder()
        {
            if (CurrentOrder != null)
                CurrentOrder = new IdleOrder(CurrentOrder?.Owner);
        }
    }
}