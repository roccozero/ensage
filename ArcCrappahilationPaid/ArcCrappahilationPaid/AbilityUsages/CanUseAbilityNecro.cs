using Ensage;
using Ensage.Common.Extensions;
using Ensage.Common.Objects.UtilityObjects;

namespace ArcCrappahilationPaid.AbilityUsages
{
    public class CanUseAbilityNecro : IUseAbilities
    {
        private readonly Ability _ability;
        private readonly Sleeper _sleeper;

        public CanUseAbilityNecro(Ability ability)
        {
            _ability = ability;
            _sleeper = new Sleeper();
        }

        public void UseAbility(Unit target)
        {
            if (_ability.CanBeCasted() && _ability.CanHit(target) && !_sleeper.Sleeping)
            {
                _ability.UseAbility(target);
                _sleeper.Sleep(500);
            }
        }
    }
}