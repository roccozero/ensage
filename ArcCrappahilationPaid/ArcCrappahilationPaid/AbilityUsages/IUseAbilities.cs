﻿using Ensage;

namespace ArcCrappahilationPaid.AbilityUsages
{
    public interface IUseAbilities
    {
        void UseAbility(Unit target);
    }
}
