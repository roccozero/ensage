﻿using System.Collections.Generic;
using ArcCrappahilationPaid.Units;
using Ensage;
using Ensage.Common;
using Ensage.Common.Extensions;
using Ensage.Common.Objects.UtilityObjects;
using Ensage.SDK.Abilities;
using Ensage.SDK.Abilities.Components;
using Ensage.SDK.Abilities.Items;
using Ensage.SDK.Extensions;
using UnitExtensions = Ensage.SDK.Extensions.UnitExtensions;

namespace ArcCrappahilationPaid.AbilityUsages
{
    public class CanUseAbilityHero : IUseAbilities, INeedInitAbilities
    {
        private static readonly MultiSleeper GlobalSleeper = new MultiSleeper();
        private readonly HeroUnit _owner;
        private readonly MultiSleeper _sleeper;
        private IEnumerable<Ability> _abilities;
        private uint _currentLevel;
        private float _extraDaggerRange;
        private List<ActiveAbility> _itemList;
        private IEnumerable<Ability> _items;
        private bool _smartSpark;
        private float _smartSparkLevel;

        public CanUseAbilityHero(HeroUnit owner)
        {
            _owner = owner;
            _sleeper = new MultiSleeper();
        }

        private Hero Me => _owner.Owner as Hero;

        /// <summary>
        ///     on start combing
        /// </summary>
        public void InitAbilities()
        {
            _abilities = _owner.GetAbilities();
            _items = _owner.GetItems();
            _itemList = new List<ActiveAbility>();
            foreach (var item in _items)
            {
                var ability = _owner.MovementManager.Context.AbilityFactory.GetAbility(item);
                if (ability is ActiveAbility ab)
                    _itemList.Add(ab);
            }

            _extraDaggerRange = 0;
            if (_owner is MainHeroUnit main)
            {
                _extraDaggerRange = main.BaseMenu.MenuForBlink.Slider.Value;
                _currentLevel = ((Hero) _owner.Owner).Level;
                _smartSpark = main.BaseMenu.SmartSpark.IsEnable;
                _smartSparkLevel = main.BaseMenu.SmartSpark.Slider.Value;
            }
            else if (_owner is TempestHeroUnit tempest)
            {
                _extraDaggerRange = tempest.BaseMenu.MenuForBlink.Slider.Value;
                _currentLevel = ((Hero) _owner.Owner).Level;
                _smartSpark = tempest.BaseMenu.SmartSpark.IsEnable;
                _smartSparkLevel = tempest.BaseMenu.SmartSpark.Slider.Value;
            }
        }

        public void UseAbility(Unit target)
        {
            if (!target.IsVisible)
                return;
            if (!target.IsAlive)
                return;
            if (UnitExtensions.IsInvisible(Me) || _sleeper.Sleeping("underInvis") ||
                Me.HasAnyModifiers("modifier_item_invisibility_edge_windwalk", "modifier_item_silver_edge_windwalk"))
                return;
            var isLinkerProtected = UnitExtensions.IsLinkensProtected(target);
            var isSpellShieldProtected = target.IsSpellShieldProtected();
            var targetDist = target.Distance2D(_owner.Owner);
            var isMagicImmune = UnitExtensions.IsMagicImmune(target);
            foreach (var ability in _itemList)
            {
                if (ability == null || ability.Item == null || !ability.Item.IsValid || !ability.CanBeCasted ||
                    !ability.CanHit(target) && !_sleeper.Sleeping("after_blink") &&
                    ability.Item.Id != AbilityId.item_blink && ability.Item.Id != AbilityId.item_hurricane_pike &&
                    ability.Item.Id != AbilityId.item_force_staff &&
                    (ability.Ability.AbilityBehavior & AbilityBehavior.UnitTarget) != 0)
                    continue;
                if (isMagicImmune && ((ability.DamageType & DamageType.Magical) != 0 ||
                                      (ability.Ability.AbilityBehavior & AbilityBehavior.UnitTarget) != 0))
                    continue;
                var abilityId = ability.Item.Id;
                if (ability is IHasTargetModifier targetMod)
                {
                    if (target.FindModifier(targetMod.TargetModifierName)?.RemainingTime >=
                        (ability is item_nullifier ? ability.GetHitTime(target) / 1000f + 0.25f : 0.35f))
                        continue;
                }
                else if (abilityId == AbilityId.item_blink &&
                         (_owner.Owner.IsInRange(target, 350) ||
                          !_owner.Owner.IsInRange(target,
                              _owner.Main.Menu.MenuForBlink.UseRange ? ability.CastRange + _extraDaggerRange : 50000)))
                {
                    continue;
                }

                if (abilityId == AbilityId.item_sheepstick)
                    if (isLinkerProtected || isSpellShieldProtected)
                        continue;
                if (abilityId == AbilityId.item_sheepstick || abilityId == AbilityId.item_bloodthorn ||
                    abilityId == AbilityId.item_orchid || abilityId == AbilityId.item_nullifier)
                {
                    if (isSpellShieldProtected) continue;
                    if (abilityId == AbilityId.item_orchid || abilityId == AbilityId.item_bloodthorn ||
                        abilityId == AbilityId.item_nullifier)
                    {
                        if (target is Hero slark && slark.HeroId == HeroId.npc_dota_hero_slark)
                            if (UnitExtensions.HasModifiers(slark,
                                new[] {"modifier_slark_dark_pact", "modifier_slark_dark_pact_pulses"}, false))
                                continue;

                        if (abilityId == AbilityId.item_nullifier)
                        {
                            if (GlobalSleeper.Sleeping("nullifierHitTime")) continue;
                            if (_owner.Main.Menu.SaveNulForAeuon)
                            {
                                var aeon = target.GetItemById(AbilityId.item_aeon_disk);
                                if (aeon != null && aeon.CanBeCasted()) continue;
                            }

                            if (_owner.Main.Menu.DoneUseNulOnHexedUnits)
                            {
                                if (GlobalSleeper.Sleeping(AbilityId.item_sheepstick))
                                    continue;
                                var mod = target.FindModifier("modifier_sheepstick_debuff");
                                var hex = _itemList.Find(x =>
                                    x is item_sheepstick && x.CanBeCasted && x.CanHit(target));
                                if (hex != null) continue;
                                if (hex == null && mod != null && target.IsInRange(_owner.Owner, 500) &&
                                    mod.RemainingTime >=
                                    ability.GetHitTime(target) / 1000f + Game.Ping / 1000f + 0.25f)
                                    continue;
                            }

                            GlobalSleeper.Sleep(ability.GetHitTime(target) + 1000, "nullifierHitTime");
                        }
                        else if (_owner.Main.Menu.DontUseBeforeNul && GlobalSleeper.Sleeping("nullifierHitTime") &&
                                 !target.HasAnyModifiers("modifier_item_nullifier_mute"))
                        {
                            continue;
                        }
                    }

                    if (GlobalSleeper.Sleeping(abilityId)) continue;
                    GlobalSleeper.Sleep(isLinkerProtected ? 150 : 500, abilityId);
                }
                else if (abilityId == AbilityId.item_dagon ||
                         abilityId >= AbilityId.item_dagon_2 && abilityId <= AbilityId.item_dagon_5)
                {
                    if (GlobalSleeper.Sleeping("dagon") &&
                        !UnitExtensions.HasModifier(target,
                            "modifier_item_ethereal_blade_ethereal"))
                        continue;
                }
                else if (abilityId == AbilityId.item_ethereal_blade)
                {
                    ability.UseAbility(target);
                    GlobalSleeper.Sleep(ability.GetHitTime(target) + 250, "dagon");
                }

                if ((ability.Ability.AbilityBehavior & AbilityBehavior.UnitTarget) != 0)
                {
                    if ((ability.Ability.TargetTeamType & TargetTeamType.Allied) != 0)
                    {
                        if (ability is item_ethereal_blade || ability is item_solar_crest ||
                            ability is item_medallion_of_courage ||
                            isLinkerProtected && (ability is item_force_staff || ability is item_hurricane_pike))
                        {
                            ability.UseAbility(target);
                        }
                        else if (ability is item_force_staff || ability is item_hurricane_pike)
                        {
                            if (targetDist <= _owner.Main.Menu.PikeForceCastRange.Value)
                                if (_owner.Owner.FindRotationAngle(target.Position) <= 0.15)
                                    //if (_owner.Owner.IsDirectlyFacing(target))
                                    ability.UseAbility(_owner.Owner);
                        }
                        else if (targetDist <= 800)
                        {
                            ability.UseAbility(_owner.Owner);
                            /*if (ability is item_force_staff || ability is item_hurricane_pike)
                            {
                                if (_owner.Owner.FindRotationAngle(target.Position) <= 0.09)
                                {
                                    ability.UseAbility(_owner.Owner);
                                }
                            }
                            else
                            {
                                ability.UseAbility(_owner.Owner);
                            }
                            */
                        }
                        else
                        {
                            continue;
                        }
                    }
                    else
                    {
                        ability.UseAbility(target);
                    }
                }
                else if ((ability.Ability.AbilityBehavior & AbilityBehavior.NoTarget) != 0)
                {
                    if (abilityId == AbilityId.item_silver_edge || abilityId == AbilityId.item_invis_sword)
                    {
                        ability.UseAbility();
                        _sleeper.Sleep(700, "underInvis");
                    }
                    else if (abilityId == AbilityId.item_magic_stick || abilityId == AbilityId.item_magic_wand)
                    {
                        var wand = ability as item_magic_wand;
                        var stick = ability as item_magic_stick;
                        if (wand != null && wand.CanBeCasted && wand.TotalHealthRestore > 150)
                            wand.UseAbility();
                        else
                            continue;

                        if (stick != null && stick.CanBeCasted && stick.TotalHealthRestore > 100)
                            stick.UseAbility();
                        else
                            continue;
                    }
                    else if (targetDist <= 600)
                    {
                        ability.UseAbility();
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    ability.UseAbility(target.Position);
                }

                ArcCrappahilation.Log.Debug($"Using [item] {ability} -> {target.NetworkName}");
                if (ability is item_blink)
                {
                    _sleeper.Sleep(500, "after_blink");
                    return;
                }

                return;
            }

            if (_sleeper.Sleeping("after_blink"))
                return;

            foreach (var ability in _abilities)
            {
                if (!ability.CanBeCasted() || _sleeper.Sleeping(ability))
                    continue;
                switch (ability.Id)
                {
                    case AbilityId.arc_warden_flux:
//                        if (ability.CanHit(target))
                        var extraRangeAbility = UnitExtensions.GetAbilityById(_owner.Owner,
                            AbilityId.special_bonus_unique_arc_warden_5);
                        var extraRange = 0;
                        if (extraRangeAbility != null && extraRangeAbility.Level > 0) extraRange = 400;

                        if (target.IsInRange(_owner.Owner, ability.GetCastRange() + extraRange))
                        {
                            if (isMagicImmune)
                                continue;
                            ability.UseAbility(target);
                            ArcCrappahilation.Log.Debug($"Using [ability] {ability} to {target.NetworkName}");
                            _sleeper.Sleep(500, ability.Id);
                            return;
                        }

                        break;
                    case AbilityId.arc_warden_magnetic_field:
                        if (!target.IsInRange(_owner.Owner, 800) ||
                            UnitExtensions.HasModifier(_owner.Owner,
                                "modifier_arc_warden_magnetic_field_attack_speed") ||
                            GlobalSleeper.Sleeping(ability.Id))
                            continue;
                        var position = _owner.Main.Menu.MagneticFieldConfig.UseInFont
                            ? Prediction.InFront(_owner.Owner, 250)
                            : target.Distance2D(_owner.Owner) <= _owner.Main.Menu.MagneticFieldConfig.RangeSlider.Value
                                ? Prediction.InFront(_owner.Owner, -250)
                                : Prediction.InFront(_owner.Owner, 350);
                        ability.UseAbility(position);
                        ArcCrappahilation.Log.Debug($"Using [ability] {ability.Id}");
                        _sleeper.Sleep(1000, ability.Id);
                        GlobalSleeper.Sleep(1000, ability.Id);
                        return;
                    case AbilityId.arc_warden_spark_wraith:
                        if (ability.CanHit(target) && _owner.Owner.IsVisibleToEnemies)
                        {
                            if (_smartSpark)
                                if (_currentLevel >= _smartSparkLevel && _owner.Owner.IsValidOrbwalkingTarget(target))
                                    continue;
                            var mod = target.FindModifier("modifier_sheepstick_debuff");
                            var time = 0.33 + Game.Ping / 1000f;
                            var nul = _itemList.Find(activeAbility =>
                                activeAbility is item_nullifier && activeAbility.CanBeCasted &&
                                activeAbility.CanHit(target));
                            if (nul != null) time = nul.GetHitTime(target) / 1000f + Game.Ping / 1000f;
                            if (mod != null && mod?.RemainingTime <= time)
                            {
                                ArcCrappahilation.Log.Debug(
                                    $"Skiping [ability] {ability.Id} cuz hex's debuff less then {time} -> {mod?.RemainingTime}");
                                continue;
                            }

                            var pos = target.BasePredict(2300 + Game.Ping * 2);
                            ability.UseAbility(pos);
                            ArcCrappahilation.Log.Debug($"Using [ability] {ability.Id} to {target.NetworkName}");
                            _sleeper.Sleep(500, ability.Id);
                            return;
                        }

                        break;
                    case AbilityId.arc_warden_tempest_double:
                        ability.UseAbility();
                        ArcCrappahilation.Log.Debug($"Using [ability] {ability}");
                        _sleeper.Sleep(500, ability.Id);
                        return;
                }
            }
        }
    }
}