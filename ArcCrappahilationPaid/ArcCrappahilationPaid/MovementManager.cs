﻿using System;
using Ensage;
using Ensage.SDK.Extensions;
using Ensage.SDK.Orbwalker;
using Ensage.SDK.Service;
using SharpDX;

namespace ArcCrappahilationPaid
{
    public class MovementManager
    {
        public readonly IServiceContext Context;
        public readonly IOrbwalkerManager MainOrbwalker;

        public readonly CustomOrbwalker Orbwalker;

        public MovementManager(IOrbwalkerManager orbwalker, IServiceContext context)
        {
            Context = context;
//            Orbwalker = orbwalker;
            /*if (!orbwalker.IsActive)
            {
                orbwalker.Activate();
            }
            orbwalker.Settings.DrawHoldRange.Value = false;
            orbwalker.Settings.DrawRange.Value = false;
            MainOrbwalker = orbwalker;*/

            Orbwalker = new CustomOrbwalker(context.Owner).Activate();
        }

        public MovementManager(IServiceContext context)
        {
            Context = context;

//            context.TargetSelector.Activate();
//
//            var orbwalker = context.GetExport<IOrbwalkerManager>().Value;
//
//            orbwalker.Activate();
//
//            orbwalker.Settings.DrawHoldRange.Value = false;
//            orbwalker.Settings.DrawRange.Value = false;
//
//            Orbwalker = orbwalker;
//
//            context.TargetSelector.Deactivate();
            Orbwalker = new CustomOrbwalker(context.Owner).Activate();
        }

        public void Orbwalk(Unit target = null, float minDisntace = 0, bool menuGetBack = true)
        {
            if (target == null)
            {
                Orbwalker.Move(Game.MousePosition);
            }
            else
            {
                if (minDisntace > 0)
                {
                    var mousePos = Game.MousePosition;
                    var targetPos = new Vector3(target.Position.X, target.Position.Y, mousePos.Z);
                    if (!menuGetBack)
                        if (Context.Owner.Distance2D(targetPos) <= minDisntace)
                        {
                            Orbwalker.Attack(target);
                            return;
                        }

                    var ownerPosExtral = new Vector3(Context.Owner.Position.X, Context.Owner.Position.Y, mousePos.Z);
                    var ownerDis = Math.Min(Context.Owner.Distance2D(mousePos), 300);
                    var ownerPos = ownerPosExtral.Extend(mousePos, ownerDis);
                    var pos = targetPos.Extend(ownerPos, minDisntace);

                    Orbwalker.OrbwalkingPoint = pos;

                    if (Orbwalker.Owner.IsDisarmed() || target.IsAttackImmune() || target.IsInvulnerable())
                        Orbwalker.Move(target.Position);
                    else
                        Orbwalker.OrbwalkTo(target);
                }
                else
                {
                    Orbwalker.OrbwalkingPoint = Vector3.Zero;
                    if (Orbwalker.Owner.IsDisarmed() || target.IsAttackImmune() || target.IsInvulnerable())
                        Orbwalker.Move(target.Position);
                    else
                        Orbwalker.OrbwalkTo(target);
                }
            }
        }

        public void Attack(Unit target, float minDisntance = 0, bool menuGetBack = true)
        {
            if (target == null)
            {
            }
            else
            {
                if (Orbwalker.CanAttack(target) && !target.IsInvulnerable())
                {
                    Orbwalker.Attack(target);
                }
                else if (Orbwalker.CanMove())
                {
                    var orbwalkPos = target.Position;
                    if (minDisntance > 0)
                    {
                        if (!menuGetBack)
                            if (Context.Owner.Distance2D(orbwalkPos) <= minDisntance)
                                return;
                        var targetPos = orbwalkPos;
                        var pos = (targetPos - Context.Owner.Position).Normalized();
                        pos *= minDisntance;
                        orbwalkPos = targetPos - pos;
                    }

                    Orbwalker.Move(orbwalkPos);
                }
            }
        }
    }
}