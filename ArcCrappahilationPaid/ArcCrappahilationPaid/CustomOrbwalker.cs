using System;
using System.Collections.Generic;
using Ensage;
using Ensage.SDK.Extensions;
using PlaySharp.Toolkit.Helper.Annotations;
using SharpDX;

namespace ArcCrappahilationPaid
{
    public sealed class CustomOrbwalker
    {
        private readonly HashSet<NetworkActivity> _attackActivities = new HashSet<NetworkActivity>
        {
            NetworkActivity.Attack,
            NetworkActivity.Attack2,
            NetworkActivity.AttackEvent
        };

        private readonly HashSet<NetworkActivity> _attackCancelActivities = new HashSet<NetworkActivity>
        {
            NetworkActivity.Idle,
            NetworkActivity.IdleRare,
            NetworkActivity.Move
        };

        public CustomOrbwalker(Unit owner)
        {
            Owner = owner;
        }

        public bool IsActive { get; private set; }

        public Vector3 OrbwalkingPoint { get; set; } = Vector3.Zero;

        private float LastAttackOrderIssuedTime { get; set; }

        private float LastAttackTime { get; set; }

        private float LastMoveOrderIssuedTime { get; set; }

        public Unit Owner { get; }

        private static float PingTime => Game.Ping / 2000f;

        private float TurnEndTime { get; set; }

        public static double AttackDelay { get; set; } = 50;
        public static float TurnDelay { get; set; } = 50;
        public static float HoldRange { get; set; } = 50;
        public static float MoveDelay { get; set; } = 50;

        /*public void Activate()
        {
            if (IsActive)
            {
                return;
            }

            IsActive = true;

            Entity.OnInt32PropertyChange += OnNetworkActivity;
        }*/
        public CustomOrbwalker Activate()
        {
            if (IsActive) return this;

            IsActive = true;

            Entity.OnInt32PropertyChange += OnNetworkActivity;
            return this;
        }

        public bool Attack(Unit unit, float time)
        {
            if (time - LastAttackOrderIssuedTime < AttackDelay / 1000f) return false;

            TurnEndTime = GetTurnTime(unit, time);

            if (Owner.Attack(unit))
            {
                LastAttackOrderIssuedTime = time;
                return true;
            }

            return false;
        }

        public bool Attack(Unit unit)
        {
            return Attack(unit, Game.RawGameTime);
        }

        public bool CanAttack(Unit target, float time)
        {
            return Owner.CanAttack() && GetTurnTime(target, time) - LastAttackTime > 1f / Owner.AttacksPerSecond;
        }

        public bool CanAttack(Unit target)
        {
            return CanAttack(target, Game.RawGameTime);
        }

        public bool CanMove(float time)
        {
            return time - 0.1f + PingTime - LastAttackTime > Owner.AttackPoint();
        }

        public bool CanMove()
        {
            return CanMove(Game.RawGameTime);
        }

        public void Deactivate()
        {
            if (!IsActive) return;

            IsActive = false;

            Entity.OnInt32PropertyChange -= OnNetworkActivity;
        }

        public float GetTurnTime(Entity unit, float time)
        {
            return time + PingTime + Owner.TurnTime(unit.NetworkPosition) + TurnDelay / 1000f;
        }


        public float GetTurnTime(Entity unit)
        {
            return GetTurnTime(unit, Game.RawGameTime);
        }

        public bool Move(Vector3 position, float time)
        {
            if (Owner.Position.Distance(position) < HoldRange) return false;

            if (time - LastMoveOrderIssuedTime < MoveDelay / 1000f) return false;

            if (!Owner.Move(position)) return false;
            LastMoveOrderIssuedTime = time;
            return true;
        }

        public bool Move(Vector3 position)
        {
            return Move(position, Game.RawGameTime);
        }

        public bool OrbwalkTo([CanBeNull] Unit target)
        {
            var time = Game.RawGameTime;

            // turning
            if (TurnEndTime > time) return false;

            // owner disabled
            if (Owner.IsChanneling() || !Owner.IsAlive || Owner.IsStunned()) return false;

            var validTarget = target != null;

            // move
            if ((!validTarget || !CanAttack(target)) && CanMove(time))
                return Move(OrbwalkingPoint != Vector3.Zero ? OrbwalkingPoint : Game.MousePosition, time);

            // attack
            if (validTarget && CanAttack(target)) return Attack(target, time);

            return false;
        }


        private void OnNetworkActivity(Entity sender, Int32PropertyChangeEventArgs args)
        {
            if (sender != Owner) return;

            if (!args.PropertyName.Equals("m_networkactivity", StringComparison.InvariantCultureIgnoreCase)) return;

            var newNetworkActivity = (NetworkActivity) args.NewValue;

            if (_attackActivities.Contains(newNetworkActivity))
                LastAttackTime = Game.RawGameTime - PingTime;
            else if (_attackCancelActivities.Contains(newNetworkActivity))
                if (!CanMove(Game.RawGameTime + 0.05f))
                    LastAttackTime = 0;
        }
    }
}