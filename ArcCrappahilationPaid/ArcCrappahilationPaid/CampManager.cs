﻿using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Ensage;
using Ensage.SDK.Helpers;
using log4net;
using PlaySharp.Toolkit.Logging;
using SharpDX;

namespace ArcCrappahilationPaid
{
    public class Camp
    {
        public Vector3 Position { get; set; }
        public bool IsEmpty { get; set; }
        public bool IsAncient { get; set; }
        public bool IsAlly { get; set; }
        public bool IsOccupied { get; set; }

        public Camp(Vector3 position, bool ancient = false, Team team = Team.Radiant)
        {
            Position = position;
            IsEmpty = false;
            IsAncient = ancient;
            IsAlly = ObjectManager.LocalPlayer.Team == team;
            IsOccupied = false;
        }

        public void SetEmpty()
        {
            IsOccupied = false;
            IsEmpty = true;
        }
    }
    public class CampManager
    {
        private static readonly ILog Log = AssemblyLogs.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public List<Camp> Camps;

        public CampManager()
        {
            Camps = new List<Camp>
            {
                new Camp(new Vector3(-1818, -4221, 256)),
                new Camp(new Vector3(-439, -3346, 256)),
                new Camp(new Vector3(473, -4745, 384)),
                new Camp(new Vector3(2966, -4579, 256)),
                new Camp(new Vector3(4533, -4412, 256)),
                new Camp(new Vector3(-4886, -445, 256)),
                new Camp(new Vector3(-3668, 871, 256)),
                new Camp(new Vector3(-3061, -61, 384), true),
                new Camp(new Vector3(60, -1853, 383), true),
                new Camp(new Vector3(3959, -545, 256), true, Team.Dire),
                new Camp(new Vector3(-782, 2332, 384), true, Team.Dire),
                new Camp(new Vector3(2546, 83, 384), team: Team.Dire),
                new Camp(new Vector3(4493, 850, 384), team: Team.Dire),
                new Camp(new Vector3(1319, 3361, 384), team: Team.Dire),
                new Camp(new Vector3(-98, 3332, 256), team: Team.Dire),
                new Camp(new Vector3(-2047, 4196, 256), team: Team.Dire),
                new Camp(new Vector3(-2753, 4635, 256), team: Team.Dire),
                new Camp(new Vector3(-4203, 3465, 256), team: Team.Dire),
            };

            var secs = (int)(Game.GameTime % 60);
            Log.Debug($"[{secs}] First refresh in {(60 - secs) * 1000}");
            UpdateManager.BeginInvoke(async () =>
            {
                while (true)
                {
                    foreach (var camp in Camps)
                    {
                        camp.IsEmpty = false;
                        camp.IsOccupied = false;
                    }
                    secs = (int)(Game.GameTime % 60);
                    var nextDelay = (60 - secs) * 1000;
                    Log.Debug($"[{secs}] Refres camps. next in {nextDelay}");
                    await Task.Delay(nextDelay);
                    secs = (int)(Game.GameTime % 60);
                    if (Game.GameTime % 60 >= 2.5)
                    {
                        await Task.Delay((60 - secs) * 1000);
                    }
                }
            }, (60 - secs) * 1000);
        }
    }
}