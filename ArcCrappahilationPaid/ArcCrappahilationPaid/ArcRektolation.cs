﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.Linq;
using ArcCrappahilationPaid.Config;
using Ensage;
using Ensage.SDK.Inventory;
using Ensage.SDK.Service;
using Ensage.SDK.Service.Metadata;
using NLog;

namespace ArcCrappahilationPaid
{
    [ExportPlugin(
        mode: StartupMode.Auto,
        name: "ArcRektolation",
        author: "JumpAttacker",
        units: new[] {  HeroId.npc_dota_hero_arc_warden })]
    public sealed class ArcCrappahilation : Plugin
    {
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();

        public BaseMenu Menu;
        public Updater Updater;

        [ImportingConstructor]
        public ArcCrappahilation([Import] IServiceContext context)
        {
            Context = context;
        }

        public IServiceContext Context { get; }
        public AutoMidas AutoMidas { get; private set; }

        public ItemHolderManager ItemHolderManager;
        public LaneHelper LaneHelper;
        public InputBlocker InputBlocker;
        protected override void OnActivate()
        {
            Log.Warn("[ArcWarden] start init");
            Menu = new BaseMenu(this);
            Log.Debug("Init BaseMenu");
            Context.MenuManager.RegisterMenu(Menu);
            Log.Debug("Register menu");
            ItemHolderManager = new ItemHolderManager(Context.TextureManager);
            Log.Debug("Init ItemHolderManager");
            Context.Inventory.Activate();
            Log.Debug("Inventory Activate");
            LoadItemsToMenu();
            Log.Debug("Items Loaded to menu");
            Updater = new Updater(this);
            Log.Debug("Init updater");
            LaneHelper = new LaneHelper(this);
            Log.Debug("Init LaneHelper");
            InputBlocker = new InputBlocker(this);
            Log.Debug("Init InputBlocker");
            AutoMidas = new AutoMidas(this);
            Log.Debug("Init AutoMidas");
            //ALLO IOBA ETO TI?
            Unit.OnModifierRemoved += (sender, args) =>
            {
                if (!Menu.OrderConfig.DisableOrder) return;
                if (sender.Name == "npc_dota_hero_arc_warden" && args.Modifier.Name == "modifier_kill")
                {
                    if (Menu.OrderConfig.AutoPushingIsActive)
                    {
                        Menu.OrderConfig.AutoPushingAction(null);
                    }
                    if (Menu.OrderConfig.TempestComboIsActive)
                    {
                        Menu.OrderConfig.TempestAction(null);
                    }
                }
            };
            Log.Warn("[ArcWarden] end init");
        }

        private void LoadItemsToMenu()
        {
            var itemsInCombo = Items.Keys.ToList();
            //ItemsInSystem = new List<ItemHolderManager.ItemHolder>();

            foreach (var inventoryItem in Context.Inventory.Items)
            {
                var item = inventoryItem.Item;
                var id = item.Id;
                if (itemsInCombo.Contains(id.ToString()) &&
                    !Menu.MenuForMainHero.ItemPriorityChanger.PictureStates.ContainsKey(id.ToString()))
                {
                    Menu.AddItemsToMenu(id);
                    //ItemsInSystem.Add(ItemHolderManager.GetOrCreate(item));
                }
            }

            Context.Inventory.CollectionChanged += (sender, args) =>
            {
                if (args.Action == NotifyCollectionChangedAction.Add)
                {
                    foreach (InventoryItem argsNewItem in args.NewItems)
                    {
                        var id = argsNewItem.Id;
                        if (itemsInCombo.Contains(id.ToString()) &&
                            !Menu.MenuForMainHero.ItemPriorityChanger.PictureStates.ContainsKey(id.ToString()))
                        {
                            Menu.AddItemsToMenu(id);
                            //ItemsInSystem.Add(ItemHolderManager.GetOrCreate(argsNewItem.Item)?.Activate());
                        }
                    }
                }
                else if (args.Action == NotifyCollectionChangedAction.Remove)
                {
                    foreach (InventoryItem argsNewItem in args.OldItems)
                    {
                        var id = argsNewItem.Id;
                        if (Menu.MenuForMainHero.ItemPriorityChanger.PictureStates.ContainsKey(id.ToString()))
                        {
                            //Menu.RemoveItemsFromMenu(id);
                            //ItemsInSystem.Remove(ItemHolderManager.GetById(id)?.Deactivate());
                        }
                    }
                }
            };
        }

        public List<ItemHolderManager.ItemHolder> ItemsInSystem;

        public static readonly Dictionary<string, byte> Items = new Dictionary<string, byte>
        {
            {"item_hurricane_pike", 1},
            {"item_mask_of_madness", 7},
            {"item_ancient_janggo", 1},
            {"item_dagon", 2},
            {"item_dagon_2", 2},
            {"item_dagon_3", 2},
            {"item_dagon_4", 2},
            {"item_dagon_5", 2},
            {"item_blink", 5},
            {"item_orchid", 4},
            {"item_manta", 1},
            {AbilityId.item_abyssal_blade.ToString(), 5},
            {AbilityId.item_force_staff.ToString(), 1},
            {AbilityId.item_nullifier.ToString(), 5},
            {AbilityId.item_diffusal_blade.ToString(), 5},
            {AbilityId.item_black_king_bar.ToString(), 5},
            {AbilityId.item_silver_edge.ToString(), 8},
            {AbilityId.item_invis_sword.ToString(), 8},
            {AbilityId.item_rod_of_atos.ToString(), 1},
            {AbilityId.item_solar_crest.ToString(), 1},
            {AbilityId.item_medallion_of_courage.ToString(), 1},
            {AbilityId.item_lotus_orb.ToString(), 1},
            {AbilityId.item_phase_boots.ToString(), 1},
            {AbilityId.item_satanic.ToString(), 1},
            {AbilityId.item_butterfly.ToString(), 1},
            {"item_arcane_boots", 1},
            {"item_guardian_greaves", 1},
            {"item_shivas_guard", 1},
            {"item_ethereal_blade", 3},
            {"item_bloodthorn", 4},
            {"item_soul_ring", 4},
            {"item_blade_mail", 4},
            {"item_veil_of_discord", 4},
            {"item_heavens_halberd", 1},
            {"item_necronomicon", 2},
            {"item_necronomicon_2", 2},
            {"item_necronomicon_3", 2},
            {"item_mjollnir", 1},
            //{ "item_hurricane_pike",1},

            {"item_sheepstick", 5},
            {"item_urn_of_shadows", 5},
            {AbilityId.item_magic_stick.ToString(), 5},
            {AbilityId.item_magic_wand.ToString(), 5},

            /*{"item_dust", 4}*/
        };
    }
}