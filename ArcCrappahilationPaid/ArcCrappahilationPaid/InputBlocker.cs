﻿using ArcCrappahilationPaid.Config;
using Ensage.SDK.Input;

namespace ArcCrappahilationPaid
{
    public class InputBlocker
    {
        private readonly ArcCrappahilation _main;
        private readonly IInputManager _inputManager;
        private readonly OrderConfig _settings;

        public InputBlocker(ArcCrappahilation main)
        {
            _main = main;
            _settings = main.Menu.OrderConfig;
            _inputManager = main.Context.Input;
        }

        public void Load()
        {
            _inputManager.RegisterHotkey("MainCombo", _settings.MainComboHotkey.Hotkey.Key, DisableKey);
            _inputManager.RegisterHotkey("TempestCombo", _settings.TempestComboHotkey.Hotkey.Key, DisableKey);
            _inputManager.RegisterHotkey("AutoPushing", _settings.AutoPushHotkey.Hotkey.Key, DisableKey);
            _inputManager.RegisterHotkey("SpamKey", _settings.SpamHotkey.Hotkey.Key, DisableKey);
        }

        private void DisableKey(KeyEventArgs keyEventArgs)
        {
            keyEventArgs.Process = false;
        }

        public void UnLoad()
        {
            _inputManager.UnregisterHotkey("MainCombo");
            _inputManager.UnregisterHotkey("TempestCombo");
            _inputManager.UnregisterHotkey("AutoPushing");
            _inputManager.UnregisterHotkey("SpamKey");
        }

        public void Toggle(bool value)
        {
            if (value)
            {
                ArcCrappahilation.Log.Info($"Load blocker");
                Load();
            }
            else
            {
                ArcCrappahilation.Log.Info($"UnLoad blocker");
                UnLoad();
            }
        }
    }
}