﻿using Ensage;

namespace ArcCrappahilationPaid.Orders
{
    public interface IOrder
    {
        void Execute(Unit target = null);
    }
}