﻿using ArcCrappahilationPaid.Units;
using Ensage;

namespace ArcCrappahilationPaid.Orders
{
    public class IdleOrder : BaseOrder
    {
        public override OrderManager.Orders Id => OrderManager.Orders.Idle;
        public IdleOrder(ControllableUnit owner) : base(owner)
        {

        }

        public override void Execute(Unit target = null)
        {
            
        }
    }
}