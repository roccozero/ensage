﻿using System;
using System.Linq;
using ArcCrappahilationPaid.AbilityUsages;
using ArcCrappahilationPaid.Units;
using Ensage;
using Ensage.Common.Extensions;
using Ensage.SDK.Extensions;
using Ensage.SDK.Helpers;

namespace ArcCrappahilationPaid.Orders
{
    public class ComboOrder : BaseOrder
    {
        public override OrderManager.Orders Id => OrderManager.Orders.Combo;
        public Unit GlobalTarget;
        private readonly bool _isMainHero;

        public ComboOrder(ControllableUnit owner) : base(owner)
        {
            _isMainHero = owner is MainHeroUnit;
            (Owner.AbilityManager as CanUseAbilityHero)?.InitAbilities();
        }

        public override void Execute(Unit target = null)
        {
            if (target != null)
            {
                if (GlobalTarget == null)
                {
                    GlobalTarget = target;
                    return;
                }
            }

            target = GlobalTarget;

            if (!Owner.IsAlive)
                return;

            if (Owner.Main.Menu.UseTravelsInCombo && !Owner.Owner.IsInRange(target, Owner.Main.Menu.RangeForTravels.Value))
            {
                var travel = Owner.Owner.GetItemById(AbilityId.item_travel_boots);
                var travel2 = Owner.Owner.GetItemById(AbilityId.item_travel_boots_2);
                if (travel != null && travel.CanBeCasted())
                {
                    var targetForTravels = GetPositionForTravels(target);
                    if (targetForTravels != null)
                    {
                        ArcCrappahilation.Log.Debug($"Using Travels [{this}] on {targetForTravels.Name} | {targetForTravels.NetworkName} | {targetForTravels.NetworkName}");
                        travel.UseAbility(targetForTravels.NetworkPosition);
                    }
                }
                if (travel2 != null && travel2.CanBeCasted())
                {
                    var targetForTravels = GetPositionForTravels(target, true);
                    if (targetForTravels != null)
                    {
                        ArcCrappahilation.Log.Debug($"Using Travels [{this}] on {targetForTravels.Name} | {targetForTravels.NetworkName} | {targetForTravels.NetworkName}");
                        travel2.UseAbility(targetForTravels.NetworkPosition);
                    }
                }
            }

            if (Ensage.SDK.Extensions.UnitExtensions.IsChanneling(Owner.Owner))
                return;

            Owner.AbilityManager.UseAbility(target);

            if (_isMainHero)
            {
                if (target.IsVisible)
                    Owner.MovementManager.Orbwalk(target, Owner.Main.Menu.MinDistance.Value, Owner.Main.Menu.GetBack);
                else
                    Owner.MovementManager.Orbwalker.Move(Owner.Main.Menu.OrderConfig.MainTargetManager.TargetPosition);
            }
            else
            {
                if (target.IsVisible)
                    Owner.MovementManager.Attack(target, Owner.Main.Menu.MinDistance.Value, Owner.Main.Menu.GetBack);
                else if (this is TempestOrder)
                    Owner.MovementManager.Orbwalker.Move(Owner.Main.Menu.OrderConfig.TempestTargetManager.TargetPosition);
                else
                    Owner.MovementManager.Orbwalker.Move(Owner.Main.Menu.OrderConfig.MainTargetManager.TargetPosition);
                //Console.WriteLine($"Current Order is {this} {Owner.OrderManager.CurrentOrder} {Owner.OrderManager.CurrentOrderId}");
            }
        }

        private Unit GetPositionForTravels(Unit target, bool checkForHeroes = false)
        {
            var dist = Owner.Owner.Distance2D(target);
            var unitsForTravels = EntityManager<Unit>.Entities.Where(x =>
                x.IsAlive && x.Name != "npc_dota_thinker" && (checkForHeroes || !(x is Hero)) &&
                x.IsAlly(Owner.Owner) && x.HealthPercent() >= 0.75f &&
                dist - x.Distance2D(target) >= 2000).OrderBy(z => z.Distance2D(target)).FirstOrDefault();

            return unitsForTravels;
        }

        public BaseOrder SetTarget(Unit target)
        {
            if (target == null)
                return this;
            GlobalTarget = target;
            return this;
        }
        public void FlushTarget()
        {
            GlobalTarget = null;
        }
    }
}