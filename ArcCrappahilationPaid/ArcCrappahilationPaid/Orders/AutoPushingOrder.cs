﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArcCrappahilationPaid.Units;
using Ensage;
using Ensage.Common.Extensions;
using Ensage.Common.Objects.UtilityObjects;
using Ensage.SDK.Extensions;
using Ensage.SDK.Helpers;
using SharpDX;
using EntityExtensions = Ensage.SDK.Extensions.EntityExtensions;
using UnitExtensions = Ensage.SDK.Extensions.UnitExtensions;

namespace ArcCrappahilationPaid.Orders
{
    public class AutoPushingOrder : BaseOrder
    {
        private static readonly CampManager CampManager = new CampManager();
        private readonly Unit _fountain;
        private readonly LaneHelper _laneHelper;
        private readonly ControllableUnit _owner;
        private readonly Sleeper _sleeper;
        private MapArea _selectedLane;
        private List<Vector3> _selectedPath;
        private int _tpCount;
        public MapArea CurrentLane;

        public AutoPushingOrder(ControllableUnit owner, LaneHelper laneHelper) : base(owner)
        {
            _owner = owner;
            _laneHelper = laneHelper;
            _sleeper = new Sleeper();
            _fountain = EntityManager<Unit>.Entities
                .FirstOrDefault(x => x.NetworkName == ClassId.CDOTA_Unit_Fountain.ToString() && x.Team == Hero.Team);
            PushOnlyTowers = false;
            if (owner is TempestHeroUnit tempo)
            {
                if (tempo.BaseMenu.AutoPushingSettings.OnlyOneTpPerOrder)
                    _tpCount = 1;
                else
                    _tpCount = -1;
            }

            PushOnlyTowers = owner.Main.Menu.AutoPushingSettings.AttackOnlyTowers;
            if (owner.Main.Menu.AutoPushingSettings.PushLaneOnMouse)
            {
                _selectedLane = laneHelper.GetLane(Game.MousePosition);
                _selectedPath = _laneHelper.GetPath(_selectedLane);
            }
            else
            {
                _selectedLane = owner.Main.Menu.AutoPushingSettings.AutoPushingPanel.GetSelectedLane();
                if (_selectedLane != MapArea.Unknown && _selectedLane != MapArea.Jungle)
                    _selectedPath = _laneHelper.GetPath(_selectedLane);
            }

            ArcCrappahilation.Log.Error($"Lane for pushing -> {_selectedLane}");
        }

        public override OrderManager.Orders Id => OrderManager.Orders.Pushing;

        public bool PushOnlyTowers { get; set; }

        public Unit Hero => Owner.Owner;

        public void UpdateLane()
        {
            _selectedLane = _owner.Main.Menu.AutoPushingSettings.AutoPushingPanel.GetSelectedLane();
            if (_selectedLane != MapArea.Unknown && _selectedLane != MapArea.Jungle)
                _selectedPath = _laneHelper.GetPath(_selectedLane);
            ArcCrappahilation.Log.Error($"Update Lane for pushing -> {_selectedLane}");
        }

        public override void Execute(Unit target = null)
        {
            if (!_sleeper.Sleeping)
            {
                _sleeper.Sleep(250);
                if (!Owner.IsAlive || UnitExtensions.IsChanneling(Owner.Owner))
                    return;
                if (Owner is TempestHeroUnit tempest)
                {
                    if (tempest.BaseMenu.AutoPushingSettings.AutoEscape)
                        if (Hero.HealthPercent() <=
                            tempest.BaseMenu.AutoPushingSettings.AutoEscapeHealthPercant.Value / 100f)
                        {
                            Owner.Owner.Move(_fountain.Position);
                            return;
                        }

                    if (tempest.BaseMenu.AutoPushingSettings.Selection.SelectedIndex == 1)
                    {
                        var leftTime = tempest.Owner.FindModifier("modifier_kill")?.RemainingTime;
                        if (leftTime <= 2.5f)
                        {
                            var itemInAutoPushing = tempest.MovementManager.Context.Inventory.Items.FirstOrDefault(x =>
                                x.Id == AbilityId.item_mjollnir && x.Item.CanBeCasted());
                            if (itemInAutoPushing != null)
                            {
                                var allyCreep =
                                    EntityManager<Creep>.Entities
                                        .Where(
                                            x =>
                                                x.IsAlive && x.Team == ObjectManager.LocalHero.Team &&
                                                Hero.IsInRange(x, 500) &&
                                                x.IsMelee).OrderByDescending(UnitExtensions.HealthPercent)
                                        .FirstOrDefault();
                                if (allyCreep != null)
                                {
                                    itemInAutoPushing.Item.UseAbility(allyCreep);
                                    ArcCrappahilation.Log.Debug(
                                        $"Use: {itemInAutoPushing.Id} in autopushing [LifeTime: {leftTime}]");
                                    _sleeper.Sleep(500);
                                }
                            }
                        }
                    }

                    if (tempest.BaseMenu.AutoPushingSettings.IsEnable &&
                        !tempest.BaseMenu.OrderConfig.TempestComboIsActive)
                    {
                        var closestHero = EntityManager<Hero>.Entities
                            .Where(x => x.IsAlive && x.IsVisible && x.IsEnemy(Hero) && !x.IsIllusion &&
                                        x.IsInRange(Hero,
                                            tempest.BaseMenu.AutoPushingSettings.Slider.Value))
                            .OrderBy(z => z.Distance2D(Hero)).FirstOrDefault();
                        if (closestHero != null)
                        {
                            Owner.OrderManager.SetOrder(new TempestOrder(Owner));
                            Owner.OrderManager.CurrentOrder.Execute(closestHero);
                            tempest.BaseMenu.OrderConfig.TempestComboIsActive = true;
                            tempest.BaseMenu.OrderConfig.AutoPushingIsActive = false;
                            tempest.BaseMenu.OrderConfig.TempestComboTarget = closestHero;
                            tempest.BaseMenu.OrderConfig.TempestTargetManager.Deactivate();
                            tempest.BaseMenu.OrderConfig.TempestTargetManager.Activate(closestHero);
                            ArcCrappahilation.Log.Debug($"Target found in autopushing. {closestHero.HeroId}");
                            UpdateManager.BeginInvoke(async () =>
                            {
                                _laneHelper.Arc.Context.Particle.DrawRange(closestHero, "TempestCombo", 150,
                                    Color.Purple);
                                while (tempest.BaseMenu.OrderConfig.TempestComboIsActive)
                                {
                                    Owner.OrderManager.CurrentOrder.Execute();
                                    await Task.Delay(75);
                                }

                                _laneHelper.Arc.Context.Particle.Remove("TempestCombo");
                                foreach (var unit in _laneHelper.Arc.Updater.Units)
                                    if (unit.OrderManager.CurrentOrderId == OrderManager.Orders.Tempest)
                                        unit.OrderManager.FlushOrder();
                                tempest.BaseMenu.OrderConfig.TempestComboTarget = null;
                            });
                            _sleeper.Sleep(250);
                            return;
                        }
                    }
                }

                var path = _selectedLane == MapArea.Unknown
                    ? _laneHelper.GetPathCache(Hero)
                    : _selectedLane == MapArea.Jungle
                        ? null
                        : _selectedPath;

                Unit creep;
                if (path == null)
                {
                    var camp = CampManager.Camps.Where(x =>
                            !x.IsEmpty)
                        .OrderBy(x => x.Position.Distance2D(Owner.Owner.Position)).FirstOrDefault();
                    if (camp != null)
                    {
                        creep = GetTarget();
                        if (creep != null)
                        {
                            //if (Owner.MovementManager.Orbwalker.CanAttack(creep))
                            TryToAttack(creep);
                        }
                        else if (camp.Position.Distance2D(Hero) <= 100)
                        {
                            var enemy = EntityManager<Unit>.Entities.FirstOrDefault(x =>
                                x.IsAlive && x.IsSpawned && x.IsVisible && x.IsEnemy(Hero) &&
                                x.IsInRange(Hero, 400));
                            if (enemy != null)
                            {
                                Owner.MovementManager.Attack(enemy);

                                if (_owner.Main.Menu.AutoPushingSettings.UseMagneticField &&
                                    (enemy.IsAncient || enemy.Health >= 1000))
                                {
                                    var field = Hero.Spellbook.Spell2;
                                    if (field.CanBeCasted())
                                    {
                                        ArcCrappahilation.Log.Debug($"Use: {field.Id} in autopushing");
                                        var pos = (enemy.NetworkPosition -
                                                   Hero.NetworkPosition).Normalized();
                                        pos *= 280 + 150;
                                        pos = enemy.NetworkPosition - pos;
                                        field.UseAbility(pos);
                                        _sleeper.Sleep(1000);
                                    }
                                }
                            }
                            else
                            {
                                camp.SetEmpty();
                            }
                        }
                        else
                        {
                            Hero.Move(camp.Position);
                        }
                    }
                    else
                    {
                        ArcCrappahilation.Log.Warn("Camp not found!");
                    }

                    return;
                }

                var lastPoint = path[path.Count - 1];
                var closestPosition = path.Where(
                        x =>
                            x.Distance2D(lastPoint) < Hero.Position.Distance2D(lastPoint) - 300)
                    .OrderBy(pos => pos.Distance2D(Hero.Position))
                    .FirstOrDefault();

                tempest = Owner as TempestHeroUnit;
                creep = GetTarget(tempest?.BaseMenu.AutoPushingSettings.IgnoreCreepsOnNotSelectedLane ?? false);
                if (creep != null)
                {
                    TryToAttack(creep);
                    /*
                    if (Owner.MovementManager.Orbwalker.CanAttack(creep))
                    {
                        TryToAttack(creep);
                    }
                    else
                    {
                        if (Owner.MovementManager.Orbwalker.CanMove())
                        {
                            Owner.MovementManager.Orbwalker.Move(creep.Position);
                        }
                    }*/
                }
                else
                {
                    Hero.Attack(closestPosition);

                    if (Owner is TempestHeroUnit t)
                    {
                        var itemInAutoPushing = t.MovementManager.Context.Inventory.Items.FirstOrDefault(x =>
                            (x.Id == AbilityId.item_force_staff || x.Id == AbilityId.item_hurricane_pike) &&
                            x.Item.CanBeCasted());
                        if (itemInAutoPushing != null && t.Owner.IsDirectlyFacing(closestPosition))
                        {
                            var anyEnemyCreep = EntityManager<Unit>.Entities.Any(x =>
                                x.IsAlive && x.IsSpawned && x.IsVisible && x.IsEnemy(Hero) &&
                                x.IsInRange(Hero, 1200));
                            if (!anyEnemyCreep) itemInAutoPushing.Item.UseAbility(t.Owner);
                        }

                        TryToTravels(t, path);
                    }
                }
            }
        }

        private void TryToAttack(Unit creep)
        {
            Owner.MovementManager.Attack(creep,
                _owner.Main.Menu.AutoPushingSettings.UseOrbWalkerDistance
                    ? _owner.Main.Menu.AutoPushingSettings.MinDistance.Value
                    : 0);
            if (!creep.IsVisible || !creep.IsSpawned)
                return;
            if (Owner is TempestHeroUnit t)
            {
                ArcCrappahilation.Log.Debug(
                    $"Attack: {creep} {creep.NetworkName} {creep.NetworkName} {creep.Name} in autopushing IsCreep: {creep is Creep} IsTower: {creep is Tower}");
                if (creep is Tower || creep is Building ||
                    creep.IsAncient && _owner.Main.Menu.AutoPushingSettings.UseMagneticField)
                {
                    var field = Hero.Spellbook.Spell2;
                    if (field.CanBeCasted())
                    {
                        ArcCrappahilation.Log.Debug($"Use: {field.Id} in autopushing");
                        var pos = (creep.NetworkPosition -
                                   Hero.NetworkPosition).Normalized();
                        pos *= 280 + 150;
                        pos = creep.NetworkPosition - pos;
                        field.UseAbility(pos);
                        _sleeper.Sleep(1000);
                    }

                    if (creep is Tower)
                    {
                        var meteor = Hero.GetItemById(AbilityId.item_meteor_hammer);
                        if (meteor != null && meteor.CanBeCasted() && meteor.CanHit(creep))
                        {
                            meteor.UseAbility(creep.Position);
                            Owner.Owner.Attack(creep.Position, true);
                            _sleeper.Sleep(3000);
                        }

                        var spark = Hero.Spellbook.Spell3;
                        if (spark.CanBeCasted())
                        {
                            ArcCrappahilation.Log.Debug($"Use: {spark.Id} in autopushing [OnTower]");
                            spark.UseAbility(creep.Position);
                            _sleeper.Sleep(500);
                        }
                    }
                }
                else
                {
                    var spark = Hero.Spellbook.Spell3;
                    if (spark.CanBeCasted())
                    {
                        ArcCrappahilation.Log.Debug($"Use: {spark.Id} in autopushing");
                        spark.UseAbility(creep.Position);
                        _sleeper.Sleep(500);
                    }

                    var itemInAutoPushing = t.MovementManager.Context.Inventory.Items.FirstOrDefault(x =>
                        (x.Id == AbilityId.item_manta || x.Id == AbilityId.item_necronomicon ||
                         x.Id == AbilityId.item_necronomicon_2 || x.Id == AbilityId.item_necronomicon_3) &&
                        x.Item.CanBeCasted());
                    if (itemInAutoPushing != null)
                    {
                        itemInAutoPushing.Item.UseAbility();
                        ArcCrappahilation.Log.Debug($"Use: {itemInAutoPushing.Id} in autopushing");
                        _sleeper.Sleep(500);
                    }

                    if (t.BaseMenu.AutoPushingSettings.Selection.SelectedIndex == 0)
                    {
                        itemInAutoPushing = t.MovementManager.Context.Inventory.Items.FirstOrDefault(x =>
                            x.Id == AbilityId.item_mjollnir && x.Item.CanBeCasted());
                        if (itemInAutoPushing != null)
                        {
                            var allyCreep =
                                EntityManager<Creep>.Entities
                                    .FirstOrDefault(
                                        x =>
                                            x.IsAlive && x.Team == ObjectManager.LocalHero.Team &&
                                            Hero.IsInRange(x, 500) &&
                                            x.HealthPercent() <= 0.92 &&
                                            x.IsMelee);
                            if (allyCreep != null)
                            {
                                itemInAutoPushing.Item.UseAbility(allyCreep);
                                ArcCrappahilation.Log.Debug($"Use: {itemInAutoPushing.Id} in autopushing");
                                _sleeper.Sleep(500);
                            }
                        }
                    }
                }
            }
        }

        private void TryToTravels(TempestHeroUnit t, IReadOnlyList<Vector3> path)
        {
            if (_tpCount == 0 && t.BaseMenu.AutoPushingSettings.OnlyOneTpPerOrder)
            {
                ArcCrappahilation.Log.Debug("Cant do more then 1 tp per order");
                return;
            }

            var leftTime = t.Owner.FindModifier("modifier_kill")?.RemainingTime;
            if (leftTime <= 9 && t.BaseMenu.AutoPushingSettings.CheckForLifeTime)
            {
                ArcCrappahilation.Log.Debug($"Cant do tp cuz lifetime {leftTime}<=9");
                return;
            }

            if (t.BaseMenu.AutoPushingSettings.UseTravels)
            {
                //var travels = t.MovementManager.Context.Inventory.Items.FirstOrDefault(x =>
                //    (x.Id == AbilityId.item_travel_boots || x.Id == AbilityId.item_travel_boots_2)/* &&
                //    x.Item.CanBeCasted()*/)
                //   ?.Item;

                var travels = t.Owner.Inventory.Items.FirstOrDefault(x =>
                    (x.Id == AbilityId.item_travel_boots || x.Id == AbilityId.item_travel_boots_2) &&
                    x.CanBeCasted());
                if (travels == null)
                {
                    ArcCrappahilation.Log.Warn(
                        "Cant find travels boots, or travels is on cooldown, will try to use Tp scroll");
                    travels = EntityManager<Item>.Entities.FirstOrDefault(x =>
                        x.Id == AbilityId.item_tpscroll && x.Owner == t.Owner && x.CanBeCasted());
                    ArcCrappahilation.Log.Warn(travels != null);
                }

                if (travels != null)
                {
                    if (!travels.CanBeCasted())
                    {
                        ArcCrappahilation.Log.Debug(
                            $"Travels cant be casted. {travels.AbilityState} cd: {travels.Cooldown}");
                        return;
                    }

                    var temp = path.ToList();
                    temp.Reverse();
                    var currentLane = _selectedLane == MapArea.Unknown
                        ? _laneHelper.GetLane(Hero)
                        : _selectedLane;
                    ArcCrappahilation.Log.Warn($"Current Lane: {currentLane} Selected Lane: {_selectedLane}");
                    if (t.BaseMenu.AutoPushingSettings.CheckForCreeps)
                    {
                        ArcCrappahilation.Log.Warn("[AutoPushing] checking for creeps");
                        var enemyCreeps =
                            EntityManager<Creep>.Entities.Where(
                                x =>
                                    x.IsValid && x.IsVisible && x.IsAlive && x.IsSpawned &&
                                    x.Team != ObjectManager.LocalHero.Team &&
                                    _laneHelper.GetLane(x) == currentLane);
                        Creep creepForTravels = null;

                        var allyCreeps =
                            EntityManager<Creep>.Entities.Where(
                                allyCreep => allyCreep.IsValid && allyCreep.IsAlive && allyCreep.IsSpawned &&
                                             allyCreep.Team == ObjectManager.LocalHero.Team &&
                                             _laneHelper.GetLane(allyCreep) == currentLane &&
                                             allyCreep.HealthPercent() > 0.75).ToList();
                        var detected = false;
                        foreach (var point in temp)
                        {
                            creepForTravels = allyCreeps.FirstOrDefault(
                                allyCreep => allyCreep.IsInRange(point, 1500)
                                             /*UnitExtensions.IsInRange(point, allyCreep, 1500)*/ &&
                                             enemyCreeps.Any(z => z.IsInRange(allyCreep, 1500)));
                            if (creepForTravels != null)
                            {
                                ArcCrappahilation.Log.Warn("[AutoPushing] ally creep with enemy creeps detected");
                                detected = true;
                                break;
                            }
                        }

                        if (!detected)
                            ArcCrappahilation.Log.Warn("[AutoPushing] ally creep with enemy creeps NOT detected");
                        if (creepForTravels != null && creepForTravels.Distance2D(Hero) > 1500)
                        {
                            var point = path[path.Count - 1];
                            var distance1 = point.Distance2D(creepForTravels);
                            var distance2 = point.Distance2D(Hero);

                            if (distance1 < distance2 || _laneHelper.GetLane(Hero) != currentLane)
                            {
                                travels.UseAbility(creepForTravels);
                                ArcCrappahilation.Log.Debug("Use: travels in autopushing");
                                _tpCount--;
                                _sleeper.Sleep(3500);
                            }
                            else
                            {
                                ArcCrappahilation.Log.Warn("[AutoPushing] Will not use travels due distance");
                            }
                        }
                        else
                        {
                            ArcCrappahilation.Log.Warn(
                                $"[AutoPushing] {(creepForTravels == null ? "Cant find ally creep with enemy creeps for tp" : $"Distance for main hero less then 1500 ({creepForTravels.Distance2D(Hero)})")} ");
                        }
                    }
                    else
                    {
                        var finalPos = temp.First();
                        var isNotScroll = travels.Id != AbilityId.item_tpscroll;
                        var ally =
                            EntityManager<Creep>.Entities.Where(
                                    allyCreep => allyCreep.IsValid && allyCreep.IsAlive &&
                                                 allyCreep.Team == ObjectManager.LocalHero.Team &&
                                                 _laneHelper.GetLane(allyCreep) == currentLane &&
                                                 allyCreep.HealthPercent() > 0.75)
                                .OrderBy(y => EntityExtensions.Distance2D(y, finalPos))
                                .FirstOrDefault();
                        var allyTwr =
                            EntityManager<Tower>.Entities.Where(
                                    allyCreep => allyCreep.IsValid && allyCreep.IsAlive &&
                                                 allyCreep.Team == ObjectManager.LocalHero.Team &&
                                                 _laneHelper.GetLane(allyCreep) == currentLane &&
                                                 allyCreep.HealthPercent() > 0.1)
                                .OrderBy(y => EntityExtensions.Distance2D(y, finalPos))
                                .FirstOrDefault();

                        Unit tpTarget = null;
                        if (ally != null && allyTwr != null)
                        {
                            var dist1 = finalPos.Distance2D(ally);
                            var dist2 = finalPos.Distance2D(allyTwr);

                            if (dist1 > dist2)
                                tpTarget = allyTwr;
                            else
                                tpTarget = ally;
                        }

                        if (tpTarget != null && tpTarget.Distance2D(Hero) > 1500)
                        {
                            var point = path[path.Count - 1];
                            var distance1 = point.Distance2D(tpTarget);
                            var distance2 = point.Distance2D(Hero);

                            if (distance1 < distance2 || _laneHelper.GetLane(Hero) != currentLane)
                            {
                                travels.UseAbility(tpTarget.Position);
                                _tpCount--;
                                ArcCrappahilation.Log.Debug(
                                    $"Use: {(isNotScroll ? "travels" : "tp scroll")} in autopushing");
                                _sleeper.Sleep(3500);
                            }
                        }
                    }
                }
                else
                {
                    ArcCrappahilation.Log.Debug("[AutoPushing]: cant find travels");
                }
            }
        }

        private Unit GetTarget(bool checkForPos = false)
        {
            if (!PushOnlyTowers)
            {
                var barracks =
                    ObjectManager.GetEntitiesFast<Building>()
                        .FirstOrDefault(
                            unit =>
                                unit.IsValid && unit.IsAlive && unit.NetworkName != "CDOTA_BaseNPC" &&
                                unit.Team != Hero.Team && !(unit is Tower) &&
                                Hero.IsValidOrbwalkingTarget(unit)
                                && unit.Name != "portrait_world_unit");

                if (barracks != null) return barracks;
                var meteor = Hero.GetItemById(AbilityId.item_meteor_hammer);
                if (meteor != null && meteor.CanBeCasted())
                {
                    var tower =
                        ObjectManager.GetEntitiesFast<Tower>()
                            .FirstOrDefault(
                                unit =>
                                    unit.IsValid && unit.IsAlive && unit.NetworkName != "CDOTA_BaseNPC" &&
                                    unit.Team != Hero.Team &&
                                    Hero.IsValidOrbwalkingTarget(unit));

                    if (tower != null) return tower;

                    var creep =
                        EntityManager<Creep>.Entities.Where(
                                unit =>
                                    unit.IsValid && unit.IsSpawned && unit.NetworkName != "CDOTA_BaseNPC" &&
                                    unit.IsAlive && unit.Team != Hero.Team &&
                                    (Hero.IsValidOrbwalkingTarget(unit) ||
                                     Hero.Distance2D(unit) <= 500) &&
                                    (!checkForPos || _selectedLane == MapArea.Unknown ||
                                     _laneHelper.GetLane(unit) == _selectedLane))
                            .OrderBy(x => x.Health)
                            .FirstOrDefault();

                    if (creep != null) return creep;
                }

                else
                {
                    var creep =
                        EntityManager<Creep>.Entities.Where(
                                unit =>
                                    unit.IsValid && unit.NetworkName != "CDOTA_BaseNPC" && unit.IsSpawned &&
                                    unit.IsAlive && unit.Team != Hero.Team &&
                                    (Hero.IsValidOrbwalkingTarget(unit) ||
                                     Hero.Distance2D(unit) <= 500) &&
                                    (!checkForPos || _selectedLane == MapArea.Unknown ||
                                     _laneHelper.GetLane(unit) == _selectedLane))
                            .OrderBy(x => x.Health)
                            .FirstOrDefault();

                    if (creep != null) return creep;

                    var tower =
                        ObjectManager.GetEntitiesFast<Tower>()
                            .FirstOrDefault(
                                unit =>
                                    unit.IsValid && unit.IsAlive && unit.NetworkName != "CDOTA_BaseNPC" &&
                                    unit.Team != Hero.Team &&
                                    Hero.IsValidOrbwalkingTarget(unit));

                    if (tower != null) return tower;
                }

                var jungleMob =
                    EntityManager<Creep>.Entities.FirstOrDefault(
                        unit =>
                            unit.IsValid && unit.IsSpawned && unit.NetworkName != "CDOTA_BaseNPC" && unit.IsAlive &&
                            unit.IsNeutral &&
                            Hero.IsValidOrbwalkingTarget(unit) &&
                            (!checkForPos || _selectedLane == MapArea.Unknown ||
                             _laneHelper.GetLane(unit) == _selectedLane));

                if (jungleMob != null) return jungleMob;

                var others =
                    ObjectManager.GetEntitiesFast<Unit>()
                        .FirstOrDefault(
                            unit =>
                                unit.IsValid && !(unit is Hero) && !(unit is Creep) && unit.IsAlive &&
                                !unit.IsInvulnerable() && unit.NetworkName != "CDOTA_BaseNPC" &&
                                unit.Team != Hero.Team &&
                                Hero.IsValidOrbwalkingTarget(unit) &&
                                unit.NetworkName != ClassId.CDOTA_BaseNPC.ToString() &&
                                (!checkForPos || _selectedLane == MapArea.Unknown ||
                                 _laneHelper.GetLane(unit) == _selectedLane));

                if (others != null) return others;

                var heroes =
                    ObjectManager.GetEntitiesFast<Hero>()
                        .FirstOrDefault(
                            unit =>
                                unit.IsValid && unit.IsAlive && unit.NetworkName != "CDOTA_BaseNPC" &&
                                !unit.IsIllusion &&
                                !unit.IsInvulnerable() && unit.Team != Hero.Team &&
                                Hero.IsValidOrbwalkingTarget(unit) &&
                                unit.NetworkName != ClassId.CDOTA_BaseNPC.ToString());

                if (heroes != null) return heroes;
            }
            else
            {
                var tower =
                    ObjectManager.GetEntitiesFast<Tower>()
                        .FirstOrDefault(
                            unit =>
                                unit.IsValid && unit.IsAlive && unit.NetworkName != "CDOTA_BaseNPC" &&
                                unit.Team != Hero.Team &&
                                Hero.IsValidOrbwalkingTarget(unit));

                if (tower != null) return tower;

                var barracks =
                    ObjectManager.GetEntitiesFast<Building>()
                        .FirstOrDefault(
                            unit =>
                                unit.IsValid && unit.IsAlive && unit.NetworkName != "CDOTA_BaseNPC" &&
                                unit.Team != Hero.Team && !(unit is Tower) &&
                                Hero.IsValidOrbwalkingTarget(unit)
                                && unit.Name != "portrait_world_unit");

                if (barracks != null) return barracks;
            }

            return null;
        }
    }
}