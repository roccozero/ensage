﻿using ArcCrappahilationPaid.Units;

namespace ArcCrappahilationPaid.Orders
{
    public class TempestOrder : ComboOrder
    {
        public override OrderManager.Orders Id => OrderManager.Orders.Tempest;
        public TempestOrder(ControllableUnit owner) : base(owner)
        {

        }
    }
}