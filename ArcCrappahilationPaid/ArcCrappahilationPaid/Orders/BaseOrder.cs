﻿using ArcCrappahilationPaid.Units;
using Ensage;

namespace ArcCrappahilationPaid.Orders
{
    public abstract class BaseOrder : IOrder
    {
        public virtual OrderManager.Orders Id => 0;
        protected BaseOrder(ControllableUnit owner)
        {
            Owner = owner;
        }

        public BaseOrder SetOwner(ControllableUnit owner)
        {
            Owner = owner;
            return this;
        }

        public ControllableUnit Owner { get; set; }
        public abstract void Execute(Unit target = null);
    }
}