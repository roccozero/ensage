﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ArcCrappahilationPaid.Units;
using Ensage;
using Ensage.SDK.Extensions;
using Ensage.SDK.Helpers;

namespace ArcCrappahilationPaid
{
    public class Updater
    {
        private readonly ArcCrappahilation _main;

        public Updater(ArcCrappahilation main)
        {
            _main = main;
            var heroes =
                EntityManager<Hero>.Entities.Where(x =>
                    x.IsAlive && x.HeroId == HeroId.npc_dota_hero_arc_warden && x.IsAlly(ObjectManager.LocalHero) && x.IsControllable);
            Units = new List<ControllableUnit>();
            foreach (var hero in heroes)
            {
                if (Units.Any(x => x.Owner.Equals(hero)))
                    continue;
                if (!hero.IsIllusion)
                {
                    if (hero.Equals(ObjectManager.LocalHero))
                    {
                        Units.Add(new MainHeroUnit(hero, main));
                        ArcCrappahilation.Log.Warn($"added MainHero to Units");
                    }
                    else
                    {
                        if (hero.HasModifier("modifier_kill"))
                        {
                            Units.Add(new TempestHeroUnit(hero, main));
                            ArcCrappahilation.Log.Warn($"added TempestHeroUnit to Units");
                        }
                    }
                }
                else
                {
                    if (hero.HasModifier("modifier_arc_warden_tempest_double"))
                    {
                        Units.Add(new TempestHeroUnit(hero, main));
                        ArcCrappahilation.Log.Warn($"added TempestHeroUnit to Units");
                    }
                    else
                    {
                        Units.Add(new IllusionHeroUnit(hero, main));
                        ArcCrappahilation.Log.Warn($"added IllusionHeroUnit to Units");
                    }
                }
            }

            UpdateManager.Subscribe(TempestChecker, 150);

            EntityManager<Hero>.EntityAdded += (sender, hero) =>
            {
                UpdateManager.BeginInvoke(() =>
                {
                    if (Units.Any(x => x.Owner.Equals(hero)))
                        return;
                    if (hero.HeroId != HeroId.npc_dota_hero_arc_warden || !hero.IsAlly(ObjectManager.LocalHero) || !hero.IsControllable)
                        return;
                    if (!hero.IsIllusion)
                    {
                        if (hero.Equals(ObjectManager.LocalHero))
                        {
                            Units.Add(new MainHeroUnit(hero, main));
                            ArcCrappahilation.Log.Warn($"added MainHeroUnit to Units");
                        }
                        else
                        {
                            if (hero.HasModifier("modifier_kill"))
                            {
                                Units.Add(new TempestHeroUnit(hero, main));
                                ArcCrappahilation.Log.Warn($"added TempestHeroUnit to Units");
                            }
                            else
                            {
                                ArcCrappahilation.Log.Warn($"[NOT] added TempestHeroUnit to Units");
                            }
                        }
                    }
                    else
                    {
                        if (hero.HasModifier("modifier_kill"))
                        {
                            Units.Add(new TempestHeroUnit(hero, main));
                            ArcCrappahilation.Log.Warn($"added TempestHeroUnit to Units");
                        }
                        else
                        {
                            Units.Add(new IllusionHeroUnit(hero, main));
                            ArcCrappahilation.Log.Warn($"added IllusionHeroUnit to Units");
                        }
                    }
                }, 100);
            };

            EntityManager<Hero>.EntityRemoved += (sender, h) =>
            {
                var hero = Units.FirstOrDefault(x => x.Owner.Equals(h));
                switch (hero)
                {
                    case null:
                        return;
                    case MainHeroUnit _:
                        return;
                    case TempestHeroUnit _:
                        return;
                }
                Units.Remove(hero);
                ArcCrappahilation.Log.Warn($"remove IllusionHeroUnit from Units");
            };


            var necros =
                EntityManager<Unit>.Entities.Where(x =>
                    x.IsAlive && x.IsControllable && (x.Name.Contains("npc_dota_necronomicon_warrior") ||
                                                      x.Name.Contains("npc_dota_necronomicon_archer")) &&
                    !Units.Any(z => x.Equals(z.Owner)));
            foreach (var necro in necros)
            {
                if (necro.IsMelee)
                {
                    Units.Add(new NecraMelee(necro, main));
                    ArcCrappahilation.Log.Warn($"added MeleeNecro to Units");
                }
                else
                {
                    Units.Add(new NecroRange(necro, main));
                    ArcCrappahilation.Log.Warn($"added RangeNecro to Units");
                }
                
            }

            EntityManager<Unit>.EntityAdded += (sender, x) =>
            {
                if (Units.Any(z => x.Equals(z.Owner)))
                    return;
                if (x.IsControllable && (x.Name.Contains("npc_dota_necronomicon_warrior") ||
                                         x.Name.Contains("npc_dota_necronomicon_archer")))
                {
                    if (x.IsMelee)
                    {
                        Units.Add(new NecraMelee(x, main));
                        ArcCrappahilation.Log.Warn($"added MeleeNecro to Units");
                    }
                    else
                    {
                        Units.Add(new NecroRange(x, main));
                        ArcCrappahilation.Log.Warn($"added RangeNecro to Units");
                    }
                }
            };
            EntityManager<Unit>.EntityRemoved += (sender, x) =>
            {
                if (!Units.Any(z => x.Equals(z.Owner)))
                    return;
                Units.RemoveAll(z => z.Owner.Equals(x));
                ArcCrappahilation.Log.Warn($"remove Necro from Units");
            };

        }

        public TempestHeroUnit TempestHero { get; set; }

        private void TempestChecker()
        {
            if (!Units.Any(x=>x is TempestHeroUnit))
            {
                var tempest =
                    EntityManager<Hero>.Entities
                        .FirstOrDefault(
                            x =>
                                x != null && x.IsAlive && x.HeroId == HeroId.npc_dota_hero_arc_warden &&
                                x.IsAlly(ObjectManager.LocalHero) && x.IsControllable &
                                x.HasModifier("modifier_arc_warden_tempest_double"));
                if (tempest != null)
                {
                    Units.Add(new TempestHeroUnit(tempest, _main));
                    ArcCrappahilation.Log.Warn($"[Tempest] added to Units");
                    UpdateManager.Unsubscribe(TempestChecker);
                }
            }
        }

        public List<ControllableUnit> Units;
        public List<ControllableUnit> AviableUnits;
        public void UpdateUnits()
        {
            AviableUnits.Clear();
            foreach (var unit in Units)
            {
                if (unit == null || !unit.IsValid || !unit.IsAlive)
                    continue;
                switch (unit.OrderManager.CurrentOrderId)
                {
                    case OrderManager.Orders.Idle:
                        break;
                    case OrderManager.Orders.Combo:
                        break;
                    case OrderManager.Orders.Pushing:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}