using System.ComponentModel;
using Ensage.SDK.Menu;
using Ensage.SDK.Menu.Items;

namespace ArcCrappahilationPaid.Config
{
    public class SmartSpark
    {
        [Tooltip("Dont use spark if target in attack range")]
        [Item("Spark in attack range")]
        [DefaultValue(false)]
        public bool IsEnable { get; set; }

        [Tooltip("Will not use spark when target in attack range if hero's lvl is higher then this value")]
        [Item("Max lvl")]
        public Slider<float> Slider { get; set; } = new Slider<float>(10, 1, 25);
    }
}