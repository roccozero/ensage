using System.ComponentModel;
using Ensage.SDK.Menu;
using Ensage.SDK.Menu.Items;

namespace ArcCrappahilationPaid.Config
{
    public class MenuForBlink
    {
        public MenuForBlink()
        {
        }

        [Item("Extra range for dagger")]
        public Slider<float> Slider { get; set; } = new Slider<float>(400, 100, 2000);

        [Item("Use range")]
        [Tooltip("If disabled -> will use dagger without range checking")]
        [DefaultValue(true)]
        public bool UseRange { get; set; }
    }
}