using System;
using ArcCrappahilationPaid.Units;
using Ensage;
using Ensage.Common.Extensions;
using Ensage.SDK.Handlers;
using Ensage.SDK.Helpers;
using SharpDX;

namespace ArcCrappahilationPaid.Config
{
    public class TargetManager
    {
        private readonly ArcCrappahilation Main;
        public readonly ControllableUnit Owner;
        public float LastTimeUnderVision;

        public Unit Target;
        public Vector3 TargetPosition;

        public TargetManager(ArcCrappahilation main, ControllableUnit owner)
        {
            Main = main;
            Owner = owner;
            Updater = UpdateManager.Subscribe(Callback, isEnabled: false);
        }

        private IUpdateHandler Updater { get; }

        public void Activate()
        {
            LastTimeUnderVision = Game.RawGameTime;
            Updater.IsEnabled = true;
        }

        public void Activate(Unit target)
        {
            SetTarget(target);
            LastTimeUnderVision = Game.RawGameTime;
            Updater.IsEnabled = true;
        }

        public void Deactivate()
        {
            Updater.IsEnabled = false;
            LastTimeUnderVision = 0f;
            Target = null;
            Main.Context.Particle.Remove("Range" + Owner.Owner.Handle);
        }

        public void SetTarget(Unit target)
        {
            Target = target;
        }

        private void Callback()
        {
            if (Target == null)
            {
            }
            else
            {
                var isVisible = Target.IsVisible;
                if (isVisible)
                    LastTimeUnderVision = Game.RawGameTime;
                TargetPosition = GetTargetPosition(isVisible);
                try
                {
                    Main.Context.Particle.DrawTargetLine(Owner.Owner, "Range" + Owner.Owner.Handle, TargetPosition);
                }
                catch (Exception e)
                {
                    ArcCrappahilation.Log.Error(e);
                }
            }
        }

        private Vector3 GetTargetPosition(bool isVisible)
        {
            if (Target == null)
            {
                ArcCrappahilation.Log.Error("Target is null in combo. Pos is zero");
                return Vector3.Zero;
            }

            return isVisible
                ? Target.NetworkPosition
                : UnitExtensions.Predict(Target,
                    (Game.RawGameTime - LastTimeUnderVision) * 1000f);
            ;
        }
    }
}