using Ensage.SDK.Helpers;
using Ensage.SDK.Menu;
using Ensage.SDK.Menu.Items;
using Ensage.SDK.Menu.ValueBinding;
using Newtonsoft.Json;

namespace ArcCrappahilationPaid.Config
{
    public class OrbwalkerConfig
    {
        [JsonIgnore] private readonly BaseMenu _baseMenu;

        public OrbwalkerConfig(BaseMenu baseMenu)
        {
            _baseMenu = baseMenu;

            AttackDelay.ValueChanging += OnValueChange;
            TurnDelay.ValueChanging += OnValueChange;
            HoldRange.ValueChanging += OnValueChange;
            MoveDelay.ValueChanging += OnValueChange;

            UpdateManager.BeginInvoke(() =>
            {
                CustomOrbwalker.AttackDelay = AttackDelay.Value;
                CustomOrbwalker.TurnDelay = TurnDelay.Value;
                CustomOrbwalker.HoldRange = HoldRange.Value;
                CustomOrbwalker.MoveDelay = MoveDelay.Value;
            }, 150);
        }

        [Item("MoveDelay")] public Slider<float> MoveDelay { get; set; } = new Slider<float>(60, 1, 200);
        [Item("AttackDelay")] public Slider<float> AttackDelay { get; set; } = new Slider<float>(5, 1, 200);
        [Item("TurnDelay")] public Slider<float> TurnDelay { get; set; } = new Slider<float>(200, 1, 200);
        [Item("HoldRange")] public Slider<float> HoldRange { get; set; } = new Slider<float>(60, 1, 200);


        private void OnValueChange(object sender, ValueChangingEventArgs<float> e)
        {
            CustomOrbwalker.AttackDelay = AttackDelay.Value;
            CustomOrbwalker.TurnDelay = TurnDelay.Value;
            CustomOrbwalker.HoldRange = HoldRange.Value;
            CustomOrbwalker.MoveDelay = MoveDelay.Value;
            ArcCrappahilation.Log.Warn($"${sender} changed to {e.Value}");
        }
    }
}