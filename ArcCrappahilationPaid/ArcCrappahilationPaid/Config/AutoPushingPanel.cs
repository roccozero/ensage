using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using ArcCrappahilationPaid.Orders;
using Ensage;
using Ensage.SDK.Helpers;
using Ensage.SDK.Input;
using Ensage.SDK.Menu;
using Ensage.SDK.Menu.Items;
using Ensage.SDK.Renderer;
using Newtonsoft.Json;
using SharpDX;
using Color = System.Drawing.Color;

namespace ArcCrappahilationPaid.Config
{
    public class Button
    {
        private static int _globalId;
        public bool Active;
        public int Id;
        public bool UnderMouse;

        public Button(MapArea lane, bool active = false)
        {
            Name = lane.ToString();
            Lane = lane;
            UnderMouse = false;
            Active = active;
            _globalId++;
            Id = _globalId;
        }

        public Button(string name, bool active = false)
        {
            Name = name;
            Lane = MapArea.Unknown;
            UnderMouse = false;
            Active = active;
            _globalId++;
            Id = _globalId;
        }

        public string Name { get; }
        public MapArea Lane { get; }
    }

    public class AutoPushingPanel
    {
        [JsonIgnore] private readonly List<Button> _buttons;

        [JsonIgnore] private readonly IInputManager _input;

        [JsonIgnore] private readonly Button _pushingOnMouse;

        [JsonIgnore] private readonly IRenderManager _render;

        [JsonIgnore] private readonly AutoPushingSettings _settings;

        [JsonIgnore] private Vector2 _drawMousePosition;

        [JsonIgnore] private bool _isMoving;

        [JsonIgnore] private Vector2 _mesText;

        [JsonIgnore] private bool _panel;

        public AutoPushingPanel(AutoPushingSettings settings)
        {
            _settings = settings;
            _render = _settings.Main.Context.RenderManager;
            _input = _settings.Main.Context.Input;
            DrawingStartPosition = Position;
            _buttons = new List<Button>
            {
                new Button(MapArea.Top),
                new Button(MapArea.Middle),
                new Button(MapArea.Bottom),
                new Button(MapArea.Jungle),
                new Button("Auto", true)
            };
            _pushingOnMouse = new Button("Lane under mouse");
            _mesText = _render.MeasureText("AutoPushing Panel", TextSize.Value);
            TextSize.ValueChanging += (sender, args) =>
            {
                _mesText = _render.MeasureText("AutoPushing Panel", args.Value);
            };
        }

        [JsonIgnore] public bool IsActive { get; set; }

        [JsonIgnore] public Slider<Vector2> DrawingStartPosition { get; set; }

        [Item("Show Auto Pushing panel")]
        [DefaultValue(true)]
        public bool Panel
        {
            get => _panel;
            set
            {
                if (_panel != value)
                {
                    if (value)
                        Activate();
                    else
                        Deactivate();
                }

                _panel = value;
            }
        }

        [Item("Panel Position")]
        public Slider<Vector2> Position { get; set; } = new Slider<Vector2>(new Vector2(200, 300), new Vector2(0, 0),
            new Vector2(Drawing.Width - 10, Drawing.Height - 10));

        /*[Item("Size X")]
        public Slider<float> SizeX { get; set; } = new Slider<float>(200, 5, 400);

        [Item("Size Y")]
        public Slider<float> SizeY { get; set; } = new Slider<float>(25, 5, 400);*/

        [Item("Text Size")] public Slider<float> TextSize { get; set; } = new Slider<float>(50, 1, 100);

        public MapArea GetSelectedLane()
        {
            var button = _buttons.FirstOrDefault(x => x.Active);
            return IsActive ? button?.Lane ?? MapArea.Unknown : MapArea.Unknown;
        }

        public void Activate()
        {
            if (IsActive)
                return;
            IsActive = true;
            _input.MouseClick += InputOnMouseClick;
            _input.MouseMove += InputOnMouseMove;
            _render.Draw += RenderOnDraw;
        }

        private void RenderOnDraw(IRenderer renderer)
        {
            var rect = new RectangleF(DrawingStartPosition.Value.X, DrawingStartPosition.Value.Y,
                _mesText.X, _mesText.Y);
            renderer.DrawFilledRectangle(rect,
                Color.FromArgb(100, 0, 0, 0),
                _settings.Main.Menu.OrderConfig.AutoPushingIsActive ? Color.GreenYellow : Color.Red
            );
            renderer.DrawText(rect, "AutoPushing Panel", Color.AliceBlue, fontSize: TextSize.Value);
            if (_settings.PushLaneOnMouse)
            {
                rect.Y += _mesText.Y;
                var clr = Color.FromArgb(100, 0, 155, 0);
                renderer.DrawFilledRectangle(rect, clr, clr);
                renderer.DrawText(rect, _pushingOnMouse.Name, Color.AliceBlue, fontSize: TextSize.Value);
            }
            else
            {
                foreach (var button in _buttons)
                {
                    rect.Y += _mesText.Y;

                    if (button.UnderMouse)
                    {
                        var clr = Color.FromArgb(100, 155, 155, 155);
                        renderer.DrawFilledRectangle(rect, clr, clr);
                    }

                    if (button.Active)
                    {
                        var clr = Color.FromArgb(100, 0, 155, 0);
                        renderer.DrawFilledRectangle(rect, clr, clr);
                    }

                    renderer.DrawText(rect, button.Name, Color.AliceBlue, fontSize: TextSize.Value);
                }
            }
        }

        public void RefreshButtons()
        {
            ////TODO: check dat shit
            foreach (var button in _buttons)
            {
                ArcCrappahilation.Log.Error($"Refreshing Button: id [{button.Id}] {button.Id == 5}");
                button.Active = button.Id == 5;
            }
        }

        public void Deactivate()
        {
            if (!IsActive)
                return;
            IsActive = false;
            _input.MouseClick -= InputOnMouseClick;
            _input.MouseMove -= InputOnMouseMove;
            _render.Draw -= RenderOnDraw;
        }

        private void InputOnMouseMove(object sender, MouseEventArgs e)
        {
            if (_isMoving)
            {
                var newValue = new Vector2(e.Position.X - _drawMousePosition.X,
                    e.Position.Y - _drawMousePosition.Y);
                newValue.X = Math.Max(Position.MinValue.X, Math.Min(Position.MaxValue.X, newValue.X));
                newValue.Y = Math.Max(Position.MinValue.Y, Math.Min(Position.MaxValue.Y, newValue.Y));
                DrawingStartPosition.Value = newValue;
            }
            else if (!_settings.PushLaneOnMouse)
            {
                /*var size = new RectangleF(DrawingStartPosition.Value.X, DrawingStartPosition.Value.Y + _mesText.Y,
                    _mesText.X, _mesText.Y * 5);
                var isIn = size.Contains(e.Position);
                if (isIn)
                {*/
                foreach (var button in _buttons)
                {
                    var size = new RectangleF(DrawingStartPosition.Value.X,
                        DrawingStartPosition.Value.Y + _mesText.Y * button.Id,
                        _mesText.X, _mesText.Y);
                    var isIn = size.Contains(e.Position);
                    button.UnderMouse = isIn;
                }

                //}
            }
        }

        private void InputOnMouseClick(object sender, MouseEventArgs e)
        {
            if ((e.Buttons & MouseButtons.Left) == 0) return;

            var size = new RectangleF(DrawingStartPosition.Value.X, DrawingStartPosition.Value.Y,
                _mesText.X, _mesText.Y);
            var isIn = size.Contains(e.Position);
            if ((e.Buttons & MouseButtons.LeftUp) == MouseButtons.LeftUp)
            {
                _isMoving = false;
            }
            else if ( /*isIn &&*/ (e.Buttons & MouseButtons.LeftDown) == MouseButtons.LeftDown)
            {
                if (isIn)
                {
                    var startPos = new Vector2(DrawingStartPosition.Value.X, DrawingStartPosition.Value.Y);
                    _drawMousePosition = e.Position - startPos;
                    _isMoving = true;
                }

                size = new RectangleF(DrawingStartPosition.Value.X, DrawingStartPosition.Value.Y + _mesText.Y,
                    _mesText.X, _mesText.Y * 5);
                isIn = size.Contains(e.Position);
                if (isIn && !_settings.PushLaneOnMouse)
                    foreach (var button in _buttons)
                    {
                        size = new RectangleF(DrawingStartPosition.Value.X,
                            DrawingStartPosition.Value.Y + _mesText.Y * button.Id,
                            _mesText.X, _mesText.Y);
                        isIn = size.Contains(e.Position);
                        button.Active = isIn;
                        if (isIn)
                            foreach (var unit in _settings.Main.Updater.Units)
                                if (unit.IsValid /*&& unit.IsAlive*/)
                                    (unit.OrderManager.CurrentOrder as AutoPushingOrder)?.UpdateLane();
                    }
            }
        }
    }
}