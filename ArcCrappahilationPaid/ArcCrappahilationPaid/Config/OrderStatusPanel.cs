using System;
using System.Collections.Generic;
using System.ComponentModel;
using Ensage;
using Ensage.SDK.Helpers;
using Ensage.SDK.Input;
using Ensage.SDK.Menu;
using Ensage.SDK.Menu.Items;
using Ensage.SDK.Renderer;
using Newtonsoft.Json;
using SharpDX;
using Color = System.Drawing.Color;

namespace ArcCrappahilationPaid.Config
{
    public class OrderStatusPanel
    {
        [JsonIgnore]
        private readonly OrderConfig _settings;
        [JsonIgnore]
        private readonly IRenderManager _render;
        [JsonIgnore]
        private readonly IInputManager _input;
        [JsonIgnore]
        private static string PanelName => "OrderStatus Panel";
        public OrderStatusPanel(OrderConfig settings)
        {
            _settings = settings;
            _render = _settings.Main.Context.RenderManager;
            _input = _settings.Main.Context.Input;
            DrawingStartPosition = Position;
            
            _mesText = _render.MeasureText(PanelName, TextSize.Value);
            TextSize.ValueChanging += (sender, args) =>
            {
                _mesText = _render.MeasureText(PanelName, args.Value);
            };

            var list = new List<HeroId>();

            foreach (var hero in EntityManager<Hero>.Entities)
            {
                if (list.Contains(hero.HeroId))
                    continue;
                list.Add(hero.HeroId);
                _settings.Main.Context.TextureManager.LoadHeroFromDota(hero.HeroId);
            }

            EntityManager<Hero>.EntityAdded += (sender, hero) =>
            {
                if (list.Contains(hero.HeroId))
                    return;
                list.Add(hero.HeroId);
                _settings.Main.Context.TextureManager.LoadHeroFromDota(hero.HeroId);
            };
        }

        [JsonIgnore]
        public bool IsActive { get; set; }
        
        [JsonIgnore]
        public Slider<Vector2> DrawingStartPosition { get; set; }

        public void Activate()
        {
            if (IsActive)
                return;
            IsActive = true;
            _input.MouseClick += InputOnMouseClick;
            _input.MouseMove += InputOnMouseMove;
            _render.Draw += RenderOnDraw;
        }


        private void RenderOnDraw(IRenderer renderer)
        {
            var rect = new RectangleF(DrawingStartPosition.Value.X, DrawingStartPosition.Value.Y,
                _mesText.X, _mesText.Y);
            renderer.DrawFilledRectangle(rect, Color.FromArgb(50, 0, 0, 0), Color.White);
            renderer.DrawText(rect, PanelName, Color.White, fontSize: TextSize.Value);
            if (Type.SelectedIndex == 0)
            {
                rect.Y += _mesText.Y;
                DrawStatus(renderer,rect, $"Main [{_settings.MainComboHotkey.Hotkey.Key}]", _settings.MainComboTarget, _settings.MainComboIsActive);
                rect.Y += _mesText.Y;
                DrawStatus(renderer, rect, $"Tempest [{_settings.TempestComboHotkey.Hotkey.Key}]", _settings.TempestComboTarget, _settings.TempestComboIsActive);
                rect.Y += _mesText.Y;
                DrawStatus(renderer, rect, $"Pushing [{_settings.AutoPushHotkey.Hotkey.Key}]", _settings.AutoPushingIsActive);
            }
            else
            {
                DrawBigStatus(renderer,ref rect, $"Main [{_settings.MainComboHotkey.Hotkey.Key}]", _settings.MainComboTarget, _settings.MainComboIsActive);
                DrawBigStatus(renderer,ref rect, $"Tempest [{_settings.TempestComboHotkey.Hotkey.Key}]", _settings.TempestComboTarget, _settings.TempestComboIsActive);
            }
        }

        private void DrawStatus(IRenderer renderer, RectangleF rect, string name, Unit target, bool status)
        {
            renderer.DrawFilledRectangle(rect, Color.FromArgb(50, 0, 0, 0), Color.White);
            var targetName = (target as Hero)?.HeroId.ToString();
            var textSize = _render.MeasureText(name, TextSize.Value);

            var newRect = new RectangleF(rect.X + textSize.X + 8, rect.Y, textSize.Y - 2, textSize.Y - 2);
            if (target != null)
            {
                renderer.DrawText(rect, $"{name}: ", Color.AliceBlue, fontSize: TextSize.Value);
                if (target is Hero)
                {
                    try
                    {
                        renderer.DrawTexture(targetName, newRect);
                    }
                    catch (Exception)
                    {
                        newRect.Y += 2;
                        newRect.Top -= 4;
                        renderer.DrawFilledRectangle(newRect, Color.Red, Color.Red);
                    }
                }
                else
                {
                    newRect.Y += 2;
                    newRect.Top -= 4;
                    renderer.DrawFilledRectangle(newRect, Color.Red, Color.Red);
                }
            }
            else
            {
                var textStatus = status ? "[+]" : "[-]";
                renderer.DrawText(rect, $"{name}: {textStatus}", Color.Black, fontSize: TextSize.Value);
            }
        }

        private void DrawBigStatus(IRenderer renderer, ref RectangleF rect, string name, Unit target, bool status)
        {
            if (!status)
                return;
            rect.Y += _mesText.Y;
            
            var targetName = (target as Hero)?.HeroId.ToString();
            var textSize = renderer.MeasureText(name, TextSize.Value);
            var filledRect = new RectangleF(rect.X, rect.Y, rect.Width, textSize.Y * 4);
            renderer.DrawFilledRectangle(filledRect, Color.FromArgb(100, 0, 0, 0),Color.White);

            if (target != null)
            {
                var newRect = new RectangleF(rect.X, rect.Y + textSize.Y, textSize.Y * 3, textSize.Y * 3);
                renderer.DrawText(rect, $"{name}", Color.AliceBlue, fontSize: TextSize.Value);
                if (target is Hero)
                {
                    try
                    {
                        renderer.DrawTexture(targetName, newRect);
                    }
                    catch (Exception)
                    {
                        newRect.Y += 2;
                        newRect.Top -= 4;
                        renderer.DrawFilledRectangle(newRect, Color.Red, Color.Red);
                    }
                }
                else
                {
                    newRect.Y += 2;
                    newRect.Top -= 4;
                    renderer.DrawFilledRectangle(newRect, Color.Red, Color.Red);
                }
            }
            else
            {
                renderer.DrawText(rect, $"{name}", Color.AliceBlue, fontSize: TextSize.Value);
            }
            rect.Y += _mesText.Y * 3;
        }


        private void DrawStatus(IRenderer renderer, RectangleF rect, string name, bool status)
        {
            renderer.DrawFilledRectangle(rect, Color.FromArgb(100, 0, 0, 0),Color.White);
            var statusText = status ? "[+]" : "[-]";
            renderer.DrawText(rect, $"{name}: {statusText}", Color.AliceBlue, fontSize: TextSize.Value);
        }

        public void Deactivate()
        {
            if (!IsActive)
                return;
            IsActive = false;
            _input.MouseClick -= InputOnMouseClick;
            _input.MouseMove -= InputOnMouseMove;
            _render.Draw -= RenderOnDraw;
        }
        [JsonIgnore]
        private bool _isMoving;
        [JsonIgnore]
        private Vector2 _drawMousePosition;
        //[JsonIgnore]
        private bool _panel;
        [JsonIgnore]
        private Vector2 _mesText;

        private void InputOnMouseMove(object sender, MouseEventArgs e)
        {
            if (_isMoving)
            {
                var newValue = new Vector2(e.Position.X - _drawMousePosition.X,
                    e.Position.Y - _drawMousePosition.Y);
                newValue.X = Math.Max(Position.MinValue.X, Math.Min(Position.MaxValue.X, newValue.X));
                newValue.Y = Math.Max(Position.MinValue.Y, Math.Min(Position.MaxValue.Y, newValue.Y));
                DrawingStartPosition.Value = newValue;
            }
        }

        private void InputOnMouseClick(object sender, MouseEventArgs e)
        {
            if ((e.Buttons & MouseButtons.Left) == 0)
            {
                return;
            }

            var size = new RectangleF(DrawingStartPosition.Value.X, DrawingStartPosition.Value.Y,
                _mesText.X, _mesText.Y);
            var isIn = size.Contains(e.Position);
            if ((e.Buttons & MouseButtons.LeftUp) == MouseButtons.LeftUp)
            {
                _isMoving = false;
            }
            else if (isIn && (e.Buttons & MouseButtons.LeftDown) == MouseButtons.LeftDown)
            {
                var startPos = new Vector2(DrawingStartPosition.Value.X, DrawingStartPosition.Value.Y);
                _drawMousePosition = e.Position - startPos;
                _isMoving = true;
            }
        }

        [Item("Show Order Status Panel")]
        [DefaultValue(true)]
        public bool Panel
        {
            get => _panel;
            set
            {
                if (_panel != value)
                {
                    if (value)
                        Activate();
                    else
                        Deactivate();
                }
                _panel = value;
            }
        }

        [Item("Panel Position")]
        public Slider<Vector2> Position { get; set; } = new Slider<Vector2>(new Vector2(300, 300), new Vector2(0, 0),
            new Vector2(Drawing.Width - 10, Drawing.Height - 10));


        [Item("Text Size")]
        public Slider<float> TextSize { get; set; } = new Slider<float>(30, 1, 100);

        [Item("Design type")]
        public Selection<string> Type { get; set; } = new Selection<string>(0, "type 1", "type 2");
    }
}