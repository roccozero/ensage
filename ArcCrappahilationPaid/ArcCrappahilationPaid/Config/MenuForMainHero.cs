using System.ComponentModel;
using Ensage;
using Ensage.SDK.Menu;
using Ensage.SDK.Menu.Items;
using Ensage.SDK.Renderer;

namespace ArcCrappahilationPaid.Config
{
    public class MenuForMainHero
    {
        public MenuForMainHero(ITextureManager contextTextureManager)
        {

            contextTextureManager.LoadAbilityFromDota(AbilityId.arc_warden_flux);
                contextTextureManager.LoadAbilityFromDota(AbilityId.arc_warden_magnetic_field);
                    contextTextureManager.LoadAbilityFromDota(AbilityId.arc_warden_spark_wraith);
            contextTextureManager.LoadAbilityFromDota(AbilityId.arc_warden_tempest_double);

            contextTextureManager.LoadAbilityFromDota(AbilityId.item_blink);

            ItemPriorityChanger = new PriorityChanger {Selectable = true};

            /*UpdateManager.BeginInvoke(() =>
            {
                AbilitySelection.AddImage(false, AbilityId.arc_warden_tempest_double.ToString());
            }, 2500);*/
        }

        [Item("Ability Selection")]
        public ImageToggler AbilitySelection { get; set; } = new ImageToggler(true,
            AbilityId.arc_warden_flux.ToString(),
            AbilityId.arc_warden_magnetic_field.ToString(),
            AbilityId.arc_warden_spark_wraith.ToString(),
            AbilityId.arc_warden_tempest_double.ToString());

        /*[Item("Item Selection")]
        public ImageToggler ItemSelection { get; set; } = new ImageToggler(true,
            AbilityId.item_blink.ToString());*/

        [Item("Use priority")]
        [DefaultValue(true)]
        public bool CustomComboPriorityHero { get; set; }

        [Item("Item Selection")] public PriorityChanger ItemPriorityChanger { get; set; }

        /*[Item("Item Selection2")]
        public PriorityChanger ItemPriorityChanger2 { get; set; } =
            new PriorityChanger(true, AbilityId.item_blink.ToString());*/

        /*[Item("Lol kek lmao")]
        public Selection<AbilityId> OurSelection { get; set; } = new Selection<AbilityId>(0, AbilityId.arc_warden_flux,
            AbilityId.arc_warden_magnetic_field, AbilityId.arc_warden_spark_wraith,
            AbilityId.arc_warden_tempest_double);

        [Item("Lol kek lmaa")]
        public Selection<string> OurSelection2 { get; set; } = new Selection<string>(0, "arc_warden_flux", "arc_warden_tempest_double");*/
    }
}