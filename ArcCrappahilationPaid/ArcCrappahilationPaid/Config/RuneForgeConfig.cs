using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Ensage;
using Ensage.SDK.Extensions;
using Ensage.SDK.Helpers;
using Ensage.SDK.Menu;
using Ensage.SDK.Menu.Items;
using Newtonsoft.Json;
using AbilityExtensions = Ensage.Common.Extensions.AbilityExtensions;

namespace ArcCrappahilationPaid.Config
{
    public class RuneForgeConfig
    {
        [JsonIgnore] private readonly BaseMenu _baseMenu;
        [JsonIgnore] private bool _mainSnatcher;
        [JsonIgnore] private bool _tempestSnatcher;

        public RuneForgeConfig(BaseMenu baseMenu)
        {
            _baseMenu = baseMenu;
            try
            {
                var mainAbility =
                    Main.Spellbook.Spells.FirstOrDefault(x => x.NetworkName == "CDOTA_Ability_ArcWarden_Scepter");
                UpdateManager.BeginInvoke(async () =>
                {
                    while (true)
                    {
                        var mode = (Modes) Selection.SelectedIndex;
                        if ((mode == Modes.Both || mode == Modes.Main) && mainAbility != null && Main.IsAlive &&
                            !Main.IsInvisible() && mainAbility.Level > 0 &&
                            AbilityExtensions.CanBeCasted(mainAbility)) mainAbility.UseAbility();

                        if ((mode == Modes.Both || mode == Modes.Tempest) && Tempest != null && Tempest.IsValid &&
                            Tempest.IsAlive && !Tempest.IsInvisible())
                        {
                            var ability = GetAbility;
                            if (ability != null && ability.Level > 0 && AbilityExtensions.CanBeCasted(ability))
                                ability.UseAbility();
                        }

                        await Task.Delay(300);
                    }
                });
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        [JsonIgnore]
        private Ability GetAbility => Tempest.Spellbook.Spells.FirstOrDefault(x =>
            x.NetworkName == "CDOTA_Ability_ArcWarden_Scepter");

        [Item("RuneForge cast mode")]
        public Selection<string> Selection { get; set; } =
            new Selection<string>("Cast on rdy [both]", "Cast on rdy [tempest]",
                "Cast on rdy [main]" /*, "Cast on summon"*/,
                "Dont cast");

        [Item("Main snatcher")]
        [DefaultValue(true)]
        public bool MainSnatcher
        {
            get => _mainSnatcher;
            set
            {
                if (_mainSnatcher != value)
                    UpdateManager.BeginInvoke(() =>
                    {
                        if (value)
                            UpdateManager.Subscribe(MainSnatcherAction, 10);
                        else
                            UpdateManager.Unsubscribe(MainSnatcherAction);
                    }, 100);

                _mainSnatcher = value;
            }
        }

        [JsonIgnore] private Hero Tempest => _baseMenu.Main.Updater.TempestHero?.Owner as Hero;

        [JsonIgnore] private Hero Main => _baseMenu.Main.Context.Owner as Hero;

        [Item("Tempest snatcher")]
        [DefaultValue(true)]
        public bool TempestSnatcher
        {
            get => _tempestSnatcher;
            set
            {
                if (_tempestSnatcher != value)
                    UpdateManager.BeginInvoke(() =>
                    {
                        if (value)
                            UpdateManager.Subscribe(TempestSnatcherAction, 10);
                        else
                            UpdateManager.Unsubscribe(TempestSnatcherAction);
                    }, 100);

                _tempestSnatcher = value;
            }
        }

        private void MainSnatcherAction()
        {
            var me = Main;
            var rune = EntityManager<Rune>.Entities.Where(x => x.IsValid && x.IsInRange(me, 210))
                .OrderBy(z => z.Distance2D(me)).FirstOrDefault();
            if (rune == null) return;
            if (me != null) me.PickUpRune(rune);
        }

        private void TempestSnatcherAction()
        {
            var me = Tempest;
            if (me == null) return;
            var rune = EntityManager<Rune>.Entities.Where(x => x.IsValid && x.IsInRange(me, 210))
                .OrderBy(z => z.Distance2D(me)).FirstOrDefault();
            if (rune == null) return;
            me.PickUpRune(rune);
        }

        private enum Modes
        {
            Both,
            Tempest,
            Main,
            OnlySummon,
            DontCast
        }
    }
}