﻿using System.ComponentModel;
using Ensage;
using Ensage.SDK.Helpers;
using Ensage.SDK.Menu;
using Ensage.SDK.Menu.Items;
using Ensage.SDK.Service;
using Newtonsoft.Json;

namespace ArcCrappahilationPaid.Config
{
    [Menu("Arc Crappahilation") /*, TextureDota("npc_dota_hero_arc_warden", @"resource\flash3\images\heroes_horizontal\npc_dota_hero_arc_warden.png")*/]
    public class BaseMenu
    {
        [JsonIgnore] public readonly ArcCrappahilation Main;

        [JsonIgnore] private bool _blocker;

        public BaseMenu(ArcCrappahilation main)
        {
            Main = main;
            //TextureHelper.LoadHeroTexture(HeroId.npc_dota_hero_arc_warden);
            Context = main.Context;
            MenuForMainHero = new MenuForMainHero(main.Context.TextureManager);
            MenuForTempestHero = new MenuForTempestHero(main.Context.TextureManager);
            MenuForBlink = new MenuForBlink();
            MagneticFieldConfig = new MagneticFieldConfig();
            SmartSpark = new SmartSpark();
            OrderConfig = new OrderConfig(main);
            AutoPushingSettings = new AutoPushingSettings(main);
            ItemCooldownHelper = new ItemCooldownHelper(main);
            AutoMidasConfig = new AutoMidasConfig(this);
            RuneForge = new RuneForgeConfig(this);
            DebugConfig = new DebugConfig(this);
            OrbwalkerConfig = new OrbwalkerConfig(this);

            /*MinDistance.ValueChanging += (sender, args) =>
            {
                if (args.Value <= 0)
                {
                    
                }
            };*/
        }

        [JsonIgnore] public IServiceContext Context { get; }

        [Menu("RuneForge usage")] public RuneForgeConfig RuneForge { get; set; }

        /*[Item("Tost")]
        public bool Tost { get; set; }*/

        [Menu("Hotkeys")] public OrderConfig OrderConfig { get; }
        [Menu("Orbwalker Config")] public OrbwalkerConfig OrbwalkerConfig { get; }

        [Menu("Main hero settings")] public MenuForMainHero MenuForMainHero { get; set; }

        [Menu("Tempest hero settings")] public MenuForTempestHero MenuForTempestHero { get; }

        [Menu("Dagger settings")] public MenuForBlink MenuForBlink { get; }

        [Menu("SmartSpark")] public SmartSpark SmartSpark { get; }

        [Menu("AutoPushing Settings")] public AutoPushingSettings AutoPushingSettings { get; }

        [Menu("Item cooldown helper Panel")] public ItemCooldownHelper ItemCooldownHelper { get; }

        [Menu("Auto Midas")] public AutoMidasConfig AutoMidasConfig { get; }

        [Menu("Magnetic Field settings")] public MagneticFieldConfig MagneticFieldConfig { get; }

        [Menu("Debug")] public DebugConfig DebugConfig { get; }

        [Item("Block player inputs for KeyBinds")]
        [Tooltip("If u change hotkeys, need to restart script")]
        public bool Blocker
        {
            get => _blocker;
            set
            {
                if (value != _blocker)
                    UpdateManager.BeginInvoke(() => { Main.InputBlocker.Toggle(value); }, 2000);

                _blocker = value;
            }
        }

        [Item("Orbwalker min distance")] public Slider<float> MinDistance { get; set; } = new Slider<float>(0, 0, 700);

        [Item("Get back on close distance")] public bool GetBack { get; set; } = true;

        [Item("Auto linker from tempest to main hero")]
        public bool AutoLinken { get; set; }

        [Item("ForceStaff/Pike cast range")]
        public Slider<float> PikeForceCastRange { get; set; } = new Slider<float>(800, 1000, 2000);

        [Item("Use travels in combo")] public bool UseTravelsInCombo { get; set; }

        [Item("Range for AutoTravels in combo")]
        public Slider<float> RangeForTravels { get; set; } = new Slider<float>(3500, 2000, 5000);

        [Item("Dont use orchid/bloodthorn before nullifier hit")]
        public bool DontUseBeforeNul { get; set; }

        [Item("Save nullifier for aeon break")]
        [DefaultValue(true)]
        public bool SaveNulForAeuon { get; set; }

        [Item("Dont use nullifier on hexed targets")]
        [DefaultValue(true)]
        public bool DoneUseNulOnHexedUnits { get; set; }

        public void AddItemsToMenu(AbilityId id)
        {
            ArcCrappahilation.Log.Debug($"try to add to menu: {id}");
            Main.Context.TextureManager.LoadAbilityFromDota(id);
            var idString = id.ToString();
            var toMainState = MenuForMainHero.ItemPriorityChanger.AddImage(true, idString);
            var toTempestState = MenuForTempestHero.ItemPriorityChanger.AddImage(true, idString);

            ArcCrappahilation.Log.Debug($"added to menu: {id} [{toMainState}] [{toTempestState}]");
        }

        public void RemoveItemsFromMenu(AbilityId id)
        {
            ArcCrappahilation.Log.Debug($"try to remove from menu: {id}");
            var idString = id.ToString();
            var toMainState = MenuForMainHero.ItemPriorityChanger.RemoveImage(idString);
            var toTempestState = MenuForTempestHero.ItemPriorityChanger.RemoveImage(idString);

            ArcCrappahilation.Log.Debug($"removed from menu: {id} [{toMainState}] [{toTempestState}]");
        }
    }
}