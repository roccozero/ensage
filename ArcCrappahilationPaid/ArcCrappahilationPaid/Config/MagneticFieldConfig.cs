using System.ComponentModel;
using System.Windows.Input;
using Ensage.SDK.Menu;
using Ensage.SDK.Menu.Attributes;
using Ensage.SDK.Menu.Items;

namespace ArcCrappahilationPaid.Config
{
    public class MagneticFieldConfig
    {
        public MagneticFieldConfig()
        {
            MainComboHotkey.Action += args =>
            {
                UseInFont = !UseInFont;
            };
        }

        [Item("[Magnetic Field] In front")]
        [Tooltip("Use Magnetic Field in front of ur hero")]
        [DefaultValue(true)]
        [PermaShow]
        public bool UseInFont { get; set; }

        [Item("Toggle key")]
        public HotkeySelector MainComboHotkey { get; set; } = new HotkeySelector(Key.None);

        [Item("Range for ignoring front settings")]
        [Tooltip("Use Magnetic Field in front of ur hero if distance > selected range")]
        public Slider<float> RangeSlider { get; set; } = new Slider<float>(300, 0, 1000);
    }
}