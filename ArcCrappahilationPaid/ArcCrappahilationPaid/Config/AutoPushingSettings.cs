using System.ComponentModel;
using Ensage.SDK.Menu;
using Ensage.SDK.Menu.Items;
using Newtonsoft.Json;

namespace ArcCrappahilationPaid.Config
{
    public class AutoPushingSettings
    {
        [JsonIgnore]
        public ArcCrappahilation Main { get; }


        public AutoPushingSettings(ArcCrappahilation main)
        {
            Main = main;
            AutoPushingPanel = new AutoPushingPanel(this);
            Slider.ValueChanging += (sender, args) =>
            {
                if (args.Value <= 0)
                {
                    if (IsEnable)
                        IsEnable = false;
                }
                else
                {
                    if (!IsEnable)
                        IsEnable = true;
                }
            };
        }

        [Item("Auto hero-targetting")]
        [Tooltip("Will attack all heroes in range")]
        [DefaultValue(false)]
        public bool IsEnable { get; set; }

        [Item("Range")]
        public Slider<int> Slider { get; set; } = new Slider<int>(1000, 0, 2000);

        [Item("Use travels")]
        [DefaultValue(true)]
        public bool UseTravels { get; set; }

        [Item("Check for creeps before travels")]
        [DefaultValue(true)]
        public bool CheckForCreeps { get; set; }

        [Item("Push lane under mouse")]
        [Tooltip("On push hotkey, units will push lane under ur mouse")]
        [DefaultValue(false)]
        public bool PushLaneOnMouse { get; set; }

        [Item("Summon tempest on order")]
        [DefaultValue(false)]
        public bool SummonTempest { get; set; }

        [Menu("Panel")]
        public AutoPushingPanel AutoPushingPanel { get; set; }

        [Item("Only one tp usages per order")]
        [DefaultValue(false)]
        public bool OnlyOneTpPerOrder { get; set; }

        [Item("Push only Towers/barracks")]
        [DefaultValue(false)]
        public bool AttackOnlyTowers { get; set; }

        [Item("Escape on low hp")]
        [DefaultValue(false)]
        public bool AutoEscape { get; set; }

        [Item("Hp % for escape")]
        public Slider<float> AutoEscapeHealthPercant { get; set; } = new Slider<float>(25, 1, 100);

        [Item("Use Magnetic Field on ancients")]
        [DefaultValue(true)]
        public bool UseMagneticField { get; set; }

        [Item("Use orbwalker min distance")]
        [DefaultValue(false)]
        public bool UseOrbWalkerDistance { get; set; }

        [Item("Orbwalker min distance [only for autopushing]")]
        public Slider<float> MinDistance { get; set; } = new Slider<float>(0, 0, 700);

        [Item("Check for life time")]
        [Tooltip("Tempest will not do Tp, if lifetime of tempest is less then 9 secs")]
        [DefaultValue(false)]
        public bool CheckForLifeTime { get; set; }

        [Item("Ignore creeps on non selected lane")]
        [Tooltip("e.g. Will not attack creeps in jungle/top/mid if selected lane is bot")]
        [DefaultValue(true)]
        public bool IgnoreCreepsOnNotSelectedLane { get; set; }

        [Item("Mjolnir mode")]
        public Selection<string> Selection { get; set; } = new Selection<string>("Use on creeps while pushing", "Use only in last few seconds before dying");

        [Item("Lane selector behaviour")]
        public Selection<string> LaneSelectorBehaviour { get; set; } = new Selection<string>("Back to def value when tempest is dying", "Do nothing");
    }
}