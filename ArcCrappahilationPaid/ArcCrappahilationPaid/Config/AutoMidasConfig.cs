using System.ComponentModel;
using Ensage.SDK.Helpers;
using Ensage.SDK.Menu;
using Newtonsoft.Json;

namespace ArcCrappahilationPaid.Config
{
    public class AutoMidasConfig
    {
        [JsonIgnore]
        private readonly BaseMenu _baseMenu;
        [JsonIgnore]
        private bool _isEnable;

        public AutoMidasConfig(BaseMenu baseMenu)
        {
            _baseMenu = baseMenu;
        }

        [Item("Enable")]
        [DefaultValue(true)]
        public bool IsEnable
        {
            get => _isEnable;
            set
            {
                if (_isEnable != value)
                {
                    UpdateManager.BeginInvoke(() =>
                    {
                        if (value)
                            UpdateManager.Subscribe(_baseMenu.Main.AutoMidas.MidasChecker, 250);
                        else
                            UpdateManager.Unsubscribe(_baseMenu.Main.AutoMidas.MidasChecker);
                    }, 100);
                }
                _isEnable = value;
            }
        }

        [Item("Use delay for midas")]
        [DefaultValue(false)]
        public bool IsDelay { get; set; }
    }
}