using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using ArcCrappahilationPaid.Orders;
using ArcCrappahilationPaid.Units;
using Ensage;
using Ensage.Common.Extensions;
using Ensage.SDK.Helpers;
using Ensage.SDK.Menu;
using Ensage.SDK.Menu.Items;
using Newtonsoft.Json;
using SharpDX;
using UnitExtensions = Ensage.SDK.Extensions.UnitExtensions;

namespace ArcCrappahilationPaid.Config
{
    public class OrderConfig
    {
        [JsonIgnore] public readonly ArcCrappahilation Main;

        [JsonIgnore] public bool AutoPushingIsActive;

        [JsonIgnore] public bool MainComboIsActive;

        [JsonIgnore] public Unit MainComboTarget;

        [JsonIgnore] public TargetManager MainTargetManager;

        [JsonIgnore] public bool SpamComboIsActive;

        [JsonIgnore] public bool TempestComboIsActive;

        [JsonIgnore] public Unit TempestComboTarget;

        [JsonIgnore] public TargetManager TempestTargetManager;

        public OrderConfig(ArcCrappahilation main)
        {
            Main = main;

            OrderStatusPanel = new OrderStatusPanel(this);
            MainComboHotkey.Action += async args =>
            {
                if (args.Flag == HotkeyFlags.Down)
                {
                    if (MainComboIsActive)
                        return;
                    MainComboIsActive = true;
                    UpdateManager.BeginInvoke(async () =>
                    {
                        while (MainComboIsActive)
                        {
                            try
                            {
                                if (MainComboTarget == null || !MainComboTarget.IsValid)
                                {
                                    var me = main.Updater.Units.First(x => x is MainHeroUnit);
                                    me.MovementManager.Orbwalk();
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e);
                            }

                            await Task.Delay(125);
                        }
                    });
                    MainTargetManager?.Deactivate();
                    MainComboTarget = null;
                    MainComboTarget = await GetTarget();
                    if (!MainComboIsActive)
                    {
                        MainComboTarget = null;
                        return;
                    }

                    if (MainComboTarget == null)
                    {
                        MainTargetManager?.Deactivate();
                        return;
                    }

                    MainTargetManager?.Activate(MainComboTarget);
                    //main.Context.Particle.DrawRange(MainComboTarget, "MainCombo", 150, Color.Red);

                    ArcCrappahilation.Log.Info($"[MainCombo] Target found -> {MainComboTarget.Name}");

                    foreach (var unit in main.Updater.Units)
                    {
                        if (!UnitExtensions.IsInRange(unit.Owner, MainComboTarget, 2500))
                            continue;
                        if (unit.OrderManager.CurrentOrderId != OrderManager.Orders.Combo)
                        {
                            if (!unit.IsAlive)
                                continue;
                            if (unit is TempestHeroUnit)
                            {
                                if (TempestTargetManager == null)
                                    TempestTargetManager = new TargetManager(main, unit);
                                if (unit.OrderManager.CurrentOrderId == OrderManager.Orders.Tempest)
                                    continue;
                            }
                            else if (unit is MainHeroUnit)
                            {
                                if (MainTargetManager == null)
                                    MainTargetManager = new TargetManager(main, unit);
                            }

                            unit.OrderManager.SetOrder(new ComboOrder(unit).SetTarget(MainComboTarget));
                        }
                    }


                    UpdateManager.BeginInvoke(async () =>
                    {
                        while (MainComboIsActive && MainComboTarget != null && MainComboTarget.IsValid)
                        {
                            try
                            {
                                if (MainComboTarget == null || !MainComboTarget.IsValid)
                                {
                                    MainTargetManager?.Deactivate();
                                    MainComboTarget = null;
                                    continue;
                                }

                                foreach (var unit in main.Updater.Units)
                                {
                                    if (unit == null || !unit.IsValid || !unit.IsAlive)
                                        continue;
                                    if (!UnitExtensions.IsInRange(unit.Owner, MainComboTarget, 2500))
                                        continue;
                                    if (unit.OrderManager.CurrentOrderId != OrderManager.Orders.Combo)
                                    {
                                        if (unit is TempestHeroUnit)
                                            if (unit.OrderManager.CurrentOrderId == OrderManager.Orders.Tempest)
                                                continue;
                                        unit.OrderManager.SetOrder(new ComboOrder(unit).SetTarget(MainComboTarget));
                                    }
                                }
                            }
                            catch (InvalidOperationException)
                            {
                            }
                            catch (Exception e)
                            {
                                ArcCrappahilation.Log.Debug(e);
                            }

                            await Task.Delay(250);
                        }
                    });


                    UpdateManager.BeginInvoke(async () =>
                    {
                        while (MainComboIsActive && MainComboTarget != null && MainComboTarget.IsValid)
                        {
                            var unitsInCombo = Math.Max(0, main.Updater.Units.Count - 2);
                            try
                            {
                                var changed = false;
                                if (MainComboTarget == null || !MainComboTarget.IsAlive)
                                {
                                    MainTargetManager?.Deactivate();
                                    MainComboTarget = null;
                                    MainComboTarget = await GetTarget();
                                    if (MainComboTarget != null) MainTargetManager?.Activate(MainComboTarget);
                                    changed = true;
                                }

                                foreach (var unit in main.Updater.Units)
                                    if (unit.OrderManager.CurrentOrderId == OrderManager.Orders.Combo)
                                    {
                                        unit.OrderManager.CurrentOrder.Execute(MainComboTarget);
                                        if (changed)
                                            (unit.OrderManager.CurrentOrder as ComboOrder)?.SetTarget(MainComboTarget);
                                        await Task.Delay(25 + unitsInCombo * 50);
                                    }
                            }
                            catch (InvalidOperationException)
                            {
                                //ignore
                            }
                            catch (Exception e)
                            {
                                ArcCrappahilation.Log.Error(e);
                            }

                            await Task.Delay(20 + unitsInCombo * 20);
                        }
                    });
                }
                else if (args.Flag == HotkeyFlags.Up)
                {
                    if (!MainComboIsActive)
                        return;
                    MainComboIsActive = false;
                    MainComboTarget = null;
                    MainTargetManager.Deactivate();
                    //main.Context.Particle.Remove("MainCombo");
                    foreach (var unit in main.Updater.Units)
                        if (unit.OrderManager.CurrentOrderId == OrderManager.Orders.Combo)
                        {
                            unit.OrderManager.FlushOrder();
                            unit.MovementManager.Orbwalker.OrbwalkingPoint = Vector3.Zero;
                        }
                }
                else
                {
                    ArcCrappahilation.Log.Warn($"On shiiiet [{args.Flag}]");
                }
            };

            /*SuperComboKey.Action += async args =>
            {
                if (args.Flag == HotkeyFlags.Down)
                {

                }
                else
                {
                    
                }
            };*/

            TempestComboHotkey.Action += TempestAction;

            /*var render = main.Context.Renderer;
            render.Draw += (sender, args) =>
            {
                render.DrawText(new Vector2(200, 200),
                    $"Main: [ {(!MainComboIsActive ? MainComboIsActive.ToString() : MainComboTarget?.Name)}] " +
                    $"Tempest: [{(!TempestComboIsActive ? TempestComboIsActive.ToString() : TempestComboTarget?.Name)}] " +
                    $"Pushing: [{AutoPushingIsActive}]",
                    System.Drawing.Color.LawnGreen, 20);
                
            };*/

            SpamHotkey.Action += args =>
            {
                switch (args.Flag)
                {
                    case HotkeyFlags.Down:
                        if (SpamComboIsActive)
                            return;
                        SpamComboIsActive = true;
                        UpdateManager.BeginInvoke(async () =>
                        {
                            var mousePos = Game.MousePosition;
                            while (SpamComboIsActive)
                            {
                                foreach (var unit in main.Updater.Units)
                                    if (unit is MainHeroUnit || unit is TempestHeroUnit)
                                    {
                                        var spark = (unit.Owner as Hero)?.GetAbilityById(AbilityId
                                            .arc_warden_spark_wraith);
                                        if (spark != null && spark.CanBeCasted()) spark.UseAbility(mousePos);
                                    }

                                await Task.Delay(500);
                            }
                        });
                        break;
                    case HotkeyFlags.Up:
                        if (!SpamComboIsActive)
                            return;
                        SpamComboIsActive = false;
                        break;
                }
            };

            AutoPushHotkey.Action += AutoPushingAction;
        }

        [Item]
        public HotkeySelector MainComboHotkey { get; set; } =
            new HotkeySelector(Key.None, HotkeyFlags.Up | HotkeyFlags.Down);

        /*[Item]
        [Tooltip("Will do combo with main+tempest and disable all others combos")]
        public HotkeySelector SuperComboKey { get; set; } = new HotkeySelector(Key.None, HotkeyFlags.Up | HotkeyFlags.Down);*/

        [Item] public HotkeySelector TempestComboHotkey { get; set; } = new HotkeySelector(Key.None, HotkeyFlags.Down);

        [Item("Summon tempest on tempest combo key")]
        [DefaultValue(false)]
        public bool SummonTempest { get; set; }

        [Item]
        public HotkeySelector SpamHotkey { get; set; } =
            new HotkeySelector(Key.None, HotkeyFlags.Up | HotkeyFlags.Down);

        [Item] public HotkeySelector AutoPushHotkey { get; set; } = new HotkeySelector(Key.None, HotkeyFlags.Down);

        [Menu("Order status panel")] public OrderStatusPanel OrderStatusPanel { get; set; }

        [Item("Disable current order when tempest die")]
        [Tooltip("For [Tempest Combo] / [Auto Pushing]")]
        [DefaultValue(true)]
        public bool DisableOrder { get; set; }

        public void AutoPushingAction(MenuInputEventArgs menuInputEventArgs)
        {
            if (!AutoPushingIsActive)
            {
                AutoPushingIsActive = !AutoPushingIsActive;
                if (TempestComboIsActive)
                {
                    TempestComboIsActive = false;
                    TempestComboTarget = null;
                    Main.Context.Particle.Remove("TempestCombo");
                }

                if (Main.Menu.AutoPushingSettings.SummonTempest)
                {
                    var ultimate = Main.Context.Owner.GetAbilityById(AbilityId.arc_warden_tempest_double);
                    if (ultimate != null && ultimate.CanBeCasted() &&
                        !Main.Updater.Units.Any(x => x is TempestHeroUnit && x.IsAlive))
                        ultimate.UseAbility();
                }

                foreach (var unit in Main.Updater.Units)
                {
                    if (!unit.IsAlive)
                        continue;
                    if (unit.OrderManager.CurrentOrderId != OrderManager.Orders.Idle &&
                        unit.OrderManager.CurrentOrderId != OrderManager.Orders.Idle) continue;
                    if (unit is MainHeroUnit) continue;
                    var comboOrder = new AutoPushingOrder(unit, Main.LaneHelper);
                    unit.OrderManager.SetOrder(comboOrder);
                }

                UpdateManager.BeginInvoke(async () =>
                {
                    while (AutoPushingIsActive)
                    {
                        foreach (var unit in Main.Updater.Units)
                        {
                            if (!unit.IsAlive)
                                continue;
                            if (unit.OrderManager.CurrentOrderId != OrderManager.Orders.Idle &&
                                unit.OrderManager.CurrentOrderId != OrderManager.Orders.Tempest) continue;
                            if (unit is MainHeroUnit) continue;
                            var comboOrder = new AutoPushingOrder(unit, Main.LaneHelper);
                            unit.OrderManager.SetOrder(comboOrder);
                        }

                        await Task.Delay(250);
                    }
                });

                UpdateManager.BeginInvoke(async () =>
                {
                    while (AutoPushingIsActive)
                    {
                        foreach (var unit in Main.Updater.Units)
                            if (unit.OrderManager.CurrentOrderId == OrderManager.Orders.Pushing)
                                unit.OrderManager.CurrentOrder.Execute();
                        await Task.Delay(75);
                    }
                });
            }
            else
            {
                AutoPushingIsActive = !AutoPushingIsActive;
                foreach (var unit in Main.Updater.Units)
                    if (unit.OrderManager.CurrentOrderId == OrderManager.Orders.Pushing)
                    {
                        unit.OrderManager.FlushOrder();
                        unit.MovementManager.Orbwalker.OrbwalkingPoint = Vector3.Zero;
                    }

                ArcCrappahilation.Log.Error(
                    $"Will refresh Buttons: {Main.Menu.AutoPushingSettings.LaneSelectorBehaviour.SelectedIndex == 0}");
                if (Main.Menu.AutoPushingSettings.LaneSelectorBehaviour.SelectedIndex == 0)
                    Main.Menu.AutoPushingSettings.AutoPushingPanel.RefreshButtons();
            }
        }

        public async void TempestAction(MenuInputEventArgs menuInputEventArgs)
        {
            if (!TempestComboIsActive)
            {
                TempestComboIsActive = !TempestComboIsActive;
                AutoPushingIsActive = false;
                TempestComboTarget = null;
                TempestComboTarget = await GetTarget();
                if (TempestComboTarget == null)
                {
                    TempestTargetManager?.Deactivate();
                    return;
                }

                TempestTargetManager?.Activate(TempestComboTarget);
                //main.Context.Particle.DrawRange(TempestComboTarget, "TempestCombo", 150, Color.Purple);
                ArcCrappahilation.Log.Info($"[TempestCombo] Target found -> {TempestComboTarget.Name}");
                var tempestUnit = Main.Updater.Units.FirstOrDefault(x => x is TempestHeroUnit);
                if (tempestUnit != null)
                {
                    if (SummonTempest && !tempestUnit.IsAlive)
                    {
                        var tempestAbility =
                            ObjectManager.LocalHero.GetAbilityById(AbilityId.arc_warden_tempest_double);
                        if (tempestAbility != null && tempestAbility.CanBeCasted())
                            tempestAbility.UseAbility();
                    }

                    try
                    {
                        foreach (var unit in Main.Updater.Units)
                        {
                            if (!unit.IsAlive)
                            {
                                await Task.Delay(250);
                                continue;
                            }

                            /*if (!UnitExtensions.IsInRange(unit.Owner, TempestComboTarget, 5000))
                                continue;*/
                            if (unit.OrderManager.CurrentOrderId ==
                                OrderManager.Orders.Idle ||
                                unit.OrderManager.CurrentOrderId == OrderManager.Orders.Pushing)
                            {
                                if (unit is TempestHeroUnit)
                                {
                                    if (TempestTargetManager == null)
                                        TempestTargetManager = new TargetManager(Main, unit);
                                    var comboOrder = new TempestOrder(unit);
                                    unit.OrderManager.SetOrder(comboOrder.SetTarget(TempestComboTarget));
                                }
                                else if (!(unit is MainHeroUnit) &&
                                         tempestUnit.Owner.Distance2D(unit.Owner) <= 1500)
                                {
                                    var comboOrder = new TempestOrder(unit);
                                    unit.OrderManager.SetOrder(comboOrder.SetTarget(TempestComboTarget));
                                }
                            }
                        }
                    }
                    catch (InvalidOperationException)
                    {
                    }
                    catch (Exception e)
                    {
                        ArcCrappahilation.Log.Error(e);
                    }
                }
                else
                {
                    if (SummonTempest)
                    {
                        var tempestAbility =
                            ObjectManager.LocalHero.GetAbilityById(AbilityId.arc_warden_tempest_double);
                        if (tempestAbility != null && tempestAbility.CanBeCasted())
                            tempestAbility.UseAbility();
                    }
                }


                UpdateManager.BeginInvoke(async () =>
                {
                    while (TempestComboIsActive)
                    {
                        try
                        {
                            tempestUnit = Main.Updater.Units.FirstOrDefault(x => x is TempestHeroUnit);
                            foreach (var unit in Main.Updater.Units)
                            {
                                if (unit.Owner == null)
                                    return;
                                if (!unit.IsAlive)
                                {
                                    await Task.Delay(250);
                                    continue;
                                }

                                if (TempestComboTarget == null || !TempestComboTarget.IsValid)
                                {
                                    TempestTargetManager?.Deactivate();
                                    TempestComboTarget = null;
                                    await Task.Delay(250);
                                    continue;
                                }

                                /*if (!UnitExtensions.IsInRange(unit.Owner, TempestComboTarget, 5000))
                                    continue;*/
                                if (unit.OrderManager.CurrentOrderId ==
                                    OrderManager.Orders.Idle || unit.OrderManager.CurrentOrderId ==
                                    OrderManager.Orders.Pushing)
                                {
                                    if (unit is TempestHeroUnit)
                                    {
                                        var comboOrder = new TempestOrder(unit);
                                        unit.OrderManager.SetOrder(comboOrder.SetTarget(TempestComboTarget));
                                    }
                                    else if (!(unit is MainHeroUnit) &&
                                             tempestUnit?.Owner.Distance2D(unit.Owner) <= 1500)
                                    {
                                        var comboOrder = new TempestOrder(unit);
                                        unit.OrderManager.SetOrder(comboOrder.SetTarget(TempestComboTarget));
                                    }
                                }
                            }
                        }
                        catch (InvalidOperationException)
                        {
                        }
                        catch (Exception e)
                        {
                            ArcCrappahilation.Log.Error(e);
                        }

                        await Task.Delay(250);
                    }
                });

                UpdateManager.BeginInvoke(async () =>
                {
                    while (TempestComboIsActive)
                    {
                        var unitsInCombo = Math.Max(0, Main.Updater.Units.Count - 2);
                        var changed = false;
                        if (TempestComboTarget == null || !TempestComboTarget.IsAlive)
                        {
                            TempestTargetManager?.Deactivate();
                            TempestComboTarget = null;
                            TempestComboTarget = await GetTarget();
                            if (TempestComboTarget != null) TempestTargetManager?.Activate(TempestComboTarget);
                            changed = true;
                        }

                        try
                        {
                            foreach (var unit in Main.Updater.Units)
                                if (unit.OrderManager.CurrentOrderId == OrderManager.Orders.Tempest)
                                {
                                    unit.OrderManager.CurrentOrder.Execute(TempestComboTarget);
                                    if (changed)
                                        (unit.OrderManager.CurrentOrder as TempestOrder)?.SetTarget(
                                            TempestComboTarget);
                                    await Task.Delay(50 + 25 * unitsInCombo);
                                }
                        }
                        catch (InvalidOperationException)
                        {
                        }
                        catch (Exception e)
                        {
                            ArcCrappahilation.Log.Error(e);
                        }

                        await Task.Delay(75 + 15 * unitsInCombo);
                    }
                });
            }
            else
            {
                TempestComboIsActive = !TempestComboIsActive;
                TempestTargetManager?.Deactivate();
                TempestComboTarget = null;
                //main.Context.Particle.Remove("TempestCombo");
                foreach (var unit in Main.Updater.Units)
                    if (unit.OrderManager.CurrentOrderId == OrderManager.Orders.Tempest)
                    {
                        unit.OrderManager.FlushOrder();
                        unit.MovementManager.Orbwalker.OrbwalkingPoint = Vector3.Zero;
                    }
            }
        }

        private async Task<Unit> GetTarget()
        {
            Unit target = null;
            while (target == null && (TempestComboIsActive || MainComboIsActive))
            {
                var mousePos = Game.MousePosition;
                target = EntityManager<Hero>.Entities
                    .Where(x => x.IsAlive && x.IsVisible && UnitExtensions.IsEnemy(x, Main.Context.Owner) &&
                                (!x.IsIllusion || x.HasModifier("modifier_morphling_replicate_manager")) &&
                                x.Distance2D(mousePos) <= 500)
                    .OrderBy(z => z.Distance2D(mousePos)).FirstOrDefault();
                var tempTarget =
                    EntityManager<Unit>.Entities.Where(
                            x =>
                                (x.NetworkName == "CDOTA_Unit_SpiritBear" || x.Name == "npc_dota_phoenix_sun") &&
                                x.IsAlive &&
                                x.Team == Main.Context.Owner.GetEnemyTeam() && x.Distance2D(mousePos) <= 500 &&
                                !x.IsIllusion)
                        .OrderBy(z => z.Distance2D(mousePos)).FirstOrDefault();
                if (target == null)
                    target = tempTarget;
                else if (tempTarget != null)
                    if (tempTarget.Distance2D(mousePos) < target.Distance2D(mousePos))
                        target = tempTarget;
                await Task.Delay(100);
            }

            return target;
        }
    }
}