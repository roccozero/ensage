using System;
using System.ComponentModel;
using Ensage;
using Ensage.SDK.Input;
using Ensage.SDK.Menu;
using Ensage.SDK.Menu.Items;
using Ensage.SDK.Renderer;
using Newtonsoft.Json;
using SharpDX;
using Color = System.Drawing.Color;

namespace ArcCrappahilationPaid.Config
{
    public class ItemCooldownHelper
    {
        [JsonIgnore]
        private readonly ArcCrappahilation _main;

        [JsonIgnore]
        private bool _isEnable;

        [JsonIgnore]
        private IRenderManager Render => _main.Context.RenderManager;

        [JsonIgnore]
        private IInputManager Input => _main.Context.Input;

        public ItemCooldownHelper(ArcCrappahilation main)
        {
            _main = main;
            DrawingStartPosition = Position;
        }
        [JsonIgnore]
        public Slider<Vector2> DrawingStartPosition { get; set; }

        [Item("Show item cooldown helper panel")]
        [DefaultValue(true)]
        public bool IsEnable
        {
            get => _isEnable;
            set
            {
                OnChange(value);
                _isEnable = value;
            }
        }

        private bool _isMoving;
        private Vector2 _drawMousePosition;

        private void OnChange(bool value)
        {
            if (value == _isEnable)
                return;
            if (value)
            {
                Render.Draw += RenderOnDraw;
            }
            else
            {
                Render.Draw -= RenderOnDraw;
            }
        }

        private void InputOnMouseMove(object sender, MouseEventArgs e)
        {
            if (!_anyItems)
                return;
            if (_isMoving)
            {
                var newValue = new Vector2(e.Position.X - _drawMousePosition.X,
                    e.Position.Y - _drawMousePosition.Y);
                newValue.X = Math.Max(Position.MinValue.X, Math.Min(Position.MaxValue.X, newValue.X));
                newValue.Y = Math.Max(Position.MinValue.Y, Math.Min(Position.MaxValue.Y, newValue.Y));
                DrawingStartPosition.Value = newValue;
            }
        }

        private void InputOnMouseClick(object sender, MouseEventArgs e)
        {
            if (!_anyItems)
                return;
            if ((e.Buttons & MouseButtons.Left) == 0)
            {
                return;
            }

            var size = new RectangleF(DrawingStartPosition.Value.X, DrawingStartPosition.Value.Y,
                SizeX.Value * _totalCount, SizeY.Value);
            var isIn = size.Contains(e.Position);
            if ((e.Buttons & MouseButtons.LeftUp) == MouseButtons.LeftUp)
            {
                _isMoving = false;
            }
            else if (isIn && (e.Buttons & MouseButtons.LeftDown) == MouseButtons.LeftDown)
            {
                var startPos= new Vector2(DrawingStartPosition.Value.X, DrawingStartPosition.Value.Y);
                _drawMousePosition = e.Position - startPos;
                _isMoving = true;
            }
        }

        [JsonIgnore]
        private bool _anyItems;
        [JsonIgnore]
        private int _totalCount;

        private bool _movable;

        private void RenderOnDraw(IRenderer renderer)
        {
            try
            {
                if (_main.Updater?.TempestHero == null || !_main.Updater.TempestHero.IsValid)
                    return;
                var size = new RectangleF(DrawingStartPosition.Value.X, DrawingStartPosition.Value.Y, SizeX.Value, SizeY.Value);
                var count = 0;
                //foreach (var itemHolder in _main.ItemHolderManager.List)
                try
                {
                    foreach (var itemHolder in _main.Updater.TempestHero.Context.Owner.Inventory.Items)
                    {
                        try
                        {
                            if (!itemHolder.IsValid)
                                continue;
                            if (itemHolder.Cooldown <= 0)
                                continue;
                            renderer.DrawTexture(itemHolder.Id.ToString(), size);
                            renderer.DrawText(new Vector2(size.X, size.Y), Math.Min(99, itemHolder.Cooldown).ToString("####"),
                                Color.PaleGreen, TextSize.Value);
                            size.X += SizeX.Value;
                            count++;
                        }
                        catch (Exception e)
                        {
                            ArcCrappahilation.Log.Error($"Item Error with {itemHolder.Id} -> {e}");
                        }
                    }
                }
                catch (Exception e)
                {
                    ArcCrappahilation.Log.Error(e);
                }
                
                _totalCount = count;
                _anyItems = count > 0;
            }
            catch (Exception e)
            {
                ArcCrappahilation.Log.Error(e);
            }
            
        }

        [Item("Panel Position")]
        public Slider<Vector2> Position { get; set; } = new Slider<Vector2>(new Vector2(200, 100), new Vector2(0, 0),
            new Vector2(Drawing.Width - 10, Drawing.Height - 10));

        [Item("Size X")]
        public Slider<float> SizeX { get; set; } = new Slider<float>(50, 5, 400);

        [Item("Size Y")]
        public Slider<float> SizeY { get; set; } = new Slider<float>(50, 5, 400);

        [Item("Text Size")]
        public Slider<float> TextSize { get; set; } = new Slider<float>(50, 1, 100);

        [Item("Movable")]
        [DefaultValue(true)]
        public bool Movable
        {
            get => _movable;
            set
            {
                if (value != _movable)
                {
                    if (value)
                    {
                        Input.MouseClick += InputOnMouseClick;
                        Input.MouseMove += InputOnMouseMove;
                    }
                    else
                    {
                        Input.MouseClick -= InputOnMouseClick;
                        Input.MouseMove -= InputOnMouseMove;
                    }
                }
                _movable = value;
            }
        }
    }
}