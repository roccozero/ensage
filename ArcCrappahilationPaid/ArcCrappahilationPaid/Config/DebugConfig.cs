using System.ComponentModel;
using ArcCrappahilationPaid.Units;
using Ensage.SDK.Menu;

namespace ArcCrappahilationPaid.Config
{
    public class DebugConfig
    {
        private readonly BaseMenu _baseMenu;
        private bool _isEnable;

        public DebugConfig(BaseMenu baseMenu)
        {
            _baseMenu = baseMenu;
        }

        [Item("Show current orders")]
        [Tooltip("Will print to console current orders of ur units")]
        [DefaultValue(false)]
        public bool IsEnable
        {
            get => _isEnable;
            set
            {
                if (_isEnable != value && value)
                {
                    _isEnable = false;
                    ArcCrappahilation.Log.Warn($"Debug -> orders status. Start");
                    foreach (var controllableUnit in _baseMenu.Main.Updater.Units)
                    {
                        var name = "other";
                        switch (controllableUnit)
                        {
                            case MainHeroUnit _:
                                name = "main";
                                break;
                            case TempestHeroUnit _:
                                name = "tempest";
                                break;
                            case IllusionHeroUnit _:
                                name = "illusion";
                                break;
                            case NecroBase _:
                                name = "necro";
                                break;
                        }
                        ArcCrappahilation.Log.Warn(
                            $"{controllableUnit.Owner.Name} -> [order: {controllableUnit.OrderManager.CurrentOrderId}] [{name}]");
                    }
                    ArcCrappahilation.Log.Warn($"Debug -> orders status. End");
                    return;
                }
                _isEnable = value;
            }
        }

        [Item("Show current tempest's items")]
        [Tooltip("Will print to console current items of ur temepst")]
        [DefaultValue(false)]
        public bool ShowItems
        {
            get => _isEnable;
            set
            {
                if (_isEnable != value && value)
                {
                    _isEnable = false;
                    ArcCrappahilation.Log.Warn($"Debug -> items. Start");
                    foreach (var controllableUnit in _baseMenu.Main.Updater.Units)
                    {
                        if (controllableUnit is TempestHeroUnit tempest)
                        {
                            foreach (var inventoryItem in tempest.Context.Owner.Inventory.Items)
                            {
                                ArcCrappahilation.Log.Warn($"item -> {inventoryItem.Id} [{inventoryItem.Name}] cd: {inventoryItem.Cooldown}");
                            }
                        }
                    }
                    ArcCrappahilation.Log.Warn($"Debug -> items. End");
                    return;
                }
                _isEnable = value;
            }
        }
    }
}