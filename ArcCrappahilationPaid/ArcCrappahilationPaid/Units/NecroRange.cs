﻿using ArcCrappahilationPaid.AbilityUsages;
using Ensage;
using Ensage.SDK.Service;

namespace ArcCrappahilationPaid.Units
{
    public class NecroRange : NecroBase
    {
        public NecroRange(Unit owner, ArcCrappahilation main) : base(main)
        {
            Owner = owner;
            MovementManager = new MovementManager(new EnsageServiceContext(owner));
            AbilityManager = new CanUseAbilityNecro(owner.Spellbook.Spell1);
        }
    }
}