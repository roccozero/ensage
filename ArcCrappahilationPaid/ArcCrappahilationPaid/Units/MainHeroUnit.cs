﻿using System.Collections.Generic;
using System.Linq;
using ArcCrappahilationPaid.AbilityUsages;
using ArcCrappahilationPaid.Config;
using Ensage;
using Ensage.SDK.Service;

namespace ArcCrappahilationPaid.Units
{
    public class MainHeroUnit : HeroUnit
    {
        public readonly BaseMenu BaseMenu;
        public MenuForMainHero MenuForMainHero => BaseMenu.MenuForMainHero;
        public MainHeroUnit(Unit owner, ArcCrappahilation main) : base(main)
        {
            BaseMenu = main.Menu;
            Owner = owner;
            MovementManager = new MovementManager(main.Context.Orbwalker, main.Context);
            AbilityManager = new CanUseAbilityHero(this);
            Abilities = Owner.Spellbook.Spells.Where(x => !x.IsHidden);
            Main.Menu.OrderConfig.MainTargetManager = new TargetManager(Main, this);
        }

        public IEnumerable<Ability> Abilities { get; set; }

        public override IEnumerable<Item> GetItems()
        {
            var items = Owner.Inventory.Items.Where(x =>
                MenuForMainHero.ItemPriorityChanger.PictureStates.ContainsKey(x.ToString()) &&
                MenuForMainHero.ItemPriorityChanger[x.Id.ToString()]);
            if (MenuForMainHero.CustomComboPriorityHero)
                items = items.OrderBy(x => MenuForMainHero.ItemPriorityChanger.Priorities[x.Id.ToString()]);
            //ArcRektolation.Log.Warn($"Get items for main hero. Count: [{items.Count()}]");
            return items;
        }

        public override IEnumerable<Ability> GetAbilities()
        {
            var items = Abilities.Where(x =>
                    MenuForMainHero.AbilitySelection.PictureStates.ContainsKey(x.ToString()) &&
                    MenuForMainHero.AbilitySelection[x.Id.ToString()])
                .OrderByDescending(x => x.Id == AbilityId.arc_warden_tempest_double);
            return items;
        }
    }
}