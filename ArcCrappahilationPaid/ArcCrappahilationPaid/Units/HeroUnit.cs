﻿using System.Collections.Generic;
using Ensage;

namespace ArcCrappahilationPaid.Units
{
    public abstract class HeroUnit : ControllableUnit
    {
        public abstract IEnumerable<Item> GetItems();
        public abstract IEnumerable<Ability> GetAbilities();

        protected HeroUnit(ArcCrappahilation main) : base(main)
        {
        }
    }
}