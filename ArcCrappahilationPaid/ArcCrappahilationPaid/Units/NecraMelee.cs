﻿using Ensage;
using Ensage.SDK.Service;

namespace ArcCrappahilationPaid.Units
{
    public class NecraMelee : NecroBase
    {
        public NecraMelee(Unit owner, ArcCrappahilation main) : base(main)
        {
            Owner = owner;
            MovementManager = new MovementManager(new EnsageServiceContext(owner));
        }
    }
}