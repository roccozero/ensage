﻿using System.Collections.Generic;
using System.Linq;
using ArcCrappahilationPaid.AbilityUsages;
using ArcCrappahilationPaid.Config;
using Ensage;
using Ensage.Common.Extensions;
using Ensage.SDK.Helpers;
using Ensage.SDK.Service;

namespace ArcCrappahilationPaid.Units
{
    public class TempestHeroUnit : HeroUnit
    {
        private MenuForTempestHero MenuMenuForTempestHero => BaseMenu.MenuForTempestHero;
        public readonly BaseMenu BaseMenu;
        private ItemHolderManager ItemHolderManager => BaseMenu.Main.ItemHolderManager;
        public List<Item> Items;
        public TempestHeroUnit(Unit owner, ArcCrappahilation arc) : base(arc)
        {
            var baseMenu = arc.Menu;
            BaseMenu = baseMenu;
            Owner = owner;
            Context = new EnsageServiceContext(owner);
            MovementManager = new MovementManager(Context);
            AbilityManager = new CanUseAbilityHero(this);
            Abilities = Owner.Spellbook.Spells.Where(x => !x.IsHidden);
            if (Main.Updater.TempestHero == null || !Main.Updater.TempestHero.IsValid)
            {
                Main.Updater.TempestHero = this;
                Main.Menu.OrderConfig.TempestTargetManager = new TargetManager(Main, this);
            }
            ItemsInSystem = new List<ItemHolderManager.ItemHolder>();
            //ItemsInSystem2 = new List<Item>();

            //Items = new List<Item>();
            Items = owner.Inventory.Items.ToList();
            //UpdateManager.Subscribe(() =>
            //{
            //ItemsInSystem2 = Context.Inventory.Items.Select(x => x.Item).ToList();
            /*foreach (var inventoryItem in Context.Inventory.Items)
            {
                if (ItemsInSystem.Any(x => x.Id == inventoryItem.Id))
                {
                    ItemsInSystem.Remove(baseMenu.Main.ItemHolderManager.GetById(inventoryItem.Id)?.Deactivate());
                    ItemHolderManager.RemoveById(inventoryItem.Id);
                }
                //if (inventoryItem.Item.Cooldown > 0)
                {
                    var item = baseMenu.Main.ItemHolderManager.GetOrCreate(inventoryItem.Item)?.Activate();
                    ItemsInSystem.Add(item);
                }
                //ArcCrappahilation.Log.Warn($"[Tempest] new item: {inventoryItem.Id} {item?.Cooldown}");
            }*/
            //}, 1000);



            /*UpdateManager.Subscribe(() =>
            {
                foreach (var inventoryItem in owner.Inventory.Items)
                {
                    if (ItemsInSystem.Any(x => x.Id == inventoryItem.Id))
                    {
                        ItemsInSystem.Remove(baseMenu.Main.ItemHolderManager.GetById(inventoryItem.Id)?.Deactivate());
                        ItemHolderManager.RemoveById(inventoryItem.Id);
                    }
                    ItemsInSystem.Add(baseMenu.Main.ItemHolderManager.GetOrCreate(inventoryItem)?.Activate());
                }
            }, 1000);*/

            UpdateManager.BeginInvoke(() =>
            {
                foreach (var inventoryItem in Context.Inventory.Inventory.Items)
                {
                    if (ItemsInSystem.Any(x => x.Id == inventoryItem.Id))
                    {
                        ItemsInSystem.Remove(baseMenu.Main.ItemHolderManager.GetById(inventoryItem.Id)?.Deactivate());
                        ItemHolderManager.RemoveById(inventoryItem.Id);
                    }
                    ItemsInSystem.Add(baseMenu.Main.ItemHolderManager.GetOrCreate(inventoryItem)?.Activate());
                }
            }, 200);

            Context.Inventory.CollectionChanged += (sender, args) =>
            {
                UpdateManager.BeginInvoke(() =>
                {
                    foreach (var inventoryItem in Context.Inventory.Inventory.Items)
                    {
                        if (ItemsInSystem.Any(x => x.Id == inventoryItem.Id))
                        {
                            ItemsInSystem.Remove(baseMenu.Main.ItemHolderManager.GetById(inventoryItem.Id)?.Deactivate());
                            ItemHolderManager.RemoveById(inventoryItem.Id);
                        }
                        ItemsInSystem.Add(baseMenu.Main.ItemHolderManager.GetOrCreate(inventoryItem)?.Activate());
                    }
                }, 200);
                
                /*if (args.Action == NotifyCollectionChangedAction.Add)
                {
                    foreach (InventoryItem argsNewItem in args.NewItems)
                    {
                        if (ItemsInSystem.Any(x => x.Id == argsNewItem.Id))
                        {
                            ItemsInSystem.Remove(baseMenu.Main.ItemHolderManager.GetById(argsNewItem.Id)?.Deactivate());
                            ItemHolderManager.RemoveById(argsNewItem.Id);
                        }
                        ItemsInSystem.Add(baseMenu.Main.ItemHolderManager.GetOrCreate(argsNewItem.Item)?.Activate());
                    }
                }
                else if (args.Action == NotifyCollectionChangedAction.Remove)
                {
                    foreach (InventoryItem argsNewItem in args.OldItems)
                    {
                        var id = argsNewItem.Id;
                        ItemsInSystem.Remove(baseMenu.Main.ItemHolderManager.GetById(id)?.Deactivate());
                        ItemHolderManager.RemoveById(id);
                    }
                }*/
            };

            UpdateManager.Subscribe(() =>
            {
                if (Main.Menu.AutoLinken)
                {
                    var sphere = Context.Inventory.Items.FirstOrDefault(x => x.Id == AbilityId.item_sphere)?.Item;
                    if (sphere != null && sphere.CanBeCasted())
                    {
                        var mainHero = Main.Context.Owner;
                        if (sphere.CanHit(mainHero))
                        {
                            sphere.UseAbility(mainHero);
                        }
                    }
                }
            },1000);
        }

        public List<ItemHolderManager.ItemHolder> ItemsInSystem { get; set; }
        public List<Item> ItemsInSystem2 { get; set; }

        public IServiceContext Context { get; set; }

        public IEnumerable<Ability> Abilities { get; set; }

        public override IEnumerable<Item> GetItems()
        {
            var items = Owner.Inventory.Items.Where(x =>
                MenuMenuForTempestHero.ItemPriorityChanger.PictureStates.ContainsKey(x.ToString()) &&
                MenuMenuForTempestHero.ItemPriorityChanger[x.Id.ToString()]);
            if (MenuMenuForTempestHero.CustomComboPriorityHero)
                items = items.OrderBy(x => MenuMenuForTempestHero.ItemPriorityChanger.Priorities[x.Id.ToString()]);
            //ArcRektolation.Log.Warn($"Get items for tempest hero. Count: [{items.Count()}]");
            return items;
        }

        public override IEnumerable<Ability> GetAbilities()
        {
            var items = Abilities.Where(x =>
                MenuMenuForTempestHero.AbilitySelection.PictureStates.ContainsKey(x.ToString()) &&
                MenuMenuForTempestHero.AbilitySelection[x.Id.ToString()]);
            return items;
        }
    }
}