﻿using ArcCrappahilationPaid.AbilityUsages;
using Ensage;

namespace ArcCrappahilationPaid.Units
{
    public class ControllableUnit
    {
        public ArcCrappahilation Main { get; }
        public Unit Owner;
        public MovementManager MovementManager;
        public IUseAbilities AbilityManager;
        
        public OrderManager OrderManager;

        public ControllableUnit(ArcCrappahilation main)
        {
            Main = main;
            AbilityManager = new CanNotUseAbility();
            OrderManager = new OrderManager(this);
        }

        public bool IsValid => Owner.IsValid;
        public bool IsAlive => Owner.IsAlive;
    }
}