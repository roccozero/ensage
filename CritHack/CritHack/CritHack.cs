﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using Ensage.SDK.Service;
using Ensage.SDK.Service.Metadata;
using Ensage.SDK.Extensions;
using Ensage.SDK.Helpers;
using Ensage.SDK.Menu;

using Ensage.Common.Menu;
using Ensage;
using NLog;

namespace CritHack
{
    [ExportPlugin(
        mode: StartupMode.Auto,
        name: "CritHack",
        version: "1.0.0.0",
        author: "JumpAttacker",
        description: "",
        units: new[] { HeroId.npc_dota_hero_phantom_assassin, HeroId.npc_dota_hero_juggernaut,
            HeroId.npc_dota_hero_skeleton_king, HeroId.npc_dota_hero_chaos_knight})]
    public sealed class CritHack : Plugin
    {
        private static readonly Logger Log = LogManager.GetCurrentClassLogger();

        [ImportingConstructor]
        public CritHack([Import] IServiceContext context)
        {
            Context = context;
        }

        public IServiceContext Context { get; }

        private float _timer;
        private Hero Me => (Hero) Context.Owner;
        protected override void OnActivate()
        {
            Factory = MenuFactory.Create("Crit hax");
            Enable = Factory.Item("Enable", true);
            UseTargerSelection = Factory.Item("Attack closest to mouse unit", false);
            Key = Factory.Item("Key", new KeyBind('0'));
            //Rate = Factory.Item("Rate", new Slider(60,1,100));
            //Rate.Item.SetTooltip("best value ~ 45-65");

            Key.PropertyChanged += (sender, args) =>
            {
                if (Key.Value.Active)
                {
                    Entity.OnInt32PropertyChange += OnNetworkActivity;
                    UpdateManager.Subscribe(Update);
                }
                else
                {
                    Entity.OnInt32PropertyChange -= OnNetworkActivity;
                    UpdateManager.Unsubscribe(Update);
                }
            };
        }

        private void OnNetworkActivity(Entity sender, Int32PropertyChangeEventArgs args)
        {
            if (sender != Me)
            {
                return;
            }

            if (!args.PropertyName.Equals("m_networkactivity", StringComparison.InvariantCultureIgnoreCase))
            {
                return;
            }

            if (args.NewValue == 1505)
            {
                _timer = Game.RawGameTime + Me.AttackPoint();
                return;
            }

            if (args.NewValue == 1503)
            {
                Me.Stop();
            }
        }

        public MenuItem<bool> UseTargerSelection { get; set; }

        public MenuItem<bool> CheckForShit { get; set; }

        private void Update()
        {
            if (Enable)
            {
                if (_timer <= Game.RawGameTime)
                {
                    if (UseTargerSelection && Context.TargetSelector.Active.GetTargets().Any())
                    {
                        var target = Context.TargetSelector.Active.GetTargets().First();
                        if (target != null)
                            Context.Owner.Attack(target);
                    }
                    else
                    {
                        Context.Owner.Attack(Context.Owner.InFront(150));
                    }
                    
                    if (Context.Owner.AttackPoint() / 2.5f < 0.3f)
                    {
                        _timer = Game.RawGameTime + 0.30f;
                    }
                    else
                    {
                        _timer = Game.RawGameTime + Context.Owner.AttackPoint() / 2.3f;
                    }
                }
            }
        }

        public MenuItem<KeyBind> Key { get; set; }

        public MenuItem<Slider> Rate { get; set; }

        public MenuItem<bool> Enable { get; set; }

        public MenuFactory Factory { get; set; }
    }
}
