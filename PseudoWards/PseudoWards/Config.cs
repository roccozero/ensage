﻿using Ensage.Common.Menu;
using Ensage.SDK.Menu;

namespace PseudoWards
{
    public class Config
    {
        private PseudoWards _main;

        public Config(PseudoWards main)
        {
            _main = main;
            Factory = MenuFactory.Create("Pseudo Wards");
            Enable = Factory.Item("Enable", true);
            SetPointKey = Factory.Item("Set point ", new KeyBind('0'));
            ClearKey = Factory.Item("Clear points", new KeyBind('0'));
        }

        public MenuItem<KeyBind> ClearKey { get; set; }

        public MenuItem<bool> Enable { get; set; }
        public MenuItem<KeyBind> SetPointKey { get; set; }

        public MenuFactory Factory { get; set; }
    }
}