﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Reflection;
using System.Linq;

using Ensage.SDK.Service;
using Ensage.SDK.Renderer;
using Ensage.Common.Menu;

using log4net;

using PlaySharp.Toolkit.Logging;

using SharpDX;

using Ensage;
using Ensage.SDK.Helpers;
using Ensage.SDK.Service.Metadata;
using Color = System.Drawing.Color;

namespace PseudoWards
{
    [ExportPlugin(
        mode: StartupMode.Auto,
        name: "Pseudo Wards")]
    public sealed class PseudoWards : Plugin
    {
        private static readonly ILog Log = AssemblyLogs.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        [ImportingConstructor]
        public PseudoWards([Import] IServiceContext context)
        {
            Context = context;
        }

        public IServiceContext Context { get; }
        public Config Config { get; private set; }

        protected override void OnActivate()
        {
            Log.Debug($"[Pseudo Wards] trying to load Config");
            Config = new Config(this);
            Log.Debug($"[Pseudo Wards] Config loaded");

            Config.Enable.PropertyChanged += (sender, args) =>
            {
                if (Config.Enable)
                    Enable();
                else
                    Disable();
            };
            if (Config.Enable)
                Enable();
            _points = new List<Vector3>();
            Log.Debug($"[Pseudo Wards] Fully loaded");
        }

        private List<Vector3> _points;
        private Hero Me=> Context.Owner as Hero;
        private void OnSetKey(object o, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (!Config.SetPointKey)
                return;
            if (!_isActive)
                return;
            if (!Me.IsAlive)
                return;
            if (!Me.IsVisibleToEnemies)
                return;
            _points.Add(Me.NetworkPosition);

            _isActive = false;
            UpdateManager.BeginInvoke(() => _isActive = true, 750);
        }
        private void OnClearKey(object o, OnValueChangeEventArgs args)
        {
            var old = args.GetOldValue<KeyBind>().Active;
            var newValue = args.GetNewValue<KeyBind>().Active;
            if (old == newValue || !newValue) return;
            _points.Clear();
        }

        private void Enable()
        {
            Context.Renderer.Draw += RendererOnDraw;
            Config.ClearKey.Item.ValueChanged += OnClearKey;
            Config.SetPointKey.PropertyChanged += OnSetKey;
        }

        private void Disable()
        {
            Context.Renderer.Draw -= RendererOnDraw;
            Config.ClearKey.Item.ValueChanged -= OnClearKey;
            Config.SetPointKey.PropertyChanged -= OnSetKey;

        }

        private IRendererManager Renderer => Context.Renderer;
        private bool _isActive = true;
        private void RendererOnDraw(object o, EventArgs eventArgs)
        {
            if (!_points.Any())
                return;
            var finalPos = new Vector2();

            foreach (var vector3 in _points)
            {
                var onScreen = Drawing.WorldToScreen(vector3, out var pos);
                if (onScreen)
                {
                    Renderer.DrawText(pos, "X", Color.Red, 15);
                }
                finalPos.X += pos.X;
                finalPos.Y += pos.Y;

            }
            finalPos.X/= _points.Count;
            finalPos.Y/= _points.Count;
            Renderer.DrawText(finalPos, "Pseudo Ward", Color.Chartreuse, 12);
            Renderer.DrawRectangle(new RectangleF(finalPos.X - 250, finalPos.Y - 250, 500, 500), Color.CornflowerBlue);
        }

        protected override void OnDeactivate()
        {
            Disable();
        }
    }
}
