﻿using System.Collections.Generic;
using System.Linq;
using Ensage;
using Ensage.SDK.Helpers;

namespace AfkHelper
{
    public class ItemChildren
    {
        public AbilityId Main;
        public AbilityId[] Childrens;

        public ItemChildren(AbilityId main, AbilityId[] childrens)
        {
            Main = main;
            Childrens = childrens;
        }
    }
    public class ItemManager
    {
        private readonly AfkHelper _main;
        private Hero Me => _main.Owner;
        private Config Config => _main.Config;

        private readonly List<ItemChildren> _itemChildrens = new List<ItemChildren>
        {
            new ItemChildren(AbilityId.item_mask_of_madness,
                new[]
                {
                    AbilityId.item_lifesteal, AbilityId.item_quarterstaff
                }),
            new ItemChildren(AbilityId.item_maelstrom,
                new[]
                {
                    AbilityId.item_javelin, AbilityId.item_mithril_hammer
                }),
            new ItemChildren(AbilityId.item_tranquil_boots,
                new[]
                {
                    AbilityId.item_boots,
                    AbilityId.item_wind_lace,
                    AbilityId.item_ring_of_regen,
                }),
        };

        public ItemManager(AfkHelper main)
        {
            _main = main;


            if (Config.Enable)
                UpdateManager.Subscribe(BuyAction, 500);

            Config.Enable.PropertyChanged += (sender, args) =>
            {
                if (Config.Enable)
                    UpdateManager.Subscribe(BuyAction, 500);
                else
                    UpdateManager.Unsubscribe(BuyAction);
            };
            //Unit.OnModifierAdded += HeroOnOnModifierAdded;
        }

        private void BuyAction()
        {
            if (IsOnFountain)
            {
                var myItems = Me.Inventory.Items.Select(x => x.Id).ToList();
                myItems.AddRange(Me.Inventory.Backpack.Select(item => item.Id));
                myItems.AddRange(Me.Inventory.Stash.Select(item => item.Id));
                var needToBuy = Config.ItemsInBuyList.FirstOrDefault(x =>
                    Config.BuyItems.Value.IsEnabled(x.ToString()) &&
                    CheckForTotalitems(myItems, x) &&
                    Ability.GetAbilityDataById(x).Cost <= Me.Player.ReliableGold + Me.Player.UnreliableGold);
                if (needToBuy == AbilityId.ability_base)
                    return;
                AfkHelper.Log.Warn($"Will Buy -> {needToBuy}");
                Player.BuyItem(Me, needToBuy);
            }
        }

        private bool CheckForTotalitems(IReadOnlyCollection<AbilityId> myItems, AbilityId wantToBuy)
        {
            if (myItems.Any(x => x == wantToBuy))
                return false;
            var findMain = _itemChildrens.Find(x => x.Childrens.Any(z => z == wantToBuy));
            if (findMain == null) return true;
            var main = findMain.Main;
            return myItems.All(x => x != main);
        }

        private void HeroOnOnModifierAdded(Unit sender, ModifierChangedEventArgs args)
        {
            if (sender.Equals(Me) && args.Modifier!=null && args.Modifier.IsValid && args.Modifier.Name == "modifier_fountain_aura_buff")
            {
                var myItems = Me.Inventory.Items.Select(x => x.Id).ToList();
                var needToBuy = Config.ItemsInBuyList.FirstOrDefault(x =>
                    Config.BuyItems.Value.IsEnabled(x.ToString()) &&
                    CheckForTotalitems(myItems, x) &&
                    Ability.GetAbilityDataById(x).Cost <= Me.Player.ReliableGold + Me.Player.UnreliableGold);
                if (needToBuy == AbilityId.ability_base)
                    return;
                AfkHelper.Log.Warn($"Will Buy -> {needToBuy}");
                Player.BuyItem(Me, needToBuy);
                Unit.OnModifierAdded -= HeroOnOnModifierAdded;
                UpdateManager.BeginInvoke(() =>
                {
                    Unit.OnModifierAdded += HeroOnOnModifierAdded;
                }, 200);
                
            }
        }

        private bool IsOnFountain =>
            Me.ActiveShop == ShopType.Base; //_main.Owner.HasAnyModifiers("modifier_fountain_aura_buff");
    }
}