﻿using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Threading.Tasks;
using Ensage;
using Ensage.SDK.Abilities.Items;
using Ensage.SDK.Inventory.Metadata;
using Ensage.SDK.Renderer;
using Ensage.SDK.Service;
using Ensage.SDK.Service.Metadata;
using SharpDX;
using Color = System.Drawing.Color;
using NLog;

namespace AfkHelper
{
    [ExportPlugin(
        mode: StartupMode.Auto,
        name: "AfkHelper"
        )]
    public sealed class AfkHelper : Plugin
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        private static extern void mouse_event(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);
        private enum MouseEvent
        {
            MouseeventfLeftdown = 2,
            MouseeventfLeftup = 4,
        }
        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool SetCursorPos(int x, int y);
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();

        [ImportingConstructor]
        public AfkHelper([Import] IServiceContext context)
        {
            Context = context;
        }

        public IServiceContext Context { get; }
        public CampManager CampManager { get; private set; }
        public AfkController AfkController { get; private set; }
        public ItemManager ItemManager { get; private set; }
        public Hero Owner { get; private set; }
        public Config Config { get; private set; }
        public LaneHelper LaneHelper { get; private set; }
        public Color TextColor;
        public const string FileName = "AfkHelperAutoSearcher.exe";

        [PermissionSet(SecurityAction.Assert, Unrestricted = true)]
        public static void Main()
        {
            var state = Game.GameState;
            ScreenSize = new Vector2(Drawing.Width, Drawing.Height);
            if (ScreenSize.IsZero)
            {
                Console.WriteLine("Ensage couldnt determine your resolution, try to launch in window mode");
                return;
            }
            var ratio = Math.Floor((decimal)(ScreenSize.X / ScreenSize.Y * 100));
            if (state == GameState.Init)
            {
                Vector2 ratesForSearch;
                Vector2 ratesForRandom;
                switch (ratio)
                {
                    case 213:
                        ratesForSearch = new Vector2(1.1450f, 1.0434f);
                        ratesForRandom = new Vector2(1.145973f, 1.35689f);
                        break;
                    case 177:
                        ratesForSearch = new Vector2(1.1519f, 1.0465f);
                        ratesForRandom = new Vector2(1.151079137f, 1.363636364f);
                        break;
                    default:
                        ratesForSearch = new Vector2(1.141f, 1.046f);
                        ratesForRandom = new Vector2(1.148325359f, 1.35f);
                        break;
                }
                //click on search
                var clickPos = new Vector2(ScreenSize.X / ratesForSearch.X, ScreenSize.Y / ratesForSearch.Y);
                Console.WriteLine($"ScreenSize:  (x:{ScreenSize.X}; y:{ScreenSize.Y}) Rate: {ratio} Click Position: (x:{clickPos.X}; y:{clickPos.Y})");
                Task.Run(async () =>
                {
                    await ClickOnPos(clickPos);
                    await Task.Delay(250);
                    await Click();
                    Console.WriteLine($"Wait for the game.");
                    while (Game.GameState != GameState.StrategyTime)
                    {
                        if (Game.GameState == GameState.HeroSelection)
                        {
                            Game.ExecuteCommand("dota_select_hero random");
                            Console.WriteLine($"Hero selection detected");
                            //1672 800
                            //await Task.Delay(150);
                            //click on random
                            //clickPos = new Vector2(ScreenSize.X / ratesForRandom.X, ScreenSize.Y / ratesForRandom.Y);
                            //await ClickOnPos(clickPos);

                        }
                        await Task.Delay(150);
                    }
                    Console.WriteLine($"Hero selected");
                });
            }

            return;
            var tempExeName = Path.Combine(Directory.GetCurrentDirectory(), FileName);

            if (Game.IsInGame)
            {
                FlushProcess(FileName);
            }
            else
            {
                if (!File.Exists(tempExeName))
                {
                    Console.WriteLine(string.Format(@"Will try to create a new file -> {0}", tempExeName));
                    using (var fsDst = new FileStream(tempExeName, FileMode.CreateNew, FileAccess.Write))
                    {
                        var bytes = GetSubExe();

                        fsDst.Write(bytes, 0, bytes.Length);
                    }

                    Properties.Resources.Search.Save(Path.Combine(Directory.GetCurrentDirectory(), "Search.bmp"));
                    Properties.Resources.Axe.Save(Path.Combine(Directory.GetCurrentDirectory(), "Axe.bmp"));
                    Properties.Resources.Random.Save(Path.Combine(Directory.GetCurrentDirectory(), "Random.bmp"));
                }
                else
                {
                    Console.WriteLine(string.Format(@"File already created -> {0}", tempExeName));
                }

                if (Process.GetProcessesByName(FileName).Length > 0)
                {
                    Console.WriteLine(@"File already running");
                }
                else
                {
                    Console.WriteLine(@"Starting");
                    Process.Start(tempExeName);
                }
            }
        }

        public static async Task ClickOnPos(Vector2 clickPos)
        {
            SetCursorPos((int)clickPos.X, (int)clickPos.Y);
            await Task.Delay(100);
            mouse_event((int)MouseEvent.MouseeventfLeftdown, (int)clickPos.X, (int)clickPos.Y, 0, 0);
            await Task.Delay(100);
            mouse_event((int)MouseEvent.MouseeventfLeftup, (int)clickPos.X, (int)clickPos.Y, 0, 0);
        }

        public static async Task Click()
        {
            mouse_event((int) MouseEvent.MouseeventfLeftdown, 0, 0, 0, 0);
            await Task.Delay(100);
            mouse_event((int)MouseEvent.MouseeventfLeftup, 0, 0, 0, 0);
        }

        public static Vector2 ScreenSize { get; set; }

        private static void FlushProcess(string fileName)
        {
            try
            {
                Console.WriteLine(string.Format(@"Trying to find a process [{0}]", fileName));
                var counter = 0;
                foreach (var proc in Process.GetProcessesByName("AfkHelperAutoSearcher"))
                {
                    Console.WriteLine(string.Format(@"Gotcha process #{0} [{1}]", ++counter, proc.ProcessName));
                    proc.Kill();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format(@"Error {0}", ex));
            }
        }

        private static Bitmap GetSubImageOne()
        {
            return Properties.Resources.Search;
        }

        public static byte[] GetSubExe()
        {
            return Properties.Resources.Searcher1;
        }

        protected override void OnActivate()
        {
            Owner = (Hero) Context.Owner;
            Config = new Config(this);
            LaneHelper = new LaneHelper(Owner);
            CampManager = new CampManager();
            AfkController = new AfkController(this);
            ItemManager = new ItemManager(this);

            Context.Inventory.Attach(this);

            if (Config.DrawCampStatus)
                Context.RenderManager.Draw += OnDraw;

            if (Config.DrawType)
                Context.RenderManager.Draw += OnDraw2;

            TextColor = Config.Enable ? Color.White : Color.Red;

            Config.Enable.PropertyChanged += (sender, args) => { TextColor = Config.Enable ? Color.White : Color.Red; };
            Config.DrawCampStatus.PropertyChanged += OnDrawToggle;
            Config.DrawType.PropertyChanged += OnDraw2Toggle;

            FlushProcess(FileName);
        }

        private void OnDraw2(IRenderer renderer)
        {
            renderer.DrawText(new Vector2(Config.PosX, Config.PosY),
                string.Format("Mode: {0} ({1})", AfkController.FarmType, AfkController.CurrentFarmType),
                TextColor, Config.TextSize);
        }

        private void OnDrawToggle(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (Config.DrawCampStatus)
                Context.RenderManager.Draw += OnDraw;
            else
                Context.RenderManager.Draw -= OnDraw;
        }
        private void OnDraw2Toggle(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (Config.DrawType)
                Context.RenderManager.Draw += OnDraw2;
            else
                Context.RenderManager.Draw -= OnDraw2;
        }

        private void OnDraw(IRenderer renderer)
        {
            foreach (var camp in CampManager.Camps)
            {
                if (Drawing.WorldToScreen(camp.Position, out var pos))
                {
                    renderer.DrawText(pos,
                        $"Empty: {(camp.IsEmpty ? "Yes" : "No")} Owners: {camp.Owners.Count}",
                        Color.AliceBlue, 15);
                }
            }
        }

        protected override void OnDeactivate()
        {
            Config?.Dispose();
            CampManager?.Dispose();
            AfkController?.Dispose();
        }

        [ItemBinding]
        public item_mask_of_madness Mom { get; set; }
        [ItemBinding]
        public item_flask Flask { get; set; }
    }
}
