﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ensage;
using Ensage.Common.Menu;
using Ensage.SDK.Menu;

namespace AfkHelper
{
    public class Config : IDisposable
    {
        private readonly AfkHelper _main;
        public List<AbilityId> ItemsInBuyList;

        public Config(AfkHelper main)
        {
            _main = main;
            Factory = MenuFactory.Create("Afk Helper");
            Enable = Factory.Item("Enable", new KeyBind('0', KeyBindType.Toggle));
            FarmEnemyCamp = Factory.Item("Farm Enemy camps", true);
            FarmAncientCamps = Factory.Item("Farm Ancient camps", false);
            SkipCampWithAllyHero = Factory.Item("Skip camp with ally heroes", true);

            EscapeOnLowHealthMode = Factory.Item("Go home on low health", true);
            EscapeOnLowHealthHealth = Factory.Item("Health %", new Slider(15, 1, 99));

            UseShrine = Factory.Item("Use Shrine", true);
            PingOnShrineBeforeUsing = Factory.Item("Ping on shrine", true);
            
            EnemyDodgeMode = Factory.Item("Dodge enemy heroes", true);
            EnemyDodgeRange = Factory.Item("Range", new Slider(600, 0, 2000));

            FarmLaneIsNoFreeCamps = Factory.Item("Farm lanes if there is no camps available", true);
            ItemsInBuyList = new List<AbilityId>
            {
                AbilityId.item_stout_shield,
                AbilityId.item_lifesteal,
                AbilityId.item_quarterstaff,
                AbilityId.item_javelin,
                AbilityId.item_mithril_hammer,
                AbilityId.item_flask,
                AbilityId.item_boots,
                AbilityId.item_wind_lace,
                AbilityId.item_ring_of_regen,
            };
            BuyItems = Factory.Item("Will buy",
                new AbilityToggler(ItemsInBuyList.ToDictionary(z => z.ToString(), z => true)));

            FarmType = Factory.Item("Farm ->", new StringList("Jungle", "Lanes", "Auto"));

            DrawCampStatus = Factory.Item("Draw Camp Status", false);
            DrawType = Factory.Item("Draw current Farm type", true);
            PosX = Factory.Item("pos x", new Slider(10, 0, 2000));
            PosY = Factory.Item("pos y", new Slider(110, 0, 2000));
            TextSize = Factory.Item("text size", new Slider(15, 1, 50));
        }

        public MenuItem<AbilityToggler> BuyItems { get; set; }

        public MenuItem<bool> FarmLaneIsNoFreeCamps { get; set; }

        public MenuItem<bool> PingOnShrineBeforeUsing { get; set; }

        public MenuItem<bool> UseShrine { get; set; }

        public MenuItem<Slider> EnemyDodgeRange { get; set; }

        public MenuItem<bool> EnemyDodgeMode { get; set; }

        public MenuItem<Slider> EscapeOnLowHealthHealth { get; set; }

        public MenuItem<bool> EscapeOnLowHealthMode { get; set; }

        //public MenuItem<KeyBind> HeroToggle { get; set; }

        public MenuItem<Slider> TextSize { get; set; }

        public MenuItem<Slider> PosX { get; set; }
        public MenuItem<Slider> PosY { get; set; }

        public MenuItem<bool> DrawType { get; set; }

        public MenuItem<StringList> FarmType { get; set; }

        public MenuItem<bool> DrawCampStatus { get; set; }

        public MenuItem<bool> FarmEnemyCamp { get; set; }

        public MenuItem<bool> SkipCampWithAllyHero { get; set; }

        public MenuItem<bool> FarmAncientCamps { get; set; }

        public MenuItem<KeyBind> Enable { get; set; }

        public MenuFactory Factory { get; set; }

        public void Dispose()
        {
            Factory?.Dispose();
        }
    }
}