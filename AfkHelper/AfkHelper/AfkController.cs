﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Ensage;
using Ensage.SDK.Extensions;
using Ensage.SDK.Geometry;
using Ensage.SDK.Handlers;
using Ensage.SDK.Helpers;
using Ensage.SDK.Renderer.Particle;
using SharpDX;

namespace AfkHelper
{
    public class AfkController : IDisposable
    {
        private readonly AfkHelper _main;
        private Config Config => _main.Config;
        private Hero Owner => _main.Owner;
        private bool SkipCampWithAllyHeroes => _main.Config.SkipCampWithAllyHero;
        private bool FarmEnemyCamp => _main.Config.FarmEnemyCamp;
        private bool FarmAncients => _main.Config.FarmAncientCamps;
        public FarmTypeEnum FarmType => (FarmTypeEnum) _main.Config.FarmType.Value.SelectedIndex;

        public FarmTypeEnum CurrentFarmType
        {
            get => _currentFarmType;
            set
            {
                AfkHelper.Log.Info($"Change order from {_currentFarmType} to {value}");
                if (_currentFarmType != FarmTypeEnum.NeedToRegen && _currentFarmType != FarmTypeEnum.UnderShrine &&
                    _currentFarmType != FarmTypeEnum.WillUseShrine && _currentFarmType != FarmTypeEnum.EscapeMode)
                {
                    AfkHelper.Log.Info($"OldFarmType changed from {OldFarmType} to {_currentFarmType}");
                    OldFarmType = _currentFarmType;
                }
                _currentFarmType = value;
            }
        }

        public FarmTypeEnum OldFarmType;

        public Vector3 ParticleVector3;
        public enum FarmTypeEnum
        {
            Jungle,
            Lane,
            Auto,
            NeedToRegen,
            EscapeMode,
            UnderShrine,
            WillUseShrine,
            Unknown
        }

        private bool _inStacking = false;
        private IEnumerable<Camp> Camps => _main.CampManager.Camps;
        private IEnumerable<Vector3> StackPoinits => _main.CampManager.StackPoints;
        private IParticleManager Particle => _main.Context.Particle;
        private Hero Me => _main.Owner;
        public AfkController(AfkHelper main)
        {
            _laneHelper = main.LaneHelper;
            _main = main;
            Config.Enable.PropertyChanged += EnableOnPropertyChanged;
            _subManager = UpdateManager.Subscribe(Controller, 350, Config.Enable);
            _particleUpdater = UpdateManager.Subscribe(ParticleUpdateCall, 0, Config.Enable);
            CurrentFarmType = FarmTypeEnum.Unknown;
            ParticleVector3 = new Vector3();
            _fountain = EntityManager<Building>.Entities.FirstOrDefault(x =>
                x.NetworkName == ClassId.CDOTA_Unit_Fountain.ToString() && x.Team == Me.Team);

            if (Config.EnemyDodgeMode && Config.Enable)
            {
                Particle.DrawRange(Me, "PussyRange", Config.EnemyDodgeRange, Color.DarkOrange);
            }
            Config.EnemyDodgeMode.PropertyChanged += (sender, args) =>
            {
                if (Config.EnemyDodgeMode && Config.Enable)
                {
                    Particle.DrawRange(Me, "PussyRange", Config.EnemyDodgeRange, Color.DarkOrange);
                }
                else
                {
                    Particle.Remove("PussyRange");
                }
            };

            Config.EnemyDodgeRange.PropertyChanged += (sender, args) =>
            {
                if (Config.EnemyDodgeMode && Config.Enable)
                    Particle.DrawRange(Me, "PussyRange", Config.EnemyDodgeRange, Color.DarkOrange);
            };

            Entity.OnInt32PropertyChange += (sender, args) =>
            {
                if (InDodge)
                    return;
                if (args.PropertyName != "m_iHealth" || args.NewValue >= args.OldValue)
                    return;
                if (sender.Equals(Me))
                {
                    Dodge();
                }
            };

            _main.Config.FarmType.PropertyChanged += (sender, args) =>
            {
                CurrentFarmType = FarmTypeEnum.Unknown;
            };
        }

        private void ParticleUpdateCall()
        {
            Particle.DrawTargetLine(Me, "Controller" + Me.Handle, ParticleVector3);
        }

        private void Controller()
        {
            if (Me == null || !Me.IsValid || !Me.IsAlive)
            {
                return;
            }

            if (CurrentFarmType != FarmTypeEnum.NeedToRegen &&
                CurrentFarmType != FarmTypeEnum.WillUseShrine && CurrentFarmType != FarmTypeEnum.UnderShrine && CurrentFarmType != FarmTypeEnum.EscapeMode)
            {
                if (Config.EscapeOnLowHealthMode)
                    if (Me.HealthPercent() <= Config.EscapeOnLowHealthHealth / 100f &&
                        !Me.HasAnyModifiers("modifier_flask_healing"))
                    {
                        CurrentFarmType = FarmTypeEnum.NeedToRegen;
                        return;
                    }
                if (_main.Flask != null && !Me.HasAnyModifiers(_main.Flask.TargetModifierName) &&
                    _main.Flask.CanBeCasted)
                {
                    /*AfkHelper.Log.Warn(
                        $"Me.Health: {Me.Health} <= {Me.MaximumHealth - _main.Flask.TotalHealthRestore + 150} ({Me.MaximumHealth}-{_main.Flask.TotalHealthRestore}+150)");*/
                    if (Me.Health < Me.MaximumHealth - _main.Flask.TotalHealthRestore + 150)
                    {
                        var enemyHeroes =
                            EntityManager<Hero>.Entities.Any(x =>
                                x.IsValid && x.IsAlive && x.IsEnemy(Me) && x.IsVisible && x.IsInRange(Me, 700));
                        if (!enemyHeroes)
                            _main.Flask.UseAbility(Me);
                    }
                }
            }

            var allyHeroes = EntityManager<Hero>.Entities.Where(x =>
                x.IsAlive && x.IsAlly(Owner) && !x.IsIllusion && !x.Equals(_main.Owner));

            switch (FarmType)
            {
                case FarmTypeEnum.Jungle when CurrentFarmType == FarmTypeEnum.Unknown:
                    CurrentFarmType = FarmTypeEnum.Jungle;
                    Jungle(Me,allyHeroes);
                    break;
                case FarmTypeEnum.Lane when CurrentFarmType == FarmTypeEnum.Unknown:
                    CurrentFarmType = FarmTypeEnum.Lane;
                    Lane(Me);
                    break;
                case FarmTypeEnum.Auto when CurrentFarmType == FarmTypeEnum.Unknown:
                    var isJungle = _laneHelper.IsJungle(Me.NetworkPosition);
                    if (isJungle)
                    {
                        CurrentFarmType = FarmTypeEnum.Jungle;
                        AfkHelper.Log.Warn($"Line detected -> Jungle");
                        Jungle(Me, allyHeroes);
                    }
                    else
                    {
                        CurrentFarmType = FarmTypeEnum.Lane;
                        AfkHelper.Log.Warn($"Line detected -> Lane");
                        Lane(Me);
                    }
                    break;
                default:
                    switch (CurrentFarmType)
                    {
                        case FarmTypeEnum.Jungle:
                            CheckForEnemies(Me);
                            Jungle(Me, allyHeroes);
                            break;
                        case FarmTypeEnum.Lane:
                            CheckForEnemies(Me);
                            Lane(Me);
                            break;
                        case FarmTypeEnum.NeedToRegen:
                            NeedToRegen(Me);
                            break;
                        case FarmTypeEnum.WillUseShrine:
                            CheckForEnemies(Me);
                            GoingToShrine(Me);
                            break;
                        case FarmTypeEnum.UnderShrine:
                            UnderShrine(Me);
                            break;
                        case FarmTypeEnum.EscapeMode:
                            EscapeAction();
                            break;
                        case FarmTypeEnum.Auto:
                            CurrentFarmType = FarmTypeEnum.Unknown;
                            AfkHelper.Log.Warn($"ShitHappened. From Auto -> {CurrentFarmType}");
                            break;
                        default:
                            AfkHelper.Log.Warn($"Wrong lane -> {CurrentFarmType}");
                            break;
                    }
                    break;
            }
        }
        private void CheckForEnemies(Hero me)
        {
            var enemyHero =
                EntityManager<Hero>.Entities.Any(x =>
                    x.IsValid && x.IsAlive && x.IsVisible && !x.IsIllusion && x.IsEnemy(Me) &&
                    x.IsInRange(Me, Config.EnemyDodgeRange));
            if (enemyHero)
            {
                CurrentFarmType = FarmTypeEnum.EscapeMode;
            }
        }

        private void EscapeAction()
        {
            var enemyHero =
                EntityManager<Hero>.Entities.Where(x =>
                        x.IsValid && x.IsAlive && x.IsVisible && !x.IsIllusion && x.IsEnemy(Me) &&
                        x.IsInRange(Me, Config.EnemyDodgeRange))
                    .OrderBy(x => x.Distance2D(Me)).FirstOrDefault();
            if (enemyHero != null)
            {
                var pointToEscape =
                    enemyHero.NetworkPosition.Extend(Me.NetworkPosition, Me.Distance2D(enemyHero) + 550);
                Me.Move(pointToEscape);
            }
            else
            {
                CurrentFarmType = OldFarmType;
            }
        }

        private void NeedToRegen(Hero me)
        {
            if (!me.HasModifier("modifier_fountain_aura_buff"))
            {
                if (Config.UseShrine)
                {
                    var anyGoodShrine =
                        EntityManager<Building>.Entities.Where(x =>
                                x.IsValid && x.IsAlly(Me) && x.NetworkName == ClassId.CDOTA_BaseNPC_Healer.ToString() && x.IsAlive &&
                                x.Spellbook.Spell1 != null && (CheckForSpell(x.Spellbook) || x.HasModifier("modifier_filler_heal_aura")))
                            .OrderBy(z => z.Distance2D(Me)).FirstOrDefault();
                    if (anyGoodShrine != null)
                    {
                        ParticleVector3 = anyGoodShrine.NetworkPosition;
                        if (Config.PingOnShrineBeforeUsing)
                        {
                            Network.MapPing(anyGoodShrine, PingType.Normal);
                            //anyGoodShrine.Spellbook.Spells.Find(x => x.Id == AbilityId.filler_ability).Announce();
                        }
                        CurrentFarmType = FarmTypeEnum.WillUseShrine;
                        return;
                    }
                }
                ParticleVector3 = _fountain.NetworkPosition;
                Me.Move(_fountain.NetworkPosition);
            }
            else if (Me.HealthPercent()>=0.90f)
            {
                if (OldFarmType == FarmTypeEnum.UnderShrine || OldFarmType == FarmTypeEnum.WillUseShrine ||
                    OldFarmType == FarmTypeEnum.EscapeMode)
                {
                    CurrentFarmType = FarmTypeEnum.Auto;
                    return;
                }
                CurrentFarmType = OldFarmType;
            }
        }

        private void GoingToShrine(Hero me)
        {
            var buff = Me.HasModifier("modifier_filler_heal");
            if (buff)
            {
                CurrentFarmType = FarmTypeEnum.UnderShrine;
                Me.Stop();
                return;
            }
            var anyGoodShrine =
                EntityManager<Building>.Entities.Where(x =>
                        x.IsValid && x.IsAlly(Me) && x.NetworkName == ClassId.CDOTA_BaseNPC_Healer.ToString() && x.IsAlive &&
                        x.Spellbook.Spell1 != null && (CheckForSpell(x.Spellbook) || x.HasModifier("modifier_filler_heal_aura")))
                    .OrderBy(z => z.Distance2D(Me)).FirstOrDefault();
            if (anyGoodShrine != null && me.IsAlive)
            {
                ParticleVector3 = anyGoodShrine.NetworkPosition;
                Me.Follow(anyGoodShrine);
            }
            else
            {
                CurrentFarmType = OldFarmType;
            }
        }

        private bool CheckForSpell(Spellbook spellbook)
        {
            var ability = spellbook.Spells.FirstOrDefault(x => x.Id == AbilityId.filler_ability);
            if (ability != null)
            {
                return ability.AbilityState == AbilityState.Ready || ability.Cooldown >= 295;
            }
            return false;
        }

        private void UnderShrine(Hero me)
        {
            var buff = Me.HasModifier("modifier_filler_heal");
            if (!buff)
            {
                CurrentFarmType = OldFarmType;
            }
        }

        private void Jungle(Hero hero, IEnumerable<Hero> allyHeroes)
        {
            var camp = Camps.Where(x =>
                    (x.Owners.Contains(hero)) && !x.IsEmpty &&
                    (FarmAncients || !x.IsAncient) && (FarmEnemyCamp || x.IsAlly) &&
                    (!SkipCampWithAllyHeroes || !allyHeroes.Any(z => z.Distance2D(x.Position) <= 500) ||
                     hero.Distance2D(x.Position) <= 300))
                .OrderBy(x => x.Position.Distance(hero.Position)).FirstOrDefault();
            if (camp == null)
            {
                if (true)
                {
                    camp = Camps.Where(x =>
                            !x.IsEmpty &&
                            (FarmAncients || !x.IsAncient) && (FarmEnemyCamp || x.IsAlly))
                        .OrderBy(x => x.Position.Distance(hero.Position)).FirstOrDefault();
                }
            }
            if (camp != null)
            {
                if (!camp.Owners.Contains(hero))
                {
                    camp.Owners.Add(hero);
                }
                if (!_inStacking)
                {
                    var distance = camp.Position.Distance2D(hero.Position);
                    var secs = (int) (Game.GameTime % 60);

                    if (secs >= 55)
                    {
                        hero.Stop();
                        _inStacking = true;
                        return;
                    }

                    if (distance >= 100)
                    {
                        if (distance <= 600)
                        {
                            var enemy = GetTarget(hero);
                            if (enemy != null)
                            {
                                if (_main.Mom != null && _main.Mom.CanBeCasted)
                                {
                                    _main.Mom.UseAbility();
                                }
                                ParticleVector3 = enemy.NetworkPosition;
                                if (!Ensage.Common.Extensions.UnitExtensions.IsAttacking(hero))
                                    hero.Attack(enemy);
                                return;
                            }
                        }
                        ParticleVector3 = camp.Position;
                        hero.Move(camp.Position);
                    }
                    else
                    {
                        /*var enemy = EntityManager<Unit>.Entities.FirstOrDefault(x =>
                            x.IsAlive && x.IsSpawned && x.IsVisible &&
                            x.IsEnemy(hero) &&
                            x.IsInRange(hero, 400));*/
                        var enemy = GetTarget(hero);
                        if (enemy != null)
                        {
                            if (_main.Mom != null && _main.Mom.CanBeCasted)
                            {
                                _main.Mom.UseAbility();
                            }
                            ParticleVector3 = enemy.NetworkPosition;
                            if (!Ensage.Common.Extensions.UnitExtensions.IsAttacking(hero))
                                //sys.Value.Attack(enemy);
                                hero.Attack(enemy);
                        }
                        else
                        {
                            camp.SetEmpty();
                        }
                    }
                }
                else
                {
                    var secs = (int) (Game.GameTime % 60);
                    if (secs < 50)
                    {
                        _inStacking = false;
                        return;
                    }

                    var closestPositionForStacking =
                        StackPoinits.OrderBy(hero.Distance2D).FirstOrDefault();
                    if (!closestPositionForStacking.IsZero)
                    {
                        ParticleVector3 = closestPositionForStacking;
                        hero.Move(closestPositionForStacking);
                    }
                }
            }
            else
            {
                if (!Config.FarmLaneIsNoFreeCamps)
                {
                    var shrine = EntityManager<Unit>.Entities
                        .Where(x => x.NetworkName == ClassId.CDOTA_BaseNPC_Healer.ToString() && x.Team == hero.Team &&
                                    x.IsAlive).OrderBy(x => x.Distance2D(hero)).FirstOrDefault();
                    if (shrine != null)
                    {
                        var pos = shrine.Position + new Vector3(-200, 300, 0);
                        hero.Move(pos);
                        ParticleVector3 = pos;
                    }
                }
                else
                {
                    CurrentFarmType = FarmTypeEnum.Lane;
                }
            }
        }

        public void Dodge()
        {
            if (CurrentFarmType != FarmTypeEnum.Lane)
                return;
            var someAlly = EntityManager<Creep>.Entities.FirstOrDefault(x => x.IsValid && x.IsAlly(Owner) && x.IsAlive && x.IsInRange(Owner, 500));
            if (someAlly != null)
            {
                InDodge = true;
                Owner.Attack(someAlly);
                Owner.Move(Owner.NetworkPosition.Extend(someAlly.NetworkPosition, someAlly.Distance2D(Owner) + 200));
                Owner.Attack(someAlly, true);
                UpdateManager.BeginInvoke(() =>
                {
                    Owner.Attack(someAlly);
                    InDodge = false;
                }, 750);
            }
        }

        public bool InDodge { get; set; }

        private void Lane(Hero hero)
        {
            if (InDodge)
                return;
            var path = _laneHelper.GetPathCache(hero);
            var lastPoint = path[path.Count - 1];
            var closestPosition = path.Where(
                    x =>
                        x.Distance(lastPoint) < hero.Position.Distance(lastPoint) - 300)
                .OrderBy(pos => pos.Distance(hero.Position))
                .FirstOrDefault();

            var closestTower = EntityManager<Tower>.Entities.Where(x =>
                    x.IsValid && x.IsAlive && x.IsEnemy(Me) && x.IsInRange(Me, x.AttackRange + 200f))
                .OrderBy(z => z.Distance2D(Me)).FirstOrDefault();

            if (closestTower != null)
            {
                var anyAllyCreep = EntityManager<Creep>.Entities.Any(x =>
                    x.IsValid && x.IsAlive && x.IsAlly(Me) && x.Distance2D(closestTower) < Me.Distance2D(closestTower));
                if (!anyAllyCreep)
                {
                    var pos = closestTower.Position.Extend(Me.Position, Me.Distance2D(closestTower) + 300);
                    Me.Move(pos);
                    return;
                }
            }

            var creep = GetTarget(hero);
            if (creep != null)
            {
                if (_main.Mom != null && _main.Mom.CanBeCasted)
                {
                    _main.Mom.UseAbility();
                }
                ParticleVector3 = creep.NetworkPosition;
                if (!Ensage.Common.Extensions.UnitExtensions.IsAttacking(hero))
                    hero.Attack(creep);
            }
            else
            {
                ParticleVector3 = closestPosition;
                hero.Attack(closestPosition);
            }
        }

        private Unit GetTarget(Hero hero)
        {
            var barracks =
                ObjectManager.GetEntitiesFast<Building>()
                    .FirstOrDefault(
                        unit =>
                            unit.IsValid && unit.IsAlive && unit.Team != hero.Team && !(unit is Tower) &&
                            hero.IsValidOrbwalkingTarget(unit) && hero.IsVisible && hero.Health>0
                            && unit.Name != "portrait_world_unit");

            if (barracks != null)
            {
                return barracks;
            }

            var creep =
                EntityManager<Creep>.Entities.Where(
                        unit =>
                            unit.IsValid && unit.IsSpawned && unit.IsAlive && unit.Team != hero.Team &&
                            (hero.IsValidOrbwalkingTarget(unit) || hero.Distance2D(unit) <= 500))
                    .OrderBy(x => x.Health)
                    .FirstOrDefault();

            if (creep != null)
            {
                return creep;
            }

            var tower =
                ObjectManager.GetEntitiesFast<Tower>()
                    .FirstOrDefault(
                        unit =>
                            unit.IsValid && unit.IsAlive && unit.Team != hero.Team &&
                            hero.IsValidOrbwalkingTarget(unit));

            if (tower != null)
            {
                return tower;
            }

            var jungleMob =
                EntityManager<Creep>.Entities.FirstOrDefault(
                    unit =>
                        unit.IsValid && unit.IsSpawned && unit.IsAlive && unit.IsNeutral &&
                        hero.IsValidOrbwalkingTarget(unit));

            if (jungleMob != null)
            {
                return jungleMob;
            }

            var others =
                ObjectManager.GetEntitiesFast<Unit>()
                    .FirstOrDefault(
                        unit =>
                            unit.IsValid && !(unit is Hero) && !(unit is Creep) && unit.IsAlive &&
                            !unit.IsInvulnerable() && unit.Team != hero.Team &&
                            hero.IsValidOrbwalkingTarget(unit) &&
                            unit.NetworkName != ClassId.CDOTA_BaseNPC.ToString());

            if (others != null)
            {
                return others;
            }

            var heroes =
                ObjectManager.GetEntitiesFast<Hero>()
                    .FirstOrDefault(
                        unit =>
                            unit.IsValid && unit.IsAlive &&
                            !unit.IsInvulnerable() && unit.Team != hero.Team &&
                            hero.IsValidOrbwalkingTarget(unit) &&
                            unit.NetworkName != ClassId.CDOTA_BaseNPC.ToString());

            if (heroes != null)
            {
                return heroes;
            }

            return null;
        }


        private void EnableOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (Config.Enable)
            {
                //EntityManager<Hero>.EntityAdded += EntityManagerOnEntityAdded;
                //EntityManager<Hero>.EntityRemoved += EntityManagerOnEntityRemoved;
                Particle.Remove("Controller" + Me.Handle);
                CurrentFarmType = FarmTypeEnum.Unknown;
                _subManager.IsEnabled = true;
                _particleUpdater.IsEnabled = true;
                Particle.DrawRange(Me, "PussyRange", Config.EnemyDodgeRange, Color.DarkOrange);
            }
            else
            {
                //EntityManager<Hero>.EntityAdded -= EntityManagerOnEntityAdded;
                //EntityManager<Hero>.EntityRemoved -= EntityManagerOnEntityRemoved;
                _subManager.IsEnabled = false;
                _particleUpdater.IsEnabled = false;
                Particle.Remove("Controller" + Me.Handle);
                CurrentFarmType = FarmTypeEnum.Unknown;
                Particle.Remove("PussyRange");
                foreach (var camp in Camps)
                {
                    camp.Owners.Clear();
                }
            }
        }

        private readonly IUpdateHandler _subManager;
        private readonly LaneHelper _laneHelper;
        private readonly IUpdateHandler _particleUpdater;
        private readonly Building _fountain;
        private FarmTypeEnum _currentFarmType;

        public void Dispose()
        {
            _subManager.IsEnabled = false;
        }
    }
}