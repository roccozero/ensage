﻿using System.Collections.Generic;
using Ensage;
using SharpDX;

namespace AfkHelper
{
    public class Camp
    {
        public Vector3 Position { get; set; }
        public bool IsEmpty { get; set; }
        public bool IsAncient { get; set; }
        public bool IsAlly { get; set; }
        public List<Hero> Owners { get; set; }
        public static int StaticId { get => ++_staticId; set => _staticId = value; }
        private static int _staticId;
        public int Id;

        public Camp(Vector3 position, bool ancient = false, Team team = Team.Radiant)
        {
            Position = position;
            IsEmpty = false;
            IsAncient = ancient;
            IsAlly = ObjectManager.LocalPlayer.Team == team;
            Owners = new List<Hero>();
            Id = StaticId;
        }

        public void SetEmpty()
        {
            Owners.Clear();
            IsEmpty = true;
        }

        public void Refresh()
        {
            Owners.Clear();
            IsEmpty = false;
        }
    }
}