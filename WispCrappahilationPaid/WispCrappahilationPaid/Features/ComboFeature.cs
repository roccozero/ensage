﻿using System;
using System.Linq;
using Ensage;
using Ensage.SDK.Abilities.npc_dota_hero_wisp;
using Ensage.SDK.Extensions;
using Ensage.SDK.Handlers;
using Ensage.SDK.Helpers;
using Ensage.SDK.Menu;

namespace WispCrappahilationPaid.Features
{
    public class ComboFeature
    {
        private readonly WispCrappahilationPaid _main;
        private IUpdateHandler _handle;
        private Hero Me => _main.Me;

        public ComboFeature(WispCrappahilationPaid main)
        {
            _main = main;
            _handle = UpdateManager.Subscribe(Updater, 10, false);
            main.Menu.OrderConfig.ComboKey.Action += args =>
            {
                var isActive = (args.Flag & HotkeyFlags.Down) != 0;
                _handle.IsEnabled = isActive;
                if (!isActive)
                    Abilities.Overcharge.Enabled = false;
            };
        }

        private void Updater()
        {
            if (!GetTarget())
                return;
            if (Me.IsValidOrbwalkingTarget(Target, 150))
            {
                if (Abilities.Overcharge.CanBeCasted)
                {
                    Abilities.Overcharge.Enabled = true;
                }
                if (Abilities.Spirits.CanBeCasted)
                {
                    Abilities.Spirits.UseAbility();
                }
            }
            _main.Context.Orbwalker.Active.OrbwalkTo(Target);
        }

        public AbilitiesInCombo Abilities => _main.AbilitiesInCombo;
        public Unit Target;
        public float CurrentRange = 150;
        public bool GetTarget()
        {
            if (Target != null && Target.IsValid && Target.IsAlive) return true;
            Target = _main.Context.TargetSelector.Active.GetTargets().FirstOrDefault();
            return false;
        }
    }
}