﻿using System;
using System.Linq;
using Ensage;
using Ensage.Common.Objects.UtilityObjects;
using Ensage.SDK.Extensions;
using Ensage.SDK.Handlers;
using Ensage.SDK.Helpers;
using Ensage.SDK.Menu;
using Ensage.SDK.Renderer.Particle;
using SharpDX;

namespace WispCrappahilationPaid.Features
{
    public class SpiritAimFeature
    {
        private readonly WispCrappahilationPaid _main;
        private readonly IUpdateHandler _aimUpdateHandler;
        public SpiritAimFeature(WispCrappahilationPaid main)
        {
            _main = main;
            _sleeper = new Sleeper();
            _aimUpdateHandler = UpdateManager.Subscribe(ParticleUpdater, isEnabled: false);
            main.Menu.OrderConfig.AimKey.Action += args =>
            {
                if (args.Flag == HotkeyFlags.Down)
                {
                    if (_aimUpdateHandler.IsEnabled)
                    {
                        _aimUpdateHandler.IsEnabled = false;
                        AimTarget = null;
                        _main.Context.Particle.Remove("wispSpiritsRange");
                    }
                    else
                    {
                        AimTarget = null;
                        _aimUpdateHandler.IsEnabled = true;
                    }
                }
            };

            main.Context.Renderer.Draw += (sender, args) =>
            {
                main.Context.Renderer.DrawText(new Vector2(150, 150),
                    $"{_aimUpdateHandler.IsEnabled} {AimTarget?.ClassId} Dist: {GetRealDist} Up? {_main.AbilitiesInCombo.Toggler.IsGoingUp}",
                    System.Drawing.Color.White);
            };
        }

        private void ParticleUpdater()
        {
            if (GetAimTarget())
            {
                var anySpirit = _main.SpiritsTracker.Spirits.FirstOrDefault();
                if (anySpirit != null)
                {
                    CurrentRange = anySpirit.Me.Distance2D(_main.Me);
                    var distance = _main.Me.Distance2D(AimTarget);
                    var realDist = Math.Abs(CurrentRange - distance);
                    GetRealDist = realDist;
                    if (!_sleeper.Sleeping)
                        if (CurrentRange < distance)
                        {
                            if (_main.AbilitiesInCombo.Toggler.Up())
                            {
                                _sleeper.Sleep(1500);
                                WispCrappahilationPaid.Log.Debug($"Go up");
                            }
                        }
                        else
                        {
                            if (_main.AbilitiesInCombo.Toggler.Down())
                            {
                                _sleeper.Sleep(1500);
                                WispCrappahilationPaid.Log.Debug($"Go down");
                            }
                        }
                }
            }
            _main.Context.Particle.AddOrUpdate(_main.Me, "wispSpiritsRange",
                "materials/ensage_ui/particles/range_display_mod.vpcf", ParticleAttachment.AbsOriginFollow, RestartType.FullRestart, 1,
                CurrentRange * 1.1f, 2, Color.Aqua);
        }

        public float GetRealDist { get; set; }

        public Unit AimTarget;
        public float CurrentRange = 150;
        private Sleeper _sleeper;

        public bool GetAimTarget()
        {
            if (AimTarget != null && AimTarget.IsValid && AimTarget.IsAlive) return true;
            AimTarget = _main.Context.TargetSelector.Active.GetTargets().FirstOrDefault();
            return false;
        }
    }
}