﻿using System.Linq;
using Ensage;
using Ensage.SDK.Abilities.npc_dota_hero_wisp;
using Ensage.SDK.Extensions;
using Ensage.SDK.Handlers;
using Ensage.SDK.Helpers;
using SharpDX;

namespace WispCrappahilationPaid.Features
{
    public class DrawingsFeature
    {
        private readonly WispCrappahilationPaid _main;
        private readonly IUpdateHandler _handle;
        private Hero Me => _main.Me;

        public DrawingsFeature(WispCrappahilationPaid main)
        {
            _main = main;
            _handle = UpdateManager.Subscribe(Updater, 100, main.Menu.AutoTether.Enable.Value);
            main.Menu.Drawings.TetherBreakRange.Changed += (sender, args) =>
            {
                _handle.IsEnabled = args.Value;
                if (!args.Value)
                {
                    _main.Context.Particle.Remove($"TetherRange");
                }
            };
        }

        private wisp_tether Tether => _main.AbilitiesInCombo.Tether;
        private void Updater()
        {
            if (Tether.Ability.IsHidden)
            {
                var hero = EntityManager<Hero>.Entities.FirstOrDefault(x =>
                    x.IsAlly(Me) && x.HasAnyModifiers(Tether.TargetModifierName) &&
                    x.HeroId != HeroId.npc_dota_hero_wisp);
                if (hero != null)
                {
                    _main.Context.Particle.DrawRange(hero, $"TetherRange", 900, Color.OrangeRed);
                }
            }
            else
            {
                _main.Context.Particle.Remove($"TetherRange");
            }
        }
    }
}