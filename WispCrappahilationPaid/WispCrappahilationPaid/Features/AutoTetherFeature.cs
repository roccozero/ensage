﻿using System;
using System.Linq;
using Ensage;
using Ensage.SDK.Abilities.npc_dota_hero_wisp;
using Ensage.SDK.Extensions;
using Ensage.SDK.Handlers;
using Ensage.SDK.Helpers;

namespace WispCrappahilationPaid.Features
{
    public class AutoTetherFeature
    {
        private readonly WispCrappahilationPaid _main;
        private IUpdateHandler _handle;
        private Hero Me => _main.Me;

        public AutoTetherFeature(WispCrappahilationPaid main)
        {
            _main = main;
            _handle = UpdateManager.Subscribe(Updater, 100, main.Menu.AutoTether.Enable.Value);
            main.Menu.AutoTether.Enable.Changed += (sender, args) =>
            {
                _handle.IsEnabled = args.Value;
            };
        }

        private wisp_tether Tether => _main.AbilitiesInCombo.Tether;
        private void Updater()
        {
            if (Tether.CanBeCasted && !Tether.Ability.IsHidden)
            {
                var hero = EntityManager<Hero>.Entities.FirstOrDefault(x => x.IsAlly(Me) &&
                                                                            _main.Menu.AutoTether.Toggle.PictureStates
                                                                                .ContainsKey(x.HeroId.ToString()) &&
                                                                            _main.Menu.AutoTether.Toggle.PictureStates[
                                                                                x.HeroId.ToString()] &&
                                                                            Tether.CanHit(x));
                if (hero != null)
                {
                    Tether.UseAbility(hero);
                }
            }
        }
    }
}