﻿using Ensage;
using Ensage.SDK.Abilities;

namespace WispCrappahilationPaid.CustomAbilities
{
    // ReSharper disable once InconsistentNaming
    public class wisp_toggler : ActiveAbility
    {
        public wisp_toggler(Ability ability) : base(ability)
        {
            Unit.OnModifierAdded += (sender, args) =>
            {
                if (args.Modifier.Name == "modifier_wisp_spirits")
                {
                    IsGoingUp = true;
                }
            };
            /*Unit.OnModifierRemoved += (sender, args) =>
            {
                if (args.Modifier.Name == "modifier_wisp_spirits")
                {
                    IsGoingUp = false;
                }
            };*/
        }

        public bool IsGoingUp = true;

        public override bool UseAbility()
        {
            if (!CanBeCasted) return false;
            var casted = base.UseAbility();
            if (!casted) return false;
            WispCrappahilationPaid.Log.Debug($"Casted. State -> {(!IsGoingUp ? "Up" : "Down")}");
            IsGoingUp = !IsGoingUp;
            return true;
        }

        public bool Up()
        {
            return !IsGoingUp && UseAbility();
        }
        public bool Down()
        {
            return IsGoingUp && UseAbility();
        }
    }
}