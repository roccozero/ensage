﻿using Ensage;
using Ensage.Common.Extensions;
using Ensage.SDK.Abilities.npc_dota_hero_wisp;
using WispCrappahilationPaid.CustomAbilities;

namespace WispCrappahilationPaid
{
    public class AbilitiesInCombo
    {
        private readonly WispCrappahilationPaid _main;

        public AbilitiesInCombo(WispCrappahilationPaid main)
        {
            _main = main;
            var context = main.Context;
            Tether = context.AbilityFactory.GetAbility<wisp_tether>();
            Spirits = context.AbilityFactory.GetAbility<wisp_spirits>();
            Toggler = new wisp_toggler(context.Owner.GetAbilityById(AbilityId.wisp_spirits_in));
            //SpiritsIn = context.AbilityFactory.GetAbility<wisp_spirits_in>();
            //SpiritsOut = context.AbilityFactory.GetAbility<wisp_spirits_in>();
            Relocate = context.AbilityFactory.GetAbility<wisp_relocate>();
            Overcharge = context.AbilityFactory.GetAbility<wisp_overcharge>();
        }

        public wisp_toggler Toggler { get; set; }

        public wisp_overcharge Overcharge { get; set; }

        //public wisp_spirits_in SpiritsOut { get; set; }

        public wisp_relocate Relocate { get; set; }

        //public wisp_spirits_in SpiritsIn { get; set; }

        public wisp_spirits Spirits { get; set; }

        public wisp_tether Tether { get; set; }
    }

    // ReSharper disable once InconsistentNaming
}