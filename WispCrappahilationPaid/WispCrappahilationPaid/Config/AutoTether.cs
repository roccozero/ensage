﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Ensage;
using Ensage.Common.Extensions;
using Ensage.SDK.Helpers;
using Ensage.SDK.Menu;
using Ensage.SDK.Menu.Items;
using Ensage.SDK.Menu.ValueBinding;
using Newtonsoft.Json;

namespace WispCrappahilationPaid.Config
{
    public class AutoTether
    {
        [JsonIgnore]
        private readonly WispCrappahilationPaid _main;
        [JsonIgnore]
        private readonly List<HeroId> _heroes;
        public AutoTether(WispCrappahilationPaid main)
        {
            _main = main;
            _heroes = new List<HeroId>();
            Toggle = new ImageToggler();
            foreach (var heroId in EntityManager<Hero>.Entities.Where(z => z.Team == main.Me.Team)
                .Select(x => x.HeroId))
            {
                if (_heroes.Contains(heroId) || heroId == HeroId.npc_dota_hero_wisp)
                    continue;
                TextureHelper.LoadHeroTexture(heroId);
                Toggle.AddImage(true, heroId.ToString());
            }

            EntityManager<Hero>.EntityAdded += (sender, hero) =>
            {
                var heroId = hero.HeroId;
                if (_heroes.Contains(heroId) || hero.Team != main.Me.Team || heroId == HeroId.npc_dota_hero_wisp)
                    return;
                TextureHelper.LoadHeroTexture(heroId);
                Toggle.AddImage(false, heroId.ToString());
            };
        }


        [Item("Enable")]
        public ValueType<bool> Enable { get; set; } = new ValueType<bool>();
        [Item("Targets")]
        public ImageToggler Toggle { get; set; }
    }
}