﻿using Ensage.SDK.Menu;
using Newtonsoft.Json;

namespace WispCrappahilationPaid.Config
{
    [Menu("Wisp Crappahilation")]
    public class BaseMenu
    {
        public BaseMenu(WispCrappahilationPaid main)
        {
            Main = main;

            OrderConfig = new OrderConfig(main);
            AutoTether = new AutoTether(main);
            Drawings = new Drawings(main);
        }
        [JsonIgnore]
        public WispCrappahilationPaid Main { get; set; }

        [Menu("Hotkeys")]
        public OrderConfig OrderConfig { get; }
        [Menu("Auto Tether")]
        public AutoTether AutoTether { get; }
        [Menu("Drawings")]
        public Drawings Drawings { get; }

    }
}
