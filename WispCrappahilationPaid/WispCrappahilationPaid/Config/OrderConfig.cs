﻿using System.Windows.Input;
using Ensage.SDK.Menu;
using Ensage.SDK.Menu.Items;
using Newtonsoft.Json;

namespace WispCrappahilationPaid.Config
{
    public class OrderConfig
    {
        [JsonIgnore]
        private readonly WispCrappahilationPaid _main;

        public OrderConfig(WispCrappahilationPaid main)
        {
            _main = main;
        }

        [Item("Combo key")]
        public HotkeySelector ComboKey { get; set; } = new HotkeySelector(Key.None, HotkeyFlags.Up | HotkeyFlags.Down);
        [Item("Aim key")]
        public HotkeySelector AimKey { get; set; } = new HotkeySelector(Key.None, HotkeyFlags.Up | HotkeyFlags.Down);
    }
}