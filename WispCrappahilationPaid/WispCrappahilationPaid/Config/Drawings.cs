﻿using Ensage.SDK.Menu;
using Ensage.SDK.Menu.ValueBinding;
using Newtonsoft.Json;

namespace WispCrappahilationPaid.Config
{
    public class Drawings
    {
        [JsonIgnore]
        private readonly WispCrappahilationPaid _main;

        public Drawings(WispCrappahilationPaid main)
        {
            _main = main;
        }

        [Item("Draw tether break range on target")]
        public ValueType<bool> TetherBreakRange { get; set; } = new ValueType<bool>();
    }
}