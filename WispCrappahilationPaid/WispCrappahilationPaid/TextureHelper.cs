﻿using System;
using System.Collections.Generic;
using Ensage;
using Ensage.SDK.Renderer;
using Ensage.SDK.Service;

namespace WispCrappahilationPaid
{
    public static class TextureHelper
    {
        public static string GamePath = Game.GamePath;
        private static readonly List<string> LoadedList = new List<string>();
        public static void Init(IServiceContext conext)
        {
            Render = conext.TextureManager;
        }

        private static ITextureManager Render { get; set; }

        public static void LoadAbilityTexture(AbilityId id)
        {
            if (LoadedList.Contains(id.ToString()))
                return;
            LoadedList.Add(id.ToString());
            WispCrappahilationPaid.Log.Debug($"Load from dota ability texture: {id}");
            Render.LoadFromDota($"{id}", $@"resource\flash3\images\spellicons\{id}.png");
        }

        public static void LoadItemTexture(AbilityId id)
        {
            if (LoadedList.Contains(id.ToString()))
            {
                WispCrappahilationPaid.Log.Debug($"Already Loaded from dota item texture: {id}");
                return;
            }
            LoadedList.Add(id.ToString());
            var itemString = id.ToString().Remove(0, 5);
            WispCrappahilationPaid.Log.Debug($"Load from dota item texture: {itemString}");
            Render.LoadFromDota($"{id}", $@"resource\flash3\images\items\{itemString}.png");
        }
        /// <summary>
        /// load horizontal hero texure
        /// </summary>
        /// <param name="id"></param>
        public static void LoadHeroTexture(HeroId id)
        {
            if (LoadedList.Contains(id.ToString()))
                return;
            LoadedList.Add(id.ToString());
            var itemString = id.ToString().Remove(0, 14);
            WispCrappahilationPaid.Log.Debug($"Load from dota hero[horizontal] texture: {id} TextureKey: [{itemString}]");
            Render.LoadFromFile(id.ToString(),
                $@"{GamePath}\game\dota\materials\ensage_ui\miniheroes\png\{itemString}.png");
        }

        public static void LoadAbilityTexture(params AbilityId[] id)
        {
            foreach (var abilityId in id)
            {
                LoadAbilityTexture(abilityId);
            }
        }


        public static void LoadItemTexture(params AbilityId[] id)
        {
            foreach (var abilityId in id)
            {
                LoadItemTexture(abilityId);
            }
        }

        public static void LoadHeroTexture(params HeroId[] id)
        {
            foreach (var abilityId in id)
            {
                LoadHeroTexture(abilityId);
            }
        }
    }
}