﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ensage;
using Ensage.SDK.Helpers;
using SharpDX;

namespace WispCrappahilationPaid
{
    public class SpiritsTracker
    {
        private readonly WispCrappahilationPaid _main;

        public class Spirit
        {
            public Entity Me;
            public ParticleEffect Effect;
            private Vector3 _oldPos;
            public Spirit(Entity me, ParticleEffect effect)
            {
                Me = me;
                Effect = effect;
                _oldPos = Me.Position;
            }

            public bool IsAlive()
            {
                if (Math.Abs(_oldPos.X - Me.Position.X) < 0.1) return false;
                _oldPos = Me.Position;
                return true;
            }
        }

        public List<Spirit> Spirits;
        public SpiritsTracker(WispCrappahilationPaid main)
        {
            _main = main;
            Spirits = new List<Spirit>();

            Entity.OnParticleEffectAdded += (sender, args) =>
            {
                if (args.Name == "particles/units/heroes/hero_wisp/wisp_guardian.vpcf")
                {
                    var spirit = new Spirit(sender, args.ParticleEffect);
                    Spirits.Add(spirit);
                    UpdateManager.BeginInvoke(async () =>
                    {
                        while (spirit.IsAlive() || Game.IsPaused)
                        {
                            await Task.Delay(100);
                        }
                        Spirits.Remove(spirit);
                    }, 50);
                }
            };
        }
    }
}