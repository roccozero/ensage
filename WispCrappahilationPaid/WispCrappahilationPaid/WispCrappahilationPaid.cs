﻿using System.ComponentModel.Composition;
using System.Linq;
using Ensage;
using Ensage.SDK.Service;
using Ensage.SDK.Service.Metadata;
using NLog;
using WispCrappahilationPaid.Config;
using WispCrappahilationPaid.Features;

namespace WispCrappahilationPaid
{
    [ExportPlugin(
        mode: StartupMode.Auto,
        name: "WispCrappahilationPaid",
        units: new[] {  HeroId.npc_dota_hero_wisp })]
    public sealed class WispCrappahilationPaid : Plugin
    {
        public static readonly Logger Log = LogManager.GetCurrentClassLogger();

        [ImportingConstructor]
        public WispCrappahilationPaid([Import] IServiceContext context)
        {
            Context = context;
        }

        public IServiceContext Context { get; }
        public SpiritsTracker SpiritsTracker { get; private set; }
        public AbilitiesInCombo AbilitiesInCombo { get; private set; }
        public SpiritAimFeature SpiritAim { get; private set; }
        public AutoTetherFeature AutoTetherFeature { get; private set; }
        public ComboFeature ComboFeature { get; private set; }
        public DrawingsFeature DrawingsFeature { get; private set; }

        protected override void OnActivate()
        {
            TextureHelper.Init(Context);

            Me = (Hero)Context.Owner;
            Menu = new BaseMenu(this);
            Context.MenuManager.RegisterMenu(Menu);
            SpiritsTracker = new SpiritsTracker(this);
            AbilitiesInCombo = new AbilitiesInCombo(this);
            SpiritAim = new SpiritAimFeature(this);
            AutoTetherFeature = new AutoTetherFeature(this);
            ComboFeature = new ComboFeature(this);
            DrawingsFeature = new DrawingsFeature(this);
        }

        public Hero Me { get; set; }

        public BaseMenu Menu { get; set; }

        public Unit Target;
        public bool GetTarget()
        {
            if (Target != null && Target.IsValid && Target.IsAlive) return true;
            Target = Context.TargetSelector.Active.GetTargets().FirstOrDefault();
            return false;
        }
    }
}