﻿using System.Linq;
using Ensage;
using Ensage.SDK.Extensions;
using Ensage.SDK.Helpers;

namespace IllusionAutoFarmer.Units
{
    public class IllusionUnit : ControlableUnit, ICanDodgeLikeHero
    {
        public IllusionUnit(Unit owner, IllusionController controller) : base(owner, controller)
        {
        }

        public void Dodge()
        {
            if (FarmType == IllusionController.FarmTypeEnum.Jungle || _laneHelper.GetLane(Owner) == MapArea.Jungle)
                return;
            var someAlly = EntityManager<Creep>.Entities.FirstOrDefault(x => x.IsValid && x.IsAlly(Owner) && x.IsAlive && x.IsInRange(Owner, 500));
            if (someAlly != null)
            {
                InDodge = true;
                _main.InDodge.Add(this);
                Owner.Attack(someAlly);
                Owner.Move(Owner.NetworkPosition.Extend(someAlly.NetworkPosition, someAlly.Distance2D(Owner) + 200));
                Owner.Attack(someAlly, true);
                UpdateManager.BeginInvoke(() =>
                {
                    Owner.Attack(someAlly);
                    _main.InDodge.Remove(this);
                    InDodge = false;
                }, 750);
            }
        }
    }
}