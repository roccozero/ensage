﻿using System.Collections.Generic;
using Ensage;

namespace IllusionAutoFarmer.Units
{
    public interface IHaveSomeActions
    {
        void JungleFarm(IEnumerable<Hero> allyHeroes);
        void LaneFarm();
    }
}