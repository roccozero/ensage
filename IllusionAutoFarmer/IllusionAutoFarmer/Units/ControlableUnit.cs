﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ensage;
using Ensage.SDK.Extensions;
using Ensage.SDK.Helpers;
using Ensage.SDK.Renderer.Particle;
using PlaySharp.Toolkit.Helper.Annotations;
using SharpDX;

namespace IllusionAutoFarmer.Units
{
    public abstract class ControlableUnit : IHaveSomeActions
    {
        public Unit Owner;
        public IParticleManager Particle => _main.Particle;
        public IllusionController.FarmTypeEnum FarmType;
        public string Id;
        public LaneHelper _laneHelper => _main._laneHelper;
        public IllusionController _main;
        public bool InDodge = false;

        protected ControlableUnit(Unit owner, IllusionController main)
        {
            Owner = owner;
            _main = main;
            FarmType = IllusionController.FarmTypeEnum.Unknown;
            Id = "IllusionController" + Owner.Handle;
            IllusionAutoFarmer.Log.Warn($"New {this} unit. ({owner.Name} | {owner.NetworkName})");
        }


        public void JungleFarm(IEnumerable<Hero> allyHeroes)
        {
            var camp = _main.Camps.Where(x =>
                    (x.Owners.Count < _main.IllusionsOnCamp || this is JustCreep || this is Eidalon ||
                     x.Owners.Contains(Owner)) && !x.IsEmpty &&
                    (_main.FarmAncients || !x.IsAncient) && (_main.FarmEnemyCamp || x.IsAlly) &&
                    (!_main.SkipCampWithAllyHeroes || !allyHeroes.Any(z => z.Distance2D(x.Position) <= 500) ||
                     Owner.Distance2D(x.Position) <= 300))
                .OrderBy(x => x.Position.Distance(Owner.Position)).FirstOrDefault();
            if (camp == null)
            {
                if (true)
                {
                    camp = _main.Camps.Where(x =>
                            !x.IsEmpty &&
                            (_main.FarmAncients || !x.IsAncient) && (_main.FarmEnemyCamp || x.IsAlly))
                        .OrderBy(x => x.Position.Distance(Owner.Position)).FirstOrDefault();
                }
            }
            if (camp != null)
            {
                if (!camp.Owners.Contains(Owner))
                {
                    camp.Owners.Add(Owner);
                }
                if (!_main.InStacking)
                {
                    var distance = camp.Position.Distance(Owner.Position);
                    var secs = (int)(Game.GameTime % 60);

                    if (secs >= 55)
                    {
                        Owner.Stop();
                        _main.InStacking = true;
                        return;
                    }

                    if (distance >= 200)
                    {
                        if (distance <= 1000)
                        {
                            var enemy = GetTarget(Owner);
                            if (enemy != null)
                            {
                                //IllusionAutoFarmer.Log.Warn($"attacking {enemy.Name} {enemy.NetworkName} {enemy.Health} isAttack?: {Ensage.Common.Extensions.UnitExtensions.IsAttacking(Owner)}");
                                Particle.DrawTargetLine(Owner, Id, enemy.Position);
                                if (!Ensage.Common.Extensions.UnitExtensions.IsAttacking(Owner))
                                    Owner.Attack(enemy);
                                return;
                            }
                        }
                        Particle.DrawTargetLine(Owner, Id, camp.Position);
                        Owner.Move(camp.Position);
                    }
                    else
                    {
                        /*var enemy = EntityManager<Unit>.Entities.FirstOrDefault(x =>
                            x.IsAlive && x.IsSpawned && x.IsVisible &&
                            x.IsEnemy(hero) &&
                            x.IsInRange(hero, 400));*/
                        var enemy = GetTarget(Owner);
                        if (enemy != null)
                        {
                            Particle.DrawTargetLine(Owner, Id, enemy.Position);
                            if (!Ensage.Common.Extensions.UnitExtensions.IsAttacking(Owner))
                                //sys.Value.Attack(enemy);
                                Owner.Attack(enemy);
                        }
                        else
                        {
                            camp.SetEmpty();
                        }
                    }
                }
                else
                {
                    var secs = (int)(Game.GameTime % 60);
                    if (secs < 50)
                    {
                        _main.InStacking = false;
                        return;
                    }

                    var closestPositionForStacking =
                        _main.StackPoints.OrderBy(x => Owner.Distance2D(x)).FirstOrDefault();
                    if (!closestPositionForStacking.IsZero)
                    {
                        Particle.DrawTargetLine(Owner, Id, closestPositionForStacking);
                        Owner.Move(closestPositionForStacking);
                    }
                }
            }
            else
            {
                var shrine = EntityManager<Unit>.Entities
                    .Where(x => x.NetworkName == ClassId.CDOTA_BaseNPC_Healer.ToString() && x.Team == Owner.Team &&
                                x.IsAlive).OrderBy(x => x.Distance2D(Owner)).FirstOrDefault();
                if (shrine != null)
                {
                    var pos = shrine.Position + new Vector3(-200, 300, 0);
                    Owner.Move(pos);
                    Particle.DrawTargetLine(Owner, Id, pos);
                }
            }
        }

        public void LaneFarm()
        {
            var path = _laneHelper.GetPathCache(Owner);
            var lastPoint = path[path.Count - 1];
            var closestPosition = path.Where(
                    x =>
                        x.Distance(lastPoint) < Owner.Position.Distance(lastPoint) - 300)
                .OrderBy(pos => pos.Distance(Owner.Position))
                .FirstOrDefault();
            var creep = GetTarget(Owner);
            if (creep != null)
            {
                Particle.DrawTargetLine(Owner, Id, creep.NetworkPosition);
                if (!Ensage.Common.Extensions.UnitExtensions.IsAttacking(Owner))
                {
                    Owner.Attack(creep);
                }
            }
            else
            {
                Particle.DrawTargetLine(Owner, Id, closestPosition);
                Owner.Attack(closestPosition);
            }
        }

        private Unit GetTarget(Unit hero)
        {
            var barracks =
                ObjectManager.GetEntitiesFast<Building>()
                    .FirstOrDefault(
                        unit =>
                            unit.IsValid && unit.IsAlive && unit.Team != hero.Team && !(unit is Tower) &&
                            hero.IsValidOrbwalkingTarget(unit)
                            && unit.Name != "portrait_world_unit");

            if (barracks != null)
            {
                return barracks;
            }

            var creep =
                EntityManager<Creep>.Entities.Where(
                        unit =>
                            unit.IsValid && unit.IsSpawned && unit.IsAlive && unit.Team != hero.Team &&
                            (hero.IsValidOrbwalkingTarget(unit) || hero.Distance2D(unit) <= 500))
                    .OrderBy(x => x.Health)
                    .FirstOrDefault();

            if (creep != null)
            {
                return creep;
            }

            var tower =
                ObjectManager.GetEntitiesFast<Tower>()
                    .FirstOrDefault(
                        unit =>
                            unit.IsValid && unit.IsAlive && unit.Team != hero.Team &&
                            hero.IsValidOrbwalkingTarget(unit));

            if (tower != null)
            {
                return tower;
            }

            var jungleMob =
                EntityManager<Creep>.Entities.FirstOrDefault(
                    unit =>
                        unit.IsValid && unit.IsSpawned && unit.IsAlive && unit.Team != hero.Team && unit.IsNeutral &&
                        hero.IsValidOrbwalkingTarget(unit));

            if (jungleMob != null)
            {
                return jungleMob;
            }

            var others =
                ObjectManager.GetEntitiesFast<Unit>()
                    .FirstOrDefault(
                        unit =>
                            unit.IsValid && !(unit is Hero) && !(unit is Creep) && unit.IsAlive &&
                            !unit.IsInvulnerable() && unit.Team != hero.Team &&
                            hero.IsValidOrbwalkingTarget(unit) &&
                            unit.NetworkName != ClassId.CDOTA_BaseNPC.ToString());

            if (others != null)
            {
                return others;
            }

            var heroes =
                ObjectManager.GetEntitiesFast<Hero>()
                    .FirstOrDefault(
                        unit =>
                            unit.IsValid && unit.IsAlive &&
                            !unit.IsInvulnerable() && unit.Team != hero.Team &&
                            hero.IsValidOrbwalkingTarget(unit) &&
                            unit.NetworkName != ClassId.CDOTA_BaseNPC.ToString());

            if (heroes != null)
            {
                return heroes;
            }

            return null;
        }

        public void Flush()
        {
            Particle.Remove(Id);

            foreach (var camp in _main.Camps)
            {
                if (camp.Owners.Contains(Owner))
                    camp.Owners.Remove(Owner);
            }
        }
    }
}