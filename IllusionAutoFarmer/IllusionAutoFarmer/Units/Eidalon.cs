﻿using System.Linq;
using Ensage;
using Ensage.SDK.Extensions;
using Ensage.SDK.Helpers;

namespace IllusionAutoFarmer.Units
{
    public class Eidalon : ControlableUnit, ICanDodgeLikeEdalon
    {
        public Eidalon(Unit owner, IllusionController controller) : base(owner, controller)
        {
        }

        public void Dodge()
        {
            var someAlly = EntityManager<Creep>.Entities.FirstOrDefault(x => x.IsValid && x.IsAlly(Owner) && x.IsAlive && x.IsInRange(Owner, 500));
            if (someAlly != null)
            {
                InDodge = true;
                _main.InDodge.Add(this);
                Owner.Attack(someAlly);
                Owner.Move(Owner.NetworkPosition.Extend(someAlly.NetworkPosition, someAlly.Distance2D(Owner) + 200));
                Owner.Attack(someAlly, true);
                UpdateManager.BeginInvoke(() =>
                {
                    Owner.Attack(someAlly);
                    _main.InDodge.Remove(this);
                    InDodge = false;
                }, 750);
            }
            else
            {
                someAlly = EntityManager<Creep>.Entities.FirstOrDefault(x =>
                    x.IsValid && !x.IsAlly(Owner) && x.IsAlive && x.IsInRange(Owner, 800));
                if (someAlly == null)
                    return;
                InDodge = true;
                _main.InDodge.Add(this);
                Owner.Move(Owner.NetworkPosition.Extend(Owner.NetworkPosition, someAlly.Distance2D(Owner) + 350));
                UpdateManager.BeginInvoke(() =>
                {
                    InDodge = false;
                    _main.InDodge.Remove(this);
                }, 1000);
            }
        }
    }
}