﻿namespace IllusionAutoFarmer.Units
{
    public interface ICanDodge
    {
        void Dodge();
    }
}