﻿using System;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Reflection;
using Ensage;
using Ensage.SDK.Helpers;
using Ensage.SDK.Renderer;
using Ensage.SDK.Service;
using Ensage.SDK.Service.Metadata;
using log4net;
using PlaySharp.Toolkit.Logging;
using SharpDX;
using Color = System.Drawing.Color;

namespace IllusionAutoFarmer
{
    [ExportPlugin(
        mode: StartupMode.Auto,
        name: "Illusion Auto Farmer"
    )]
    public sealed class IllusionAutoFarmer : Plugin
    {
        public static readonly ILog Log = AssemblyLogs.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        [ImportingConstructor]
        public IllusionAutoFarmer([Import] IServiceContext context, [Import] Map map)
        {
            Context = context;
            Map = map;
        }

        public IServiceContext Context { get; }
        public Map Map { get; }
        public CampManager CampManager { get; private set; }
        public IllusionController IllusionController { get; private set; }
        public Hero Owner { get; private set; }
        public Config Config { get; private set; }
        public LaneHelper LaneHelper { get; private set; }
        public Color TextColor;

        protected override void OnActivate()
        {
            Owner = (Hero) Context.Owner;
            Config = new Config(this);
            LaneHelper = new LaneHelper(this);
            CampManager = new CampManager();
            IllusionController = new IllusionController(this);

            if (Config.DrawCampStatus)
                Context.RenderManager.Draw += OnDraw;

            if (Config.DrawType)
                Context.RenderManager.Draw += OnDraw2;

            TextColor = Config.Enable ? Color.White : Color.Red;

            Config.Enable.PropertyChanged += (sender, args) => { TextColor = Config.Enable ? Color.White : Color.Red; };
            Config.DrawCampStatus.PropertyChanged += OnDrawToggle;
            Config.DrawType.PropertyChanged += OnDraw2Toggle;
        }

        private void OnDraw2(IRenderer renderer)
        {
            renderer.DrawText(new Vector2(Config.PosX, Config.PosY), $"Mode: {IllusionController.FarmType}",
                TextColor, Config.TextSize);
        }

        private void OnDrawToggle(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (Config.DrawCampStatus)
                Context.RenderManager.Draw += OnDraw;
            else
                Context.RenderManager.Draw -= OnDraw;
        }

        private void OnDraw2Toggle(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (Config.DrawType)
                Context.RenderManager.Draw += OnDraw2;
            else
                Context.RenderManager.Draw -= OnDraw2;
        }

        private void OnDraw(IRenderer renderer)
        {
            var g = 0;
            foreach (var camp in CampManager.Camps)
            {
                g++;
                if (Drawing.WorldToScreen(camp.Position, out var pos))
                {
                    renderer.DrawText(pos,
                        $"Empty: {(camp.IsEmpty ? "Yes" : "No")} Owners: {camp.Owners.Count}",
//                        $"Empty: {(camp.IsEmpty ? "Yes" : "No")} Owners: {camp.Owners.Count} id: {g} {camp.Position.X}:{camp.Position.Y}:{camp.Position.Z} ancient? {camp.IsAncient}",
                        Color.AliceBlue, 15);
                }
            }
        }

        protected override void OnDeactivate()
        {
            Config?.Dispose();
            CampManager?.Dispose();
            IllusionController?.Dispose();
        }
    }
}