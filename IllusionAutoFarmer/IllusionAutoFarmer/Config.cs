﻿using System;
using System.Windows.Forms.Design.Behavior;
using Ensage.Common.Menu;
using Ensage.SDK.Menu;

namespace IllusionAutoFarmer
{
    public class Config : IDisposable
    {
        private readonly IllusionAutoFarmer _main;

        public Config(IllusionAutoFarmer main)
        {
            _main = main;
            Factory = MenuFactory.Create("Units Auto Farmer");
            Enable = Factory.Item("Enable", new KeyBind('0', KeyBindType.Toggle));
            //HeroToggle = Factory.Item("Hero toggle", new KeyBind('0', KeyBindType.Toggle));
            FarmEnemyCamp = Factory.Item("Farm Enemy camps", true);
            FarmAncientCamps = Factory.Item("Farm Ancient camps", false);
            SkipCampWithAllyHero = Factory.Item("Skip camp with ally heroes", true);
            FarmType = Factory.Item("Farm ->", new StringList("Jungle", "Lanes", "Auto"));
            Behavior = Factory.Item("Behavior(illusions & bear) ->", new StringList("Dont care about damage", "Trying to avoid take extra damage"));
            IllusionsOnOneCamp = Factory.Item("Units on one camp", new Slider(2, 1, 6));
            IllusionsOnOneLane = Factory.Item("Units on lane", new Slider(1, 1, 6));

            DrawCampStatus = Factory.Item("Draw Camp Status", false);
            DrawType = Factory.Item("Draw current Farm type", true);
            PosX = Factory.Item("pos x", new Slider(10, 0, 2000));
            PosY = Factory.Item("pos y", new Slider(110, 0, 2000));
            TextSize = Factory.Item("text size", new Slider(15, 1, 50));

            EnableForIllusions = Factory.Item("Enable for Illusions", true);
            EnableForCreeps = Factory.Item("Enable for creeps", true);
            EnableForBear = Factory.Item("Enable for Lone Druid's bear", true);
        }

        public MenuItem<bool> EnableForCreeps { get; set; }
        public MenuItem<bool> EnableForBear { get; set; }

        public MenuItem<bool> EnableForIllusions { get; set; }

        public MenuItem<Slider> IllusionsOnOneLane { get; set; }

        //public MenuItem<KeyBind> HeroToggle { get; set; }

        public MenuItem<Slider> TextSize { get; set; }

        public MenuItem<Slider> PosX { get; set; }
        public MenuItem<Slider> PosY { get; set; }

        public MenuItem<bool> DrawType { get; set; }

        public MenuItem<StringList> FarmType { get; set; }
        public MenuItem<StringList> Behavior { get; }
        public MenuItem<bool> DrawCampStatus { get; set; }

        public MenuItem<bool> FarmEnemyCamp { get; set; }

        public MenuItem<bool> SkipCampWithAllyHero { get; set; }

        public MenuItem<Slider> IllusionsOnOneCamp { get; set; }

        public MenuItem<bool> FarmAncientCamps { get; set; }

        public MenuItem<KeyBind> Enable { get; set; }

        public MenuFactory Factory { get; set; }

        public void Dispose()
        {
            Factory?.Dispose();
        }
    }
}