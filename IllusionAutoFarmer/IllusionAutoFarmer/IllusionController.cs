﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Ensage;
using Ensage.SDK.Extensions;
using Ensage.SDK.Handlers;
using Ensage.SDK.Helpers;
using Ensage.SDK.Renderer.Particle;
using IllusionAutoFarmer.Units;
using SharpDX;

namespace IllusionAutoFarmer
{
    public class IllusionController : IDisposable
    {
        private readonly IllusionAutoFarmer _main;
        private Config Config => _main.Config;
        private Hero Owner => _main.Owner;
        public bool SkipCampWithAllyHeroes => _main.Config.SkipCampWithAllyHero;
        public bool FarmEnemyCamp => _main.Config.FarmEnemyCamp;
        public int IllusionsOnCamp => _main.Config.IllusionsOnOneCamp.Value.Value;
        private int IllusionsOnLane => _main.Config.IllusionsOnOneLane.Value.Value;
        public bool FarmAncients => _main.Config.FarmAncientCamps;
        public FarmTypeEnum FarmType => (FarmTypeEnum) _main.Config.FarmType.Value.SelectedIndex;
        public BehaviorEnum BehaviorType => (BehaviorEnum) _main.Config.Behavior.Value.SelectedIndex;
        public List<ControlableUnit> InDodge;

        public enum FarmTypeEnum
        {
            Jungle,
            Lane,
            Auto,
            TopLane,
            MiddleLane,
            BotLane,
            Unknown
        }

        public enum BehaviorEnum
        {
            Tanky,
            Pussy
        }

        public bool InStacking = false;
        public IEnumerable<Camp> Camps => _main.CampManager.Camps;
        public IEnumerable<Vector3> StackPoints => _main.CampManager.StackPoints;
        public IParticleManager Particle => _main.Context.Particle;

        public IllusionController(IllusionAutoFarmer main)
        {
            _laneHelper = main.LaneHelper;
            _main = main;
            Config.Enable.PropertyChanged += EnableOnPropertyChanged;
            _subManager = UpdateManager.Subscribe(Controller, 500, Config.Enable);
            InDodge = new List<ControlableUnit>();
            //if (Config.Enable)
            //{
            //EntityManager<Hero>.EntityAdded += EntityManagerOnEntityAdded;
            //EntityManager<Hero>.EntityRemoved += EntityManagerOnEntityRemoved;

            EntityManager<Unit>.EntityAdded += EntityManagerOnEntityAdded;
            EntityManager<Unit>.EntityRemoved += EntityManagerOnEntityRemoved;

            Unit.OnModifierAdded += UnitOnOnModifierAdded;
            if (Config.Enable)
                Entity.OnInt32PropertyChange += EntityOnOnInt32PropertyChange;
            //if (BehaviorType == BehaviorEnum.Pussy)


            /*Config.Behavior.PropertyChanged += (sender, args) =>
            {
                if (BehaviorType == BehaviorEnum.Pussy)
                    Entity.OnInt32PropertyChange += EntityOnOnInt32PropertyChange;
                else
                    Entity.OnInt32PropertyChange -= EntityOnOnInt32PropertyChange;
            };*/
            /*Config.HeroToggle.PropertyChanged += (sender, args) =>
            {
                if (Config.HeroToggle)
                {
                    if (!System.ContainsKey(_main.Owner))
                        System.Add(_main.Owner, FarmTypeEnum.Unknown);
                }
                else
                {
                    EntityManagerOnEntityRemoved(null, _main.Owner);
                }
            };*/
            //}
            Units = new List<ControlableUnit>();

            foreach (var unit in EntityManager<Unit>.Entities.Where(x =>
                x.IsValid && x.IsAlly(Owner) && x.IsControllable &&
                (x.HasAnyModifiers("modifier_dominated") || x.NetworkName.Equals("CDOTA_Unit_SpiritBear") ||
                 x is Hero hero && hero.IsIllusion)))
            {
                if (unit is Hero hero && hero.IsIllusion)
                {
                    AddUnit(unit, UnitType.Illusion);
                }
                else if (unit.Name.Contains("eidolon"))
                {
                    AddUnit(unit, UnitType.Edailon);
                }
                else if (unit.NetworkName.Equals("CDOTA_Unit_SpiritBear"))
                {
                    IllusionAutoFarmer.Log.Warn($"NEW BEAR");
                    AddUnit(unit, UnitType.Bear);
                }
                else
                {
                    AddUnit(unit, UnitType.Creep);
                }
            }
        }


        public enum UnitType
        {
            Illusion,
            Creep,
            Edailon,
            Bear
        }

        public bool AddUnit(Unit unit, UnitType type)
        {
            if (unit.IsEnemy(Owner))
                return false;
            if (!unit.IsControllable)
                return false;
            if (Units.Any(x => x.Owner.Equals(unit)))
            {
                return false;
            }

            if (unit.HasModifiers(new[] {"modifier_lycan_wolf_uncontrollable"}))
            {
                return false;
            }

            if (unit.NetworkName.Equals("CDOTA_Unit_Broodmother_Web") ||
                unit.Name.Contains("monkey_king_wukongs_command") ||
                unit.Name.Equals("npc_dota_wraith_king_skeleton_warrior") || unit.Equals(ObjectManager.LocalHero))
                return false;
            ControlableUnit controlableUnit = null;
            switch (type)
            {
                case UnitType.Illusion:
                    if (!Config.EnableForIllusions)
                        break;
                    controlableUnit = new IllusionUnit(unit, this);
                    break;
                case UnitType.Creep:
                    if (!Config.EnableForCreeps)
                        break;
                    controlableUnit = new JustCreep(unit, this);
                    break;
                case UnitType.Edailon:
                    if (!Config.EnableForCreeps)
                        break;
                    controlableUnit = new Eidalon(unit, this);
                    break;
                case UnitType.Bear:
                    if (!Config.EnableForBear)
                        break;
                    controlableUnit = new BearUnit(unit, this);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }

            if (controlableUnit == null) return false;
            Units.Add(controlableUnit);
            return true;
        }

        public bool RemoveUnit(Unit unit)
        {
            var target = Units.Find(x => x.Owner.Equals(unit));
            if (target == null || target.Owner.NetworkName.Equals("CDOTA_Unit_SpiritBear"))
                return false;
            target.Flush();
            Units.Remove(target);
            return true;
        }

        public bool RemoveUnit(ControlableUnit unit)
        {
            if (Units.Contains(unit))
            {
                Units.Remove(unit);
                return true;
            }

            return false;
        }

        public List<ControlableUnit> Units;

        private void EntityOnOnInt32PropertyChange(Entity sender, Int32PropertyChangeEventArgs args)
        {
            if (args.PropertyName != "m_iHealth" || args.NewValue >= args.OldValue ||
                !Units.Exists(x => x.Owner.Equals(sender) && x is ICanDodge)) return;
            var hero = Units.Find(x => x.Owner.Equals(sender));
            /*if (hero.FarmType != FarmTypeEnum.Lane)
                return;*/
            if (!InDodge.Contains(hero))
            {
                (hero as ICanDodge)?.Dodge();
            }
        }

        private async void Controller()
        {
            var allyHeroes = EntityManager<Hero>.Entities.Where(x =>
                x.IsAlive && x.IsAlly(Owner) && !x.IsIllusion && !x.Equals(_main.Owner));
            var flushSystem = Units.ToList();
            foreach (var owner in flushSystem)
            {
                await Task.Delay(50);
                var hero = owner.Owner;
                if (hero == null || !hero.IsValid || !hero.IsAlive)
                {
                    if (hero != null)
                    {
                        EntityManagerOnEntityRemoved(null, hero);
                    }

                    continue;
                }

                if (owner is ICanDodge && owner.InDodge)
                    continue;


                if (FarmType == FarmTypeEnum.Jungle)
                {
                    owner.FarmType = FarmTypeEnum.Jungle;
                    owner.JungleFarm(allyHeroes);
                }
                else if (FarmType == FarmTypeEnum.Lane)
                {
                    owner.FarmType = FarmTypeEnum.Lane;
                    owner.LaneFarm();
                }
                else if (FarmType == FarmTypeEnum.Auto && owner.FarmType == FarmTypeEnum.Unknown)
                {
                    var isJungle = _laneHelper.IsJungle(hero.NetworkPosition);
                    if (isJungle)
                    {
                        owner.FarmType = FarmTypeEnum.Jungle;
                        Console.WriteLine($"Line detected -> Jungle");
                        owner.JungleFarm(allyHeroes);
                    }
                    else
                    {
                        if (Units.Count(x => x.FarmType == FarmTypeEnum.Lane) < IllusionsOnLane || owner is Eidalon ||
                            owner is JustCreep)
                        {
                            owner.FarmType = FarmTypeEnum.Lane;
                            Console.WriteLine($"Line detected -> Lane");
                            owner.LaneFarm();
                        }
                        else
                        {
                            owner.FarmType = FarmTypeEnum.Jungle;
                            Console.WriteLine($"Line detected -> Lane(overflow) -> Jungle");
                            owner.JungleFarm(allyHeroes);
                        }
                    }
                }
                else
                {
                    if (owner.FarmType == FarmTypeEnum.Jungle)
                    {
                        owner.JungleFarm(allyHeroes);
                    }
                    else if (owner.FarmType == FarmTypeEnum.Lane)
                    {
                        owner.LaneFarm();
                    }
                    else
                    {
                        Console.WriteLine($"Wrong lane -> {owner.FarmType}");
                    }
                }
                
            }
        }

        private void EnableOnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
        {
            if (Config.Enable)
            {
                //EntityManager<Hero>.EntityAdded += EntityManagerOnEntityAdded;
                //EntityManager<Hero>.EntityRemoved += EntityManagerOnEntityRemoved;
                _subManager.IsEnabled = true;
                Entity.OnInt32PropertyChange += EntityOnOnInt32PropertyChange;
            }
            else
            {
                //EntityManager<Hero>.EntityAdded -= EntityManagerOnEntityAdded;
                //EntityManager<Hero>.EntityRemoved -= EntityManagerOnEntityRemoved;
                _subManager.IsEnabled = false;
                Entity.OnInt32PropertyChange -= EntityOnOnInt32PropertyChange;
                foreach (var camp in Camps)
                {
                    camp.Owners.Clear();
                }

                foreach (var j in Units)
                {
                    j.Flush();
                }
            }
        }

        private void EntityManagerOnEntityRemoved(object sender, Unit hero)
        {
            RemoveUnit(hero);
        }

        private void EntityManagerOnEntityAdded(object sender, Unit unit)
        {
            UpdateManager.BeginInvoke(() =>
            {
                if (unit != null && unit.IsValid && !(unit is Courier) && unit.IsControllable)
                {
                    if (unit is Hero hero && hero.IsIllusion)
                    {
                        AddUnit(unit, UnitType.Illusion);
                    }
                    else if (unit.Name.Contains("eidolon"))
                    {
                        AddUnit(unit, UnitType.Edailon);
                    }
                    else if (unit.NetworkName.Equals("CDOTA_Unit_SpiritBear"))
                    {
                        AddUnit(unit, UnitType.Bear);
                    }
                    else
                    {
                        AddUnit(unit, UnitType.Creep);
                    }

                    //System.Add(unit, FarmTypeEnum.Unknown /*new MovementManager(new EnsageServiceContext(hero))*/);
                }
            }, 50);
        }

        private void UnitOnOnModifierAdded(Unit unit, ModifierChangedEventArgs modifierargs)
        {
            if (unit == null || !unit.IsValid || !unit.IsControllable) return;
            var mod = modifierargs.Modifier;
            if (mod.Name != "modifier_dominated") return;
            if (unit is Hero hero && hero.IsIllusion)
            {
                AddUnit(unit, UnitType.Illusion);
            }
            else if (unit.Name.Contains("eidolon"))
            {
                AddUnit(unit, UnitType.Edailon);
            }
            else if (unit.NetworkName.Equals("CDOTA_Unit_SpiritBear"))
            {
                AddUnit(unit, UnitType.Bear);
            }
            else
            {
                AddUnit(unit, UnitType.Creep);
            }
        }

        private readonly IUpdateHandler _subManager;
        public readonly LaneHelper _laneHelper;

        public void Dispose()
        {
            EntityManager<Hero>.EntityAdded -= EntityManagerOnEntityAdded;
            EntityManager<Unit>.EntityAdded -= EntityManagerOnEntityAdded;
            EntityManager<Hero>.EntityRemoved -= EntityManagerOnEntityRemoved;
            EntityManager<Unit>.EntityRemoved -= EntityManagerOnEntityRemoved;
            _subManager.IsEnabled = false;
        }
    }
}