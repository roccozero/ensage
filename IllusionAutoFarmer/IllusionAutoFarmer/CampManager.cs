﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Ensage;
using Ensage.SDK.Helpers;
using log4net;
using PlaySharp.Toolkit.Logging;
using SharpDX;

namespace IllusionAutoFarmer
{
    public class CampManager : IDisposable
    {
        private static readonly ILog Log = AssemblyLogs.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public List<Camp> Camps;
        public List<Vector3> StackPoints;

        public CampManager()
        {
            IsActive = true;
            Camps = new List<Camp>
            {
                new Camp(new Vector3(-1818, -4221, 256)),
                new Camp(new Vector3(-225, -3321, 384)),
                new Camp(new Vector3(473, -4745, 384)),
                new Camp(new Vector3(2966, -4579, 256)),
                new Camp(new Vector3(4533, -4412, 256)),
                new Camp(new Vector3(-4886, -445, 256)),
                new Camp(new Vector3(-3668, 871, 256)),
                new Camp(new Vector3(-3061, -61, 384), true),
                new Camp(new Vector3(60, -1853, 383), true),
                new Camp(new Vector3(3959, -545, 256), true, Team.Dire),
                new Camp(new Vector3(-782, 2332, 384), true, Team.Dire),
                new Camp(new Vector3(2546, 83, 384), team: Team.Dire),
                new Camp(new Vector3(4493, 850, 384), team: Team.Dire),
                new Camp(new Vector3(1319, 3361, 384), team: Team.Dire),
                new Camp(new Vector3(-98, 3332, 256), team: Team.Dire),
                new Camp(new Vector3(-2047, 4196, 256), team: Team.Dire),
                new Camp(new Vector3(-2753, 4635, 256), team: Team.Dire),
                new Camp(new Vector3(-4203, 3465, 256), team: Team.Dire),
            };

            StackPoints = new List<Vector3>
            {
                new Vector3(-3143, -3955, 255),
                new Vector3(-1799, -1916, 256),
                new Vector3(899, -3258, 383),
                new Vector3(5424, -4605, 384),
                new Vector3(2286, -3364, 384),
                new Vector3(1998, -1093, 256),
                new Vector3(3716, 2173, 256),
                new Vector3(-3485, -1525, 384),
                new Vector3(-5404, 1392, 384),
                new Vector3(-5640, 3849, 384),
                new Vector3(-3455, 6014, 384),
                new Vector3(-711, 5263, 384),
                new Vector3(219, 4681, 383),
                new Vector3(588, 2144, 384),
            };

            foreach (var camp in Camps)
            {
                switch (camp.Id)
                {
                    case 2:
//                        camp.Position = new Vector3(-106, -3445, 384);
                        camp.IsAncient = false;
                        break;
                    case 9:
                        camp.IsAncient = false;
                        break;
                    case 15:
                        camp.IsAncient = false;
                        break;
                    case 11:
                        camp.IsAncient = false;
                        break;
                    case 3:
                        camp.Position = new Vector3(501, -4628, 384);
                        break;
                    case 7:
                        camp.Position = new Vector3(-3719, 882, 384);
                        break;
                    case 8:
                        camp.Position = new Vector3(-2558, -540, 384);
                        break;
                    case 17:
                        camp.Position = new Vector3(-2440, 4804, 256);
                        break;
                    case 16:
                        camp.Position = new Vector3(-1874, 4415, 384);
                        break;
                    case 10:
                        camp.Position = new Vector3(4240, -322, 384);
                        break;
                }
            }

            var secs = (int)(Game.GameTime % 60);
            //Log.Debug($"[{secs}] First refresh in {(60 - secs) * 1000}");
            UpdateManager.BeginInvoke(async () =>
            {
                while (IsActive)
                {
                    foreach (var camp in Camps)
                    {
                        camp.Refresh();
                    }
                    secs = (int)(Game.GameTime % 60);
                    var nextDelay = (60 - secs) * 1000;
                    //Log.Debug($"[{secs}] Refres camps. next in {nextDelay}");
                    await Task.Delay(nextDelay);
                    secs = (int)(Game.GameTime % 60);
                    if (Game.GameTime % 60 >= 2.5)
                    {
                        await Task.Delay((60 - secs) * 1000);
                    }
                }
                Log.Debug($"Dispose for CampManager");
            }, (60 - secs) * 1000);
        }

        public bool IsActive { get; set; }

        public void Dispose()
        {
            IsActive = false;
        }
    }
}