﻿using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;
using SharpDX;

namespace MeepoCrappahilation
{
    public class RunePlaceHolder
    {
        public MeepoBase Owner;
        public Vector3 Position;

        public RunePlaceHolder(Vector3 position)
        {
            Position = position;
        }
    }
}