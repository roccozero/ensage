﻿using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;
using MeepoCrappahilation.Meepos_Shiit.OrderBase;

namespace MeepoCrappahilation.Meepos_Shiit
{
    public class OrderSystem
    {
        public enum OrderTypes
        {
            Error,
            Combo,
            Idle,
            IdleOnBase,
            JungleFarm,
            LanePushing,
            Healing,
            RunePicking,
            Dodge
        }

        private readonly MeepoBase _meepo;

        public OrderBase.OrderBase CurrentOrder;
        public OrderBase.OrderBase LastOrder;

        public OrderSystem(MeepoBase meepo)
        {
            _meepo = meepo;
            CurrentOrder = new IdleOrder(_meepo);
            LastOrder = new IdleOrder(_meepo);
        }

        public void Execute()
        {
            CurrentOrder.Execute();
        }

        public void FlushOrder()
        {
            (CurrentOrder as OrderBaseWithPussy)?.FlushRangeChecker();
            CurrentOrder = new IdleOrder(_meepo);
            LastOrder = new IdleOrder(_meepo);
        }

        public void SetOrder(OrderBase.OrderBase setOrder)
        {
            if (setOrder.Id == OrderTypes.Combo && !_meepo.Main.CanExecute &&
                CurrentOrder.Id == OrderTypes.Healing) return;
            if (setOrder.Id != CurrentOrder.Id && CurrentOrder.CanBeLast) LastOrder = CurrentOrder;
            (CurrentOrder as OrderBaseWithPussy)?.FlushRangeChecker();

            CurrentOrder = setOrder;
            (CurrentOrder as OrderBaseWithPussy)?.OnChange();
        }
    }
}