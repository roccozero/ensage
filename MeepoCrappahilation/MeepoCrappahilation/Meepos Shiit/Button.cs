﻿using Ensage;
using SharpDX;

namespace MeepoCrappahilation.Meepos_Shiit
{
    public class Button
    {
        public static Vector2 StartPosition;
        public readonly OrderBase.OrderBase Order;

        public Button(OrderBase.OrderBase type)
        {
            Order = type;
            Text = type.Id.ToString();
            TextSize = Drawing.MeasureText(Text, "Arial", GlobalTextSize, FontFlags.None) + new Vector2(4, 0);
        }

        public Button(OrderBase.OrderBase type, Vector2 startPosition, Vector2 textSize)
        {
            Text = type.Id.ToString();
            Order = type;
            StartPosition = startPosition;
            GlobalTextSize = textSize;

            TextSize = Drawing.MeasureText(Text, "Arial", GlobalTextSize, FontFlags.None) + new Vector2(4, 0);
        }

        public Vector2 TextSize { get; set; }

        public static Vector2 GlobalTextSize { get; set; }

        public string Text { get; set; }

        public Vector2 CalcPosition(int id)
        {
            return StartPosition + new Vector2(0, TextSize.Y * id + 1 * id);
        }

        public void DrawButton(Vector2 startPos, Color color)
        {
            Drawing.DrawRect(startPos, TextSize, color);
            Drawing.DrawRect(startPos, TextSize, Color.Black, true);
            Drawing.DrawText(Text, startPos + new Vector2(2, 0), GlobalTextSize, Color.White, FontFlags.None);
        }
    }
}