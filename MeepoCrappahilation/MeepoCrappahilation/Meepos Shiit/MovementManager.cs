﻿using Ensage;
using Ensage.SDK.Extensions;
using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;

namespace MeepoCrappahilation.Meepos_Shiit
{
    public class MovementManager
    {
        private readonly MeepoBase _meepo;

        //public readonly IOrbwalkerManager Orbwalker;
        public readonly MeepoOrbwalker Orbwalker;

        public MovementManager(MeepoOrbwalker orbwalker)
        {
            Orbwalker = orbwalker.Activate();
        }

        public MovementManager(MeepoBase meepo)
        {
            _meepo = meepo;
            Orbwalker = new MeepoOrbwalker(meepo.Hero).Activate();
        }

        public void Orbwalk(Unit target)
        {
            if (target == null)
            {
                Orbwalker.Move(Game.MousePosition);
            }
            else
            {
                if (target.IsVisible)
                {
                    if (target.IsAttackImmune())
                        Orbwalker.Move(target.Position);
                    else
                        Orbwalker.OrbwalkTo(target);
                }
                else
                {
                    Orbwalker.Move(_meepo.Main.TargetManager.TargetPosition);
                }
            }
        }

        public void Attack(Unit target)
        {
            if (target == null)
            {
            }
            else
            {
                if (Orbwalker.CanAttack(target))
                {
                    Orbwalker.Attack(target);
                }
                else
                {
                    if (_meepo.Main.TargetManager.Target != null && _meepo.Main.TargetManager.Target.Equals(target))
                    {
                        Orbwalker.Move(_meepo.Main.TargetManager.TargetPosition);
                    }
                    else
                    {
                        if (Orbwalker.CanMove())
                            Orbwalker.Move(target.NetworkPosition);
                    }
                }
            }
        }
    }
}