﻿using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;

namespace MeepoCrappahilation.Meepos_Shiit.OrderBase
{
    public class IdleOnBaseOrder : OrderBase
    {
        public IdleOnBaseOrder(MeepoBase owner, bool dummyOrder = false) : base(owner)
        {
            if (!dummyOrder)
                Hero.Move(Fountain.Position);
        }

        public override OrderSystem.OrderTypes Id => OrderSystem.OrderTypes.IdleOnBase;
    }
}