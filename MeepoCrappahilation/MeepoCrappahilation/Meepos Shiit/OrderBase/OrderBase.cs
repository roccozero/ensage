﻿using System.Linq;
using Ensage;
using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;

namespace MeepoCrappahilation.Meepos_Shiit.OrderBase
{
    public abstract class OrderBase
    {
        public Unit Fountain;
        public string Name;

        protected OrderBase(MeepoBase owner)
        {
            Owner = owner;
            Fountain = ObjectManager.GetEntities<Unit>()
                .FirstOrDefault(x => x.NetworkName == "CDOTA_Unit_Fountain" && x.Team == Hero.Team);
            if (Fountain == null)
            {
            }
        }

        public virtual OrderSystem.OrderTypes Id => OrderSystem.OrderTypes.Error;
        public virtual bool CanBeLast => true;
        public virtual bool CanFlushWithMouse => true;
        public MeepoBase Owner { get; set; }
        public Hero Hero => Owner.Hero;

        public virtual void Execute()
        {
        }
    }
}