﻿using System;
using System.Linq;
using Ensage;
using Ensage.Common.Extensions;
using Ensage.Common.Menu;
using Ensage.SDK.Helpers;
using Ensage.SDK.Menu;
using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;

namespace MeepoCrappahilation.Meepos_Shiit.OrderBase
{
    public class RunePicking : OrderBaseWithPussy
    {
        public RunePicking(MeepoBase owner) : base(owner)
        {
            if (EnablePussyEscaper == null)
            {
                EnablePussyEscaper = owner.Main.Config.JungleFarm.Item("Pussy escaper while rune checking", true);
                EnablePussyEscaper.Item.SetTooltip(
                    "if meepo can see any enemy in selected range -> meepo will do auto escape");
                EnablePussyEscaper.PropertyChanged += (sender, args) =>
                {
                    if (EnablePussyEscaper && owner.OrderSystem.CurrentOrder.Id == OrderSystem.OrderTypes.Healing)
                        DrawRangeChecker(PussyEscaperRange);
                    else
                        FlushRangeChecker();
                };
            }


            if (PussyEscaperRange == null)
            {
                PussyEscaperRange = owner.Main.Config.JungleFarm.Item("Pussy escaper range in rune checking mode",
                    new Slider(1000, 1, 2500));
                PussyEscaperRange.PropertyChanged += (sender, args) =>
                {
                    if (EnablePussyEscaper) DrawRangeChecker(PussyEscaperRange);
                };
            }

            if (EnablePussyEscaper != null && EnablePussyEscaper) DrawRangeChecker(PussyEscaperRange);
        }

        public override OrderSystem.OrderTypes Id => OrderSystem.OrderTypes.RunePicking;
        public override bool CanBeLast => false;

        public MenuItem<Slider> PussyEscaperRange { get; set; }

        public MenuItem<bool> EnablePussyEscaper { get; set; }

        public override void Execute()
        {
            var closestBount = Owner.Main.RuneManager.BountyPoints
                .Where(y => y.Owner == Owner)
                .OrderBy(x => x.Position.Distance2D(Hero))
                .FirstOrDefault();
            if (closestBount?.Owner != null)
            {
                CheckForPussyEscape(EnablePussyEscaper, PussyEscaperRange);
                if (closestBount.Position.Distance2D(Hero) <= 100)
                {
                    var findRune = EntityManager<Rune>.Entities.Where(x => x.Distance2D(Hero) <= 500 && x.IsVisible)
                        .ToList();
                    if (findRune.Any())
                        Hero.PickUpRune(findRune.First());
                    else if (Math.Abs((Math.Round(Game.GameTime) - 2) % 300) >= 1f) closestBount.Owner = null;
                }
                else
                {
                    Hero.Move(closestBount.Position);
                }
            }
            else
            {
                Owner.OrderSystem.SetOrder(Owner.OrderSystem.LastOrder);
            }
        }
    }
}