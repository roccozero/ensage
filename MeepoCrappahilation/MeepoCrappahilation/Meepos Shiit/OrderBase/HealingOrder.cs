﻿using System.Linq;
using Ensage;
using Ensage.Common.Enums;
using Ensage.Common.Extensions;
using Ensage.Common.Extensions.SharpDX;
using Ensage.Common.Menu;
using Ensage.SDK.Helpers;
using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;
using UnitExtensions = Ensage.SDK.Extensions.UnitExtensions;

namespace MeepoCrappahilation.Meepos_Shiit.OrderBase
{
    public class HealingOrder : OrderBase
    {
        public string Status = string.Empty;

        public HealingOrder(MeepoBase owner) : base(owner)
        {
        }

        public override OrderSystem.OrderTypes Id => OrderSystem.OrderTypes.Healing;
        private int HeroRange => Owner.Main.Config.AutoEscape.GetValue<Slider>("Hero Range").Value;
        private int CreepRange => Owner.Main.Config.AutoEscape.GetValue<Slider>("Creep Range").Value;
        public override bool CanFlushWithMouse => false;

        public override void Execute()
        {
            if (Owner.GetHealthPercent >= 0.85f && Hero.HasModifier("modifier_fountain_aura_buff"))
            {
                Status = "On fountain & full";
                switch (Owner.OrderSystem.LastOrder.Id)
                {
                    case OrderSystem.OrderTypes.Combo when !Owner.Main.CanExecute && Owner.Main.Config.IsSuperPro:
                        Owner.OrderSystem.SetOrder(new IdleOnBaseOrder(Owner));
                        Status = string.Empty;
                        return;
                    case OrderSystem.OrderTypes.Combo when !Owner.Main.CanExecute:
                        Owner.OrderSystem.SetOrder(new IdleOrder(Owner));
                        break;
                    default:
                        if (Owner.Main.CanExecute && Owner.OrderSystem.LastOrder.Id == OrderSystem.OrderTypes.Idle)
                            if (Owner.Main.Config.IsSuperPro)
                                Owner.OrderSystem.SetOrder(new IdleOnBaseOrder(Owner));
                            else
                                Owner.OrderSystem.SetOrder(new ComboOrder(Owner));
                        else
                            Owner.OrderSystem.SetOrder(Owner.OrderSystem.LastOrder);
                        break;
                }
            }
            else
            {
                if (Hero.HasModifier("modifier_fountain_aura_buff"))
                {
                    Status = "On fountain";
                    return;
                }

                var travel = Hero.GetItemById(ItemId.item_travel_boots) ??
                             Hero.GetItemById(ItemId.item_travel_boots_2);
                var dist = Hero.Distance2D(Fountain);
                var closestMeepo = Owner.Main.Updater.AllMeepos
                    .Where(x => !x.Equals(Owner) && x.Hero.Distance2D(Fountain) < dist &&
                                x.Hero.Distance2D(Hero) >= 1000)
                    .OrderBy(x => x.Hero.Distance2D(Fountain)).FirstOrDefault();
                var closestEnemy = EntityManager<Hero>.Entities.Where(y =>
                    UnitExtensions.IsEnemy(y, Owner.Hero) && y.IsAlive && y.IsVisible &&
                    Owner.AbilityManager.Earthbind.CanHit(y)).OrderBy(z => z.Distance2D(Owner.Hero)).FirstOrDefault();

                var notOnlyOneNearTarget = Owner.Main.Target == null || !Owner.Main.Config.IsSuperPro ||
                                           Owner.Main.Updater.AllMeepos.Any(x =>
                                               !(x is IllusionMeepo) && !x.Equals(Owner) &&
                                               !x.AbilityManager.W.IsInAbilityPhase &&
                                               UnitExtensions.IsInRange(x.Hero, Owner.Main.Target, 1000));

                if (Owner.AbilityManager.Earthbind.CanBeCasted && !Owner.Hero.IsInvisible())
                    if (closestEnemy != null)
                    {
                        Owner.AbilityManager.EarthBind(closestEnemy);
                        Status = "Earthbind using";
                    }

                var distEnemy = closestEnemy?.Distance2D(Owner.Hero);
                //MeepoCrappahilation.Log.Warn($"DistEnemy: {distEnemy} dist: {dist}");
                if (dist >= 1000 && (distEnemy > HeroRange || closestEnemy == null))
                {
                    var enemyCreep = EntityManager<Creep>.Entities.Any(x =>
                        x.IsAlive && x.IsSpawned && !UnitExtensions.IsAlly(x, Hero) &&
                        UnitExtensions.IsInRange(x, Hero, CreepRange) && x.IsMoving);
                    //MeepoCrappahilation.Log.Warn($"IsEnemyCreepHere: {enemyCreep}");
                    if (!enemyCreep)
                    {
                        if (closestMeepo != null)
                            if (notOnlyOneNearTarget && UsePoof(closestMeepo))
                            {
                                Status = "Using poof";
                                return;
                            }

                        if (travel != null && travel.CanBeCasted() && dist >= 2000) ////TODO: add travel checker to menu
                        {
                            Status = "Using travels";
                            travel.UseAbility(Fountain.Position);
                        }
                    }
                }

                if (Hero.HasModifier("modifier_bloodseeker_rupture"))
                {
                    Status = "Under rupture";
                    Hero.Stop();
                    if (closestMeepo != null)
                        if (notOnlyOneNearTarget && UsePoof(closestMeepo))
                            return;
                    if (travel != null && travel.CanBeCasted() && dist >= 2000)
                        travel.UseAbility(Fountain.Position);
                }
                else
                {
                    var tower = EntityManager<Tower>.Entities.FirstOrDefault(x =>
                        x.IsValid && x.IsAlive && UnitExtensions.IsEnemy(x, Hero) &&
                        UnitExtensions.IsInRange(x, Hero, 1000));
                    if (tower != null)
                    {
                        Status = "Under tower: moving away from tower";
                        Hero.Move(tower.NetworkPosition.Extend(Hero.NetworkPosition, Hero.Distance2D(tower) + 250f));
                        if (closestMeepo != null)
                        {
                            if (!notOnlyOneNearTarget || !UsePoof(closestMeepo, true))
                            {
                                Hero.Move(Fountain.Position, true);
                                Status = "Under tower: move to fountain";
                            }
                            else
                            {
                                Status = "Under tower: poof";
                            }
                        }
                        else
                        {
                            var anyAlly = Owner.Main.Updater.AllMeepos
                                .Where(x => !x.Equals(Owner) && x.Hero.Distance2D(Fountain) < dist &&
                                            x.Hero.Distance2D(Hero) >= 500)
                                .OrderBy(x => x.Hero.Distance2D(Fountain)).FirstOrDefault();
                            if (anyAlly == null || !notOnlyOneNearTarget || !UsePoof(anyAlly, true))
                            {
                                Hero.Move(Fountain.Position, true);
                                Status = "Under tower: move to fountain [2]";
                            }
                            else
                            {
                                Status = "Under tower: poof [2]";
                            }
                        }
                    }
                    else
                    {
                        Status = "Move to fountain";
                        //MeepoCrappahilation.Log.Warn($"cant find tower");
                        Hero.Move(Fountain.Position);
                    }
                }
            }
        }

        public bool UsePoof(MeepoBase closestMeepo, bool queued = false)
        {
            var poof = Owner.AbilityManager.W;
            if (poof.CanBeCasted() && !poof.IsInAbilityPhase)
            {
                poof.UseAbility(closestMeepo.Hero, queued);
                Hero.Stop(true);
                UpdateManager.Unsubscribe(Owner.BehaviourManager.AutoEscapeUpdater);
                UpdateManager.BeginInvoke(() =>
                {
                    //if (BehaviourManager.AutoEscapeEnable)
                    UpdateManager.Subscribe(Owner.BehaviourManager.AutoEscapeUpdater, 300);
                }, 2000);
                return true;
            }

            return false;
        }
    }
}