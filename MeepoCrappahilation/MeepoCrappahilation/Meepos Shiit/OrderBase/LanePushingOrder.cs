﻿using System.Linq;
using Ensage;
using Ensage.Common.Extensions;
using Ensage.Common.Menu;
using Ensage.Common.Objects.UtilityObjects;
using Ensage.SDK.Helpers;
using Ensage.SDK.Menu;
using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;
using SharpDX;
using UnitExtensions = Ensage.SDK.Extensions.UnitExtensions;

namespace MeepoCrappahilation.Meepos_Shiit.OrderBase
{
    public class LanePushingOrder : OrderBaseWithPussy
    {
        private readonly Sleeper _sleeper;

        public LanePushingOrder(MeepoBase owner) : base(owner)
        {
            _sleeper = new Sleeper();

            if (EnablePussyEscaper == null)
            {
                EnablePussyEscaper = owner.Main.Config.LanePushing.Item("Pussy escaper", true);
                EnablePussyEscaper.Item.SetTooltip(
                    "if meepo can see any enemy in selected range -> meepo will do auto escape");
                EnablePussyEscaper.PropertyChanged += (sender, args) =>
                {
                    if (EnablePussyEscaper && owner.OrderSystem.CurrentOrder.Id == OrderSystem.OrderTypes.Healing)
                        DrawRangeChecker(PussyEscaperRange);
                    else
                        FlushRangeChecker();
                };
            }

            if (UsePoof == null)
            {
                UsePoof = owner.Main.Config.LanePushing.Item("Use Poof", true);
                PoofManaChecker = owner.Main.Config.LanePushing.Item("Mana checker for poof", new Slider(25, 1, 100));
            }

            if (PussyEscaperRange == null)
            {
                PussyEscaperRange =
                    owner.Main.Config.LanePushing.Item("Pussy escaper range", new Slider(1000, 1, 2500));
                //PussyEscaperHealth = owner.Main.Config.LanePushing.Item("Pussy escaper health (%)", new Slider(100, 1, 100));
                PussyEscaperRange.PropertyChanged += (sender, args) =>
                {
                    if (EnablePussyEscaper) DrawRangeChecker(PussyEscaperRange);
                };
            }

            if (EnablePussyEscaper != null && EnablePussyEscaper) DrawRangeChecker(PussyEscaperRange);
        }

        public override OrderSystem.OrderTypes Id => OrderSystem.OrderTypes.LanePushing;

        public MenuItem<Slider> PussyEscaperHealth { get; set; }

        public MenuItem<Slider> PoofManaChecker { get; set; }

        public MenuItem<bool> UsePoof { get; set; }

        public MenuItem<Slider> PussyEscaperRange { get; set; }

        public MenuItem<bool> EnablePussyEscaper { get; set; }

        public override void Execute()
        {
            if (!_sleeper.Sleeping)
            {
                CheckForPussyEscape(EnablePussyEscaper, PussyEscaperRange);
                var path = Owner.Main.LaneHelper.GetPathCache(Hero);
                var lastPoint = path[path.Count - 1];
                var closestPosition = path.Where(
                        x =>
                            x.Distance2D(lastPoint) < Hero.Position.Distance2D(lastPoint) - 300)
                    .OrderBy(pos => CheckForDist(pos, Hero))
                    .FirstOrDefault();
                _sleeper.Sleep(250);
                var closestTower = EntityManager<Tower>.Entities
                    .Where(x => x.IsAlive && UnitExtensions.IsEnemy(x, Hero)).OrderBy(z => z.Distance2D(Hero))
                    .FirstOrDefault();

                if (closestTower != null && UnitExtensions.IsInRange(closestTower, Hero, 1000))
                {
                    var myDist = Hero.Distance2D(closestTower);
                    var allyCreeps = EntityManager<Creep>.Entities.Where(x =>
                        UnitExtensions.IsAlly(x, Hero) && x.IsSpawned && x.IsAlive &&
                        UnitExtensions.IsInRange(x, closestTower, 700) && x.Distance2D(closestTower) <= myDist);
                    var enumerable = allyCreeps as Creep[] ?? allyCreeps.ToArray();
                    if (enumerable.Any())
                    {
                        if (closestTower.AttackTarget.Equals(Hero))
                        {
                            var creepForAggro = enumerable.FirstOrDefault();
                            if (creepForAggro != null)
                            {
                                Hero.Attack(creepForAggro);
                                return;
                            }

                            Hero.Move(Fountain.Position);
                            return;
                        }
                    }
                    else
                    {
                        Hero.Move(Fountain.Position);
                        return;
                    }
                }

                var creep = GetTarget();
                if (creep != null)
                {
                    if (Owner.MovementManager.Orbwalker.CanAttack(creep))
                    {
                        Hero.Attack(creep);

                        if (UsePoof && Owner.Hero.Mana / Owner.Hero.MaximumMana >= PoofManaChecker / 100F)
                        {
                            var creepsWithLowHp = EntityManager<Creep>.Entities.Count(x =>
                                x.IsSpawned && x.IsAlive && UnitExtensions.IsEnemy(x, Owner.Hero) &&
                                UnitExtensions.IsInRange(x, Owner.Hero, 375) &&
                                UnitExtensions.HealthPercent(x) <= 0.75);
                            if (creepsWithLowHp >= 2) Owner.AbilityManager.PoofToMyself();
                        }
                    }
                }
                else
                {
                    Hero.Attack(closestPosition);
                }
            }
        }

        private float CheckForDist(Vector3 pos, Unit hero)
        {
            return pos.Distance2D(hero.Position);
        }

        private Unit GetTarget()
        {
            var barracks =
                ObjectManager.GetEntitiesFast<Building>()
                    .FirstOrDefault(
                        unit =>
                            unit.IsValid && unit.IsAlive && unit.Team != Hero.Team && !(unit is Tower) &&
                            UnitExtensions.IsValidOrbwalkingTarget(Hero, unit)
                            && unit.Name != "portrait_world_unit");

            if (barracks != null) return barracks;

            var jungleMob =
                EntityManager<Creep>.Entities.FirstOrDefault(
                    unit =>
                        unit.IsValid && unit.IsSpawned && unit.IsAlive && unit.IsNeutral &&
                        UnitExtensions.IsValidOrbwalkingTarget(Hero, unit));

            if (jungleMob != null) return jungleMob;

            var creep =
                EntityManager<Creep>.Entities.Where(
                        unit =>
                            unit.IsValid && unit.IsSpawned && unit.IsAlive && unit.Team != Hero.Team &&
                            (UnitExtensions.IsValidOrbwalkingTarget(Hero, unit) || Hero.Distance2D(unit) <= 500))
                    .OrderBy(x => x.Health)
                    .FirstOrDefault();

            if (creep != null) return creep;
            var tower =
                ObjectManager.GetEntitiesFast<Tower>()
                    .FirstOrDefault(
                        unit =>
                            unit.IsValid && unit.IsAlive && unit.Team != Hero.Team &&
                            UnitExtensions.IsValidOrbwalkingTarget(Hero, unit));

            if (tower != null) return tower;
            var others =
                ObjectManager.GetEntitiesFast<Unit>()
                    .FirstOrDefault(
                        unit =>
                            unit.IsValid && !(unit is Hero) && !(unit is Creep) && unit.IsAlive &&
                            !UnitExtensions.IsInvulnerable(unit) && unit.Team != Hero.Team &&
                            UnitExtensions.IsValidOrbwalkingTarget(Hero, unit) &&
                            unit.NetworkName != ClassId.CDOTA_BaseNPC.ToString());

            if (others != null) return others;
            return null;
        }
    }
}