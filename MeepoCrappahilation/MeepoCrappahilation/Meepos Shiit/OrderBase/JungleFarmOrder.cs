﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ensage;
using Ensage.Common.Extensions;
using Ensage.Common.Menu;
using Ensage.SDK.Helpers;
using Ensage.SDK.Menu;
using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;
using SharpDX;
using UnitExtensions = Ensage.SDK.Extensions.UnitExtensions;

namespace MeepoCrappahilation.Meepos_Shiit.OrderBase
{
    public class JungleFarmOrder : OrderBaseWithPussy
    {
        private bool _inStacking;

        public JungleFarmOrder(MeepoBase owner) : base(owner)
        {
            if (PoofManaChecker == null)
                PoofManaChecker = owner.Main.Config.JungleFarm.Item("Poof mana checker (%)", new Slider(25));
            if (EnablePussyEscaper == null)
            {
                EnablePussyEscaper = owner.Main.Config.JungleFarm.Item("Pussy escaper", true);
                EnablePussyEscaper.Item.SetTooltip(
                    "if meepo can see any enemy in selected range -> meepo will do auto escape");
                EnablePussyEscaper.PropertyChanged += (sender, args) =>
                {
                    if (EnablePussyEscaper && owner.OrderSystem.CurrentOrder.Id == OrderSystem.OrderTypes.Healing)
                        DrawRangeChecker(PussyEscaperRange);
                    else
                        FlushRangeChecker();
                };
            }


            if (PussyEscaperRange == null)
            {
                PussyEscaperRange = owner.Main.Config.JungleFarm.Item("Pussy escaper range", new Slider(1000, 1, 2500));
                PussyEscaperRange.PropertyChanged += (sender, args) =>
                {
                    if (EnablePussyEscaper) DrawRangeChecker(PussyEscaperRange);
                };
            }

            if (EnablePussyEscaper != null && EnablePussyEscaper) DrawRangeChecker(PussyEscaperRange);

            _inStacking = false;
        }

        public MenuItem<Slider> PussyEscaperRange { get; set; }

        public MenuItem<bool> EnablePussyEscaper { get; set; }

        public static MenuItem<Slider> PoofManaChecker { get; set; }
        public override OrderSystem.OrderTypes Id => OrderSystem.OrderTypes.JungleFarm;
        private bool FarmAncients => Owner.Main.Config.JungleFarm.GetValue<bool>("Farm ancients");
        private bool FarmEnemyCamp => Owner.Main.Config.JungleFarm.GetValue<bool>("Farm enemy camps");
        private bool FarmTogether => !Owner.Main.Config.JungleFarm.GetValue<bool>("1 camp -> 1 meepo");

        private bool Recheck =>
            Owner.Main.Config.JungleFarm.GetValue<bool>("Help other meepos if there not any free camps");

        private bool UsePoof => Owner.Main.Config.JungleFarm.GetValue<bool>("Use poof");
        private bool CheckForBounty => Owner.Main.Config.JungleFarm.GetValue<bool>("Check bounty runes");

        private bool SkipCampWithAllyHeroes =>
            Owner.Main.Config.JungleFarm.GetValue<bool>("Dont farm jungle spot with ally heroes");

        private IEnumerable<Camp> Camps => Owner.Main.CampManager.Camps;
        private IEnumerable<Vector3> StackPoinits => Owner.Main.CampManager.StackPoints;

        public override void Execute()
        {
            if (CheckForBounty && !_inStacking)
            {
                var closestBount = Owner.Main.RuneManager.BountyPoints
                    .Where(y => y.Owner == null || y.Owner == Owner)
                    .OrderBy(x => x.Position.Distance2D(Owner.Hero))
                    .FirstOrDefault();

                var findRune = EntityManager<Rune>.Entities
                    .Where(x => x.RuneType == RuneType.Bounty && x.Distance2D(Hero) <= 1000 && x.IsVisible).ToList();
                if (findRune.Any())
                {
                    Hero.PickUpRune(findRune.First());
                    return;
                }

                if (closestBount != null)
                {
                    var time = Math.Abs((Math.Round(Game.GameTime) +
                                         Owner.Hero.Distance2D(closestBount.Position) / Owner.Hero.MovementSpeed) %
                                        300) <= 1f;
                    if (time)
                    {
                        if (closestBount.Owner == null)
                        {
                            closestBount.Owner = Owner;
                            Owner.OrderSystem.SetOrder(new RunePicking(Owner));
                        }

                        return;
                    }
                }
            }

            CheckForPussyEscape(EnablePussyEscaper, PussyEscaperRange);
            var allyHeroes = EntityManager<Hero>.Entities.Where(x =>
                x.IsAlive && UnitExtensions.IsAlly(x, Owner.Hero) && x.HeroId != HeroId.npc_dota_hero_meepo);
            var camp = Camps.Where(x =>
                    (x.Owner == null || x.Owner == Owner || FarmTogether) && !x.IsEmpty &&
                    (FarmAncients || !x.IsAncient) && (FarmEnemyCamp || x.IsAlly) &&
                    (!SkipCampWithAllyHeroes || !allyHeroes.Any(z => z.Distance2D(x.Position) <= 500) ||
                     Owner.Hero.Distance2D(x.Position) <= 300))
                .OrderBy(x => x.Position.Distance2D(Owner.Hero.Position)).FirstOrDefault();
            if (camp == null)
                if (Recheck)
                    camp = Camps.Where(x =>
                            !x.IsEmpty &&
                            (FarmAncients || !x.IsAncient) && (FarmEnemyCamp || x.IsAlly))
                        .OrderBy(x => x.Position.Distance2D(Owner.Hero.Position)).FirstOrDefault();
            if (camp != null)
            {
                if (camp.Owner == null) camp.Owner = Owner;
                if (!_inStacking)
                {
                    var distance = camp.Position.Distance(Owner.Hero.Position);
                    var secs = (int) (Game.GameTime % 60);

                    if (secs >= 55)
                    {
                        Owner.Hero.Stop();
                        _inStacking = true;
                        return;
                    }

                    if (distance >= 200)
                    {
                        if (distance <= 600)
                        {
                            var enemy = EntityManager<Unit>.Entities.FirstOrDefault(x =>
                                x.IsAlive && x.IsSpawned && x.IsVisible &&
                                UnitExtensions.IsEnemy(x, Owner.Hero) &&
                                UnitExtensions.IsInRange(x, Owner.Hero, 250));
                            if (enemy != null)
                            {
                                if (UsePoof && !camp.IsAncient &&
                                    Owner.Hero.Mana / Owner.Hero.MaximumMana >= PoofManaChecker / 100F)
                                    Owner.AbilityManager.PoofToMyself();
                                Owner.MovementManager.Attack(enemy);
                                //MeepoCrappahilation.Log.Warn($"[{Owner.Index}] Attacking (2) {enemy.NetworkName} {enemy.Name}");
                                return;
                            }
                        }

                        Owner.Hero.Move(camp.Position);
                        if (UsePoof && Owner.Hero.Mana / Owner.Hero.MaximumMana >= PoofManaChecker / 100F)
                        {
                            var closestMeepo = Owner.Main.Updater.AllMeepos.Where(x => !x.Equals(Owner))
                                .OrderBy(x => x.Hero.Distance2D(camp.Position)).FirstOrDefault();
                            if (closestMeepo != null && closestMeepo.Hero.Distance2D(camp.Position) + 500 <=
                                Owner.Hero.Distance2D(camp.Position))
                                Owner.AbilityManager.PoofTo(closestMeepo.Hero);
                        }

                        //MeepoCrappahilation.Log.Warn($"[{Owner.Index}] Dist {distance}");
                    }
                    else
                    {
                        var enemy = EntityManager<Unit>.Entities.FirstOrDefault(x =>
                            x.IsAlive && x.IsSpawned && x.IsVisible &&
                            UnitExtensions.IsEnemy(x, Owner.Hero) &&
                            UnitExtensions.IsInRange(x, Owner.Hero, 400));
                        if (enemy != null)
                        {
                            if (UsePoof && !camp.IsAncient &&
                                Owner.Hero.Mana / Owner.Hero.MaximumMana >= PoofManaChecker / 100F)
                                Owner.AbilityManager.PoofToMyself();
                            Owner.MovementManager.Attack(enemy);
                            //MeepoCrappahilation.Log.Warn($"[{Owner.Index}] Attacking {enemy.NetworkName} {enemy.Name}");
                        }
                        else
                        {
                            //MeepoCrappahilation.Log.Warn($"[{Owner.Index}] Set camp # {camp.Id} to fresh");
                            camp.SetEmpty();
                        }
                    }
                }
                else
                {
                    var secs = (int) (Game.GameTime % 60);
                    if (secs < 50)
                    {
                        _inStacking = false;
                        return;
                    }

                    var closestPositionForStacking =
                        StackPoinits.OrderBy(x => x.Distance2D(Owner.Hero)).FirstOrDefault();
                    if (!closestPositionForStacking.IsZero) Owner.Hero.Move(closestPositionForStacking);
                }
            }
            else
            {
                var shrine = EntityManager<Unit>.Entities
                    .Where(x => x.NetworkName == ClassId.CDOTA_BaseNPC_Healer.ToString() && x.Team == Owner.Hero.Team &&
                                x.IsAlive).OrderBy(x => x.Distance2D(Owner.Hero)).FirstOrDefault();
                if (shrine != null) Owner.Hero.Move(shrine.Position + new Vector3(-200, 300, 0));
            }
        }
    }
}