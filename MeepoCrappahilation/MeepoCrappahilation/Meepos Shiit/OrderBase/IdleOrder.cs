﻿using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;

namespace MeepoCrappahilation.Meepos_Shiit.OrderBase
{
    public class IdleOrder : OrderBase
    {
        public IdleOrder(MeepoBase owner) : base(owner)
        {
        }

        public override OrderSystem.OrderTypes Id => OrderSystem.OrderTypes.Idle;
    }
}