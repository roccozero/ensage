﻿using Ensage;
using Ensage.SDK.Helpers;
using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;

namespace MeepoCrappahilation.Meepos_Shiit.OrderBase
{
    public class DodgeOrder : OrderBase
    {
        private readonly MeepoBase _owner;
        private float Time;

        public DodgeOrder(MeepoBase owner) : base(owner)
        {
            _owner = owner;
            Time = Game.RawGameTime + 2.5f;
            UpdateManager.Subscribe(TimeChecker, 125);
        }

        public override OrderSystem.OrderTypes Id => OrderSystem.OrderTypes.Dodge;

        public override bool CanBeLast => false;
        public override bool CanFlushWithMouse => false;

        private void TimeChecker()
        {
            if (Time - Game.RawGameTime <= 0)
            {
                _owner.OrderSystem.FlushOrder();
                UpdateManager.Unsubscribe(TimeChecker);
            }
        }

        public void UpdateTime()
        {
            Time = Game.RawGameTime + 2.5f;
        }
    }
}