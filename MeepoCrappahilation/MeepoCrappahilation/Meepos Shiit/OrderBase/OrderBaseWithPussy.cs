﻿using System.Collections.Generic;
using System.Linq;
using Ensage;
using Ensage.SDK.Extensions;
using Ensage.SDK.Helpers;
using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;
using SharpDX;

namespace MeepoCrappahilation.Meepos_Shiit.OrderBase
{
    public abstract class OrderBaseWithPussy : OrderBase
    {
        public float Range;

        protected OrderBaseWithPussy(MeepoBase owner) : base(owner)
        {
        }

        public void CheckForPussyEscape(bool isEnable, float range)
        {
            if (!isEnable)
                return;
            Range = range;
            var enemy = EntityManager<Hero>.Entities.Any(x =>
                x.IsAlive && x.IsEnemy(Owner.Hero) && x.IsVisible && x.IsInRange(Owner.Hero, range));
            if (enemy)
            {
                Owner.OrderSystem.SetOrder(new HealingOrder(Owner));
                var closestEnemy = EntityManager<Hero>.Entities.Where(y =>
                            y.IsEnemy(Owner.Hero) && y.IsAlive && y.IsVisible &&
                            Owner.AbilityManager.Earthbind.CanHit(y)
                        /*Ensage.Common.Extensions.AbilityExtensions.CanHit(Owner.AbilityManager.Earthbind, y)*/)
                    .OrderBy(z => z.Distance2D(Owner.Hero)).FirstOrDefault();

                if (Owner.AbilityManager.Earthbind.CanBeCasted && !Owner.Hero.IsInvisible())
                    if (closestEnemy != null)
                        Owner.AbilityManager.EarthBind(closestEnemy);
                var enemies = EntityManager<Hero>.Entities.Where(x => x.IsValid && x.IsAlive && x.IsEnemy(Hero));
                var closestMeepo = Owner.Main.Updater.AllMeepos
                    .Where(x => !x.Equals(Owner) && x.Hero.Distance2D(Hero) >= 750 && CheckForEnemies(x, enemies))
                    .OrderBy(x => x.Hero.Distance2D(Fountain)).FirstOrDefault();
                if (closestMeepo != null) (Owner.OrderSystem.CurrentOrder as HealingOrder)?.UsePoof(closestMeepo);
            }
        }

        private bool CheckForEnemies(MeepoBase meepo, IEnumerable<Hero> enemies)
        {
            return enemies.Select(enemy => meepo.Hero.IsInRange(enemy, 1300)).All(inInRange => !inInRange);
        }

        public void DrawRangeChecker(float range)
        {
            UpdateManager.BeginInvoke(() =>
            {
                if (Owner.OrderSystem.CurrentOrder.Id == OrderSystem.OrderTypes.JungleFarm ||
                    Owner.OrderSystem.CurrentOrder.Id == OrderSystem.OrderTypes.LanePushing ||
                    Owner.OrderSystem.CurrentOrder.Id == OrderSystem.OrderTypes.RunePicking)
                {
                    Range = range;
                    Owner.Main.Context.Particle.DrawRange(Owner.Hero, $"pussyRange {Owner.Index}", range, Color.Red);
                }
            }, 5);
        }

        public void FlushRangeChecker()
        {
            Owner.Main.Context.Particle.Remove($"pussyRange {Owner.Index}");
        }

        public void OnChange()
        {
            Owner.Main.Context.Particle.DrawRange(Owner.Hero, $"pussyRange {Owner.Index}", Range, Color.Red);
        }
    }
}