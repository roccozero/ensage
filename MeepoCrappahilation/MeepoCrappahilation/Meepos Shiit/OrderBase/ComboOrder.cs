﻿using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;

namespace MeepoCrappahilation.Meepos_Shiit.OrderBase
{
    public class ComboOrder : OrderBase
    {
        public ComboOrder(MeepoBase owner) : base(owner)
        {
        }

        public override OrderSystem.OrderTypes Id => OrderSystem.OrderTypes.Combo;
        public override bool CanFlushWithMouse => false;
    }
}