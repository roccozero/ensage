using Ensage;
using Ensage.SDK.Extensions;

namespace MeepoCrappahilation.Meepos_Shiit.MeepoTypes
{
    public class IllusionMeepo : MeepoBase
    {
        public IllusionMeepo(MeepoCrappahilation main, Hero hero) : base(main, hero)
        {
        }

        public bool IsPoofable()
        {
            var modifier = Hero.GetModifierByName("modifier_illusion");
            return modifier?.RemainingTime >= 1.7;
        }
    }
}