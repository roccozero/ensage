using System.Linq;
using Ensage;
using Ensage.Abilities;
using MeepoCrappahilation.Features;

namespace MeepoCrappahilation.Meepos_Shiit.MeepoTypes
{
    public class MainMeepo : MeepoBase
    {
        public MainMeepo(MeepoCrappahilation main, Hero hero) : base(main, hero)
        {
            AbilityManager = new AbilityManager(this);

            BehaviourManager = new BehaviourManager(this);

            if (PoofHelper.DrawPoofRange)
                main.Context.Particle.DrawRange(Hero, $"RangeFor{Index}", 375f, PoofHelper.RangeColor);

            var ultimate =
                hero.Spellbook.Spells.First(x => x.Id == AbilityId.meepo_divided_we_stand) as DividedWeStand;
            if (ultimate != null)
                Index = ultimate.UnitIndex;
            else
                MeepoCrappahilation.Log.Error($"cant init ultimate for meepo -> {hero.Handle} [{this}]");
        }
    }
}