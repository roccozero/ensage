﻿using Ensage;

namespace MeepoCrappahilation.Meepos_Shiit.MeepoTypes
{
    public abstract class MeepoBase
    {
        public readonly MeepoCrappahilation Main;
        public AbilityManager AbilityManager;
        public BehaviourManager BehaviourManager;
        public Hero Hero;
        public int Index = -1;
        public MovementManager MovementManager;
        public OrderSystem OrderSystem;

        protected MeepoBase(MeepoCrappahilation main, Hero hero)
        {
            Main = main;
            Hero = hero;
            MeepoCrappahilation.Log.Info($"[MeepoInit] Added new meepo [{hero.Handle}] [{this}]");

            OrderSystem = new OrderSystem(this);

            MovementManager = new MovementManager(this);
        }

        public bool IsAlive => Hero.IsAlive;
        public float GetHealthPercent => Hero.Health / (float) Hero.MaximumHealth;
        public float BlockForHealing { get; set; }
    }

    /*public class Meepo
    {
        public readonly MeepoCrappahilation Main;
        public Hero Hero;
        public OrderSystem OrderSystem;
        public MovementManager MovementManager;
        public AbilityManager AbilityManager;
        public BehaviourManager BehaviourManager;
        public bool IsMainHero;
        public bool IsInDodge;
        public bool IsAlive => Hero.IsAlive;
        public int Index;
        private static readonly ILog Log = AssemblyLogs.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public float GetHealthPercent => (float)Hero.Health / (float)Hero.MaximumHealth;
        public float BlockForHealing { get; set; }

        public Meepo(MeepoCrappahilation main, Hero me)
        {
            
            Main = main;
            Hero = me;
            IsMainHero = false;
            
            var ultimate = me.Spellbook.SpellR as DividedWeStand;
            if (ultimate != null)
            {
                Index = ultimate.UnitIndex;
                IsMainHero = ultimate.UnitIndex == 0;
            }
            else
            {
                Log.Error($"cant init ultimate for meepo -> {me.Handle} \r [{this}]");
            }
            OrderSystem = new OrderSystem(this);

            MovementManager = new MovementManager(this);
            AbilityManager = new AbilityManager(this);
            
            BehaviourManager = new BehaviourManager(this);
        }
    }*/
}