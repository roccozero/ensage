﻿using System.Collections.Generic;
using Ensage;
using Ensage.Common;
using Ensage.Common.Objects.UtilityObjects;
using Ensage.SDK.Input;
using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;
using MeepoCrappahilation.Meepos_Shiit.OrderBase;
using SharpDX;

namespace MeepoCrappahilation.Meepos_Shiit
{
    public class GuiOrderChanger
    {
        private readonly Sleeper _sleeper;
        private List<Button> _buttonList;

        public bool CanDraw;

        public GuiOrderChanger()
        {
            _sleeper = new Sleeper();
            CanDraw = false;
        }

        public MeepoBase Owner { get; set; }

        public void CreateButtons(Vector2 startPos, Vector2 getTextSize)
        {
            _buttonList = new List<Button>
            {
                new Button(new IdleOrder(Owner), startPos, getTextSize),
                new Button(new JungleFarmOrder(Owner)),
                new Button(new LanePushingOrder(Owner)),
                new Button(new HealingOrder(Owner)),
                new Button(new IdleOnBaseOrder(Owner, true))
            };
        }

        public void SetMeepo(MeepoBase meepo, Vector2 vector2, Vector2 getTextSize)
        {
            if (_sleeper.Sleeping)
                return;
            _sleeper.Sleep(250);
            if (Owner == null)
            {
                Owner = meepo;
                CanDraw = true;
                CreateButtons(vector2, getTextSize);
            }
            else
            {
                Owner = null;
                CanDraw = false;
            }
        }

        public void DrawMenu()
        {
            var count = 0;
            var mousePosition = Game.MouseScreenPosition;

            foreach (var button in _buttonList)
            {
                var color = new Color(0, 155, 255, 75);
                var startPos = button.CalcPosition(count++);
                if (Utils.IsUnderRectangle(mousePosition, startPos.X, startPos.Y, button.TextSize.X, button.TextSize.Y))
                {
                    if ((Owner?.Main.Context.Input.ActiveButtons & MouseButtons.Left) != 0)
                    {
                        Owner?.OrderSystem.SetOrder(button.Order);
                        Owner = null;
                        CanDraw = false;
                    }

                    color = new Color(0, 255, 0, 75);
                }

                button.DrawButton(startPos, color);
            }
        }
    }
}