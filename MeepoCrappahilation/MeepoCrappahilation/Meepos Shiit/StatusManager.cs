﻿using System;
using System.Reflection;
using Ensage;
using Ensage.Common;
using Ensage.Common.Menu;
using Ensage.Common.Objects;
using Ensage.SDK.Input;
using Ensage.SDK.Menu;
using log4net;
using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;
using MeepoCrappahilation.Meepos_Shiit.OrderBase;
using PlaySharp.Toolkit.Logging;
using SharpDX;

namespace MeepoCrappahilation.Meepos_Shiit
{
    public class StatusManager : Movable
    {
        private static readonly ILog Log = AssemblyLogs.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly MeepoCrappahilation _main;

        public StatusManager(MeepoCrappahilation main, IInputManager input) : base(input)
        {
            _main = main;
            PositionX = Config.StatusHelper.Item("Position X", new Slider(116, 0, 500));
            PositionY = Config.StatusHelper.Item("Position Y", new Slider(139, 0, 500));
            Config.StatusHelper.Item<bool>("Enable drawing").PropertyChanged += (sender, args) =>
            {
                if (IsEnable)
                    Drawing.OnDraw += DrawingOnOnDraw;
                else
                    Drawing.OnDraw -= DrawingOnOnDraw;
            };
            if (IsEnable) Drawing.OnDraw += DrawingOnOnDraw;

            GuiOrderChanger = new GuiOrderChanger();
        }

        private Config Config => _main.Config;
        private bool IsEnable => Config.StatusHelper.GetValue<bool>("Enable drawing");
        private bool IsMoving => Config.StatusHelper.GetValue<bool>("Enable Moving");
        private bool DrawPoofCooldown => Config.StatusHelper.GetValue<bool>("Draw Poof cooldown");
        private bool DrawNetCooldown => Config.StatusHelper.GetValue<bool>("Draw Net cooldown");
        private bool DrawDebugInfo => Config.StatusHelper.GetValue<bool>("Draw Debug info");
        private Vector2 GetTextSize => new Vector2(Config.StatusHelper.GetValue<Slider>("Text Size").Value);

        private int GetIconSize => Config.StatusHelper.GetValue<Slider>("Icon Size").Value;

        /*private int PositionX
        {
            get { return Config.StatusHelper.GetValue<Slider>("Position X").Value; }
            set
            {
                Config.StatusHelper.Target.Item("MeepoCrappahilation.Statushelper.PositionX").SetValue(new Slider(value, 0, 500));
                //Config.StatusHelper.Item("Position X", new Slider(value, 0, 500));
            }
        }

        private int PositionY
        {
            get { return Config.StatusHelper.GetValue<Slider>("Position Y").Value; }
            set
            {
                Config.StatusHelper.Target.Item("MeepoCrappahilation.Statushelper.PositionY").SetValue(new Slider(value, 0, 500));
                //Config.StatusHelper.Item("Position Y", new Slider(value, 0, 500));
            }
        }*/
        public IInputManager InputManager => _main.Context.Input;

        public MenuItem<Slider> PositionY { get; set; }

        public MenuItem<Slider> PositionX { get; set; }
        public GuiOrderChanger GuiOrderChanger { get; }

        private void DrawingOnOnDraw(EventArgs args)
        {
            var startPos = new Vector2(PositionX, PositionY);
            var maxSize = new Vector2();
            var mPos = Game.MouseScreenPosition;
            var canDraw = GuiOrderChanger.CanDraw;
            foreach (var meepo in _main.Updater.AllMeepos)
            {
                if (meepo is IllusionMeepo)
                    continue;
                var order = meepo.OrderSystem.CurrentOrder.Id;
                var textSize = Drawing.MeasureText(order.ToString(), "Arial", GetTextSize, FontFlags.None);
                Drawing.DrawText($"{order}", startPos, GetTextSize, Color.White, FontFlags.None);
                Drawing.DrawRect(startPos, textSize, Color.White, true);
                if (Utils.IsUnderRectangle(mPos, startPos.X, startPos.Y, textSize.X, textSize.Y))
                    if ((InputManager.ActiveButtons & MouseButtons.Left) != 0)
                        GuiOrderChanger.SetMeepo(meepo, startPos + new Vector2(textSize.X + 10, 0), GetTextSize);

                if (!canDraw && DrawPoofCooldown)
                    DrawPoofStatus(meepo.AbilityManager.W, startPos + new Vector2(textSize.X + 5, 0),
                        new Vector2(textSize.Y));
                if (!canDraw && DrawNetCooldown)
                {
                    var extraPos = DrawPoofCooldown ? 5 + textSize.Y : 0;
                    DrawNetStatus(meepo.AbilityManager.Earthbind.Ability,
                        startPos + new Vector2(textSize.X + 5 + extraPos, 0),
                        new Vector2(textSize.Y));
                }

                if (!canDraw && DrawDebugInfo && meepo.OrderSystem.CurrentOrder is HealingOrder healingOrder)
                {
                    var pos = DrawPoofCooldown
                        ? startPos + new Vector2(textSize.X + 5 + textSize.Y * (DrawNetCooldown ? 2 : 1), 0)
                        : startPos + new Vector2(textSize.X + 5 + (DrawNetCooldown ? textSize.Y : 0), 0);

                    var text = healingOrder.Status;
                    Drawing.DrawText(text, pos, new Vector2(textSize.Y), Color.White, FontFlags.None);
                }

                startPos += new Vector2(0, GetIconSize);
                maxSize.X = Math.Max(maxSize.X, textSize.X);
                maxSize.Y += GetIconSize;
            }

            if (canDraw)
                GuiOrderChanger.DrawMenu();

            if (IsMoving)
            {
                var tempStartPos = new Vector2(PositionX, PositionY);
                if (CanMoveWindow(ref tempStartPos, maxSize, true))
                {
                    PositionX.Item.SetValue(new Slider((int) tempStartPos.X, 0, 500));
                    PositionY.Item.SetValue(new Slider((int) tempStartPos.Y, 0, 500));
                }
            }
        }

        private void DrawPoofStatus(Ability poof, Vector2 pos, Vector2 size)
        {
            Drawing.DrawRect(pos, size, Textures.GetSpellTexture(poof.Id.ToString()));
            var state = poof.AbilityState;
            var clr = new Color(0);
            if (poof.IsInAbilityPhase)
                clr = new Color(0, 155, 0, 150);
            else
                switch (state)
                {
                    case AbilityState.Ready:
                        break;
                    case AbilityState.NotEnoughMana:
                        clr = new Color(0, 0, 155, 100);
                        break;
                    case AbilityState.OnCooldown:
                        clr = new Color(255, 255, 255, 100);
                        break;
                    case AbilityState.NotLearned:
                        clr = new Color(100, 100, 100, 150);
                        break;
                }
            Drawing.DrawRect(pos, size, clr);
            if (state == AbilityState.OnCooldown)
            {
                var cd = ((int) poof.Cooldown + 1).ToString();
                var textSize = size * 0.75f;
                var measureText = Drawing.MeasureText(cd, "Arial", textSize, FontFlags.None);
                var startPos = pos + new Vector2(size.X / 2 - measureText.X / 2, size.Y / 2 - measureText.Y / 2);
                Drawing.DrawRect(startPos, measureText, new Color(0, 0, 0, 200));
                Drawing.DrawText(cd, startPos, textSize, Color.White, FontFlags.None);
            }

            Drawing.DrawRect(pos, size, Color.White, true);
        }

        private void DrawNetStatus(Ability ability, Vector2 pos, Vector2 size)
        {
            Drawing.DrawRect(pos, size, Textures.GetSpellTexture(ability.Id.ToString()));
            var state = ability.AbilityState;
            var clr = new Color(0);
            if (ability.IsInAbilityPhase)
                clr = new Color(0, 155, 0, 150);
            else
                switch (state)
                {
                    case AbilityState.Ready:
                        break;
                    case AbilityState.NotEnoughMana:
                        clr = new Color(0, 0, 155, 100);
                        break;
                    case AbilityState.OnCooldown:
                        clr = new Color(255, 255, 255, 100);
                        break;
                    case AbilityState.NotLearned:
                        clr = new Color(100, 100, 100, 150);
                        break;
                }
            Drawing.DrawRect(pos, size, clr);
            if (state == AbilityState.OnCooldown)
            {
                var cd = ((int) ability.Cooldown + 1).ToString();
                var textSize = size * 0.75f;
                var measureText = Drawing.MeasureText(cd, "Arial", textSize, FontFlags.None);
                var startPos = pos + new Vector2(size.X / 2 - measureText.X / 2, size.Y / 2 - measureText.Y / 2);
                Drawing.DrawRect(startPos, measureText, new Color(0, 0, 0, 200));
                Drawing.DrawText(cd, startPos, textSize, Color.White, FontFlags.None);
            }

            Drawing.DrawRect(pos, size, Color.White, true);
        }
    }
}