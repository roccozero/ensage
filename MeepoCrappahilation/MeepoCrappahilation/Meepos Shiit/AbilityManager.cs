﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ensage;
using Ensage.Common.Enums;
using Ensage.Common.Extensions;
using Ensage.Common.Menu;
using Ensage.Common.Objects.UtilityObjects;
using Ensage.SDK.Abilities;
using Ensage.SDK.Abilities.Components;
using Ensage.SDK.Abilities.Items;
using Ensage.SDK.Extensions;
using Ensage.SDK.Helpers;
using Ensage.SDK.Inventory.Metadata;
using Ensage.SDK.Prediction;
using MeepoCrappahilation.Abilities;
using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;
using SharpDX;
using AbilityId = Ensage.AbilityId;
using UnitExtensions = Ensage.SDK.Extensions.UnitExtensions;

// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace MeepoCrappahilation.Meepos_Shiit
{
    public class AbilityManager
    {
        public static Sleeper EarthBindGlobalCooldown = new Sleeper();

        public static List<HeroId> HeroesWithEscape = new List<HeroId>
        {
            HeroId.npc_dota_hero_storm_spirit,
            HeroId.npc_dota_hero_queenofpain,
            HeroId.npc_dota_hero_antimage,
            HeroId.npc_dota_hero_faceless_void,
            HeroId.npc_dota_hero_wisp,
            HeroId.npc_dota_hero_magnataur,
            HeroId.npc_dota_hero_night_stalker,
            HeroId.npc_dota_hero_riki,
            HeroId.npc_dota_hero_slark,
            HeroId.npc_dota_hero_weaver
        };

        private static readonly MultiSleeper Sleeper = new MultiSleeper();
        private readonly MeepoBase _meepo;

        public Earthbind Earthbind;

        public Ability /*Q, */
            W;

        public AbilityManager(MeepoBase meepo)
        {
            _meepo = meepo;

            //Q = meepo.Hero.Spellbook.SpellQ;
            W = meepo.Hero.Spellbook.SpellW;
            Earthbind = new Earthbind(meepo.Hero.Spellbook.SpellQ);
            //Console.WriteLine($"EarthBind: {Earthbind.Range} {Earthbind.Speed} {Earthbind.CastRange} {Earthbind.CastPoint} {Earthbind.Radius}");
            PoofStartTime = float.MaxValue;
            Entity.OnBoolPropertyChange += (sender, args) =>
            {
                /*if (args.PropertyName == "m_bInAbilityPhase")
                {
                    MeepoCrappahilation.Log.Warn($"senderId: {sender.ClassId} {sender.NetworkName} CLASSIDSHIT: {ClassId.CDOTA_Ability_Meepo_Poof} {sender.Owner.ClassId}");
                }*/
                try
                {
                    if (args.PropertyName == "m_bInAbilityPhase" &&
                        sender != null &&
                        Hero != null &&
                        sender.Owner.Equals(Hero) &&
                        sender.NetworkName == ClassId.CDOTA_Ability_Meepo_Poof.ToString())
                        PoofStartTime = args.NewValue ? Game.RawGameTime : float.MaxValue;
                }
                catch (Exception)
                {
                    //Console.WriteLine(e);
                }
            };

            Drawing.OnDraw += args =>
            {
                if (Math.Abs(PoofStartTime - float.MaxValue) < 0.01)
                    return;
                var delta = (float) ((Game.RawGameTime - PoofStartTime) * 70 / 1.5);
                var startPosition = Drawing.WorldToScreen(Hero.Position) - new Vector2(35, 0);
                if (startPosition.IsZero)
                    return;
                Drawing.DrawRect(startPosition + new Vector2(0, 32), new Vector2(delta, 10),
                    WCanHit ? Color.YellowGreen : Color.OrangeRed);
                Drawing.DrawRect(startPosition + new Vector2(0, 32), new Vector2(70, 10),
                    new Color(0, 0, 0, 100));
                Drawing.DrawRect(startPosition + new Vector2(0, 32), new Vector2(70, 10),
                    Color.Black, true);
            };

            if (meepo is MainMeepo)
            {
                var itemManager = meepo.Main.Context.Inventory;
                itemManager.Attach(this);
            }
        }

        public float PoofStartTime { get; set; }

        public MeepoCrappahilation Main => _meepo.Main;

        public Hero Hero => _meepo.Hero;

        public bool WCanHit { get; set; }

        [ItemBinding] public item_blink Blink { get; set; }
        [ItemBinding] public item_sheepstick Hex { get; set; }
        [ItemBinding] public item_abyssal_blade Abyssal { get; set; }
        [ItemBinding] public item_diffusal_blade Diffusal { get; set; }
        [ItemBinding] public item_ethereal_blade Ethereal { get; set; }
        [ItemBinding] public item_bloodthorn Bloodthorn { get; set; }
        [ItemBinding] public item_orchid Orchid { get; set; }
        [ItemBinding] public item_silver_edge Silver { get; set; }
        [ItemBinding] public item_invis_sword ShadowBlade { get; set; }
        [ItemBinding] public item_solar_crest Solar { get; set; }
        [ItemBinding] public item_medallion_of_courage Medal { get; set; }
        [ItemBinding] public item_nullifier Nullifier { get; set; }
        [ItemBinding] public item_manta Manta { get; set; }
        [ItemBinding] public item_veil_of_discord Veil { get; set; }

        private float MinDistanceForBlink =>
            _meepo.Main.Config.AbilitiesInCombo.GetValue<Slider>("Blink min distance").Value;

        public bool PoofToMyself()
        {
            if (!W.CanBeCasted() || !Hero.CanCast() || W.IsInAbilityPhase)
                return false;
            W.UseAbility(_meepo.Hero);
            return true;
        }

        public bool PoofTo(Unit target)
        {
            if (!IsAbilityEnabled(W.Id))
                return false;
            if (target == null || !W.CanBeCasted() || !Hero.CanCast() || W.IsInAbilityPhase ||
                UnitExtensions.IsMagicImmune(target) && target.Distance2D(Hero) <= 450)
                return false;
            var isStunned = UnitExtensions.IsStunned(target);
            if (Earthbind.CanBeCasted)
            {
                var netMod = target.GetModifierByName("modifier_meepo_earthbind");
                if (netMod != null && netMod.RemainingTime <= 1.5f + Game.Ping / 1000f)
                {
                    var anyOtherMeepoWithFreeNetAndNotInPoofAction =
                        _meepo.Main.Updater.AllMeepos.Any(x => !(x is IllusionMeepo) &&
                                                               x.OrderSystem.CurrentOrder.Id ==
                                                               OrderSystem.OrderTypes.Combo && !x.Equals(_meepo) &&
                                                               x.AbilityManager.Earthbind.CanBeCasted &&
                                                               x.AbilityManager.Earthbind.CanHit(target) &&
                                                               !x.AbilityManager.W.IsInAbilityPhase);
                    if (anyOtherMeepoWithFreeNetAndNotInPoofAction) return false;
                }
                else
                {
                    if (IsClosestMeepo(target.NetworkPosition) && !isStunned) return false;
                }
            }

            var closestMeepo = isStunned
                ? Main.Updater.AllMeepos.OrderBy(x => x.Hero.Distance2D(target))
                    .FirstOrDefault()
                : Main.Updater.AllMeepos /*.Where(x=> !(x is IllusionMeepo))*/.OrderBy(x => x.Hero.Distance2D(target))
                    .FirstOrDefault(x => !x.Hero.Equals(Hero));
            WCanHit = true;
            if (closestMeepo != null)
            {
                WCanHit = target.IsInRange(closestMeepo.Hero, 375);
                if (target.IsInRange(closestMeepo.Hero, 375))
                {
                    W.UseAbility(closestMeepo.Hero);
                    UpdateManager.BeginInvoke(async () =>
                    {
                        await Task.Delay(350);
                        while (W.IsInAbilityPhase)
                        {
                            if (target.IsInRange(closestMeepo.Hero, 375))
                            {
                                WCanHit = true;
                            }
                            else
                            {
                                WCanHit = false;
                                Hero.Stop();
                                //break;
                            }

                            await Task.Delay(10);
                        }
                    });
                    WCanHit = false;
                    return true;
                }
            }

            return false;
        }

        public bool PoofTo(Vector3 pos)
        {
            if (!W.CanBeCasted() || !Hero.CanCast())
                return false;

            W.UseAbility(pos);
            return true;
        }

        public bool PoofToAllyMeepo(Hero target)
        {
            if (!W.CanBeCasted() || !Hero.CanCast() || W.IsInAbilityPhase)
                return false;

            W.UseAbility(target);
            return true;
        }

        public bool EarthBind(Vector3 pos)
        {
            if (!Earthbind.CanBeCasted || !Hero.CanCast())
                return false;

            Earthbind.UseAbility(pos);
            return true;
        }

        public bool EarthBind(Unit target)
        {
            if (!IsAbilityEnabled(Earthbind.Ability.Id))
                return false;
            if (target == null || !Earthbind.CanBeCasted || !Hero.CanCast() || !Earthbind.CanHit(target) ||
                Earthbind.Ability.IsInAbilityPhase || W.IsInAbilityPhase ||
                UnitExtensions
                    .IsMagicImmune(target) /*|| target.IsUnitState(UnitState.Stunned)*/ || Sleeper.Sleeping("w4hex"))
                return false;
            if (!IsClosestMeepo(target.NetworkPosition))
            {
                MeepoCrappahilation.Log.Info($"Net #{_meepo.Index} NOT casted. Not closest");
                return false;
            }

            if (EarthBindGlobalCooldown.Sleeping)
            {
                MeepoCrappahilation.Log.Info($"Net #{_meepo.Index} NOT casted. Sleeper");
                return false;
            }

            /*var anyCastedAtThisTime =
                EntityManager<Unit>.Entities.Any(x =>
                    x.Team == Hero.Team && x.NetworkName == ClassId.CDOTA_BaseNPC.ToString() && x.Health == 150 &&
                    x.DayVision == 300 && x.NightVision == 300);
            if (anyCastedAtThisTime)
            {
                MeepoCrappahilation.Log.Info($"Net #{_meepo.Index} NOT casted. Found some nets in air");
                return false;
            }*/
            var isHeroWithEscape = false;
            if (target is Hero hero)
            {
                var heroid = hero.HeroId;
                isHeroWithEscape = HeroesWithEscape.Contains(heroid);
            }

            if (!isHeroWithEscape && Sleeper.Sleeping("w4eblade"))
                return false;
            var eModifier = target.FindModifier("modifier_meepo_earthbind") ??
                            /*target.FindModifier(Hex?.TargetModifierName) ??*/
                            (!(isHeroWithEscape || target.GetItemById(AbilityId.item_blink)?.Cooldown <= 3f) &&
                             target.MovementSpeed <= 300f
                                ? target.FindModifier(Ethereal?.TargetModifierName)
                                : null);
            var hitDelay = Earthbind.GetHitTime(target) / 1000f; //.GetHitDelay(target);
            if (hitDelay < eModifier?.RemainingTime - 0.15)
                return false;
            var stunDuration = target.ImmobileDuration(); //Ensage.Common.Utils.DisableDuration(target);
            if (W.CanHit(target) && W.CanBeCasted() && stunDuration > 1.6 + Game.Ping / 1000) return false;

            var input = Earthbind.GetPredictionInput(target);
            var output = Earthbind.GetPredictionOutput(input);
            if (Earthbind.UseAbility(target, HitChance.Medium))
            {
                EarthBindGlobalCooldown.Sleep(Math.Max(500, hitDelay * 1000f + 150f));
                MeepoCrappahilation.Log.Info(
                    $"Net #{_meepo.Index} casted to {target.Name} with {output.HitChance} hit chance");
                return true;
            }

            MeepoCrappahilation.Log.Info($"Net #{_meepo.Index} NOT casted. HitChance: {output.HitChance}");
            return false;
        }

        private bool IsItemEnabled(AbilityId id)
        {
            return _meepo.Main.Config.AbilitiesInCombo.GetValue<AbilityToggler>("Items: ").IsEnabled(id.ToString());
        }

        private bool IsLinkenBreakerEnabled(AbilityId id)
        {
            return _meepo.Main.Config.AbilitiesInCombo.GetValue<AbilityToggler>("Linken breaker: ")
                .IsEnabled(id.ToString());
        }

        private bool IsAbilityEnabled(AbilityId id)
        {
            return _meepo.Main.Config.AbilitiesInCombo.GetValue<AbilityToggler>("Abilities: ").IsEnabled(id.ToString());
        }

        private bool IsItemEnabled(ActiveAbility ability)
        {
            return IsItemEnabled(ability.Item.Id);
        }

        private bool IsLinkenBreakerEnabled(ActiveAbility ability)
        {
            return IsLinkenBreakerEnabled(ability.Item.Id);
        }

        private bool UseInvis(ActiveAbility[] invis)
        {
            foreach (var ability in invis)
            {
                if (ability == null || !ability.CanBeCasted || !IsItemEnabled(ability)) continue;
                ability.UseAbility();
                return true;
            }

            return false;
        }

        public bool UseItems(Unit target, bool usePoof = true)
        {
            if (Blink != null && IsItemEnabled(Blink) && Blink.CanBeCasted &&
                !Sleeper.Sleeping("SmartBlink") && /*Blink.CanHit(target)*/
                target.IsInRange(Hero, Blink.CastRange + 100) && !target.IsInRange(Hero, MinDistanceForBlink))
            {
                Blink.UseAbility(Hero.Position.Extend(target.Position,
                    Math.Min(Hero.Distance2D(target), Blink.CastRange - 5)));
                Sleeper.Sleep(Game.Ping * 2.5f, "After blink2");
            }

            if (Hero.IsInRange(target, 1500) && UseInvis(new ActiveAbility[] {Silver, ShadowBlade}))
            {
                Sleeper.Sleep(500, "Invis");
                return false;
            }

            if (Manta != null && IsItemEnabled(Manta) && Manta.CanBeCasted && Hero.IsInRange(target, 550))
                Manta.UseAbility();

            var linkenProtected = UnitExtensions.IsLinkensProtected(target);
            var list =
                new List<RangedAbility> {Abyssal, Diffusal, Ethereal, Bloodthorn, Orchid, Nullifier, Hex, Medal, Solar}
                    .Where(x =>
                        x != null && IsItemEnabled(x) && (!linkenProtected || IsLinkenBreakerEnabled(x)) &&
                        x.CanBeCasted && x.CanHit(target));
            var isHeroWithEscape = false;
            if (target is Hero hero)
            {
                var heroid = hero.HeroId;
                var dangItem = hero.GetItemById(ItemId.item_blink)?.CanBeCasted() ??
                               hero.GetItemById(ItemId.item_black_king_bar)?.CanBeCasted();
                isHeroWithEscape = HeroesWithEscape.Contains(heroid) || dangItem.HasValue;
            }

            if (Veil != null && Veil.CanBeCasted && Veil.CanHit(target) &&
                !target.HasAnyModifiers(Veil.TargetModifierName))
                Veil.UseAbility(target);

            foreach (var ability in list)
            {
                if (ability is IHasTargetModifier targetMod)
                    if (UnitExtensions.HasModifier(target, targetMod.TargetModifierName) ||
                        ability.Item.Id == AbilityId.item_blink && Hero.Distance2D(target) <= 350)
                        continue;

                var isHex = ability is item_sheepstick;
                var isAbyssal = ability is item_abyssal_blade;
                var isEtherial = ability is item_ethereal_blade;
                if (isHex && !linkenProtected)
                {
                    Sleeper.Sleep(500, "w4hex");
                }
                else if (isAbyssal && !linkenProtected)
                {
                    Sleeper.Sleep(500, "w4ablade");
                }
                else if (isEtherial && !linkenProtected)
                {
                    MeepoCrappahilation.Log.Warn(
                        $"DamageFromEReal [1]: {ability.GetDamage(target)}>{target.Health + target.HealthRegeneration * ability.GetHitTime(target) / 1000f} UsePoof: ${usePoof}");
                    var ebDamage = ability.GetDamage(target);
                    if (!(ebDamage >
                          target.Health + target.HealthRegeneration * ability.GetHitTime(target) / 1000f) &&
                        (!TargetWillDieFromPoofs(target, ebDamage) || !usePoof))
                        continue;
                    Sleeper.Sleep(1000, "w4eblade");
                }

                if (target.IsUnitState(UnitState.Stunned) ||
                    (Sleeper.Sleeping("w4ablade") ||
                     target.HasAnyModifiers(Hex?.TargetModifierName, Ethereal?.TargetModifierName) ||
                     Sleeper.Sleeping("w4eblade")) && isHex && !isHeroWithEscape)
                    continue;

                if ((target.IsUnitState(UnitState.Hexed) || Sleeper.Sleeping("w4hex")) && isEtherial) continue;

                ability.UseAbility(target);
            }

            return false;
        }

        private bool TargetWillDieFromPoofs(Unit target, float ebDamage)
        {
            var countOnLowRange = _meepo.Main.Updater.AllMeepos.Where(x => !(x is IllusionMeepo) &&
                                                                           x.OrderSystem.CurrentOrder.Id ==
                                                                           OrderSystem.OrderTypes.Combo &&
                                                                           x.AbilityManager.W.CanBeCasted() &&
                                                                           x.AbilityManager.W.CanHit(target));
            var totalDamageFromLowRange = countOnLowRange.Sum(meepoBase =>
                meepoBase.Hero.CalculateSpellDamage(target, DamageType.Magical,
                    _meepo.Main.PoofDamagehelper.GetPoofDamage * 2f));

            var healsBeforePoofDamage = target.Health + target.HealthRegeneration * 1.5f - ebDamage;
            MeepoCrappahilation.Log.Warn($"DamageFromPoof [1]: {totalDamageFromLowRange}>{healsBeforePoofDamage}");
            if (totalDamageFromLowRange > healsBeforePoofDamage) return true;

            countOnLowRange = _meepo.Main.Updater.AllMeepos.Where(x => !(x is IllusionMeepo) &&
                                                                       x.OrderSystem.CurrentOrder.Id ==
                                                                       OrderSystem.OrderTypes.Combo &&
                                                                       x.AbilityManager.W.CanBeCasted());
            totalDamageFromLowRange = countOnLowRange.Sum(meepoBase =>
                meepoBase.Hero.CalculateSpellDamage(target, DamageType.Magical,
                    _meepo.Main.PoofDamagehelper.GetPoofDamage * (meepoBase.Hero.IsInRange(target, 375) ? 2f : 1f)));
            MeepoCrappahilation.Log.Warn($"DamageFromPoof [2]: {totalDamageFromLowRange}>{healsBeforePoofDamage}");
            return totalDamageFromLowRange > healsBeforePoofDamage;
        }

        public void UseAbilities(Unit target, bool useEarthBind = true, bool usePoof = true, bool useItems = true)
        {
            if (target == null)
                return;
            if (!Sleeper.Sleeping("After blink"))
            {
                if (_meepo.Main.Config.SmartBlink && Blink != null && !Sleeper.Sleeping("SmartBlink") &&
                    Blink.CanBeCasted && Hero.IsInRange(target, Blink.CastRange + 200) && !Hero.IsInRange(target, 500))
                {
                    Sleeper.Sleep(1800, "SmartBlink");
                    Sleeper.Sleep(1350, "PrepareForBlink");
                }

                if (Sleeper.Sleeping("SmartBlink") && Blink != null && Blink.CanBeCasted)
                {
                    foreach (var meepo in Main.Updater.GetFreeMeepos(_meepo.Main.OrderManager
                        .DoComboOnlyForSelectedMeepos))
                    {
                        if (meepo is MainMeepo) continue;

                        meepo.AbilityManager.PoofToAllyMeepo(Main.Owner);
                    }

                    if (_meepo is MainMeepo && !Sleeper.Sleeping("PrepareForBlink"))
                        if (Blink != null && Blink.CanBeCasted)
                            if (Hero.IsInRange(target, Blink.CastRange + 200))
                            {
                                var pos = Hero.Position.Extend(target.NetworkPosition,
                                    Math.Min(1150, Hero.Distance2D(target)));
                                Blink.UseAbility(pos);
                                Sleeper.Sleep(1000, "After blink");
                                Sleeper.Sleep(Game.Ping * 2.5f, "After blink2");
                                Sleeper.Reset("SmartBlink");
                                Sleeper.Reset("PrepareForBlink");
                            }

                    //MeepoCrappahilation.Log.Warn($"Return #{_meepo.Index}");
                    return;
                }
            }

            if (Sleeper.Sleeping("Invis"))
                return;
            if (useItems)
                UseItems(target, usePoof);
            if (Sleeper.Sleeping("Invis"))
                return;
            var blinkStillNotCasted = Blink != null && IsItemEnabled(AbilityId.item_blink) && Blink.CanBeCasted;
            if (useEarthBind && !blinkStillNotCasted)
            {
                if (!Sleeper.Sleeping("After blink2"))
                    EarthBind(target);
                else
                    return;
            }

            if (usePoof && !Sleeper.Sleeping("After blink2"))
                PoofTo(target);
        }

        private bool IsClosestMeepo(Vector3 pos)
        {
            return Main.Updater.AllMeepos.Where(x => !(x is IllusionMeepo) && x.AbilityManager.Earthbind.CanBeCasted)
                .OrderBy(x => pos.Distance2D(x.Hero.NetworkPosition)).First().Hero.Equals(Hero);
        }
    }
}