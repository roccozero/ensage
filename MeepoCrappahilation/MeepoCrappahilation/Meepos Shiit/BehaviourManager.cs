﻿using System.Linq;
using Ensage;
using Ensage.Common.Menu;
using Ensage.SDK.Extensions;
using Ensage.SDK.Helpers;
using Ensage.SDK.Menu;
using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;
using MeepoCrappahilation.Meepos_Shiit.OrderBase;

namespace MeepoCrappahilation.Meepos_Shiit
{
    public class BehaviourManager
    {
        private readonly MeepoBase _meepo;

        public BehaviourManager(MeepoBase meepo)
        {
            _meepo = meepo;
            if (AutoEscapeEnable == null)
                AutoEscapeEnable = _meepo.Main.Config.AutoEscape.Item("Enable", true);
            if (PercentForEscape == null)
                PercentForEscape = _meepo.Main.Config.AutoEscape.Item("Health percent for auto escape", new Slider(15));
            if (AutoEscapeEnable)
                UpdateManager.Subscribe(AutoEscapeUpdater, 300);
            AutoEscapeEnable.PropertyChanged += (sender, args) =>
            {
                if (AutoEscapeEnable)
                    UpdateManager.Subscribe(AutoEscapeUpdater, 300);
                else
                    UpdateManager.Unsubscribe(AutoEscapeUpdater);
            };
            UpdateManager.Subscribe(OrderExecutor, 300);
        }

        private bool EseEtherial => _meepo.Main.Config.AutoEscape.GetValue<bool>("Use Ethereal for saving");

        public static MenuItem<Slider> PercentForEscape { get; set; }

        public static MenuItem<bool> AutoEscapeEnable { get; set; }

        private void OrderExecutor()
        {
            if (!_meepo.IsAlive)
                return;
            var orderManager = _meepo.OrderSystem;
            orderManager.CurrentOrder?.Execute();
        }

        public void AutoEscapeUpdater()
        {
            if (_meepo.Main.SuperProFeature != null && _meepo.Main.Config.IsSuperPro &&
                _meepo.Main.SuperProFeature.DisableHealingOrder && _meepo.Main.CanExecute)
                return;
            if (!_meepo.IsAlive)
                return;
            var orderManager = _meepo.OrderSystem;
            var currentOrder = orderManager.CurrentOrder;
            if (EseEtherial && _meepo is MainMeepo)
                if (_meepo.AbilityManager.Ethereal != null && _meepo.AbilityManager.Ethereal.CanBeCasted)
                {
                    var meepoOnLowHp =
                        _meepo.Main.Updater.AllMeepos.Where(x =>
                                x.GetHealthPercent <= PercentForEscape / 100f &&
                                _meepo.AbilityManager.Ethereal.CanHit(x.Hero) && EntityManager<Hero>.Entities.Any(y =>
                                    y.IsEnemy(_meepo.Hero) && y.IsAlive && y.IsVisible &&
                                    y.IsInRange(x.Hero, 1500))).OrderBy(z => z.Hero.Health)
                            .FirstOrDefault();
                    if (meepoOnLowHp != null) _meepo.AbilityManager.Ethereal.UseAbility(meepoOnLowHp.Hero);
                }

            if (currentOrder.Id != OrderSystem.OrderTypes.Healing)
            {
                if (_meepo.GetHealthPercent <= PercentForEscape / 100f)
                    if (Game.RawGameTime - _meepo.BlockForHealing >= 5)
                        orderManager.SetOrder(new HealingOrder(_meepo));
            }
            else if (currentOrder.Id == OrderSystem.OrderTypes.Healing)
            {
                currentOrder.Execute();
            }
        }
    }
}