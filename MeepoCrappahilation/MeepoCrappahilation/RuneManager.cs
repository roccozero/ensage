﻿using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using Ensage;
using Ensage.SDK.Helpers;
using log4net;
using PlaySharp.Toolkit.Logging;
using SharpDX;

namespace MeepoCrappahilation
{
    public class RuneManager
    {
        private static readonly ILog Log = AssemblyLogs.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public List<RunePlaceHolder> BountyPoints;

        public RuneManager()
        {
            BountyPoints = new List<RunePlaceHolder>
            {
                new RunePlaceHolder(new Vector3(-4326, 1604, 384)),
                new RunePlaceHolder(new Vector3(-3157, 3737, 256)),
                new RunePlaceHolder(new Vector3(3692, -3614, 256)),
                new RunePlaceHolder(new Vector3(4173, -1691, 384))
            };

            var secs = (int) (Game.GameTime % 120);
            Log.Debug($"[{secs}] First refresh [rune] in {(120 - secs) * 1000}");
            UpdateManager.BeginInvoke(async () =>
                {
                    while (true)
                    {
                        secs = (int) (Game.GameTime % 120);
                        var nextDelay = (120 - secs) * 1000;
                        Log.Debug($"[{secs}] Refres runes. next in {nextDelay}");
                        await Task.Delay(nextDelay);
                        secs = (int) (Game.GameTime % 120);
                        if (Game.GameTime % 120 >= 2.5) await Task.Delay((120 - secs) * 1000);
                    }
                }, (120 - secs) * 1000);
        }
    }
}