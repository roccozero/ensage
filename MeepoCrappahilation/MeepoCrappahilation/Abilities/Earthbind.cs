﻿using Ensage;
using Ensage.SDK.Abilities;
using Ensage.SDK.Abilities.Components;
using Ensage.SDK.Extensions;
using Ensage.SDK.Prediction;
using Ensage.SDK.Prediction.Collision;

namespace MeepoCrappahilation.Abilities
{
    public class Earthbind : CircleAbility, IHasTargetModifier
    {
        public Earthbind(Ability ability) : base(ability)
        {
        }

        public override float Speed => Ability.GetAbilitySpecialData("speed");
        public override float Radius => Ability.GetAbilitySpecialData("radius");
        public override float Duration => Ability.GetAbilitySpecialData("duration");

        public override CollisionTypes CollisionTypes => CollisionTypes.None;

        public override bool HasAreaOfEffect => true;

        public override PredictionSkillshotType PredictionSkillshotType => PredictionSkillshotType.SkillshotCircle;

        public string TargetModifierName => "modifier_meepo_earthbind";
    }

    /*public class Poof : ActiveAbility, IAreaOfEffectAbility
    {
        public Poof(Ability ability) : base(ability)
        {

        }

        public float Radius => Ability.GetAbilitySpecialData("radius");
    }*/
}