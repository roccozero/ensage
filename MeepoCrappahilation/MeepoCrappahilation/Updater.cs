﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ensage;
using Ensage.Abilities;
using Ensage.SDK.Helpers;
using MeepoCrappahilation.Meepos_Shiit;
using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;

namespace MeepoCrappahilation
{
    public class Updater : IDisposable
    {
        private readonly MeepoCrappahilation _main;
        public List<MeepoBase> AllMeepos;

        public Updater(MeepoCrappahilation main)
        {
            _main = main;
            AllMeepos = new List<MeepoBase>();
            foreach (var hero in EntityManager<Hero>.Entities)
            {
                if (hero.Team != main.Team || hero.HeroId != HeroId.npc_dota_hero_meepo) continue;
                MeepoBase meepo = null;
                if (!hero.IsIllusion)
                {
                    var ultimate =
                        hero.Spellbook.Spells.First(x => x.Id == AbilityId.meepo_divided_we_stand) as DividedWeStand;
                    if (ultimate != null)
                    {
                        var index = ultimate.UnitIndex;
                        if (index == 0)
                            meepo = new MainMeepo(_main, hero);
                        else
                            meepo = new NormalMeepo(_main, hero);
                    }
                }
                else
                {
                    meepo = new IllusionMeepo(_main, hero);
                }

                if (meepo != null)
                    AllMeepos.Add(meepo);
            }

            AllMeepos = new List<MeepoBase>(AllMeepos.OrderBy(x => x.Index));

            EntityManager<Hero>.EntityAdded += EntityManagerOnEntityAdded;

            UpdateManager.Subscribe(SelectedUpdater, 50);

            UpdateManager.Subscribe(() => { AllMeepos.RemoveAll(x => x is IllusionMeepo && !x.IsAlive); }, 250);
        }

        public IEnumerable<MeepoBase> SelectedMeepos { get; set; }

        public void Dispose()
        {
            EntityManager<Hero>.EntityAdded -= EntityManagerOnEntityAdded;
            AllMeepos.Clear();
        }

        private void SelectedUpdater()
        {
            var selectedUnits = _main.LocalPlayer.Selection
                .Where(x => x is Hero && ((Hero) x).HeroId == HeroId.npc_dota_hero_meepo)
                .ToList();

            SelectedMeepos = AllMeepos.Where(x => selectedUnits.Contains(x.Hero) && x.IsAlive);
        }

        private void EntityManagerOnEntityAdded(object sender, Hero hero)
        {
            if (hero.Team != _main.Team || hero.HeroId != HeroId.npc_dota_hero_meepo ||
                AllMeepos.Any(x => x.Hero.Handle == hero.Handle))
                return;
            MeepoBase meepo = null;
            if (!hero.IsIllusion)
            {
                var ultimate =
                    hero.Spellbook.Spells.First(x => x.Id == AbilityId.meepo_divided_we_stand) as DividedWeStand;

                if (ultimate != null)
                {
                    var index = ultimate.UnitIndex;
                    if (index == 0)
                        meepo = new MainMeepo(_main, hero);
                    else
                        meepo = new NormalMeepo(_main, hero);
                }
            }
            else
            {
                meepo = new IllusionMeepo(_main, hero);
            }

            AllMeepos.Add(meepo);

            AllMeepos = new List<MeepoBase>(AllMeepos.OrderBy(x => x.Index));
        }

        public List<MeepoBase> GetIdleMeepos()
        {
            var list = AllMeepos.Where(x => x.OrderSystem.CurrentOrder.Id == OrderSystem.OrderTypes.Idle);
            return new List<MeepoBase>(list);
        }

        public List<MeepoBase> GetFreeMeepos(bool checkForSelected = false)
        {
            var list = AllMeepos.Where(x =>
                (x is IllusionMeepo || x.OrderSystem.CurrentOrder.Id != OrderSystem.OrderTypes.Healing &&
                 x.OrderSystem.CurrentOrder.Id != OrderSystem.OrderTypes.IdleOnBase) &&
                (!checkForSelected || SelectedMeepos.Contains(x)));
            return new List<MeepoBase>(list);
        }
    }
}