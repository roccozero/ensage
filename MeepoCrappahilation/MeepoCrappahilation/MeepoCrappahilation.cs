﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using Ensage;
using Ensage.SDK.Extensions;
using Ensage.SDK.Helpers;
using Ensage.SDK.Service;
using Ensage.SDK.Service.Metadata;
using log4net;
using MeepoCrappahilation.Features;
using MeepoCrappahilation.Meepos_Shiit;
using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;
using PlaySharp.Toolkit.Logging;
using SharpDX;

namespace MeepoCrappahilation
{
    [ExportPlugin(
        mode: StartupMode.Auto,
        name: "Meepo Crappahilation",
        units: new[] {HeroId.npc_dota_hero_meepo})]
    public sealed class MeepoCrappahilation : Plugin
    {
        public static readonly ILog Log = AssemblyLogs.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        [ImportMany]
#pragma warning disable 649
        private IEnumerable<IFeature> _features;
#pragma warning restore 649

        public CampManager CampManager;
        public Config Config;
        public LaneHelper LaneHelper;
        public Player LocalPlayer;

        public OrderManager OrderManager;
        public PoofDamageHelper PoofDamagehelper;
        public RuneManager RuneManager;
        public SuperProFeature SuperProFeature;

        public Hero Target;

        [ImportingConstructor]
        public MeepoCrappahilation([Import] IServiceContext context)
        {
            Context = context;
        }

        public IServiceContext Context { get; }
        public Hero Owner { get; set; }
        public Team Team { get; set; }
        public Updater Updater { get; set; }

        public StatusManager StatusManager { get; private set; }
        public TargetManager TargetManager { get; private set; }
        public bool CanExecute => OrderManager.ComboKey.Value.Active;

        protected override void OnActivate()
        {
            Owner = (Hero) Context.Owner;
            Log.Warn("1");
            LocalPlayer = Owner.Player;
            Log.Warn("2");
            Team = Owner.Team;
            Log.Warn("3");
            Config = new Config(this);
            Log.Warn("4");
            if (!Context.TargetSelector.IsActive)
                Context.TargetSelector.Activate();
            foreach (var feature in _features)
            {
                if (feature is OrderManager manager)
                {
                    OrderManager = manager;
                }
                else if (feature is SuperProFeature proFeature)
                {
                    SuperProFeature = proFeature;
                    break;
                }
                else if (feature is PoofDamageHelper helper)
                {
                    PoofDamagehelper = helper;
                }

                feature.Activate(Config);
                Log.Warn($"feature: {feature}");
            }

            Log.Warn("5");
            Updater = new Updater(this);
            Log.Warn("6");
            CampManager = new CampManager();
            Log.Warn("7");
            RuneManager = new RuneManager();
            Log.Warn("8");
            LaneHelper = new LaneHelper(this);
            Log.Warn("9");
            StatusManager = new StatusManager(this, Context.Input);
            Log.Warn("10");
            TargetManager = new TargetManager(this, Owner);
            Log.Warn("11");
            UpdateManager.Subscribe(TargetUpdater, 25);
            Log.Warn("12");
            //UpdateManager.Subscribe(IndicatorUpdater);
            UpdateManager.Subscribe(() =>
            {
                if (CanExecute)
                {
                    if (!Config.IsSuperPro || Updater.AllMeepos.Count < 3)
                    {
                        if (Target == null || !Target.IsValid || !Target.IsAlive)
                        {
                            Target = (Hero) Context.TargetSelector.GetTargets()
                                .FirstOrDefault(); //EntityManager<Hero>.Entities.FirstOrDefault(x => x.Team == Owner.GetEnemyTeam());
                            if (Target != null)
                            {
                                TargetManager.Activate(Target);
                                Context.Particle.DrawRange(Target, "circle_target", 125, Color.YellowGreen);
                                if (Context.TargetSelector.IsActive) Context?.TargetSelector?.Deactivate();
                            }
                            else
                            {
                                foreach (var meepo in OrderManager.DoComboOnlyForSelectedMeepos
                                    ? Updater.GetFreeMeepos(true)
                                    : Updater.GetFreeMeepos())
                                    meepo.MovementManager.Orbwalk(null);
                                Context.Particle.Remove("circle_target");
                                Context.Particle.Remove("line_target");
                            }

                            return;
                        }

                        var bladeMail = Target.HasAnyModifiers("modifier_item_blade_mail_reflect");
                        foreach (var meepo in OrderManager.DoComboOnlyForSelectedMeepos
                            ? Updater.GetFreeMeepos(true)
                            : Updater.GetFreeMeepos())
                        {
                            var notIllusion = !(meepo is IllusionMeepo);
                            var notInInvis = !meepo.Hero.HasAnyModifiers("modifier_item_invisibility_edge_windwalk",
                                "modifier_item_silver_edge_windwalk");
                            if (!bladeMail || Config.GetBmBehaviour == Config.BmBehaviour.All)
                            {
                                if (notIllusion)
                                    if (notInInvis || !meepo.Hero.IsInRange(Target, 1500))
                                        meepo.AbilityManager.UseAbilities(Target);

                                meepo.MovementManager.Orbwalk(Target);
                            }
                            else
                            {
                                if (notIllusion && notInInvis)
                                    meepo.AbilityManager.UseAbilities(Target, true, false);
                                meepo.MovementManager.Orbwalk(Config.GetBmBehaviour == Config.BmBehaviour.Attack
                                    ? Target
                                    : null);
                            }
                        }
                    }
                    else
                    {
                        SuperProFeature?.ExecuteCombo();
                    }
                }
                else
                {
                    if (TargetManager.IsActive)
                    {
                        Target = null;
                        TargetManager.Deactivate();
                        Context.Particle.Remove("circle_target");
                        Context.Particle.Remove("line_target");
                    }
                }
            }, 200);
            Log.Warn("14");
            //FeatureChecker.PrintNames();//Meepo Extra Paid Feature
            if (true || FeatureChecker.IsFeatureActive("MeepoExtraPaidFeature", "Meepo Extra Paid Feature"))
            {
                Log.Warn("[Meepo] Activate paid feature");
                SuperProFeature?.Activate(Config);
                Config.UpdateForPaidFeature(1);
                Log.Warn("16");
            }
            else
            {
                Log.Warn("[Meepo] Cant find Meepo extra paid feature");
                Config.UpdateForPaidFeature(-1);
            }
        }


        protected override void OnDeactivate()
        {
            UpdateManager.Unsubscribe(TargetUpdater);
            //UpdateManager.Unsubscribe(IndicatorUpdater);
            Updater?.Dispose();
            foreach (var feature in _features) feature.Dispose();
            Config?.Dispose();
        }

        private void IndicatorUpdater()
        {
            if (Target != null)
                Context.Particle.DrawDangerLine(Owner, "line_target", Target.Position);
        }

        private void TargetUpdater()
        {
            if (!CanExecute && Target != null)
            {
                if (!Context.TargetSelector.IsActive) Context.TargetSelector.Activate();
                //Cancel();
                Target = null;
                TargetManager.Deactivate();
                Context.Particle.Remove("circle_target");
                Context.Particle.Remove("line_target");
            }
        }
    }
}