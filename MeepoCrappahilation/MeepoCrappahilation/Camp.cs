﻿using Ensage;
using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;
using SharpDX;

namespace MeepoCrappahilation
{
    public class Camp
    {
        private static int _staticId;
        public int Id;

        public Camp(Vector3 position, bool ancient = false, Team team = Team.Radiant)
        {
            Position = position;
            IsEmpty = false;
            IsAncient = ancient;
            IsAlly = ObjectManager.LocalPlayer.Team == team;
            IsOccupied = false;
            Owner = null;
            Id = StaticId;
        }

        public Vector3 Position { get; set; }
        public bool IsEmpty { get; set; }
        public bool IsAncient { get; set; }
        public bool IsAlly { get; set; }
        public bool IsOccupied { get; set; }
        public MeepoBase Owner { get; set; }

        public static int StaticId
        {
            get => ++_staticId;
            set => _staticId = value;
        }

        public void SetEmpty()
        {
            IsOccupied = false;
            Owner = null;
            IsEmpty = true;
        }
    }
}