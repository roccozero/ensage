﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MeepoCrappahilation
{
    public static class FeatureChecker
    {
        private const int V = 0b101000010;
        private static readonly Dictionary<string, Assembly> AssemblyFeatures = new Dictionary<string, Assembly>();

        static FeatureChecker()
        {
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
                AssemblyFeatures.Add(assembly.GetName().Name, assembly);
        }

        public static bool IsFeatureActive(params string[] names)
        {
            return names.Select(name => AssemblyFeatures.Any(x => x.Key == name && IsValid(x.Value))).Any(any => any);
        }

        public static bool IsFeatureActive(string assemblyName)
        {
            return AssemblyFeatures.Any(x => x.Key == assemblyName && IsValid(x.Value));
        }

        public static void PrintNames()
        {
            Console.WriteLine("------------Assemlies------------");
            foreach (var assembly in AssemblyFeatures)
            {
                var name = assembly.Key;
                Console.WriteLine($">> AssemblyName: {name}");
            }
        }

        private static bool IsValid(Assembly asm)
        {
            //Console.WriteLine($"Checking for valid -> {asm.GetName().Name}");
            var result = -1;
            try
            {
                /*MeepoCrappahilation.Log.Warn($"1");
                foreach (var t2 in asm.GetTypes())
                {
                    MeepoCrappahilation.Log.Warn($"[Type] [{t2.FullName}] [{t2.Name}]");
                }*/
                //var t = asm.GetType($"{asm.GetName().Name}.Test");
                var name = asm.GetName().Name.Replace(" ", "");
                var t = asm.GetType($"{name}.Test");
                //MeepoCrappahilation.Log.Warn($"2 t!=null is {t!=null}");
                if (t != null)
                {
                    var method = t.GetMethod("ValidTest");
                    //MeepoCrappahilation.Log.Warn($"3");
                    //var o = Activator.CreateInstance(t, null);
                    if (method != null)
                    {
                        //MeepoCrappahilation.Log.Warn($"4");
                        var r = method.Invoke(null, null);
                        //MeepoCrappahilation.Log.Warn($"5");
                        result = (int) r;
                        //MeepoCrappahilation.Log.Warn($"6");
                    }
                    else
                    {
                        MeepoCrappahilation.Log.Warn("Fake method");
                    }
                }
                else
                {
                    MeepoCrappahilation.Log.Warn("Cant find method");
                }
            }
            catch (Exception e)
            {
                MeepoCrappahilation.Log.Warn($"Error [{asm.GetName().Name}]: {e}");
            }

            return result == V;
        }
    }
}