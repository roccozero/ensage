using Ensage;
using Ensage.Common.Extensions;
using Ensage.SDK.Handlers;
using Ensage.SDK.Helpers;
using SharpDX;

namespace MeepoCrappahilation
{
    public class TargetManager
    {
        private readonly MeepoCrappahilation _main;
        public readonly Hero Owner;
        public float LastTimeUnderVision;

        public Unit Target;
        public Vector3 TargetPosition;

        public TargetManager(MeepoCrappahilation main, Hero owner)
        {
            _main = main;
            Owner = owner;
            Updater = UpdateManager.Subscribe(Callback, isEnabled: false);
        }

        public bool IsActive => Updater.IsEnabled;
        private IUpdateHandler Updater { get; }

        public void Activate()
        {
            LastTimeUnderVision = 0f;
            Updater.IsEnabled = true;
        }

        public void Activate(Unit target)
        {
            LastTimeUnderVision = 0f;
            SetTarget(target);
            Updater.IsEnabled = true;
            MeepoCrappahilation.Log.Warn($"[TargetManager] Activate: {target.Name}");
        }

        public void Deactivate()
        {
            Updater.IsEnabled = false;
            LastTimeUnderVision = 0f;
            Target = null;
            _main.Context.Particle.Remove("Range" + Owner.Owner.Handle);
            MeepoCrappahilation.Log.Warn("[TargetManager] Deactivate");
        }

        public void SetTarget(Unit target)
        {
            Target = target;
        }

        private void Callback()
        {
            if (Target == null)
            {
            }
            else
            {
                var isVisible = Target.IsVisible;
                if (isVisible)
                    LastTimeUnderVision = Game.RawGameTime;
                TargetPosition = GetTargetPosition(isVisible);
                _main.Context.Particle.DrawTargetLine(Owner, "Range" + Owner.Owner.Handle, TargetPosition);
            }
        }

        private Vector3 GetTargetPosition(bool isVisible)
        {
            if (Target == null)
                return Vector3.Zero;
            return isVisible
                ? Target.NetworkPosition
                : Target.Predict((Game.RawGameTime - LastTimeUnderVision) * 1000f);
            ;
        }
    }
}