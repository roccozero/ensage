﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Ensage;
using Ensage.Common.Menu;
using Ensage.SDK.Extensions;
using Ensage.SDK.Menu;
using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;

namespace MeepoCrappahilation
{
    public class Config : IDisposable
    {
        public enum BmBehaviour
        {
            NetOnly,
            Attack,
            All
        }

        public readonly MeepoCrappahilation MeepoCrappahilation;

        public Config(MeepoCrappahilation main)
        {
            MeepoCrappahilation = main;
            Factory = MenuFactory.Create("Meepo Crappahilation");
            OrderManager = Factory.Menu("Order Manager");
            PoofHelper = Factory.MenuWithTexture("Poof Helper", AbilityId.meepo_poof.ToString());
            StatusHelper = Factory.Menu("Status helper");
            AbilitiesInCombo = Factory.Menu("Abilities in combo");
            AutoEscape = Factory.Menu("Auto Escape");
            JungleFarm = Factory.Menu("Jungle Farm");
            LanePushing = Factory.Menu("Lane Pushing");
            AutoDodge = Factory.Menu("Auto Dodge");
            PoofDamageHelper = Factory.Menu("Poof Damage Helper");

            BlademailBehaviour =
                Factory.Item("BladeMail behaviour", new StringList("Use only earthbind", "Only Attacking", "All"));
            SmartBlink = Factory.Item("Smart blink", true);
            SmartBlink.Item.SetTooltip("Super smart blink+poof combo wombo");

            SmartBlinkKey = Factory.Item("Smart blink key combo", new KeyBind('0'));
            SmartBlinkKey.Item.SetTooltip("Poof + blink after 1.3 secs on mouse position");

            SmartBlinkKey.PropertyChanged += async (sender, args) =>
            {
                if (SmartBlinkKey)
                {
                    var mainHero = main.Updater.AllMeepos.First(x => x is MainMeepo);
                    var blink = mainHero.AbilityManager.Blink;
                    if (blink != null && blink.CanBeCasted)
                    {
                        foreach (var meepo in main.Updater.GetFreeMeepos())
                        {
                            if (meepo is MainMeepo)
                                continue;
                            meepo.AbilityManager.PoofToAllyMeepo(mainHero.Hero);
                        }

                        await Task.Delay(1350);
                        var pos = Game.MousePosition;
                        var blinkPos = mainHero.Hero.NetworkPosition.Extend(pos,
                            Math.Min(blink.CastRange, mainHero.Hero.NetworkPosition.Distance(pos)));
                        blink.UseAbility(blinkPos);
                    }
                }
            };

            NetKey = Factory.Item("Net Key", new KeyBind('0'));
            NetKey.PropertyChanged += (sender, args) =>
            {
                if (NetKey)
                {
                    var pos = Game.MousePosition;
                    var closestToPos = main.Updater.GetFreeMeepos()
                        .Where(x => x.AbilityManager.Earthbind.CanBeCasted &&
                                    x.AbilityManager.Earthbind.CastRange + 400f >= x.Hero.Distance2D(pos))
                        .OrderBy(z => z.Hero.Distance2D(pos)).FirstOrDefault();
                    closestToPos?.AbilityManager.EarthBind(pos);
                }
            };
            ComboBehaviourHotkeyChanger = Factory.Item("Combo behaviour changer", new KeyBind('0'));
            var b = false;
            ComboBehaviourHotkeyChanger.PropertyChanged += (sender, args) =>
            {
                if (!ComboBehaviourHotkeyChanger.Value.Active)
                    return;
                b = !b;
                var isPaid = ComboMode.Item.GetValue<StringList>().SList.Any(x => x.Equals("Super pr@"));
                ComboMode.Item.SetValue(isPaid
                    ? new StringList(new[] {"Default", "Super pr@"}, b ? 1 : 0)
                    : new StringList(new[] {"Default", "[need to buy]"}, b ? 1 : 0));
            };
        }

        public MenuItem<KeyBind> ComboBehaviourHotkeyChanger { get; set; }

        public MenuItem<StringList> ComboMode { get; set; }

        public MenuItem<KeyBind> NetKey { get; set; }

        public MenuItem<KeyBind> SmartBlinkKey { get; set; }

        public MenuItem<bool> SmartBlink { get; set; }
        public MenuItem<StringList> BlademailBehaviour { get; set; }

        public BmBehaviour GetBmBehaviour => (BmBehaviour) BlademailBehaviour.Value.SelectedIndex;

        public MenuItem<bool> BlockPlayerInputWhileCombo { get; set; }

        public MenuFactory LanePushing { get; set; }

        public MenuFactory JungleFarm { get; set; }

        public MenuFactory StatusHelper { get; set; }

        public MenuFactory PoofHelper { get; set; }

        public MenuFactory Factory { get; set; }

        public MenuFactory OrderManager { get; set; }

        public MenuFactory AbilitiesInCombo { get; set; }
        public MenuFactory AutoEscape { get; set; }
        public MenuFactory AutoDodge { get; set; }
        public MenuFactory PoofDamageHelper { get; }
        public MenuFactory SeuperProFeature { get; set; }

        public bool IsSuperPro => ComboMode.Value.SelectedValue.Equals("Super pr@");

        public void Dispose()
        {
            Factory?.Dispose();
        }

        public void UpdateForPaidFeature(int featureId)
        {
            switch (featureId)
            {
                case 1:
                    ComboMode =
                        Factory.Item("Combo behaviour", new StringList("Default", "Super pr@"));
                    break;
                default:
                    ComboMode =
                        Factory.Item("Combo behaviour", new StringList("Default", "[need to buy]"));
                    break;
            }
        }
    }
}