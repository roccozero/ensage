﻿using System.Linq;
using System.Reflection;
using Ensage;
using Ensage.Common.Menu;
using Ensage.SDK.Extensions;
using Ensage.SDK.Menu;
using log4net;
using MeepoCrappahilation.Meepos_Shiit;
using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;
using MeepoCrappahilation.Meepos_Shiit.OrderBase;
using PlaySharp.Toolkit.Logging;

namespace MeepoCrappahilation.Features
{
    public class OrderManager : IFeature
    {
        private static readonly ILog Log = AssemblyLogs.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private bool _disableTimer = true;
        private Config _main;

        public MenuItem<bool> DoComboOnlyForSelectedMeepos { get; set; }


        public MenuItem<KeyBind> IdleOnBase { get; set; }

        public MenuItem<bool> BackToOldOrderAfterCombo { get; set; }

        public MenuItem<KeyBind> LaneFarmingKey { get; set; }

        public MenuItem<KeyBind> JungleFarmingKey { get; set; }

        public MenuItem<KeyBind> HealingKey { get; set; }

        public MenuItem<KeyBind> ComboKey { get; set; }

        public MenuItem<KeyBind> IdleKey { get; set; }

        public MenuFactory Factory { get; set; }

        public void Activate(Config main)
        {
            _main = main;
            Factory = main.OrderManager;
            IdleKey = Factory.Item("Idle", new KeyBind('0'));
            IdleKey.Item.ValueChanged += KeyChanger;
            ComboKey = Factory.Item("Combo", new KeyBind('0'));
            BackToOldOrderAfterCombo = Factory.Item("Back to last order after combo", false);
            main.BlockPlayerInputWhileCombo = Factory.Item("Block player input while doing combo", true);
            DoComboOnlyForSelectedMeepos = Factory.Item("Do combo only for selected meepos", false);
            ComboKey.Item.ValueChanged += KeyChanger;
            HealingKey = Factory.Item("Healing", new KeyBind('0'));
            HealingKey.Item.ValueChanged += KeyChanger;
            JungleFarmingKey = Factory.Item("Jungle farming", new KeyBind('0'));
            JungleFarmingKey.Item.ValueChanged += KeyChanger;
            LaneFarmingKey = Factory.Item("Lane farming", new KeyBind('0'));
            LaneFarmingKey.Item.ValueChanged += KeyChanger;
            IdleOnBase = Factory.Item("Idle on base", new KeyBind('0'));
            IdleOnBase.Item.ValueChanged += KeyChanger;
        }

        public void Dispose()
        {
            Factory?.Dispose();
        }

        private void KeyChanger(object sender, OnValueChangeEventArgs args)
        {
            if (!(sender is MenuItem item))
                return;
            var newOne = args.GetNewValue<KeyBind>().Active;
            var oldOne = args.GetOldValue<KeyBind>().Active;
            if (!newOne || oldOne)
            {
                if (!newOne)
                    if (item.DisplayName == "Combo")
                    {
                        _disableTimer = true;
                        foreach (var meepo in _main.MeepoCrappahilation.Updater.AllMeepos.Where(x =>
                            x.OrderSystem.CurrentOrder.Id == OrderSystem.OrderTypes.Combo))
                            meepo.OrderSystem.SetOrder(BackToOldOrderAfterCombo
                                ? meepo.OrderSystem.LastOrder
                                : new IdleOrder(meepo));
                    }

                return;
            }

            /*foreach (var meepo in _main.MeepoCrappahilation.Updater.SelectedMeepos)
            {
                meepo.OrderSystem.SetOrder(_ordersDictionary[item.DisplayName]);
            }*/
            switch (item.DisplayName)
            {
                case "Idle":
                    foreach (var meepo in _main.MeepoCrappahilation.Updater.SelectedMeepos)
                        if (meepo.OrderSystem.CurrentOrder.Id == OrderSystem.OrderTypes.Idle)
                            meepo.OrderSystem.SetOrder(meepo.OrderSystem.LastOrder);
                        else
                            meepo.OrderSystem.SetOrder(new IdleOrder(meepo));

                    break;
                case "Combo":
                    if (_main.IsSuperPro &&
                        _main.MeepoCrappahilation.Updater.AllMeepos.Count(x => !(x is IllusionMeepo)) >= 3)
                    {
                        var fountain = ObjectManager.GetEntities<Unit>()
                            .FirstOrDefault(x =>
                                x.NetworkName == ClassId.CDOTA_Unit_Fountain.ToString() && x.Team ==
                                _main.MeepoCrappahilation.Updater.AllMeepos.First().Hero.Team);
                        var anyMeepoOnBase = _main.MeepoCrappahilation.Updater.AllMeepos.Where(x =>
                                x.OrderSystem.CurrentOrder.Id == OrderSystem.OrderTypes.IdleOnBase)
                            .OrderBy(z => z.Hero.Distance2D(fountain)).FirstOrDefault();
                        if (anyMeepoOnBase != null)
                        {
                            var countOfIdle = _main.MeepoCrappahilation.Updater.AllMeepos.Count(x =>
                                x.OrderSystem.CurrentOrder.Id == OrderSystem.OrderTypes.IdleOnBase);
                            if (countOfIdle == 1)
                            {
                                //soo, all right   
                            }
                            else
                            {
                                //make only 1
                                foreach (var meepo in _main.MeepoCrappahilation.Updater.AllMeepos.Where(x =>
                                    x.OrderSystem.CurrentOrder.Id == OrderSystem.OrderTypes.IdleOnBase &&
                                    x.Index != anyMeepoOnBase.Index))
                                    meepo.OrderSystem.SetOrder(new ComboOrder(meepo));
                            }
                        }
                        else
                        {
                            //make one meepo idle to base
                            anyMeepoOnBase = _main.MeepoCrappahilation.Updater.AllMeepos
                                .OrderBy(z => z.Hero.Distance2D(fountain)).FirstOrDefault();
                            anyMeepoOnBase?.OrderSystem.SetOrder(new IdleOnBaseOrder(anyMeepoOnBase));
                        }

                        foreach (var meepo in _main.MeepoCrappahilation.Updater.GetFreeMeepos())
                            meepo.OrderSystem.SetOrder(new ComboOrder(meepo));
                    }
                    else
                    {
                        _disableTimer = false;
                        foreach (var meepo in _main.MeepoCrappahilation.Updater.GetFreeMeepos())
                            meepo.OrderSystem.SetOrder(new ComboOrder(meepo));

//                        UpdateManager.BeginInvoke(async () =>
//                        {
//                            MeepoCrappahilation.Log.Warn("lets start");
//                            while (ComboKey.Value.Active)
//                            {
//                                var meeposInCombo = _main.MeepoCrappahilation.Updater.AllMeepos.Where(x =>
//                                    x.OrderSystem.CurrentOrder.Id == OrderSystem.OrderTypes.Combo);
//                                var meepoBases = meeposInCombo as MeepoBase[] ?? meeposInCombo.ToArray();
//                                MeepoCrappahilation.Log.Warn(
//                                    $"Selected: {_main.MeepoCrappahilation.Owner.Player.Selection.Count(x => x is Meepo)} InCombo {meepoBases.Count()}");
//                                if (_main.MeepoCrappahilation.Owner.Player.Selection.Count(x => x is Meepo) !=
//                                    meepoBases.Count())
//                                {
//                                    var first = true;
//                                    foreach (var meepo in meepoBases)
//                                    {
//                                        if (first)
//                                        {
//                                            meepo.Hero.Select();
//                                            first = false;
//                                        }
//                                        else
//                                        {
//                                            meepo.Hero.Select(true);
//                                        }
//                                    }
//                                }
//
//                                await Task.Delay(8000);
//                            }
//                        }, 500);
                    }

                    break;
                case "Healing":
                    foreach (var meepo in _main.MeepoCrappahilation.Updater.SelectedMeepos)
                        meepo.OrderSystem.SetOrder(new HealingOrder(meepo));

                    break;
                case "Jungle farming":
                    foreach (var meepo in _main.MeepoCrappahilation.Updater.SelectedMeepos)
                        meepo.OrderSystem.SetOrder(new JungleFarmOrder(meepo));

                    break;
                case "Lane farming":
                    foreach (var meepo in _main.MeepoCrappahilation.Updater.SelectedMeepos)
                        meepo.OrderSystem.SetOrder(new LanePushingOrder(meepo));

                    break;
                case "Idle on base":
                    foreach (var meepo in _main.MeepoCrappahilation.Updater.SelectedMeepos)
                        meepo.OrderSystem.SetOrder(new IdleOnBaseOrder(meepo));
                    /*var fountain = ObjectManager.GetEntities<Unit>()
                            .FirstOrDefault(x => x.ClassId == ClassId.CDOTA_Unit_Fountain && x.Team == meepo.Hero.Team);
                        if (fountain != null) meepo.Hero.Move(fountain.Position);*/

                    break;
            }
        }
    }
}