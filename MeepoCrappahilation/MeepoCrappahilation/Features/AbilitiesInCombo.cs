﻿using System.Collections.Generic;
using System.Reflection;
using Ensage;
using Ensage.Common.Menu;
using Ensage.SDK.Menu;
using log4net;
using PlaySharp.Toolkit.Logging;

namespace MeepoCrappahilation.Features
{
    internal class AbilitiesInCombo : IFeature
    {
        private static readonly ILog Log = AssemblyLogs.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private Config _main;

        public MenuItem<AbilityToggler> LinkenBreaker { get; set; }

        public MenuItem<Slider> BlinkMinDist { get; set; }

        public MenuItem<AbilityToggler> Items { get; set; }

        public MenuItem<AbilityToggler> Abilities { get; set; }

        public MenuFactory Factory { get; set; }

        public void Activate(Config main)
        {
            _main = main;
            Factory = main.AbilitiesInCombo;
            var dict = new Dictionary<string, bool>
            {
                {AbilityId.meepo_earthbind.ToString(), true},
                {AbilityId.meepo_poof.ToString(), true}
            };
            var items = new Dictionary<string, bool>
            {
                {AbilityId.item_sheepstick.ToString(), true},
                {AbilityId.item_blink.ToString(), true},
                {AbilityId.item_orchid.ToString(), true},
                {AbilityId.item_bloodthorn.ToString(), true},
                {AbilityId.item_abyssal_blade.ToString(), true},
                {AbilityId.item_diffusal_blade.ToString(), true},
                {AbilityId.item_ethereal_blade.ToString(), true},
                {AbilityId.item_silver_edge.ToString(), true},
                {AbilityId.item_invis_sword.ToString(), true},
                {AbilityId.item_medallion_of_courage.ToString(), true},
                {AbilityId.item_solar_crest.ToString(), true},
                {AbilityId.item_nullifier.ToString(), true},
                {AbilityId.item_manta.ToString(), true},
                {AbilityId.item_veil_of_discord.ToString(), true}
            };
            var linkens = new Dictionary<string, bool>
            {
                {AbilityId.item_sheepstick.ToString(), true},
                {AbilityId.item_orchid.ToString(), true},
                {AbilityId.item_bloodthorn.ToString(), true},
                {AbilityId.item_abyssal_blade.ToString(), true},
                {AbilityId.item_diffusal_blade.ToString(), true},
                {AbilityId.item_ethereal_blade.ToString(), true},
                {AbilityId.item_nullifier.ToString(), true}
            };

            Abilities = Factory.Item("Abilities: ", new AbilityToggler(dict));
            Items = Factory.Item("Items: ", new AbilityToggler(items));
            LinkenBreaker = Factory.Item("Linken breaker: ", new AbilityToggler(linkens));
            BlinkMinDist = Factory.Item("Blink min distance", new Slider(350, 0, 1000));
        }

        public void Dispose()
        {
            _main?.Dispose();
            Factory?.Dispose();
        }
    }
}