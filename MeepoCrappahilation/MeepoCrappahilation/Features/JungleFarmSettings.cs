﻿using System;
using System.Reflection;
using Ensage;
using Ensage.SDK.Menu;
using log4net;
using PlaySharp.Toolkit.Logging;
using SharpDX;

namespace MeepoCrappahilation.Features
{
    internal class JungleFarmSettings : IFeature
    {
        private static readonly ILog Log = AssemblyLogs.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private Config _main;

        public MenuFactory Factory { get; set; }

        public void Activate(Config main)
        {
            _main = main;
            Factory = main.JungleFarm;
            Factory.Item("Farm ancients", false);
            Factory.Item("Farm enemy camps", false);
            Factory.Item("1 camp -> 1 meepo", true);
            Factory.Item("Help other meepos if there not any free camps", true);
            Factory.Item("Use poof", true);
            Factory.Item("Check bounty runes", true);
            Factory.Item("Dont farm jungle spot with ally heroes", true);
            var status = Factory.Item("Draw camp status", true);
            var runeStatus = Factory.Item("Draw rune status", true);
            if (status)
                Drawing.OnDraw += DrawingOnOnDraw;
            if (runeStatus)
                Drawing.OnDraw += RuneDrawing;
            status.PropertyChanged += (sender, args) =>
            {
                if (status)
                    Drawing.OnDraw += DrawingOnOnDraw;
                else
                    Drawing.OnDraw -= DrawingOnOnDraw;
            };
            runeStatus.PropertyChanged += (sender, args) =>
            {
                if (runeStatus)
                    Drawing.OnDraw += RuneDrawing;
                else
                    Drawing.OnDraw -= RuneDrawing;
            };

            //var boolka=Factory.GetValue<bool>("Farm ancients");
        }

        public void Dispose()
        {
            Drawing.OnDraw -= DrawingOnOnDraw;
            _main?.Dispose();
        }

        private void RuneDrawing(EventArgs args)
        {
            foreach (var rune in _main.MeepoCrappahilation.RuneManager.BountyPoints)
            {
                Vector2 pos;
                var onScreen = Drawing.WorldToScreen(rune.Position, out pos);
                if (onScreen)
                    Drawing.DrawText($"[Meepo will check: {rune.Owner != null}]", pos,
                        new Vector2(15), Color.White, FontFlags.None);
            }
        }

        private void DrawingOnOnDraw(EventArgs eventArgs)
        {
            foreach (var camp in _main.MeepoCrappahilation.CampManager.Camps)
            {
                Vector2 pos;
                var onScreen = Drawing.WorldToScreen(camp.Position, out pos);
                if (onScreen)
                    Drawing.DrawText($"[Checked: {camp.IsEmpty}] [Under Farm: {camp.Owner != null}]", pos,
                        new Vector2(15), Color.White, FontFlags.None);
            }
        }
    }
}