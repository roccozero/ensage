﻿using System.Reflection;
using Ensage.Common.Menu;
using Ensage.SDK.Menu;
using log4net;
using PlaySharp.Toolkit.Logging;

namespace MeepoCrappahilation.Features
{
    internal class AutoEscape : IFeature
    {
        private static readonly ILog Log = AssemblyLogs.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private Config _main;

        public MenuFactory Factory { get; set; }

        public void Activate(Config main)
        {
            _main = main;
            Factory = main.AutoEscape;
            Factory.Item("Use Ethereal for saving", true);
            Factory.Item("Move/stop commands will block healing order for 5 seconds", true);
            Factory.Item("Hero Range", "HeroRange", new Slider(1600, 1, 2500));
            Factory.Item("Creep Range", "CreepRange", new Slider(800, 1, 2500));
        }

        public void Dispose()
        {
            _main?.Dispose();
        }
    }
}