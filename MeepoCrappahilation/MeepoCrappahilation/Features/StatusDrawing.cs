﻿using System;
using System.Reflection;
using Ensage;
using Ensage.Common;
using Ensage.Common.Menu;
using Ensage.SDK.Menu;
using log4net;
using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;
using PlaySharp.Toolkit.Logging;
using SharpDX;

namespace MeepoCrappahilation.Features
{
    internal class StatusDrawing : IFeature
    {
        private static readonly ILog Log = AssemblyLogs.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private Config _main;

        public MenuItem<bool> DrawNetCooldown { get; set; }

        public MenuItem<bool> DrawDebugInfo { get; set; }

        public MenuItem<Slider> NumberPositionY { get; set; }

        public MenuItem<Slider> NumberTextSize { get; set; }

        public MenuItem<bool> EnableNumber { get; set; }

        public MenuItem<Slider> IconSize { get; set; }

        public MenuItem<Slider> TextSize { get; set; }

        public MenuItem<Slider> PosY { get; set; }

        public MenuItem<Slider> PosX { get; set; }

        public MenuItem<bool> EnableMoving { get; set; }

        public MenuItem<bool> EnableStatusHelper { get; set; }

        public MenuItem<bool> DrawPoofCooldown { get; set; }

        public MenuFactory Factory { get; set; }

        public void Activate(Config main)
        {
            _main = main;
            Factory = main.StatusHelper;
            EnableStatusHelper = Factory.Item("Enable drawing", true);
            DrawPoofCooldown = Factory.Item("Draw Poof cooldown", true);
            DrawNetCooldown = Factory.Item("Draw Net cooldown", true);
            DrawDebugInfo = Factory.Item("Draw Debug info", false);
            EnableMoving = Factory.Item("Enable Moving", false);
            /*PosX = Factory.Item($"Position X", new Slider(120, 0, 500));
            PosY = Factory.Item($"Position Y", new Slider(130, 0, 500));*/
            TextSize = Factory.Item("Text Size", new Slider(130, 0, 500));
            IconSize = Factory.Item("Icon Size", new Slider(76, 50, 100));
            var numbers = Factory.Menu("Draw number of meepo");
            EnableNumber = numbers.Item("Enable", true);
            NumberTextSize = numbers.Item("Text Size", new Slider(50, 1, 100));
            NumberPositionY = numbers.Item("Text Position Y", new Slider(75, 0, 150));

            if (EnableNumber)
                Drawing.OnDraw += DrawingOnOnDraw;

            EnableNumber.PropertyChanged += (sender, args) =>
            {
                if (EnableNumber)
                    Drawing.OnDraw += DrawingOnOnDraw;
                else
                    Drawing.OnDraw -= DrawingOnOnDraw;
            };
        }

        public void Dispose()
        {
            Factory?.Dispose();
        }

        private void DrawingOnOnDraw(EventArgs args)
        {
            foreach (var meepo in _main.MeepoCrappahilation.Updater.AllMeepos)
            {
                if (meepo is IllusionMeepo)
                    continue;
                var hero = meepo.Hero;
                var screenPosition = HUDInfo.GetHPbarPosition(hero);
                if (!screenPosition.IsZero)
                {
                    screenPosition += new Vector2(HUDInfo.GetHPBarSizeX() / 2 - NumberTextSize * 0.2f, 0);
                    Drawing.DrawText($"{meepo.Index + 1}", screenPosition - new Vector2(0, NumberPositionY),
                        new Vector2(NumberTextSize),
                        Color.White, FontFlags.None);
                }
            }
        }
    }
}