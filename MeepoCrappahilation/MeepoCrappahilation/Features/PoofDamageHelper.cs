﻿using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Ensage;
using Ensage.Common;
using Ensage.Common.Menu;
using Ensage.Common.Objects;
using Ensage.SDK.Extensions;
using Ensage.SDK.Helpers;
using Ensage.SDK.Menu;
using log4net;
using PlaySharp.Toolkit.Logging;
using SharpDX;

namespace MeepoCrappahilation.Features
{
    public class PoofDamageHelper : IFeature
    {
        private static readonly ILog Log = AssemblyLogs.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private float _getExtraDamage;
        private Config _main;
        private Ability _poof;
        private Hero Me => _main.MeepoCrappahilation.Owner;

        public MenuItem<Slider> HeroIconSize { get; set; }

        public MenuItem<Slider> IconSize { get; set; }

        public float GetPoofDamage => _poof.GetAbilitySpecialData("poof_damage") + _getExtraDamage;

        public MenuItem<bool> EnableOnHeroes { get; set; }

        public MenuItem<bool> EnableOnCreeps { get; set; }

        public MenuFactory Factory { get; set; }

        public void Dispose()
        {
            ActivateCreepsDrawing(false);
            ActivateHeroesDrawing(false);
            Factory?.Dispose();
        }

        public void Activate(Config main)
        {
            _main = main;
            Factory = main.PoofDamageHelper;

            EnableOnCreeps = Factory.Item("Enable on creeps", true);
            EnableOnCreeps.Item.SetTooltip("Draw damage from poofs. Will not work after 15 min");
            IconSize = Factory.Item("Icon size", new Slider(30, 5, 50));

            EnableOnHeroes = Factory.Item("Enable on heroes", true);
            EnableOnHeroes.Item.SetTooltip("Draw count of poof for kill");
            HeroIconSize = Factory.Item("Hero Icon size", new Slider(30, 5, 50));

            if (EnableOnCreeps) ActivateCreepsDrawing();
            EnableOnCreeps.PropertyChanged += (sender, args) => { ActivateCreepsDrawing(EnableOnCreeps); };

            if (EnableOnHeroes) ActivateHeroesDrawing();
            EnableOnHeroes.PropertyChanged += (sender, args) => { ActivateHeroesDrawing(EnableOnHeroes); };
            UpdateManager.BeginInvoke(async () =>
            {
                while (true)
                {
                    var isEarly = Math.Floor(Game.RawGameTime / 60) < 17;
                    if (isEarly)
                    {
                    }
                    else
                    {
                        ActivateCreepsDrawing(false);
                        Log.Warn(
                            $"Poof damage on creeps was disabled cuz GameTime: {Math.Floor(Game.RawGameTime / 60)} >= 15 mins");
                        break;
                    }

                    await Task.Delay(5000);
                }
            }, 500);


            _poof = Me.Spellbook.SpellW;
            UpdateManager.BeginInvoke(async () =>
            {
                while (true)
                {
                    var extraDamage = Me.GetAbilityById(AbilityId.special_bonus_unique_meepo_2);
                    var lifeSteal = Me.GetAbilityById(AbilityId.special_bonus_lifesteal_10);
                    if (extraDamage.Level > 0)
                    {
                        _getExtraDamage = 30;
                        break;
                    }

                    if (lifeSteal.Level > 0)
                    {
                        _getExtraDamage = 0f;
                        break;
                    }

                    await Task.Delay(1000);
                }
            }, 500);
        }

        private void ActivateCreepsDrawing(bool enable = true)
        {
            var min = Math.Floor(Game.RawGameTime / 60);
            var isEarly = Math.Floor(min) < 17;
            Log.Debug(
                $"DrawingOnCreepsToggle {(enable ? "On" : "Off")} Current min: {Math.Floor(min)}");
            if (enable && isEarly)
                Drawing.OnDraw += DrawingCreeps;
            else if (EnableOnCreeps || !isEarly) Drawing.OnDraw -= DrawingCreeps;
        }

        private void ActivateHeroesDrawing(bool enable = true)
        {
            if (enable)
                Drawing.OnDraw += DrawingHeroes;
            else if (EnableOnHeroes) Drawing.OnDraw -= DrawingHeroes;
        }

        private void DrawingHeroes(EventArgs args)
        {
            var myTeam = Me.Team;
            var heroes =
                EntityManager<Hero>.Entities.Where(x => x.IsValid && x.IsAlive && x.Team != myTeam && x.IsVisible);
            foreach (var hero in heroes)
            {
                /*var isOnCreen = Drawing.WorldToScreen(hero.Position, out var pos);
                if (isOnCreen)
                {
                    
                }*/
                var pos = HUDInfo.GetHPbarPosition(hero);
                if (pos.IsZero) continue;
                var size = new Vector2(HeroIconSize);
                pos -= new Vector2(size.X + 5, 0);
                Drawing.DrawRect(pos, size, Textures.GetSpellTexture(AbilityId.meepo_poof.ToString()));
                var dmg = Me.CalculateSpellDamage(hero, DamageType.Magical, GetPoofDamage);
                var healthLeft = Math.Floor(hero.Health / dmg);
                var text = $"{healthLeft}";
                var textSize = Drawing.MeasureText(text, "Arial",
                                   new Vector2(size.Y * 1, size.Y / 2), FontFlags.AntiAlias) * 0.75f;
                var textPos = pos + new Vector2(size.X / 2 - textSize.X / 2, size.Y / 2 - textSize.Y / 2);
                Drawing.DrawText(
                    text,
                    textPos,
                    new Vector2(textSize.Y, 0),
                    Color.White,
                    FontFlags.AntiAlias | FontFlags.StrikeOut);
                Drawing.DrawRect(pos, size, Color.WhiteSmoke, true);
            }
        }

        private void DrawingCreeps(EventArgs eventArgs)
        {
            var myTeam = Me.Team;
            var myMeepos = _main.MeepoCrappahilation.Updater.AllMeepos;
            var creeps =
                EntityManager<Creep>.Entities.Where(x =>
                    x.IsValid && x.IsAlive && x.IsSpawned && x.Team != myTeam && x.IsVisible);
            foreach (var creep in creeps)
            {
                //var isOnCreen = Drawing.WorldToScreen(creep.Position, out var pos);
                //if (!isOnCreen) continue;
                var pos = HUDInfo.GetHPbarPosition(creep);
                if (pos.IsZero) continue;
                if (!myMeepos.Any(x => x.Hero.IsInRange(creep, 1000))) continue;
                var size = new Vector2(IconSize);
                pos -= new Vector2(size.X + 5, 0);
                Drawing.DrawRect(pos, size, Textures.GetSpellTexture(AbilityId.meepo_poof.ToString()));
                var dmg = GetPoofDamage * 2;
                var healthLeft = Math.Abs(creep.Health - dmg);
                if (creep.Health <= dmg)
                {
                    Drawing.DrawRect(pos, size, Color.YellowGreen, true);
                }
                else
                {
                    Drawing.DrawRect(pos, size, Color.OrangeRed, true);
                    var text = $"{healthLeft}";
                    var textSize = Drawing.MeasureText(text, "Arial",
                                       new Vector2(size.Y * 1, size.Y / 2), FontFlags.AntiAlias) * 0.75f;
                    var textPos = pos + new Vector2(size.X / 2 - textSize.X / 2, size.Y / 2 - textSize.Y / 2);
                    Drawing.DrawText(
                        text,
                        textPos,
                        new Vector2(textSize.Y, 0),
                        Color.White,
                        FontFlags.AntiAlias | FontFlags.StrikeOut);
                }
            }
        }
    }
}