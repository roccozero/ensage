﻿using System;
using System.ComponentModel.Composition;

namespace MeepoCrappahilation.Features
{
    [InheritedExport]
    internal interface IFeature : IDisposable
    {
        void Activate(Config main);
    }
}