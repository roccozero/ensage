﻿using System.Linq;
using Ensage;
using Ensage.Common.Extensions;
using Ensage.Common.Menu;
using Ensage.Common.Objects.UtilityObjects;
using Ensage.SDK.Extensions;
using Ensage.SDK.Menu;
using Ensage.SDK.Renderer.Particle;
using Ensage.SDK.TargetSelector;
using MeepoCrappahilation.Meepos_Shiit;
using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;
using MeepoCrappahilation.Meepos_Shiit.OrderBase;
using SharpDX;

namespace MeepoCrappahilation.Features
{
    public class SuperProFeature : IFeature
    {
        private static readonly Sleeper GlobalPoofCooldown = new Sleeper();
        private Config _config;

        public MenuItem<Slider> BackToFightHealthCheck { get; set; }

        public MenuItem<bool> DisableHealingOrder { get; set; }

        public MenuItem<Slider> HealthCheck { get; set; }

        public MenuItem<bool> UsePoofOnlyToBase { get; set; }


        private Hero Target
        {
            get => _config.MeepoCrappahilation.Target;
            set => _config.MeepoCrappahilation.Target = value;
        }

        private ITargetSelector TargetSelector => _config.MeepoCrappahilation.Context.TargetSelector;
        private IParticleManager Particle => _config.MeepoCrappahilation.Context.Particle;
        private OrderManager OrderManager => _config.MeepoCrappahilation.OrderManager;

        private Updater Updater => _config.MeepoCrappahilation.Updater;

        public void Dispose()
        {
        }

        public void Activate(Config main)
        {
            _config = main;
            main.SeuperProFeature = main.Factory.Menu("Super Pro settings");
            UsePoofOnlyToBase = main.SeuperProFeature.Item("Use poof only to base in combo", true);
            UsePoofOnlyToBase.Item.SetTooltip(
                "If u disable it, it will inc ur chances to die");
            HealthCheck = main.SeuperProFeature.Item("Use poof if health(%) < selected value ", new Slider(70, 1));
            BackToFightHealthCheck =
                main.SeuperProFeature.Item("Back to fight health(%) < selected value ", new Slider(70, 1));
            DisableHealingOrder = main.SeuperProFeature.Item("Disable auto healing order for this combo", false);
            DisableHealingOrder.Item.SetTooltip(
                "can cause bugs if you will not disable healing order (can work not correctly under tower)");
        }

        public bool Health(MeepoBase meepo)
        {
            return meepo.Hero.HealthPercent() <= HealthCheck / 100f;
        }

        public bool HealthBackToFight(MeepoBase meepo)
        {
            return meepo.Hero.HealthPercent() >= BackToFightHealthCheck / 100f;
        }

        public void ExecuteCombo()
        {
            if (Target == null || !Target.IsValid || !Target.IsAlive)
            {
                Target = (Hero) TargetSelector.GetTargets()
                    .FirstOrDefault(); //EntityManager<Hero>.Entities.FirstOrDefault(x => x.Team == Owner.GetEnemyTeam());
                if (Target != null)
                {
                    _config.MeepoCrappahilation.TargetManager.Activate(Target);
                    Particle.DrawRange(Target, "circle_target", 125, Color.Purple);
                    if (TargetSelector.IsActive) TargetSelector?.Deactivate();
                }
                else
                {
                    foreach (var meepo in OrderManager.DoComboOnlyForSelectedMeepos
                        ? Updater.GetFreeMeepos(true)
                        : Updater.GetFreeMeepos())
                        meepo.MovementManager.Orbwalk(null);
                    Particle.Remove("circle_target");
                    Particle.Remove("line_target");
                }

                return;
            }

            var bladeMail = Target.HasAnyModifiers("modifier_item_blade_mail_reflect");
            var idleMeepos =
                Updater.AllMeepos.Where(x =>
                    !(x is IllusionMeepo) && x.OrderSystem.CurrentOrder.Id == OrderSystem.OrderTypes.IdleOnBase &&
                    !x.AbilityManager.W.IsInAbilityPhase).ToList();
            var countIdleMeepos = idleMeepos.Count;
            if (countIdleMeepos > 1)
                foreach (var meepo in idleMeepos)
                {
                    var inFountain = meepo.Hero.IsInRange(meepo.OrderSystem.CurrentOrder.Fountain, 1000);
                    if (!inFountain)
                    {
                        var notOnlyOneMeepo = meepo.Main.Updater.GetFreeMeepos().Any(x =>
                            !(x is IllusionMeepo) && x.Hero.IsInRange(Target, 250) &&
                            !x.AbilityManager.W.IsInAbilityPhase &&
                            x.Index != meepo.Index);
                        if (notOnlyOneMeepo)
                        {
                            var meepoOnBase = meepo.Main.Updater.AllMeepos.FirstOrDefault(x => !(x is IllusionMeepo) &&
                                                                                               x.Hero.IsInRange(
                                                                                                   meepo.OrderSystem
                                                                                                       .CurrentOrder
                                                                                                       .Fountain,
                                                                                                   1000) &&
                                                                                               !x.AbilityManager.W
                                                                                                   .IsInAbilityPhase);
                            if (meepoOnBase != null)
                            {
                                if (meepo.AbilityManager.PoofToAllyMeepo(meepoOnBase.Hero))
                                {
                                    meepo.OrderSystem.SetOrder(new IdleOnBaseOrder(meepo));
                                    GlobalPoofCooldown.Sleep(150f);
                                    break;
                                }

                                meepo.Hero.Move(meepo.OrderSystem.CurrentOrder.Fountain.Position);
                            }
                        }
                    }

                    if (inFountain && meepo.AbilityManager != null && meepo.AbilityManager.W.CanBeCasted() &&
                        HealthBackToFight(meepo))
                    {
                        var notOnlyOneMeepo = meepo.Main.Updater.GetFreeMeepos().Where(x =>
                            x.Hero.IsInRange(Target, 1000) && !x.AbilityManager.W.IsInAbilityPhase &&
                            !(x is IllusionMeepo) &&
                            x.Index != meepo.Index).OrderBy(z => z.Hero.Distance2D(Target)).FirstOrDefault();
                        if (notOnlyOneMeepo != null)
                        {
                            if (meepo.AbilityManager.PoofToAllyMeepo(notOnlyOneMeepo.Hero))
                            {
                                meepo.OrderSystem.SetOrder(new ComboOrder(meepo));
                                MeepoCrappahilation.Log.Debug(
                                    $"meepo #{meepo.Index} uses poof to meepo #{notOnlyOneMeepo.Index}");
                            }
                        }
                        else
                        {
                            MeepoCrappahilation.Log.Debug(
                                $"meepo #{meepo.Index} cant find any meepo around target");
                        }
                    }
                }

            foreach (var meepo in OrderManager.DoComboOnlyForSelectedMeepos
                ? Updater.GetFreeMeepos(true)
                : Updater.GetFreeMeepos())
            {
                if (meepo is IllusionMeepo)
                {
                    meepo.MovementManager.Attack(Target);
                    continue;
                }

                var notInInvis = !meepo.Hero.HasAnyModifiers("modifier_item_invisibility_edge_windwalk",
                    "modifier_item_silver_edge_windwalk");
                if (Health(meepo) && meepo.AbilityManager.W.CanBeCasted() && !GlobalPoofCooldown.Sleeping)
                {
                    var notOnlyOneMeepo = meepo.Main.Updater.AllMeepos.Any(x => !(x is IllusionMeepo) &&
                                                                                x.Hero.IsInRange(Target, 250) &&
                                                                                !x.AbilityManager.W.IsInAbilityPhase &&
                                                                                x.Index != meepo.Index);
                    if (notOnlyOneMeepo)
                    {
                        var meepoOnBase = meepo.Main.Updater.AllMeepos.FirstOrDefault(x => !(x is IllusionMeepo) &&
                                                                                           x.Hero.IsInRange(
                                                                                               meepo.OrderSystem
                                                                                                   .CurrentOrder
                                                                                                   .Fountain, 1000) &&
                                                                                           !x.AbilityManager.W
                                                                                               .IsInAbilityPhase);
                        if (meepoOnBase != null)
                            if (meepo.AbilityManager.PoofToAllyMeepo(meepoOnBase.Hero))
                            {
                                meepo.OrderSystem.SetOrder(new IdleOnBaseOrder(meepo));
                                meepo.Hero.Move(meepo.OrderSystem.CurrentOrder.Fountain.Position, true);
                                GlobalPoofCooldown.Sleep(150f);
                                continue;
                            }
                    }
                }

                if (!bladeMail || _config.GetBmBehaviour == Config.BmBehaviour.All)
                {
                    if (notInInvis || !meepo.Hero.IsInRange(Target, 1500))
                        meepo.AbilityManager?.UseAbilities(Target, usePoof: !UsePoofOnlyToBase);

                    meepo.MovementManager.Orbwalk(Target);
                }
                else
                {
                    if (notInInvis) meepo.AbilityManager?.UseAbilities(Target, true, false);
                    meepo.MovementManager.Orbwalk(_config.GetBmBehaviour == Config.BmBehaviour.Attack
                        ? Target
                        : null);
                }

                if (!meepo.Hero.IsInRange(Target, 800))
                {
                    var notOnlyOneMeepo = meepo.Main.Updater.AllMeepos.FirstOrDefault(x =>
                        x.Hero.IsInRange(Target, 1000) &&
                        (x.AbilityManager is null || !x.AbilityManager.W.IsInAbilityPhase) &&
                        x.Index != meepo.Index && (!(x is IllusionMeepo) || ((IllusionMeepo) x).IsPoofable()));
                    if (notOnlyOneMeepo != null)
                        if (meepo.AbilityManager != null && meepo.AbilityManager.PoofToAllyMeepo(notOnlyOneMeepo.Hero))
                        {
                            meepo.OrderSystem.SetOrder(new ComboOrder(meepo));
                            MeepoCrappahilation.Log.Debug(
                                $"(2) meepo #{meepo.Index} uses poof to meepo #{notOnlyOneMeepo.Index} (order: {meepo.OrderSystem.CurrentOrder.Id} {notOnlyOneMeepo.OrderSystem.CurrentOrder.Id})");
                            return;
                        }
                }
            }
        }
    }
}