﻿using System.Linq;
using System.Reflection;
using Ensage;
using Ensage.SDK.Extensions;
using Ensage.SDK.Helpers;
using Ensage.SDK.Menu;
using log4net;
using MeepoCrappahilation.Meepos_Shiit;
using MeepoCrappahilation.Meepos_Shiit.OrderBase;
using PlaySharp.Toolkit.Logging;
using SharpDX;

namespace MeepoCrappahilation.Features
{
    internal class AutoDodge : IFeature
    {
        private static readonly ILog Log = AssemblyLogs.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private Config _main;

        public MenuItem<bool> DodgeEnable { get; set; }

        public MenuFactory Factory { get; set; }

        public void Activate(Config main)
        {
            _main = main;
            Factory = main.AutoDodge;
            DodgeEnable = Factory.Item("Dodge dangerous abilities", true);

            if (DodgeEnable)
                Entity.OnParticleEffectAdded += EntityOnOnParticleEffectAdded;

            DodgeEnable.PropertyChanged += (sender, args) =>
            {
                if (DodgeEnable)
                    Entity.OnParticleEffectAdded += EntityOnOnParticleEffectAdded;
                else
                    Entity.OnParticleEffectAdded -= EntityOnOnParticleEffectAdded;
            };
        }

        public void Dispose()
        {
            _main?.Dispose();
        }

        private void EntityOnOnParticleEffectAdded(Entity sender, ParticleEffectAddedEventArgs args)
        {
            var name = args.Name;
            if (name == "particles/units/heroes/hero_lich/lich_chain_frost.vpcf" ||
                name == "particles/econ/items/lich/lich_ti8_immortal_arms/lich_ti8_chain_frost.vpcf")
                UpdateManager.BeginInvoke(() =>
                {
                    var position = args.ParticleEffect.GetControlPoint(0);

                    TryToDodge(position);
                }, 50);
        }

        public void TryToDodge(Vector3 position)
        {
            var getMeepos =
                _main.MeepoCrappahilation.Updater.AllMeepos.Where(x =>
                    x.Hero.IsInRange(position, 850)).ToList();

            var owner = _main.MeepoCrappahilation.Owner;
            var middlePosition = getMeepos.Aggregate(position, (current, hero) => current + hero.Hero.Position);
            var unitCount = getMeepos.Count + 1;

            middlePosition /= unitCount;
            var illuAngle = 360.0f / unitCount;
            var currentHeroDir = position; // - ownerPos;
            if (unitCount > 1)
            {
                var unitsAround = EntityManager<Unit>.Entities.Where(x =>
                        x.IsAlive && x.IsSpawned && x.IsVisible && x.IsAlly(owner) &&
                        x.IsInRange(position, 850))
                    .ToList();
                if (unitsAround.Count >= 2)
                {
                    Log.Debug(
                        $"Chain detected. Meepos: {getMeepos.Count}. Position: {position}. Units: {unitsAround.Count}");
                    foreach (var meepo in getMeepos)
                    {
                        if (meepo.OrderSystem.CurrentOrder.Id == OrderSystem.OrderTypes.Dodge)
                            (meepo.OrderSystem.CurrentOrder as DodgeOrder)?.UpdateTime();
                        else
                            meepo.OrderSystem.SetOrder(new DodgeOrder(meepo));


                        currentHeroDir = currentHeroDir.Rotated(MathUtil.DegreesToRadians(illuAngle));

                        var dir = currentHeroDir.Normalized();
                        dir *= 850;
                        var movePos = middlePosition + dir;

                        meepo.Hero.Move(movePos);
                        Log.Debug($"[{meepo.Index}] Move");
                    }
                }
            }
        }
    }
}