﻿using System.Reflection;
using Ensage.SDK.Menu;
using log4net;
using PlaySharp.Toolkit.Logging;

namespace MeepoCrappahilation.Features
{
    internal class LanePushingSettings : IFeature
    {
        private static readonly ILog Log = AssemblyLogs.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private Config _main;
        public MenuFactory Factory { get; set; }

        public void Activate(Config main)
        {
            _main = main;
            Factory = main.LanePushing;
        }

        public void Dispose()
        {
            _main?.Dispose();
            Factory?.Dispose();
        }
    }
}