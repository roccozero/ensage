﻿using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Ensage;
using Ensage.Common.Menu;
using Ensage.SDK.Helpers;
using Ensage.SDK.Menu;
using log4net;
using MeepoCrappahilation.Meepos_Shiit;
using MeepoCrappahilation.Meepos_Shiit.MeepoTypes;
using PlaySharp.Toolkit.Logging;
using SharpDX;

namespace MeepoCrappahilation.Features
{
    internal class PoofHelper : IFeature
    {
        public static Color RangeColor = new Color(0, 50, 255, 70);
        private static readonly ILog Log = AssemblyLogs.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private Config _main;

        private bool HealingStopper =>
            _main.AutoEscape.GetValue<bool>("Move/stop commands will block healing order for 5 seconds");

        public static MenuItem<bool> DrawPoofRange { get; set; }

        public MenuItem<Slider> DelayPoof { get; set; }

        public MenuItem<KeyBind> PoofToSelectedKey { get; set; }

        public MenuItem<bool> Enable { get; set; }

        public MenuFactory Factory { get; set; }

        public void Activate(Config main)
        {
            _main = main;
            Factory = main.PoofHelper;
            Enable = Factory.Item("Enable", true);
            Enable.Item.SetTooltip("When u using poof on 1 meepo, other selected meepos will also use poof");
            DrawPoofRange = Factory.Item("Draw poof range", true);
            DrawPoofRange.PropertyChanged += (sender, args) =>
            {
                foreach (var meepo in main.MeepoCrappahilation.Updater.AllMeepos)
                    if (DrawPoofRange)
                        main.MeepoCrappahilation.Context.Particle.DrawRange(meepo.Hero, $"RangeFor{meepo.Index}", 375f,
                            RangeColor);
                    else
                        main.MeepoCrappahilation.Context.Particle.Remove($"RangeFor{meepo.Index}");
            };
            PoofToSelectedKey = Factory.Item("Poof To Selected Hero", new KeyBind('0'));
            DelayPoof = Factory.Item("Delay between poofs", new Slider(0, 0, 150));
            PoofToSelectedKey.Item.SetTooltip("Poof all meepos (except meepo with healing status) to selected meepo");
            PoofToSelectedKey.PropertyChanged += (sender, args) =>
            {
                if (!PoofToSelectedKey)
                    return;
                var target = main.MeepoCrappahilation.Updater.SelectedMeepos.FirstOrDefault();
                if (target != null)
                {
                    if (DelayPoof == 0)
                        foreach (var selected in main.MeepoCrappahilation.Updater.GetFreeMeepos()
                            .Where(x => !(x is IllusionMeepo)))
                        {
                            if (selected.Equals(target))
                                continue;
                            selected.AbilityManager.PoofToAllyMeepo(target.Hero);
                        }
                    else
                        UpdateManager.BeginInvoke(async () =>
                        {
                            foreach (var selected in main.MeepoCrappahilation.Updater.GetFreeMeepos()
                                .Where(x => !(x is IllusionMeepo)))
                            {
                                if (selected.Equals(target))
                                    continue;
                                selected.AbilityManager.PoofToAllyMeepo(target.Hero);
                                await Task.Delay(DelayPoof);
                            }
                        });
                }
            };

            if (Enable)
                Player.OnExecuteOrder += PlayerOnOnExecuteOrder;
            Enable.PropertyChanged += (sender, args) =>
            {
                if (Enable)
                    Player.OnExecuteOrder += PlayerOnOnExecuteOrder;
                else
                    Player.OnExecuteOrder -= PlayerOnOnExecuteOrder;
            };

            Player.OnExecuteOrder += (sender, args) =>
            {
                if (!args.IsPlayerInput)
                    return;
                var me = _main.MeepoCrappahilation.Updater.SelectedMeepos.FirstOrDefault();
                if (me == null)
                    return;
                var order = args.OrderId;
                if (order == OrderId.Stop || order == OrderId.MoveLocation)
                    foreach (var meepo in _main.MeepoCrappahilation.Updater.SelectedMeepos)
                        if (meepo.OrderSystem.CurrentOrder.CanFlushWithMouse)
                        {
                            meepo.OrderSystem.FlushOrder();
                        }
                        else if (meepo.OrderSystem.CurrentOrder.Id == OrderSystem.OrderTypes.Healing)
                        {
                            if (HealingStopper)
                            {
                                meepo.OrderSystem.FlushOrder();
                                meepo.BlockForHealing = Game.RawGameTime;
                            }
                        }
                        else
                        {
                            if (HealingStopper && meepo.GetHealthPercent <= BehaviourManager.PercentForEscape / 100f)
                                meepo.BlockForHealing = Game.RawGameTime;
                            if (main.BlockPlayerInputWhileCombo) args.Process = false;
                        }
            };
        }

        public void Dispose()
        {
            if (Enable)
                Player.OnExecuteOrder -= PlayerOnOnExecuteOrder;
            Factory?.Dispose();
        }

        private void PlayerOnOnExecuteOrder(Player sender, ExecuteOrderEventArgs args)
        {
            if (!args.IsPlayerInput)
                return;
            var me = _main.MeepoCrappahilation.Updater.SelectedMeepos.FirstOrDefault();
            if (me == null)
                return;

            var order = args.OrderId;
            if (order == OrderId.AbilityTarget || order == OrderId.AbilityLocation)
                if (args.Ability.Id == AbilityId.meepo_poof)
                {
                    args.Process = false;

                    if (DelayPoof == 0)
                    {
                        var targetPos = args.TargetPosition;
                        var isZero = targetPos.IsZero;
                        foreach (var meepo in _main.MeepoCrappahilation.Updater.SelectedMeepos)
                        {
                            meepo.AbilityManager.PoofTo(isZero ? args.Target.Position : targetPos);
                            MeepoCrappahilation.Log.Debug(
                                $"PoofHelper: id# {meepo.Index} using poof to [{targetPos}] [{args.Target?.Position}]");
                        }
                    }
                    else
                    {
                        UpdateManager.BeginInvoke(async () =>
                        {
                            foreach (var meepo in _main.MeepoCrappahilation.Updater.SelectedMeepos)
                            {
                                var targetPos = args.TargetPosition;
                                var isZero = targetPos.IsZero;
                                if (args.Target == null) continue;
                                meepo.AbilityManager.PoofTo(isZero ? args.Target.Position : targetPos);
                                MeepoCrappahilation.Log.Debug(
                                    $"PoofHelper: id# {meepo.Index} using poof to [{targetPos}] [{args.Target?.Position}]");
                                await Task.Delay(DelayPoof);
                            }
                        });
                    }
                }

            //else 
        }
    }
}