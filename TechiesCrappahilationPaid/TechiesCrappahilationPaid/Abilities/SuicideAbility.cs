﻿using System.Linq;
using System.Threading.Tasks;
using Ensage;
using Ensage.SDK.Abilities;
using Ensage.SDK.Extensions;
using Ensage.SDK.Helpers;

namespace TechiesCrappahilationPaid.Abilities
{
    public class SuicideAbility : CircleAbility
    {
        public SuicideAbility(Ability ability) : base(ability)
        {
            UpdateManager.BeginInvoke(async () =>
            {
                _hasExtraDamage = false;
                var extraDamage = Owner.GetAbilityById(AbilityId.special_bonus_unique_techies);
                var extraAbility = Owner.GetAbilityById(AbilityId.special_bonus_mp_regen_6);
                while (extraDamage.Level == 0 && extraAbility.Level == 0)
                {
                    await Task.Delay(500);
                }

                _hasExtraDamage = extraDamage.Level > 0;
            });
        }

        private bool _hasExtraDamage;
        public override DamageType DamageType { get; } = DamageType.Magical;

        public override UnitState AppliesUnitState { get; } = UnitState.Silenced;

        public override float GetDamage(params Unit[] targets)
        {
            if (!targets.Any()) return RawDamage;
            return base.GetDamage(targets);
        }

        public float GetDamage(Unit target)
        {
            return DamageHelpers.GetSpellDamage(Ability.GetAbilitySpecialData("damage") + (_hasExtraDamage ? 300 : 0),
                Owner.GetSpellAmplification(),
                Ability.GetDamageReduction(target, DamageType));
        }
    }
}