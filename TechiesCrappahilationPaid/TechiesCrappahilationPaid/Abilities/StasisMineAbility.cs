﻿using Ensage;
using Ensage.SDK.Abilities;

namespace TechiesCrappahilationPaid.Abilities
{
    public class StasisMineAbility : CircleAbility
    {
        public StasisMineAbility(Ability ability) : base(ability)
        {
        }
    }
}