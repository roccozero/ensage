using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Ensage;
using Ensage.Common;
using Ensage.Common.Menu;
using Ensage.SDK.Helpers;
using Ensage.SDK.Menu;
using Ensage.SDK.Renderer;
using SharpDX;
using Color = System.Drawing.Color;

namespace TechiesCrappahilationPaid.Features
{
    public class SuicideDamage
    {
        private readonly TechiesCrappahilationPaid _main;

        private Hero Me => _main.Me;
        public readonly Dictionary<Hero, Vector2> PlayerPositions;
        private IRenderManager Renderer => _main.Context.RenderManager;

        public SuicideDamage(TechiesCrappahilationPaid main)
        {
            _main = main;
            var suicide = main.MenuManager.VisualSubMenu.Menu("Suicide");
            ShowSuicideDamage = suicide.Item("Show suicide damage on heroes", true);
            SuicideType = suicide.Item("Suicide Draw type", new StringList("text", "icon"));

            PlayerPositions = new Dictionary<Hero, Vector2>();

            Drawing.OnDraw += args =>
            {
                foreach (var enemy in TargetManager.Targets)
                {
                    if (enemy != null && enemy.IsValid)
                        if (PlayerPositions.ContainsKey(enemy))
                        {
                            PlayerPositions[enemy] = HUDInfo.GetHPbarPosition(enemy);
                        }
                        else
                        {
                            PlayerPositions.Add(enemy, HUDInfo.GetHPbarPosition(enemy));
                        }
                }
            };

            Renderer.Draw += (renderer) =>
            {
                if (!ShowSuicideDamage.Value) return;
                foreach (var g in PlayerPositions.ToList())
                {
                    var enemy = g.Key;
                    if (!_main.Updater.BombDamageManager.DamageDictionary.ContainsKey(enemy.HeroId) ||
                        !enemy.IsVisible || !enemy.IsAlive)
                        continue;
                    var pos = g.Value;
//                    var w2s = Drawing.WorldToScreen(pos);
                    if (pos.IsZero)
                        continue;
                    var drawPos = pos;
                    if (SuicideType.Value.SelectedIndex == 0)
                    {
                        var damage = Math
                            .Round(_main.Updater.BombDamageManager.DamageDictionary[enemy.HeroId].HealthAfterSuicide, 1)
                            .ToString(CultureInfo.InvariantCulture);
                        var size = renderer.MeasureText(damage);
                        drawPos.X -= size.X + 5;
                        renderer.DrawFilledRectangle(new RectangleF(drawPos.X - 1, drawPos.Y, size.X, size.Y),
                            Color.Black,
                            Color.FromArgb(155, Color.Black));
                        renderer.DrawText(drawPos, damage, Color.White);
                    }
                    else
                    {
                        var willDie = _main.Updater.BombDamageManager.DamageDictionary[enemy.HeroId].HeroWillDieSuicide;
                        var rect = new RectangleF(drawPos.X - 26, drawPos.Y, 24, 24);
                        renderer.DrawTexture($"{AbilityId.techies_suicide}_icon", rect);
                        renderer.DrawFilledRectangle(rect, willDie ? Color.LimeGreen : Color.Red,
                            Color.FromArgb(35, willDie ? Color.LimeGreen : Color.Red), 0.5f);
                    }
                }
            };
        }

        public MenuItem<StringList> SuicideType { get; set; }

        public MenuItem<bool> ShowSuicideDamage { get; set; }
    }

    public static class TargetManager
    {
        public static readonly List<Hero> Targets = new List<Hero>();

        public static void Init(TechiesCrappahilationPaid main)
        {
            var me = main.Me;
            UpdateManager.Subscribe(() =>
            {
                var enemies = EntityManager<Hero>.Entities.Where(x =>
                    x.IsValid && x.Team != me.Team && !x.IsIllusion && IsHeroOrDangerUnit(x));
                foreach (var enemy in enemies)
                {
                    if (Targets.Contains(enemy))
                        continue;
                    Targets.Add(enemy);
                    main.MenuManager.Targets.Value.Add(enemy.HeroId.ToString());
                }

                foreach (var enemy in Targets.ToList())
                {
                    if (enemy is null || !enemy.IsValid)
                        Targets.Remove(enemy);
                }
            }, 500);
        }

        private static bool IsHeroOrDangerUnit(Unit unit)
        {
            return unit is Hero || unit.NetworkName == "CDOTA_Unit_SpiritBear";
        }
    }
}