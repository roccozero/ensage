using System.Linq;
using Ensage.Common;
using Ensage.SDK.Renderer;
using SharpDX;

namespace TechiesCrappahilationPaid.Features
{
    public class StackInfo
    {
        private readonly TechiesCrappahilationPaid _main;

        public StackInfo(TechiesCrappahilationPaid main)
        {
            _main = main;

            if (main.MenuManager.DrawStacks)
                main.Context.RenderManager.Draw += RenderManagerOnDraw;

            main.MenuManager.DrawStacks.PropertyChanged += (sender, args) =>
            {
                if (main.MenuManager.DrawStacks)
                {
                    main.Context.RenderManager.Draw += RenderManagerOnDraw;
                }
                else
                {
                    main.Context.RenderManager.Draw -= RenderManagerOnDraw;
                }
            };
        }

        private void RenderManagerOnDraw(IRenderer renderer)
        {
            foreach (var bomb in _main.Updater.BombManager.RemoteMines.Where(x =>
                x.Owner.IsValid && x.Owner.IsAlive && x.Stacker.IsActive))
            {
                if (_main.MenuManager.StackDontDrawSolo && bomb.Stacker.Counter == 1)
                    continue;
                var topPos = HUDInfo.GetHPbarPosition(bomb.Owner);
                if (topPos.IsZero)
                    continue;
                var size = new Vector2((float) HUDInfo.GetHPBarSizeX(bomb.Owner),
                    (float) HUDInfo.GetHpBarSizeY(bomb.Owner));
                var text = bomb.Stacker.Counter.ToString();
                var textSize = renderer.MeasureText(text, 30);
                var textPos = topPos + new Vector2(size.X / 2 - textSize.X / 2, size.Y * 2);
                renderer.DrawText(textPos, text, System.Drawing.Color.White, 30);
            }
        }
    }
}