using Ensage.SDK.Renderer;

namespace TechiesCrappahilationPaid.Features.ViewDamageFromBombs
{
    public interface IViewBombCount
    {
        void Draw(IRenderer renderer);
    }
}