using System;
using System.Collections.Generic;
using System.Linq;
using Ensage;
using Ensage.Common.Extensions;
using Ensage.SDK.Helpers;
using Ensage.SDK.Renderer;
using Ensage.SDK.Service;

namespace TechiesCrappahilationPaid.Helpers
{
    public static class TextureHelper
    {
        private static readonly List<HeroId> LoadedHeroes = new List<HeroId>();
        private static readonly List<AbilityId> LoadedAbilities = new List<AbilityId>();

        private static ITextureManager Render { get; set; }

        public static void Init(IServiceContext context)
        {
            Render = context.TextureManager;

            ObjectManager.LocalHero.Spellbook.Spells.Select(x => x.Id).ForEach(LoadFromDota);

            EntityManager<Hero>.Entities.ForEach(hero =>
            {
                var heroId = hero.HeroId;
                if (LoadedHeroes.Contains(heroId) || hero.IsIllusion) return;
                LoadedHeroes.Add(heroId);
                LoadFromDota(heroId);
                
                
//                hero.Inventory.Items.Select(x => x.Id).ForEach(LoadFromDota);
//                hero.Spellbook.Spells.Select(x => x.Id).ForEach(LoadFromDota);
            });

            EntityManager<Hero>.EntityAdded += (sender, hero) =>
            {
                var heroId = hero.HeroId;
                if (LoadedHeroes.Contains(heroId) || hero.IsIllusion) return;
                LoadedHeroes.Add(heroId);
                LoadFromDota(heroId);
//                hero.Inventory.Items.Select(x => x.Id).ForEach(LoadFromDota);
//                hero.Spellbook.Spells.Select(x => x.Id).ForEach(LoadFromDota);
            };
        }

        public static void LoadFromDota(HeroId heroId)
        {
            var unitName = heroId.ToString();
            var file = $"panorama\\images\\heroes\\icons\\{(object) unitName}_png.vtex_c";
            TechiesCrappahilationPaid.Log.Warn($"Trying to load {heroId} from {file}");
            try
            {
                Render.LoadFromDota(unitName + "_icon", file);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void LoadFromDota(AbilityId ability)
        {
            if (LoadedAbilities.Contains(ability))
                return;
            
            var abilityId = ability.ToString();
            var isItem = ability <= (AbilityId) 5003;
            var file =
                $"panorama\\images\\{(isItem ? (object) "items" : (object) "spellicons")}\\" +
                $"{(isItem ? (object) abilityId.Substring("item_".Length) : (object) abilityId)}_png.vtex_c";

            TechiesCrappahilationPaid.Log.Warn($"Trying to load {ability} from {file}");
            try
            {
                LoadedAbilities.Add(ability);
                Render.LoadFromDota(ability + "_icon", file);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public static void LoadFromDota(params AbilityId[] abilities)
        {
            abilities.ForEach(LoadFromDota);
        }

        public static void LoadFromDota(params HeroId[] heroes)
        {
            heroes.ForEach(LoadFromDota);
        }
    }
}